//     ____       __ _____  ____
//    / __ \     / // ___/ / __ \
//   / /_/ /__  / / \__ \ / /_/ /
//  / ____// /_/ / ___/ // _, _/   PixInsight JavaScript Runtime
// /_/     \____/ /____//_/ |_|    PJSR Version 1.0
// ----------------------------------------------------------------------------
// pjsr/RBFType.jsh - Released 2025-02-19T18:36:01Z
// ----------------------------------------------------------------------------
// This file is part of the PixInsight JavaScript Runtime (PJSR).
// PJSR is an ECMA-262-5 compliant framework for development of scripts on the
// PixInsight platform.
//
// Copyright (c) 2003-2025 Pleiades Astrophoto S.L. All Rights Reserved.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __PJSR_RBFType_jsh
#define __PJSR_RBFType_jsh

/*!
 * Implemented radial basis functions (RBFs) for surface spline
 * interpolation/approximation.
 *
 * The symbols used in the following table are:
 *
 *    phi(r)   The radial basis function evaluated at a point (x,y)
 *    r        Distance to center, r^2 = (x-xc)^2 + (y-yc)^2
 *    (xc,yc)  Center point
 *    eps      Shape parameter
 *
 * N.B. The values of these macros must be coherent with the
 * pcl::RadialBasisFunction enumeration (see pcl/SurfaceSpline.h).
 */
#define RBFType_Unknown             -1
#define RBFType_VariableOrder        0    // phi(r) = (r^2)^(m-1) * Ln( r^2 )
#define RBFType_ThinPlateSpline      1    // phi(r) = r^2 * Ln( r )
#define RBFType_Gaussian             2    // phi(r) = Exp( -(eps*r)^2 )
#define RBFType_Multiquadric         3    // phi(r) = Sqrt( 1 + (eps*r)^2 )
#define RBFType_InverseMultiquadric  4    // phi(r) = 1/Sqrt( 1 + (eps*r)^2 )
#define RBFType_InverseQuadratic     5    // phi(r) = 1/( 1 + (eps*r)^2 )
#define RBFType_DDMVariableOrder     100  // Variable-order polyharmonic DDM-RBF
#define RBFType_DDMThinPlateSpline   101  // Thin plate spline DDM-RBF
#define RBFType_DDMMultiquadric      102  // Multiquadric DDM-RBF
#define RBFType_Default             RBFType_ThinPlateSpline

#endif   // __PJSR_RBFType_jsh

// ----------------------------------------------------------------------------
// EOF pjsr/RBFType.jsh - Released 2025-02-19T18:36:01Z
