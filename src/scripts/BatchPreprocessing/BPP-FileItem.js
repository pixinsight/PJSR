// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// BPP-FileItem.js - Released 2025-01-28T18:19:31Z
// ----------------------------------------------------------------------------
//
// This file is part of:
// - WeightedBatchPreprocessing script version 2.8.3
// - FastBatchPreprocessing script version 1.1.6
//
// Copyright (c) 2019-2025 Roberto Sartori
// Copyright (c) 2020-2021 Adam Block
// Copyright (c) 2019 Tommaso Rubechi
// Copyright (c) 2012 Kai Wiechen
// Copyright (c) 2012-2025 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/**
 * File Item constructor. A File Item references a file on disk along with all its processed versions.
 *
 * @param {String} filePath the file path
 * @param {ImageType} imageType type of image
 * @param {String} filter filter name
 * @param {Number} binning image's binning
 * @param {Number} exposureTime image's exposure time
 * @param {{String:String}?} fileKeywords name-value map containing the file header metadata
 * @param {Object} size {width:Numeric, height:Numeric} image's width and height
 * @param {Object} matchingSizes { mode: {width:Numeric, height:Numeric}} the size to use when matching the group of the given mode
 * @param {Number} height image's height
 * @param {Boolean} isCFA true if the file contains a CFA image
 * @param {Boolean} isMaster true if the image is a master file
 * @param {{String:String}?} keywords the keywords key-value map
 * @param {{String:Any}?} solverParams an object containing the solver parameters for the files, like the observation date, the pixel size
 *                                     and the focal length
 * @param {Overscan?} overscan an Overscan object containing the overscan regions
 */
function FileItem( filePath, imageType, filter, binning, exposureTime, fileKeywords, size, matchingSizes, isCFA, isMaster, keywords, solverParams, overscan )
{
   this.__base__ = Object;
   this.__base__();

   let
   {
      smartNaming
   } = WBPPUtils.shared();

   this.filePath = filePath;
   this.imageType = imageType;
   this.binning = binning;
   this.filter = filter;
   this.exposureTime = exposureTime;
   this.fileKeywords = fileKeywords;
   this.enabled = true;
   // real image size
   this.size = size;
   this.matchingSizes = {};
   this.matchingSizes[ WBPPGroupingMode.PRE ] = size;
   this.matchingSizes[ WBPPGroupingMode.POST ] = size;
   this.createdWithSmartNamingEnabled = engine.smartNamingOverride;

   this.solverParams = {
      observationDate: "",
      observationTimestamp: NaN,
      ra: NaN,
      dec: NaN,
      pixelSize: NaN,
      focalLength: NaN
   };

   if ( solverParams != undefined )
   {
      let keys = Object.keys( solverParams );
      for ( let i = 0; i < keys.length; ++i )
         this.solverParams[ keys[ i ] ] = solverParams[ keys[ i ] ];
   }

   if ( matchingSizes != undefined )
   {
      let keys = Object.keys( matchingSizes );
      for ( let i = 0; i < keys.length; ++i )
         this.matchingSizes[ keys[ i ] ] = matchingSizes[ keys[ i ] ];
   }

   this.isCFA = isCFA;
   this.isMaster = isMaster;
   this.keywords = keywords || {};

   this.overscan = new Overscan()
   if ( overscan != undefined )
      this.overscan.copyFrom( overscan );

   /**
    * Updates the keywords associated to the file
    */
   this.updateKeywords = () =>
   {
      this.keywords = {};
      engine.keywords.names().forEach( keywordName =>
      {
         // get the keyword from the file path
         let value = smartNaming.getCustomKeyValueFromPath( keywordName, this.filePath );
         if ( value != undefined )
            this.keywords[ keywordName ] = smartNaming.sanitizeKeywordValue( value );
         else
         {
            // get the keyword from the file header
            value = this.fileKeywords[ keywordName ];
            if ( value != undefined )
               this.keywords[ keywordName ] = smartNaming.sanitizeKeywordValue( value );
         }
      } );
   }

   /**
    * Initializes the internal data referencing the file.
    */
   this.initForProcessing = () =>
   {
      this.processed = {};
      this.current = {};
      this.descriptor = {};
      this.localNormalizationFile = {};
      this.drizzleFile = {};
      this.isReference = {};
      this.current[ "default" ] = this.filePath;
      this.descriptor[ "default" ] = undefined;
      this.localNormalizationFile[ "default" ] = undefined;
      this.drizzleFile[ "default" ] = undefined;
      this.isReference[ "default" ] = false;
   };

   /**
    * Invoked when a processing step has been performed successfully.
    *
    * @param {WBPPFrameProcessingStep} step step executed
    * @param {String} filePath processed output file
    * @param {String} associatedID defines the ID that identifies an associated file, if not
    *                             provided or empty string then the default id is used
    */
   this.processingSucceeded = ( step, filePath, associatedID ) =>
   {
      if ( associatedID == null || associatedID == "" )
         associatedID = "default";
      if ( this.processed[ associatedID ] == null )
         this.processed[ associatedID ] = {};
      this.processed[ associatedID ][ step ] = filePath;
      this.current[ associatedID ] = filePath;
   };

   /**
    * Mark the file as failed to process. Indicates that an error occurred.
    *
    */
   this.processingFailed = ( associatedID ) =>
   {
      if ( associatedID == null || associatedID == "" )
         associatedID = "default";
      this.current[ associatedID ] = undefined;
   };

   /**
    * Checks if the file processing is failed.
    *
    */
   this.failed = ( associatedID ) =>
   {
      if ( associatedID == null || associatedID == "" )
         associatedID = "default";
      return this.current[ associatedID ] == undefined;
   };

   /**
    * Stores the associated local normalization file
    *
    * @param {*} filePath the local normalization file path
    * @param {*} associatedID the associated ID
    */
   this.addLocalNormalizationFile = ( filePath, associatedID ) =>
   {
      if ( associatedID == null || associatedID == "" )
         associatedID = "default";
      this.localNormalizationFile[ associatedID ] = filePath;
   };

   /**
    * Stores the associated drizzle file
    *
    * @param {*} filePath the drizzle file path
    * @param {*} associatedID the associated ID
    */
   this.addDrizzleFile = ( filePath, associatedID ) =>
   {
      if ( associatedID == null || associatedID == "" )
         associatedID = "default";
      this.drizzleFile[ associatedID ] = filePath;
   };

   /**
    * Marks the file item as reference. This information tracks that the file item
    * has been used as a reference frame to align other images.
    *
    * @param {*} associatedID
    */
   this.markAsReference = ( associatedID ) =>
   {
      if ( associatedID == null || associatedID == "" )
         associatedID = "default";
      this.isReference[ associatedID ] = true;
   };

   this.initForProcessing();
}

FileItem.prototype = new Object;

// ----------------------------------------------------------------------------
// EOF BPP-FileItem.js - Released 2025-01-28T18:19:31Z
