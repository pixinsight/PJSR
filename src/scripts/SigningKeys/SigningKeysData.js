// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// SigningKeysData.js - Released 2022-04-13T15:13:50Z
// ----------------------------------------------------------------------------
//
// This file is part of PixInsight Signing Keys Maintenance Utility Script 1.0
//
// Copyright (c) 2021-2022 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/*
 * PixInsight Signing Keys Maintenance Utility
 *
 * Copyright (C) 2021-2022 Pleiades Astrophoto. All Rights Reserved.
 * Written by Juan Conejero (PTeam)
 *
 * Working Data.
 */

#include <pjsr/DataType.jsh>

function SigningKeysWorkingData()
{
   this.__base__ = Object;
   this.__base__();

   this.outputFilePath = "";
   this.developerId = "";
   this.localSigningIdentity = false;
   this.exportKeysFile = false;
   this.inputFilePath = "";

   this.saveSettings = function()
   {
      function save( key, type, value )
      {
         Settings.write( SETTINGS_KEY_BASE + key, type, value );
      }

      save( "outputFilePath", DataType_String, this.outputFilePath );
      save( "developerId", DataType_String, this.developerId );
      save( "localSigningIdentity", DataType_Boolean, this.localSigningIdentity );
   };

   this.loadSettings = function()
   {
      function load( key, type )
      {
         return Settings.read( SETTINGS_KEY_BASE + key, type );
      }

      let o = load( "outputFilePath", DataType_String );
      if ( o != null )
         this.outputFilePath = o;

      o = load( "developerId", DataType_String );
      if ( o != null )
         this.developerId = o;

      o = load( "localSigningIdentity", DataType_Boolean );
      if ( o != null )
         this.localSigningIdentity = o;
   };

   this.reset = function()
   {
      this.outputFilePath = "";
      this.developerId = "";
      this.localSigningIdentity = false;
      this.exportKeysFile = false;
      this.inputFilePath = "";
   };
}

SigningKeysWorkingData.prototype = new Object;

/*
 * Global working data object
 */
var g_workingData = new SigningKeysWorkingData;

// ----------------------------------------------------------------------------
// EOF SigningKeysData.js - Released 2022-04-13T15:13:50Z
