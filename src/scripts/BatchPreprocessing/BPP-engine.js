// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// BPP-engine.js - Released 2025-01-28T18:19:31Z
// ----------------------------------------------------------------------------
//
// This file is part of:
// - WeightedBatchPreprocessing script version 2.8.3
// - FastBatchPreprocessing script version 1.1.6
//
// Copyright (c) 2019-2025 Roberto Sartori
// Copyright (c) 2020-2021 Adam Block
// Copyright (c) 2019 Tommaso Rubechi
// Copyright (c) 2012 Kai Wiechen
// Copyright (c) 2012-2025 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#include <pjsr/Compression.jsh>
#include <pjsr/CryptographicHash.jsh>
#include <pjsr/ImageType.jsh>
#include <pjsr/LinearDefectDetection.jsh>
#include <pjsr/LinearPatternSubtraction.jsh>
#include <pjsr/PropertyAttribute.jsh>
#include <pjsr/PropertyType.jsh>
#include <pjsr/SourceCodeFlag.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/UndoFlag.jsh>

#define USE_SOLVER_LIBRARY true
#define STAR_CSV_FILE   File.systemTempDirectory + "/stars.csv"

#include "BPP-consoleLogger.js"
#include "BPP-processLogger.js"
#include "BPP-operationQueue.js"
#include "BPP-LNReferenceSelector.js"

#include "../AdP/CommonUIControls.js"
#include "../AdP/AstronomicalCatalogs.jsh"
#include "../AdP/WCSmetadata.jsh"
#include "../AdP/ImageSolver.js"
#include "../AdP/SearchCoordinatesDialog.js"

#include "BPP-Keywords.js"
#include "BPP-FileItem.js"
#include "BPP-ActiveFrame.js"
#include "BPP-FrameGroup.js"
#include "BPP-FrameGroupsManager.js"

// ----------------------------------------------------------------------------

/**
 * Overscan region constructor
 *
 */
function OverscanRegions()
{
   this.__base__ = Object;
   this.__base__();

   this.enabled = false; // whether to apply this overscan correction
   this.sourceRect = new Rect( 0 ); // source overscan region
   this.targetRect = new Rect( 0 ); // image region to be corrected

   this.isValid = function()
   {
      if ( !this.enabled )
         return true;
      if ( !this.sourceRect.isNormal || !this.targetRect.isNormal )
         return false;
      if ( this.sourceRect.x0 < 0 || this.sourceRect.y0 < 0 ||
         this.targetRect.x0 < 0 || this.targetRect.y0 < 0 )
         return false;
      return true;
   };
}

OverscanRegions.prototype = new Object;

// ----------------------------------------------------------------------------

/**
 * Overscan object constructor
 *
 */
function Overscan()
{
   this.__base__ = Object;
   this.__base__();

   this.enabled = false; // whether overscan correction is globally enabled

   this.overscan = new Array; // four overscan source and target regions
   this.overscan.push( new OverscanRegions );
   this.overscan.push( new OverscanRegions );
   this.overscan.push( new OverscanRegions );
   this.overscan.push( new OverscanRegions );

   this.imageRect = new Rect( 0 ); // image region (i.e. the cropping rectangle)

   this.isValid = function()
   {
      if ( !this.enabled )
         return true;
      for ( let i = 0; i < 4; ++i )
         if ( !this.overscan[ i ].isValid() )
            return false;
      if ( !this.imageRect.isNormal )
         return false;
      if ( this.imageRect.x0 < 0 || this.imageRect.y0 < 0 )
         return false;
      return true;
   };

   this.hasOverscanRegions = function()
   {
      for ( let i = 0; i < 4; ++i )
         if ( this.overscan[ i ].enabled )
            return true;
      return false;
   };

   this.reset = function()
   {
      this.enabled = false;
      for ( let i = 0; i < 4; ++i )
         this.overscan[ i ].enabled = false;
   };

   this.updateWithKeyword = function( key, value )
   {
      let updateRect = function( rect, str, value )
      {
         switch ( str )
         {
            case "X0":
               rect.x0 = value;
               break;
            case "Y0":
               rect.y0 = value;
               break;
            case "X1":
               rect.x1 = value;
               break;
            case "Y1":
               rect.y1 = value;
               break;
         }
      };

      // quickly check of the keyword format
      if ( key.length != 7 )
         return;
      if ( key[ 0 ] != 'O' || key[ 1 ] != 'S' )
         return;
      if ( key[ 4 ] != '0' && key[ 4 ] != '1' )
         return;
      if ( key[ 5 ] != 'X' && key[ 5 ] != 'Y' )
         return;
      let intValue;
      let indx;
      let coord = key.substring( 5, 7 );
      try
      {
         intValue = parseInt( value );
         indx = parseInt( key.substring( 4, 5 ) );
      }
      catch ( _ )
      {
      }
      if ( intValue == undefined || indx == undefined )
         return;

      switch ( key.substring( 0, 4 ) )
      {
         case "OSIR": // image region
            updateRect( this.imageRect, coord, intValue );
            this.enabled = true;
            break;
         case "OSSR": // source region
            updateRect( this.overscan[ indx ].sourceRect, coord, intValue );
            this.overscan[ indx ].enabled = true;
            break;
         case "OSTR": // target region
            updateRect( this.overscan[ indx ].targetRect, coord, intValue );
            this.overscan[ indx ].enabled = true;
            break;
      }
   };

   this.copyFrom = function( source )
   {
      if ( source == undefined )
         return;
      this.enabled = source.enabled;
      this.imageRect = new Rect( source.imageRect );
      for ( let i = 0; i < 4; ++i )
      {
         this.overscan[ i ].enabled = source.overscan[ i ].enabled;
         this.overscan[ i ].sourceRect = new Rect( source.overscan[ i ].sourceRect );
         this.overscan[ i ].targetRect = new Rect( source.overscan[ i ].targetRect );
      }
   };

   this.toString = () =>
   {
      let rectToTooltip = function( R )
      {
         return "  x0: " + R.x0 + "<br/>" +
                "  y0: " + R.x0 + "<br/>" +
                "  x1: " + R.x0 + "<br/>" +
                "  y1: " + R.x0 + "<br/>";
      };

      let str = "<p><b>Image region</b><br/>" + rectToTooltip( this.imageRect );
      for ( let i = 0; i < 4; ++i )
         if ( this.overscan[ i ].enabled )
            str += "<br/>Region #" + i + ":<br/>" +
            "[source rect]<br/>" + rectToTooltip( this.overscan[ i ].sourceRect ) +
            "[target rect]<br/>" + rectToTooltip( this.overscan[ i ].sourceRect );
      str += "</p>";
      return str;
   }
}

Overscan.prototype = new Object;

// ----------------------------------------------------------------------------
// BPP OPERATION QUEUE
let BPPOperationQueue = function()
{
   this.__base__ = OperationQueue;
   this.__base__();

   let
   {
      elapsedTimeToString
   } = WBPPUtils.shared();

   //

   this.onTerminationRequest = () =>
   {
      this.requestInterruption();
      console.abort( true /* don't ask */ );
   }

   /**
    * This function saves the current pipeline status.
    * This information is handled by the test saving operation.
    */
   this.getExecutionStatus = () =>
   {
      let executionStatus = {
         totalExecutionTime: this.executionTime,
         ops: []
      };

      for ( let i = 0; i < this.operations.length; ++i )
      {
         if ( this.operations[ i ].operation.trackable )
         {
            executionStatus.ops[ i ] = {};
            executionStatus.ops[ i ].operation = this.operations[ i ].operation;
            executionStatus.ops[ i ].name = this.operations[ i ].operation.name;
         }
      }

      return executionStatus;
   }

   //

   this.stepCallback = () =>
   {
      switch ( this.status )
      {
         case OperationQueueStatus.RUNNING: //
            if ( !this.currentOperation() || this.executionMonitorDialog == undefined )
            {
               // INITIALIZED, READY TO RUN THE FIRST OPERATION
               // show the monitor view and start the refresh timer
               this.executionMonitorDialog = new ExecutionMonitorDialog( false /* as report */ , this.requestInterruption, undefined /* operations */ , this.onTerminationRequest );
               this.executionMonitorDialog.show();
               this.updateExecutionReport();
               this.executionMonitorDialog.adjustDialogFrame();
               // start the event and GUI refresh timer
               this.updaterTimer.start();
            }
            // flush the logs
            engine.consoleLogger.flush();
            break;

         case OperationQueueStatus.INACTIVE: //
            // EXECUTION COMPLETED, dismiss the execution monitor as tracker and show it as report
            this.updaterTimer.stop();
            this.hideExecutionMonitorDialog();
            // remove the temporary saved parameters
            engine.removeRunningConfiguration();

            // if we are in automation mode and
            //   - not on "LoadOnly"
            //   - not recording the test
            // return;
            if ( engine.automationMode && !engine.testLoadOnly && !engine.recordTest )
               return;

            // store the execution status to be eventually saved in the test export information
            engine.executionStatus = this.getExecutionStatus();

            // in fast mode we always clear the cache
            if ( engine.fastMode )
               engine.executionCache.reset();

            // export the resulting test (with no cache) if we're recording
            if ( engine.automationMode && engine.recordTest && engine.testFile )
            {
               console.noteln( "STORE RECORDED TEST INTO ", engine.testFile )
               engine.executionCache.reset();
               let json = engine.exportParameters( true /* toJson */ );
               if ( File.exists( engine.testFile ) )
                  File.remove( engine.testFile );
               File.writeTextFile( engine.testFile, json );
               return;
            }

            this.executionMonitorDialog = new ExecutionMonitorDialog( true /* as report */ , undefined /* request interruption callback */ , this.trackableOperations() );
            this.executionMonitorDialog.noteLabel.text = "Executed in " + elapsedTimeToString( this.executionTime );

            // blocking execution
            this.executionMonitorDialog.execute();
            this.updateExecutionReport();
            break;
      }
      // RUNNING a task, refresh the status
      this.updateExecutionReport();
   };

   //

   // refresh timer
   this.updaterTimer = new Timer
   this.updaterTimer.interval = 1;
   this.updaterTimer.periodic = true;
   this.updaterTimer.dialog = this;
   this.updaterTimer.onTimeout = () =>
   {
      if ( this.executionMonitorDialog )
      {
         this.executionMonitorDialog.updateRunningOperation( this.trackableOperations() );
         this.executionMonitorDialog.noteLabel.text = "Elapsed: " + elapsedTimeToString( this.elapsed.value );
      }
      // refresh the progress report
      processEvents();
      gc();
   };
   this.updaterTimer.stop();

   //

   this.trackableOperations = () =>
   {
      return this.operations.filter( item => ( item.operation.trackable || false ) ).map( item => item.operation )
   };

   //

   this.updateExecutionReport = function()
   {
      if ( this.executionMonitorDialog )
         this.executionMonitorDialog.updateWithOperations( this.trackableOperations() );
   };

   //

   this.hideExecutionMonitorDialog = function()
   {
      // execution dialog
      if ( this.executionMonitorDialog )
      {
         this.executionMonitorDialog.ok();
         this.executionMonitorDialog = undefined;
      }
      // step termination box
      if ( this.waitingMsgBox )
      {
         this.waitingMsgBox.ok();
         this.waitingMsgBox = undefined;
      }
   };
}
BPPOperationQueue.prototype = new OperationQueue;

// ---------------------------------------------------------------------------
// BPP OERATIONS

/**
 * BPP Operation Block is a specialization of an Operation Block providing supplementary
 * information to support the tracking and the estimation( space and time ) of execution.
 *
 * @param {String} name the operation name
 * @param {Object} group the group handled by the operation
 * @param {Bool} trackable TRUE if the operation is tracked in the execution monitor
 */
let BPPOperationBlock = function( name, group, trackable )
{
   this.__base__ = OperationBlock;
   this.__base__();

   this.name = name;
   this.group = group;
   this.trackable = trackable;
   this.hasWarnings = false;
   this.statusMessage = "";

   // only trackable WBPP operations trigger the pipeline event script
   this.triggersEventScript = trackable;

   // operation execution time (in seconds)
   this.executionTime = 0;
   // average time per frame
   this.averageExecution = 0;

   this.run = ( environment, requestInterruption ) =>
   {
      return this._run( environment, requestInterruption );
   };

   this.updateGroupDescription = ( withActiveFrames, reportFileItems ) =>
   {
      this.groupDescription = ( group ? group.toShortString( withActiveFrames, reportFileItems ) : "" );
   };

   this.spaceRequired = () =>
   {
      return 0;
   };

   this.updateGroupDescription();
}
BPPOperationBlock.prototype = new OperationBlock;

// ----------------------------------------------------------------------------

/**
 * This object is responsible of handling the WBPP execution cache.
 * The execution cache is the set of information that allows to check if a process needs to be
 * executed or if its configuration is unchanged, the input files are unchanged and the output files
 * are already in the disk and unchanged since a previous execution. In this case, the process can be
 * skipped since it would produce exactly the already-existing output data.
 *
 */
function ExecutionCache()
{

   let hasher = new CryptographicHash( CryptographicHash_SHA1 );

   let
   {
      getLastModifiedDate
   } = WBPPUtils.shared();

   /**
    * Generates a cache key for the given string.
    *
    * @param {String} string
    * @return {String}
    */
   this.keyFor = ( string ) =>
   {
      return hasher.hash( ByteArray.stringToUTF8( string ) ).toHex();
   };

   /**
    * Clears the internal cache.
    *
    */
   this.reset = () =>
   {
      this.cache = {};
   };

   /**
    * Returns true if there is a cache data associated to the given key.
    *
    * @param {*} key
    * @return {*}
    */
   this.hasCacheForKey = ( key ) =>
   {
      return this.cache[ key ] != undefined;
   };

   /**
    * Retrieves the cached data associated to the given key.
    *
    * @param {*} key
    * @return {*}
    */
   this.cacheForKey = ( key ) =>
   {
      return this.cache[ key ];
   };

   /**
    * Sets the cache associated to the given key.
    *
    * @param {*} key
    * @param {*} value
    */
   this.setCache = ( key, value ) =>
   {
      this.cache[ key ] = value;
      console.writeln( "Cache saved with key ", key );
   };

   this.unsetCache = ( key ) =>
   {
      this.cache[ key ] = undefined;
      console.writeln( "Cache with key ", key, " purged" );
   }

   /**
    * Returns true if the file is unchanged since the last time its last modified date was cached for the given cache key
    *
    * @param {String} key
    * @param {String} filePath
    * @return {*}
    */
   this.isFileUnmodified = ( key, filePath ) =>
   {
      if ( arguments.length != 2 )
      {
         console.criticalln( "*** Error: isFileUnmodified() called with the wrong number of arguments (", arguments.length, ")" );
         return false;
      }
      if ( filePath == undefined || filePath.length == 0 )
         return false;
      let LMD = this.cache[ "LMD_" + key + filePath ];
      let currentLMD = getLastModifiedDate( filePath );
      let result = false;
      if ( currentLMD && LMD )
         result = currentLMD == LMD;
      console.writeln( "[cache] - file ", ( result ? "is unmodified" : "has changed" ), " <raw>" + File.extractNameAndExtension( filePath ) + "</raw> [ ", key, " ] - cached [", LMD, "] - current [", currentLMD, "]" );
      return result;
   };

   /**
    * Stores the last modified date of the given file in the cache to the given cache key
    *
    * @param {String} key
    * @param {String} filePath
    */
   this.cacheFileLMD = ( key, filePath ) =>
   {
      if ( arguments.length != 2 )
      {
         console.criticalln( "*** Error: cacheFileLMD() called with the wrong number of arguments (", arguments.length, ")" );
         return;
      }
      // cached the data only if file exists
      let LMD = getLastModifiedDate( filePath );
      if ( LMD )
      {
         console.writeln( "[cache] - set LMD [", key, "] <raw>" + File.extractNameAndExtension( filePath ) + "</raw>: ", LMD );
         let cacheKey = "LMD_" + key + filePath;
         this.setCache( cacheKey, LMD );
         this.__trackLMDKeys( filePath, cacheKey, LMD );
      }
   };

   /**
    * Invalidates the cached last modified date for the given file.
    *
    * @param {*} filePath
    */
   this.invalidateFileLMD = ( key, filePath ) =>
   {
      if ( arguments.length != 2 )
      {
         console.criticalln( "*** Error: invalidateFileLMD() called with the wrong number of arguments (", arguments.length, ")" );
         return;
      }
      console.writeln( "[cache] - invalidate LMD [", key, "] <raw>" + File.extractNameAndExtension( filePath ) + "</raw>" );
      let cacheKey = "LMD_" + key + filePath;
      this.unsetCache( cacheKey );
      this.__trackLMDKeys( filePath, cacheKey, undefined );
   };

   /**
    * Exports the cache to a string representation.
    *
    */
   this.toString = () =>
   {
      return JSON.stringify( this.cache, null, 2 );
   };

   /**
    * Reconstruct the cache from a string representation.
    *
    * @param {*} string
    */
   this.fromString = ( string ) =>
   {
      try
      {
         this.cache = JSON.parse( string );
      }
      catch ( e )
      {
         this.reset();
         console.warningln( "** Warning: Execution cache parsing failed." )
      }
   };

   /**
    * Approximates the size by returning the length of the JSON representation
    *
    */
   this.size = () =>
   {
      return this.toString().length
   };

   this.reset();

   /**
    * Internal function that tracks the inverse mapping from a file LMD to the list of associated keys.
    * This is useful to update the LMD when a file gets changed but the changes should not impact the cached LMD.
    * Actually this occurs when the astrometric solution needs to be written: since only the header file changes
    * and the data remains the same we want to update the previously cached LMD with the updated LMD after we save
    * the file with the new astrometric solution.
    *
    * @param {*} filePath
    * @param {*} key
    * @param {*} LMD
    */
   this.__trackLMDKeys = ( filePath, key, LMD ) =>
   {
      let fileHistory = this.cache[ filePath ];
      if ( fileHistory == undefined || fileHistory.length == 0 )
      {
         this.setCache( filePath, [
         {
            lmd: LMD,
            keys: [ key ]
         } ] );
      }
      else
      {
         if ( fileHistory[ fileHistory.length - 1 ].lmd == LMD )
            fileHistory[ fileHistory.length - 1 ].keys.push( key )
         else
         {
            fileHistory.push(
            {
               lmd: LMD,
               keys: [ key ]
            } )
         }
         this.setCache( filePath, fileHistory );
      }
   };

   /**
    * Updates the LMD to the new one for all cached files. This function could be called from outside to
    * explicitly modify a previously cached LMD for a given file.
    * This is used when we save the astrometric solution since the data regards the header file and not the image, so we
    * want to keep the file cached after it has been saved and its LMD changes (thus we need to update the cached LMD
    * with the new LMD).
    *
    * @param {*} filePath
    * @param {*} previousLMD
    * @param {*} newLMD
    */
   this.updateLMD = ( filePath, previousLMD, newLMD ) =>
   {
      let fileHistoryUpdated = false;
      let fileHistory = this.cache[ filePath ];
      if ( fileHistory != undefined && fileHistory.length > 0 )
         for ( let i = 0; i < fileHistory.length; ++i )
            if ( fileHistory[ i ].lmd == previousLMD )
            {
               for ( let j = 0; j < fileHistory[ i ].keys.length; ++j )
               {
                  let key = fileHistory[ i ].keys[ j ];
                  this.setCache( key, newLMD );
               }
               fileHistory[ i ].lmd = newLMD;
               fileHistoryUpdated = true;
               break;
            }
      if ( fileHistoryUpdated )
         this.setCache( filePath, fileHistory );
   };
}

// ----------------------------------------------------------------------------

/**
 * Main StackEngine object constructor
 *
 */
function StackEngine()
{
   this.__base__ = Object;
   this.__base__();

   this.diagnosticMessages = new Array;

   // memoization cache
   this.findGroupMemoization = {};

   // allocate structures
   this.overscan = new Overscan;
   this.combination = new Array( 4 );
   this.rejection = new Array( 4 );
   this.percentileLow = new Array( 4 );
   this.percentileHigh = new Array( 4 );
   this.sigmaLow = new Array( 4 );
   this.sigmaHigh = new Array( 4 );
   this.linearFitLow = new Array( 4 );
   this.linearFitHigh = new Array( 4 );
   this.ESD_Outliers = new Array( 4 );
   this.ESD_Significance = new Array( 4 );
   this.RCR_Limit = new Array( 4 );

   this.groupsManager = new FrameGroupsManager();

   this.operationQueue = new BPPOperationQueue();

   // generic execution cache object handled by pipeline steps
   this.executionCache = new ExecutionCache();

   // console logger
   this.consoleLogger = new ConsoleLogger();

   // process logger
   this.processLogger = new ProcessLogger();
}

StackEngine.prototype = new Object;

// ----------------------------------------------------------------------------
// OVERSCAN MANAGEMENT
// ----------------------------------------------------------------------------
/**
 * Sets the global overscan settings from a specific group settings.
 *
 * @param {*} group the group containing the overscan settings
 */
StackEngine.prototype.setOverscanInfoFromGroup = function( group )
{
   if ( group.hasMaster )
      this.overscan.copyFrom( group.fileItems[ 0 ].overscan );
}

// ----------------------------------------------------------------------------
// REFERENCE FRAME MANAGEMENT
// ----------------------------------------------------------------------------
StackEngine.prototype.getBestReferenceFrameModes = function()
{
   // store the current selected item
   let selections = [ "manual", "auto" ];
   let postProcessKeywords = engine.keywords.keywordsForMode( WBPPGroupingMode.POST );
   for ( let i = 0; i < postProcessKeywords.length; ++i )
      selections.push( "auto by " + postProcessKeywords[ i ].name );
   // cached reference frames are available only in WBPP
   return selections;
};

StackEngine.prototype.setBestReferenceFrameMode = function( index )
{
   let entries = this.getBestReferenceFrameModes();
   let string = entries[index];
   if ( string == "manual" )
   {
      this.bestFrameReferenceMethod = WBPPBestReferenceMethod.MANUAL;
      this.bestFrameReferenceKeyword = "";
   }
   else if ( string == "auto" )
   {
      this.bestFrameReferenceMethod = WBPPBestReferenceMethod.AUTO_SINGLE;
      this.bestFrameReferenceKeyword = "";
   }
   else
   {
      this.bestFrameReferenceMethod = WBPPBestReferenceMethod.AUTO_KEYWORD;
      this.bestFrameReferenceKeyword = string.replace( "auto by ", "" );
   }
};

StackEngine.prototype.bestReferenceFrameModeIndex = function()
{
   if ( this.bestFrameReferenceMethod == WBPPBestReferenceMethod.MANUAL || this.bestFrameReferenceMethod == WBPPBestReferenceMethod.AUTO_SINGLE )
      return this.bestFrameReferenceMethod;

   let options = this.getBestReferenceFrameModes().map( item => item.replace( "auto by ", "" ) );

   for ( let i = 0; i < options.length; ++i )
      if ( options[ i ] == this.bestFrameReferenceKeyword )
         return i;

   // error, not able to find the index. Change the method to AUTO SINGLE
   this.bestFrameReferenceMethod = WBPPBestReferenceMethod.AUTO_SINGLE;
   return 1;
};

// ----------------------------------------------------------------------------
// GROUPS GETTERS
// ----------------------------------------------------------------------------

//
/**
 * Returns a sorted list of groups of a given type and mode.
 *
 * @param {ImageType} type type of groups to be retrieved
 * @param {WBPPGroupingMode} mode the grouping mode
 * @returns the sorted list of groups
 */
StackEngine.prototype.getSortedGroupsOfType = function( type, mode )
{
   let groupsByType = [];
   let groups = this.groupsManager.groupsForMode( mode );
   for ( let i = 0; i < groups.length; ++i )
      if ( groups[ i ].imageType == type )
         groupsByType.push( groups[ i ] );

   // get the ordered keywords
   let keywords;

   // sort result
   groupsByType.sort( ( a, b ) =>
   {
      // sort image size first
      let asize = a.size.width * a.size.height;
      let bsize = b.size.width * b.size.height;
      if ( asize != bsize )
         return asize < bsize ? -1 : 1;

      // master file on bottom
      if ( a.hasMaster != b.hasMaster )
         return a.hasMaster ? -1 : 1;
      // filter by binning
      if ( a.binning != b.binning )
         return a.binning > b.binning ? -1 : 1;

      // for Flats, the filter is the only third sorting rule
      if ( type == ImageType.FLAT )
         return a.filter.localeCompare( b.filter );

      // filter by duration
      if ( a.exposureTime != b.exposureTime )
         return a.exposureTime > b.exposureTime ? -1 : 1;

      // filter by filter name
      if ( a.filter != b.filter )
         return a.filter.localeCompare( b.filter );

      // allocate the sorting keywords only one time if needed
      if ( keywords == undefined )
         keywords = engine.keywords.sortedNames( mode );

      // filter by keywords
      for ( let j = 0; j < keywords.length; ++j )
      {
         let aKeyword = a.keywords[ keywords[ j ] ];
         let bKeyword = b.keywords[ keywords[ j ] ];
         if ( aKeyword != undefined && bKeyword == undefined )
            return -1;
         if ( aKeyword == undefined && bKeyword != undefined )
            return 1;
         if ( aKeyword != bKeyword )
            return aKeyword.localeCompare( bKeyword );
      }
      // sorting not defined, groups have the same sorting precedence (this should never happen)
      return 0;
   } );
   // resort inserting associated channels and recombined just after the master groups
   let sorted = [];
   // first pass, extract non associated groups and map the associated ones
   // linkedGroupIDmap is a map [groupID] => {R: group?, B: group?, B: group?...} mapping the groups
   // that have linked groups and the linked groups mapped on the associated channels, for a quick second pass insertion
   let linkedGroupIDmap = {};
   let parentGroups = [];
   for ( let i = 0; i < groupsByType.length; ++i )
   {
      let linkedGroupID = groupsByType[ i ].linkedGroupID;
      if ( linkedGroupID == undefined )
         parentGroups.push( groupsByType[ i ] );
      else
      {
         if ( !linkedGroupIDmap[ linkedGroupID ] )
            linkedGroupIDmap[ linkedGroupID ] = {}
         linkedGroupIDmap[ linkedGroupID ][ groupsByType[ i ].associatedRGBchannel ] = groupsByType[ i ];
      }
   }
   // second pass, insert the parents and the associated immediately after
   for ( let i = 0; i < parentGroups.length; ++i )
   {
      sorted.push( parentGroups[ i ] );
      let groupID = parentGroups[ i ].id;
      if ( linkedGroupIDmap[ groupID ] )
         for ( let j = 0; j < WBPPAssociatedChannel.sorted.length; ++j )
            if ( linkedGroupIDmap[ groupID ][ WBPPAssociatedChannel.sorted[ j ] ] )
               sorted.push( linkedGroupIDmap[ groupID ][ WBPPAssociatedChannel.sorted[ j ] ] );
   }

   return sorted;
};

// ----------------------------------------------------------------------------

/**
 * Search for the groups that are calibrated by the provided group.
 * NB: by default this function looks the calibration groups with mode WBPPGroupingMode = .PRE
 *
 * @param {FrameGroup} group the group that calibrates all the returned groups
 * @returns the array of groups that are calibrated by the provided group
 */
StackEngine.prototype.getGroupsCalibratedBy = function( group )
{
   let calibratedBy = [];

   // Scan all pre-processing groups, search for the calibration files for each group
   // of the same type of the provided group, if the ID matches then the looped group
   // is calibrated with the provided file
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );
   for ( let i = 0; i < groups.length; ++i )
   {
      let cg = groups[ i ];

      if ( cg == group )
         continue;

      let cf = this.getCalibrationGroupsFor( cg );

      if ( group.imageType == ImageType.BIAS && group == cf.masterBias )
      {
         calibratedBy.push( cg );
         continue
      }

      if ( group.imageType == ImageType.DARK && group == cf.masterDark )
      {
         calibratedBy.push( cg );
         continue
      }

      if ( group.imageType == ImageType.FLAT && group == cf.masterFlat )
      {
         calibratedBy.push( cg );
         continue
      }
   }
   return calibratedBy;
};

// ----------------------------------------------------------------------------

/**
 * Search for the groups that calibrate the provided group.
 *
 * @param {FrameGroup} group the group for which we want to retrieve the calibration groups
 * @returns the array of groups that calibrate the provided group
 */
StackEngine.prototype.getCalibrationGroupsFor = function( group )
{
   let calibrationGroups = {
      masterBias: undefined,
      masterDark: undefined,
      masterFlat: undefined,
   };
   // a master file is not calibrated
   if ( group.hasMaster )
      return calibrationGroups;

   let mb, md, mf;
   let size = group.size;
   let binning = group.binning;
   let exposureTime = group.exposureTime;
   let filter = group.filter;
   let exactDarkExposureTime = false;
   let isCFA = group.isCFA;

   switch ( group.imageType )
   {
      case ImageType.DARK:
         // dark frames are never calibrated
         break;
      case ImageType.FLAT:
         // look for compatible master dark and master bias
         mb = this.getMasterBiasGroup( binning, size, false /*isMaster*/ , group.keywords );
         if ( !group.forceNoDark )
            md = group.overrideDark || this.getMasterDarkGroup( ImageType.FLAT, binning, exposureTime, size, mb /* exactDarkExposureTime: yes if we have a master bias */ , group.keywords );
         break;
      case ImageType.LIGHT:
         mb = this.getMasterBiasGroup( binning, size, false, group.keywords );
         if ( !group.forceNoDark )
            md = group.overrideDark || this.getMasterDarkGroup( ImageType.LIGHT, binning, exposureTime, size, exactDarkExposureTime, group.keywords );
         if ( !group.forceNoFlat )
            mf = group.overrideFlat || this.getMasterFlatGroup( binning, size, filter, isCFA, false /*isMaster*/ , group.keywords );
         break;
   }

   // Advanced logic: remove master bias when it is not necessary
   // MasterBias is needed only when:
   // 1. master dark is NOT present
   // 2. if master dark is present and optimized
   // 3. master dark is present but does not contain the bias
   if ( mb )
   {
      let masterDarkISNotPResent = !md;
      let masterDarkIsOptimized = md && group.optimizeMasterDark;

      if ( masterDarkISNotPResent || masterDarkIsOptimized )
         calibrationGroups.masterBias = mb;
   }
   if ( md )
      calibrationGroups.masterDark = md;
   if ( mf )
      calibrationGroups.masterFlat = mf;

   return calibrationGroups;
};

// ----------------------------------------------------------------------------

/**
 * Returns an array of { group, count } objects containing a group and the number
 * of matching keywords, filtering by the image type provided.
 * For each group, the number of matching keywords is counted, groups with the same
 * count are grouped together and the groups with the same keyword matching count is
 * sorted by keyword precedence.
 *
 * @param {ImageType} imageType
 * @param {Object} size {width:Numeric, height:Numeric}
 * @param {{String:String}} keywords the keywords key-value map
 * @param {Boolean} onlyHighestMatches if true returns the list of groups with the
 *                                     highest number of matching keywords, otherwise
 *                                     it returns all the matching groups
 * @returns
 */
StackEngine.prototype.getCompatibleCalibrationGroups = function( imageType, size, keywords, onlyHighestMatches )
{
   // preselect the compatible groups
   let matchingGroups = {};
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );
   for ( let i = 0; i < groups.length; ++i )
      if ( groups[ i ].imageType == imageType )
      {
         // overscan handling: when we search for a group with overscan enabled we have to handle the case
         // where a flat group has a master into it, then the master size must match the current overscan
         // area size to match, otherwise the regular matching is applied
         let compatibleSize = groups[ i ].size.width == size.width && groups[ i ].size.height == size.height;
         if ( imageType == ImageType.FLAT &&
            groups[ i ].hasMaster )
         {
            if ( engine.overscan.enabled )
            {
               // in case of overscan is enabled then also the size of the master must match the size of the overscan area
               let W = engine.overscan.imageRect.x1 - engine.overscan.imageRect.x0;
               let H = engine.overscan.imageRect.y1 - engine.overscan.imageRect.y0;
               compatibleSize = compatibleSize && groups[ i ].fileItems[ 0 ].size.width == W && groups[ i ].fileItems[ 0 ].size.height == H;
            }
            else
            {
               // in case of overscan is not active then the size of the master must match the provided size
               compatibleSize = compatibleSize && groups[ i ].fileItems[ 0 ].size.width == size.width && groups[ i ].fileItems[ 0 ].size.height == size.height;
            }
         }

         if ( compatibleSize )
         {
            // enable the strict direct matching i.e. we exclude a group if it
            // has a keyword that is not in the set of the provided keywords
            let count = groups[ i ].keywordsMatchCount(
               keywords,
               true, /* strictDirectMatching */
               false /* strictInverseMatching */
            );
            if ( count >= 0 )
            {
               if ( !matchingGroups[ count ] )
                  matchingGroups[ count ] = [];
               matchingGroups[ count ].push( i );
            }
         }
      }

   // matchingGroups is a map between the count number and the array of compatible groups
   // we select the groups with the highest matching count
   let sortedCounts = Object.keys( matchingGroups );
   sortedCounts.sort();
   sortedCounts.reverse();

   // search for the best dark within the candidates
   let compatibleGroups = [];

   // extract the candidates
   if ( sortedCounts.length > 0 )
   {
      // keep only the highest matching if required
      if ( onlyHighestMatches )
         sortedCounts = [ sortedCounts[ 0 ] ];

      // internally sort groups by keyword precedence
      let keywords = engine.keywords.names();
      sortedCounts.forEach( count =>
      {
         // get the groups with the current count value
         let currentGroups = matchingGroups[ count ].map( i => groups[ i ] );
         // sort by keyword precedence
         currentGroups.sort( ( a, b ) =>
         {
            // process keywords respecting the order, if a keyword is found in group A
            // but not in group B then A has precedence and the other way around. If they
            // have the same keywords then the behavior is undefined
            for ( let j = 0; j < keywords.length; ++j )
            {
               let aKeyword = a.keywords[ keywords[ j ] ];
               let bKeyword = b.keywords[ keywords[ j ] ];
               if ( aKeyword != undefined && bKeyword == undefined )
                  return -1;
               if ( aKeyword == undefined && bKeyword != undefined )
                  return 1;
            }
            return 0;
         } );
         compatibleGroups = compatibleGroups.concat( currentGroups.map( g => (
         {
            group: g,
            count: count
         } ) ) );
      } );
   }

   return compatibleGroups;
};

// ----------------------------------------------------------------------------

/**
 * Returns the list of groups that matches the criteria provided.
 * This function accepts an object with key - value that must be matched by the returned group.
 *
 * @param {{}} properties key-value pair to be matched
 * @returns
 */
StackEngine.prototype.getCalibrationGroupsMatching = function( properties )
{
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );
   return groups.reduce( ( matchingGroups, group ) =>
   {
      let keys = Object.keys( properties );
      for ( let i = 0; i < keys.length; ++i )
      {
         let key = keys[ i ];
         // handle the special case of the 'size' property object
         if ( key == "size" )
         {
            if ( group[ key ] && ( group[ key ].width != properties[ key ].width || group[ key ].height != properties[ key ].height ) )
               return matchingGroups;
         }
         else if ( group[ key ] != properties[ key ] )
            return matchingGroups;
      }
      matchingGroups.push( group );
      return matchingGroups;
   }, [] );
};

// ----------------------------------------------------------------------------

/**
 * Search for the Master Bias Group matching the given parameters.
 *
 * @param {Numeric} binning binning to match
 * @param {Boolean} isMaster true if the group has to contain a master file, false otherwise
 * @param {{String:String}} keywords the keywords key-value map
 * @returns the matching masterBias group on success, undefined otherwise
 */
StackEngine.prototype.getMasterBiasGroup = function( binning, size, isMaster, keywords )
{
   let compatibleBias = this.getCompatibleCalibrationGroups( ImageType.BIAS, size, keywords, true /* onlyHighestMatches */ );

   for ( let i = 0; i < compatibleBias.length; ++i )
      if ( !isMaster || compatibleBias[ i ].group.hasMaster )
         if ( compatibleBias[ i ].group.binning == binning )
            return compatibleBias[ i ].group;
   return undefined;
};

// ----------------------------------------------------------------------------

/**
 * Returns the group containing or generating the best matching master dark given the parameters.
 *
 * @param {ImageType} imageType the imate type for which we want the matching dark (flat or light frames)
 * @param {Numeric} binning binning to match
 * @param {Numeric} exposureTime exposure time to search for
 * @param {Object} size {width:Numeric, height:Numeric}
 * @param {Boolean} findExactExposureTime true if exposure time must match exactly, false otherwise
 * @param {{String: String}} keywords { key: value } keywords object
 * @param {Boolean} isMaster true if the group must already contain the master file
 * @param {Boolean} logResult true if the search result of the master dark needs to print some log
 *                      information on the console
 * @returns the matching master dark group
 */
StackEngine.prototype.getMasterDarkGroup = function( imageType, binning, exposureTime, size, findExactExposureTime, keywords, isMaster, logResult )
{
   // Assume no binning when binning is unknown.
   if ( binning <= 0 )
      binning = 1;

   // Ensure we get the most exposed master dark frame when the exposure time
   // is unknown. This favors scaling down dark current during optimization.
   let knownTime = exposureTime > 0;
   if ( !knownTime )
      exposureTime = 1.0e+10;

   // By default we do not search for exact duration darks.
   if ( findExactExposureTime === undefined )
      findExactExposureTime = false;

   // search for the best dark within the candidates
   let masterDarkGroup = undefined;
   let candidateDarks = this.getCompatibleCalibrationGroups( ImageType.DARK, size, keywords, imageType !== ImageType.LIGHT /* onlyHighestMatches */ );

   let foundTime = 1.0e+20;
   let bestSoFar = 1.0e+20;
   let bestMatchingCount = -1;
   for ( let i = 0; i < candidateDarks.length; ++i )
      if ( !isMaster || candidateDarks[ i ].group.hasMaster )
         if ( candidateDarks[ i ].group.imageType == ImageType.DARK )
            if ( candidateDarks[ i ].group.binning == binning )
            {
               let d = Math.abs( candidateDarks[ i ].group.exposureTime - exposureTime );
               if ( d <= bestSoFar && ( !findExactExposureTime || ( findExactExposureTime && d < CONST_FLAT_DARK_TOLERANCE ) ) )
               {
                  // if the best equals the current exposure then we keep it only if the new best has a higher number matching keywords
                  if ( d == bestSoFar )
                     if ( candidateDarks[ i ].length <= bestMatchingCount )
                        continue;

                  bestMatchingCount = candidateDarks[ i ].length;
                  masterDarkGroup = candidateDarks[ i ].group;
                  foundTime = candidateDarks[ i ].group.exposureTime;
                  bestSoFar = d;
               }
            }

   if ( masterDarkGroup && logResult )
      if ( foundTime > 0 )
      {
         if ( findExactExposureTime )
            console.noteln( "<end><cbr><br>* Searching for a master flat dark with exposure time = " +
               exposureTime + "s -- found." );
         else if ( knownTime )
            console.noteln( "<end><cbr><br>* Searching for a master dark frame with exposure time = ",
               exposureTime + "s -- best match is ", foundTime + "s" );
         else
            console.noteln( "<end><cbr><br>* Using master dark frame with exposure time = ",
               foundTime + "s to calibrate unknown exposure time frame(s)." );
      }
   else
   {
      if ( findExactExposureTime )
         console.noteln( "<end><cbr><br>* Searching for a master flat dark with exposure time = ",
            exposureTime + "s -- not found." );
      else if ( knownTime )
         console.noteln( "<end><cbr><br>* Searching for a master dark frame with exposure time = ",
            exposureTime + "s -- best match is a master dark frame of unknown exposure time." );
      else
         console.noteln( "<end><cbr><br>* Master dark match with an unknown exposure time." );
   }

   return masterDarkGroup;
};

// ----------------------------------------------------------------------------

/**
 * Returns the group containing or generating the best matching master flat given the parameters.
 *
 * @param {Numeric} binning
 * @param {Object} size {width:Numeric, height:Numeric}
 * @param {String} filter
 * @param {Boolean} isMaster true if the group must already contain the master file
 * @returns the matching master flat group
 */
StackEngine.prototype.getMasterFlatGroup = function( binning, size, filter, isCFA, isMaster, keywords )
{
   // search for the best flat group within the candidates
   let candidateFlats = this.getCompatibleCalibrationGroups( ImageType.FLAT, size, keywords, false /* onlyHighestMatches */ );

   for ( let i = 0; i < candidateFlats.length; ++i )
      if ( !isMaster || candidateFlats[ i ].group.hasMaster )
         if ( candidateFlats[ i ].group.imageType == ImageType.FLAT )
            if ( candidateFlats[ i ].group.binning == binning && candidateFlats[ i ].group.filter == filter && candidateFlats[ i ].group.isCFA == isCFA )
               return candidateFlats[ i ].group;
   return undefined;
};

// ----------------------------------------------------------------------------
// FRAME GROUP CHECKS
// ----------------------------------------------------------------------------

/**
 * Returns true if groups of the given type and mode exist.
 *
 * @param {ImageType} imageType
 * @param {WBPPGroupingMode} mode
 * @returns
 */
StackEngine.prototype.hasFrames = function( imageType, mode )
{
   let groups = this.groupsManager.groupsForMode( mode );
   for ( let i = 0; i < groups.length; ++i )
      if ( groups[ i ].imageType == imageType )
         return true;
   return false;
};

// ----------------------------------------------------------------------------

/**
 * Returns true if bias frames exists.
 *
 * @returns
 */
StackEngine.prototype.hasBiasFrames = function()
{
   return this.hasFrames( ImageType.BIAS, WBPPGroupingMode.PRE );
};

/**
 * Returns true if dark frames exists.
 *
 * @returns
 */
StackEngine.prototype.hasDarkFrames = function()
{
   return this.hasFrames( ImageType.DARK, WBPPGroupingMode.PRE );
};

/**
 * Returns true if flat frames exists.
 *
 * @returns
 */
StackEngine.prototype.hasFlatFrames = function()
{
   return this.hasFrames( ImageType.FLAT, WBPPGroupingMode.PRE );
};

/**
 * Returns true if light frame groups with the given mode exists.
 *
 * @returns
 */
StackEngine.prototype.hasLightFrames = function( mode )
{
   return this.hasFrames( ImageType.LIGHT, mode );
};

// ----------------------------------------------------------------------------
// StackEngine Presets
// ----------------------------------------------------------------------------

/**
 * Constructor of the presets Dialog.
 */
function PresetsDialog()
{
   this.__base__ = Dialog;
   this.__base__();
   this.windowTitle = "Presets";

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.spacing = 8;

   this.addPreset = ( title, description, preset ) =>
   {
      let control = new ParametersControl( title, this );
      control.setScaledFixedWidth( 400 );

      let label = new Label( control );
      label.wordWrapping = true;
      label.useRichText = true;
      label.text = description;

      let button = new PushButton( control );
      button.text = "APPLY";
      button.onClick = () =>
      {
         this.done( preset );
      }
      let sizer = new HorizontalSizer;
      sizer.addStretch();
      sizer.add( button );
      sizer.addStretch();

      control.add( label );
      control.add( sizer );
      this.sizer.add( control );
   };

   // MAX QUALITY
   this.addPreset(
      "Maximum quality",
      "<p><b>Maximum quality with no compromises.</b><br/>" +
      "Local normalization is enabled with its default maximum number of stars used for scale evaluation, " +
      "the PSF type is set to <i>Auto</i>.</p>",
      WBPPPresets.BEST_QUALITY
   );

   // FASTER / MID QUALITY
   this.addPreset(
      "Faster with good quality",
      "<p><b>Faster with sub-optimal quality results.</b><br/>" +
      "Local normalization is enabled with a reduced number of stars used for scale evaluation (500), " +
      "the PSF type is set to <i>Moffat 4</i>.</p>",
      WBPPPresets.MID
   );

   // FASTER / MID QUALITY
   this.addPreset(
      "Fastest with lower quality",
      "<p><b>Fastest method with lower quality results.</b><br/>" +
      "Local normalization is disabled.</p>",
      WBPPPresets.FAST
   );

   //
   this.ensureLayoutUpdated();
   this.setFixedSize();
}
PresetsDialog.prototype = new Dialog;

/**
 * Configure the engine parameters by applying the given preset.
 *
 * @param {*} preset
 */
StackEngine.prototype.applyPreset = function( preset )
{
   switch ( preset )
   {
      case WBPPPresets.BEST_QUALITY:
         // enable and configure local normalization
         this.localNormalization = true;
         this.localNormalizationPsfType = WBPPLocalNormalizationPsfType.AUTO;
         this.localNormalizationPsfMaxStars = DEFAULT_LOCALNORMALIZATION_PSF_MAX_STARS;
         break;
      case WBPPPresets.MID:
         this.localNormalization = true;
         this.localNormalizationPsfType = WBPPLocalNormalizationPsfType.MOFFAT_4;
         this.localNormalizationPsfMaxStars = 500;
         break;
      case WBPPPresets.FAST:
         this.localNormalization = false;
         break;
   }
};

// ----------------------------------------------------------------------------
// StackEngine Methods
// ----------------------------------------------------------------------------

/**
 * Returns the image type given the keyword value. Invoked when IMAGETYP is found in FITS header.
 *
 * @param {String} value IMAGETYP keyword's value
 * @returns the image type, UNKNOWN if type cannot be inferred
 */
StackEngine.imageTypeFromKeyword = function( value )
{
   switch ( value.toLowerCase().replace( " ", "" ) )
   {
      case "bias":
      case "biasframe":
      case "masterbias":
         return ImageType.BIAS;
      case "dark":
      case "darkframe":
      case "masterdark":
      case "flatdark":
      case "darkflat":
         return ImageType.DARK;
      case "flat":
      case "flatframe":
      case "flatfield":
      case "masterflat":
         return ImageType.FLAT;
      case "light":
      case "lightframe":
      case "scienceframe":
      case "science":
      case "masterlight":
         return ImageType.LIGHT;
      default:
         return ImageType.UNKNOWN;
   }
};

// ----------------------------------------------------------------------------

/**
 * Checks if the file is to be considered a master file from the IMAGETYP keyword's value.
 *
 * @param {String} value IMAGETYP keyword's value
 * @returns true if file is to be used as master file
 */
StackEngine.isMasterFromKeyword = function( value )
{
   return value.toLowerCase().indexOf( "master" ) >= 0;
};

// ----------------------------------------------------------------------------

/**
 * Returns the string describing the image type.
 *
 * @param {ImageType} imageType
 * @returns
 */
StackEngine.imageTypeToString = function( imageType )
{
   return [ "Bias", "Dark", "Flat", "Light" ][ imageType ];
};

// ----------------------------------------------------------------------------

/**
 * Returns the string describing the master file associated to the provided image type.
 *
 * @param {ImageType} imageType
 * @returns
 */
StackEngine.imageTypeToMasterKeywordValue = function( imageType )
{
   return [ "Master Bias", "Master Dark", "Master Flat", "Master Light" ][ imageType ];
};

// ----------------------------------------------------------------------------
// WARNINGS AND DIAGNOSTICS
// ----------------------------------------------------------------------------
StackEngine.prototype.errorPrefix = "<span style=\"color:#DD1111;\"><b>Error</b>: ";
StackEngine.prototype.errorPostfix = "</span>";
StackEngine.prototype.warningPrefix = "<span style=\"color:#CC00CC;\"><b>Warning</b>: ";
StackEngine.prototype.warningPostfix = "</span>";
StackEngine.prototype.notePrefix = "<span style=\"color:#009900;\"><b>Note</b></span>: <span style=\"white-space:break-spaces;\">";
StackEngine.prototype.notePostfix = "</span>";

/**
 * Diagnostic messages generator.
 */
StackEngine.prototype.runDiagnostics = function()
{
   let
   {
      cleanFilterName,
      isEmptyString,
      paddedLabel,
      readableSize
   } = WBPPUtils.shared();

   this.messages = [ 0 ];

   this.pushTop = () =>
   {
      this.messages.push( this.diagnosticMessages.length );
   };
   this.top = () =>
   {
      return this.messages[ this.messages.length - 1 ];
   };
   this.popTop = () =>
   {
      if ( this.messages.length > 1 )
         this.messages.pop();
   };

   let preprocessGroups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );
   let postprocessGroups = this.groupsManager.groupsForMode( WBPPGroupingMode.POST );

   /**
    * Removes all messages that are not errors or warnings until an error or warning is retrieved or
    * the top pointer is reached.
    */
   this.cleanUp = () =>
   {
      while ( this.top() < this.diagnosticMessages.length && this.diagnosticMessages.length > 0 )
      {
         let msg = this.diagnosticMessages[ this.diagnosticMessages.length - 1 ];
         if ( !msg.startsWith( StackEngine.prototype.errorPrefix ) && !msg.startsWith( StackEngine.prototype.warningPrefix ) && !msg.startsWith( StackEngine.prototype.notePrefix ) )
            this.diagnosticMessages.pop();
         else
            return;
      }
   };

   /**
    * Pushes an error message
    *
    * @param {*} message the error message
    */
   this.error = function( message )
   {
      this.diagnosticMessages.push( StackEngine.prototype.errorPrefix + "<i>" + message + "</i>." + StackEngine.prototype.errorPostfix );
   };

   /**
    * Pushes a warning message.
    *
    * @param {*} message the warning message
    */
   this.warning = function( message )
   {
      this.diagnosticMessages.push( StackEngine.prototype.warningPrefix + "<i>" + message + "</i>." + StackEngine.prototype.warningPostfix );
   };

   /**
    * Pushes a note message.
    *
    * @param {*} message the note message
    */
   this.note = function( message )
   {
      this.diagnosticMessages.push( StackEngine.prototype.notePrefix + message + "." + StackEngine.prototype.notePostfix );
   };

   /**
    * Adds a generic text to the diagnostic, appending a period at the end.
    *
    * @param {*} text
    */
   this.genericText = function( text )
   {
      this.diagnosticMessages.push( text + "." );
   };

   /**
    * Adds a header
    *
    * @param {*} text the header title
    */
   this.headerText = function( text )
   {
      this.diagnosticMessages.push( "<br><br><b>==== " + text + "</b>" );
   };

   // initial clean up
   this.clearDiagnosticMessages();

   // ........................................................................
   this.pushTop();
   this.headerText( "Check for long file path limitation" );
   if ( this.outputDirectory != "" && File.directoryExists( this.outputDirectory ) )
   {
      let foldersList = [ this.outputDirectory ];
      while ( true )
      {
         foldersList.push( "WBPP_64_characters_folder_name_to_test_long_path_accessibility_" );
         let curDir = foldersList.join( "/" );
         try
         {
            File.createDirectory( curDir, true /* create intermediate dir */ );
         }
         catch ( e )
         {
            this.warning( "Cannot create files with a <b>path longer than 256 characters</b>. This is a limitation imposed by the " +
               "operating system and can cause the failure of one or more steps.<br>" +
               "Ensure that you remove this limitation by properly configuring your operating system settings." );
            break;
         }
         if ( curDir.length > 260 )
            break;
      }

      // clean up
      while ( foldersList.length > 1 )
      {
         let curDir = foldersList.join( "/" );
         try
         {
            File.removeDirectory( curDir );
         }
         catch ( e )
         {};
         foldersList.pop();
      }
   }
   this.cleanUp();
   this.popTop();

   // ........................................................................

   this.pushTop();
   this.headerText( "Check XISF writer" );
   try
   {
      let F = new FileFormat( ".xisf", false /*toRead*/ , true /*toWrite*/ );
      if ( F == null )
         throw '';
      if ( !F.canStoreFloat )
         this.error( "The " + F.name + " format cannot store 32-bit floating point image data" );
      if ( !F.canStoreKeywords )
         this.warning( "The " + F.name + " format does not support keywords" );
      if ( !F.canStoreProperties || !F.supportsViewProperties )
         this.warning( "The " + F.name + " format does not support image properties" );
      if ( F.isDeprecated )
         this.warning( "Using a deprecated output file format: " + F.name );

   }
   catch ( x )
   {
      this.error( "No installed file format can write .xisf files" );
   }
   this.cleanUp();
   this.popTop();

   // ........................................................................

   this.pushTop();
   this.headerText( "Check output directory" );

   let hasOutputDirectoryIssue = false;

   if ( isEmptyString( this.outputDirectory ) )
   {
      this.error( "No output directory specified" );
      hasOutputDirectoryIssue = true;
   }
   else if ( !File.directoryExists( this.outputDirectory ) )
   {
      this.error( "The specified output directory does not exist: " + this.outputDirectory );
      hasOutputDirectoryIssue = true;
   }
   else
   {
      try
      {
         let f = new File;
         let n = this.outputDirectory + "/__pixinsight_checking__";
         for ( let u = 1;; ++u )
         {
            let nu = File.appendToName( n, u.toString() );
            if ( !File.exists( nu ) )
            {
               n = nu;
               break;
            }
         }
         f.createForWriting( n );
         f.close();
         File.remove( n );
      }
      catch ( x )
      {
         this.error( "Cannot access the output directory for writing: " + this.outputDirectory );
         hasOutputDirectoryIssue = true;
      }
   }

   this.cleanUp();
   this.popTop();

   // ........................................................................

   this.pushTop();
   this.headerText( "Check bias/dark/flat/light groups" );

   // global configuration
   if ( preprocessGroups.length == 0 && postprocessGroups.length == 0 )
      this.error( "No input frames have been provided" );
   else
   {
      if ( !this.hasBiasFrames() )
         this.note( "No bias frames have been provided" );

      if ( !this.hasDarkFrames() )
         this.note( "No dark frames have been provided" );

      if ( !this.hasFlatFrames() )
         this.note( "No flat frames have been provided" );

      if ( !this.hasLightFrames( WBPPGroupingMode.PRE ) )
         this.note( "No light frames have been provided" );
   }

   this.cleanUp();
   this.popTop();

   // ........................................................................

   // Diagnostic pre-processes BIAS, DARK, FLAT and LIGHT frames
   let groupsOrder = [ ImageType.BIAS, ImageType.DARK, ImageType.FLAT, ImageType.LIGHT ];

   for ( let k = 0; k < groupsOrder.length; ++k )
   {
      for ( let i = 0; i < preprocessGroups.length; ++i )
      {
         if ( preprocessGroups[ i ].imageType != groupsOrder[ k ] )
            continue;

         this.pushTop();
         this.headerText( preprocessGroups[ i ].toString() );

         // check file existence
         for ( let j = 0; j < preprocessGroups[ i ].fileItems.length; ++j )
            if ( !File.exists( preprocessGroups[ i ].fileItems[ j ].filePath ) )
               this.error( "Nonexistent input file: " + preprocessGroups[ i ].fileItems[ j ].filePath );

         // filter name character set
         if ( !isEmptyString( preprocessGroups[ i ].filter ) )
            if ( cleanFilterName( preprocessGroups[ i ].filter ) != preprocessGroups[ i ].filter )
               this.warning( "Invalid file name characters will be replaced with dashes " +
                  "in filter name: \'" + preprocessGroups[ i ].filter + "\'" );

         // various checks for flat and light frames
         if ( ( preprocessGroups[ i ].imageType == ImageType.FLAT || preprocessGroups[ i ].imageType == ImageType.LIGHT ) &&
            !preprocessGroups[ i ].hasMaster )
         {
            let cf = this.getCalibrationGroupsFor( preprocessGroups[ i ] )
            let masterDarkExposureDifferenceIsHigh = cf.masterDark && Math.abs( cf.masterDark.exposureTime - preprocessGroups[ i ].exposureTime ) > 5;

            // check if neither bias nor dark are available
            if ( !cf.masterBias && !cf.masterDark && !cf.masterFlat )
            {
               this.note( "No Master Bias, Master Dark and Master Flat have been provided to calibrate this group. Frames will not be calibrated" );
               continue;
            }

            // check if neither bias nor dark are available
            if ( !cf.masterBias && !cf.masterDark )
               this.note( "Neither Master Bias nor Master Dark will be used to calibrate the frames" );

            // check if only dark is found but it does not contain the master bias
            if ( !cf.masterBias && cf.masterDark && preprocessGroups[ i ].optimizeDarks )
               this.warning( "Frames will be calibrated using an optimized Master Dark but no Master Bias has been found. Optimizing a Master Dark without subtracting the Master Bias could (likely) lead to improper results" );

            // check if dark exposure difference is too much
            if ( masterDarkExposureDifferenceIsHigh )
            {
               if ( preprocessGroups[ i ].optimizeMasterDark )
                  this.note( 'Frames will be calibrated using an optimized Master Dark with an exposure time of ' + cf.masterDark.exposureTime + ' sec' );
               else
                  this.warning( 'Frames will be calibrated using a Master Dark with a non-matching exposure time of ' + cf.masterDark.exposureTime + ' sec' );
            }

            // check CC parameters
            if ( preprocessGroups[ i ].imageType == ImageType.LIGHT )
            {
               let ccWarning = preprocessGroups[ i ].validateCC( cf.masterDark );
               if ( ccWarning )
                  this.warning( ccWarning );

                // Cosmetic correction check
               if ( preprocessGroups[ i ].ccData.CCTemplate && preprocessGroups[ i ].ccData.CCTemplate.length > 0 )
               {
                  let CC = ProcessInstance.fromIcon( preprocessGroups[ i ].ccData.CCTemplate );
                  if ( CC == null )
                     this.warning( "Missing Cosmetic Correction process icon: " + preprocessGroups[ i ].ccData.CCTemplate );
                  else
                  {
                     if ( !( CC instanceof CosmeticCorrection ) )
                        this.warning( "The specified process icon does not transport an instance " +
                           "of Cosmetic Correction: " + preprocessGroups[ i ].ccData.CCTemplate );
                     else
                     {
                        if ( !CC.useMasterDark && !CC.useAutoDetect && !CC.useDefectList )
                           this.warning( "The specified Cosmetic Correction instance does not define " +
                              "a valid correction operation: " + preprocessGroups[ i ].ccData.CCTemplate );
                     }
                  }
               }

               // check flats for light frames
               if ( !cf.masterFlat )
                  this.note( "No Master Flat will be used to calibrate the frames" );

            }
         }

         // ----------------------------------------

         // check rejection for bias/dark/flat groups that do not have a master (so they will be integrated)
         if ( preprocessGroups[ i ].imageType != ImageType.LIGHT && !preprocessGroups[ i ].hasMaster )
         {
            let r = preprocessGroups[ i ].rejectionIsGood( this.rejection[ preprocessGroups[ i ].imageType ] );
            if ( !r[ 0 ] ) // if not good
               this.warning( "Integration of " + preprocessGroups[ i ].toString() + ": " + r[ 1 ] ); // reason
         }

         this.cleanUp();
         this.popTop();
      }
   }

   this.pushTop();
   this.headerText( "Check registration configuration" );

   // ----------------------------------------

   if ( !engine.imageRegistration && engine.integrate )
   {
      this.warning( "You decided to integrate your light frames but registration is disabled. " +
         "Ensure that your light frames are already aligned or enable the registration to properly " +
         "align them before generating the master light frames" );
   }

   // ----------------------------------------

   if ( engine.imageRegistration && engine.reuseLastReferenceFrames )
   {
      let failed = false;
      for ( let i = 0; i < postprocessGroups.length; ++i )
         if ( postprocessGroups[ i ].__reference_frame__ == undefined )
         {
            failed = true;
            this.error( "Missing last registration reference frame for group <b>" + postprocessGroups[ i ].toString() + "</b>");
         }
         else
         {
            if ( !File.exists( postprocessGroups[ i ].__reference_frame__ ) )
            {
               failed = true;
               this.error( "Last registration reference frame <b>[" + postprocessGroups[ i ].__reference_frame__ + "]</b> not found for group <b>" + postprocessGroups[ i ].toString() + "</b>");
            }
         }
      if ( failed )
         this.error( "<b>WBPP needs to be executed first using a registration mode to assign a reference frame to all groups</b>" );
   }

   this.cleanUp();
   this.popTop();

   // ----------------------------------------

   this.pushTop();
   this.headerText( "Check local normalization configuration" );

   console.noteln("engine.localNormalization: ", engine.localNormalization)
   console.noteln("engine.reuseLastLNReferenceFrames: ", engine.reuseLastLNReferenceFrames)
   if ( engine.localNormalization && engine.reuseLastLNReferenceFrames )
   {
      let failed = false;
      for ( let i = 0; i < postprocessGroups.length; ++i )
         if ( postprocessGroups[ i ].__ln_reference_frame__ == undefined )
         {
            failed = true;
            this.error( "Missing last local normalization reference frame for group <b>" + postprocessGroups[ i ].toString() + "</b>");
         }
         else
         {
            if ( !File.exists( postprocessGroups[ i ].__ln_reference_frame__ ) )
            {
               failed = true;
               this.error( "Last local normalization reference frame <b>[" + postprocessGroups[ i ].__ln_reference_frame__ + "]</b> not found for group <b>" + postprocessGroups[ i ].toString() + "</b>");
            }
         }
      if ( failed )
         this.error( "<b>WBPP needs to be executed first enabling local normalization to assign a local normalization reference frame to all groups</b>" );
   }

   this.cleanUp();
   this.popTop();

   // ----------------------------------------
   this.pushTop();
   this.headerText( "Check weights configuration" );

   if ( engine.subframesWeightsMethod == WBPPSubframeWeightsMethod.PSFScaleSNR &&
      engine.integrate &&
      !engine.localNormalization )
   {
      this.error( "The integration of light frames using the PSF Scale SNR weighting method requires Local Normalization to be enabled" )
   }

   this.cleanUp();
   this.popTop();

   // ----------------------------------------

   for ( let i = 0; i < postprocessGroups.length; ++i )
   {
      if ( postprocessGroups[ i ].imageType != ImageType.LIGHT )
         continue;

      this.pushTop();
      this.headerText( postprocessGroups[ i ].toString() );

      let needsIntegration = postprocessGroups[ i ].associatedRGBchannel == undefined || postprocessGroups[ i ].associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB;

      if ( this.integrate &&
         needsIntegration &&
         postprocessGroups[ i ].fileItems.length < 3 )
      {
         this.error( "Only " + postprocessGroups[ i ].fileItems.length + " frames provided. Cannot integrate less than 3 light frames" );
      }

      // check rejection for light post-processing groups if integration is enabled
      if ( this.integrate &&
         needsIntegration &&
         postprocessGroups[ i ].imageType == ImageType.LIGHT )
      {
         let r = postprocessGroups[ i ].rejectionIsGood( this.rejection[ postprocessGroups[ i ].imageType ] );
         if ( !r[ 0 ] ) // if not good
            this.warning( "Integration of " + postprocessGroups[ i ].toString() + ": " + r[ 1 ] ); // reason
      }

      // check if enough files are provided when drizzle integration is active
      if ( postprocessGroups[ i ].isDrizzleEnabled() &&
         needsIntegration && postprocessGroups[ i ].fileItems.length < 15 )
      {
         this.warning( "Drizzle Integration of " + postprocessGroups[ i ].toString() + " (" + postprocessGroups[ i ].fileItems.length + "): drizzle requires more than 15 frames to produce optimal results" );
      }
      if ( postprocessGroups[ i ].isDrizzleEnabled() && postprocessGroups[ i ].fileItems.length > 40 && !postprocessGroups[ i ].drizzleFast() )
      {
         this.warning( "Drizzle Integration of " + postprocessGroups[ i ].toString() + " (" + postprocessGroups[ i ].fileItems.length + "): drizzling that amount of frames <b>may significantly impact the execution time</b>" );
      }
      this.cleanUp();
      this.popTop();
   }

   // ........................................................................

   // Check overscan
   if ( this.overscan.enabled )
   {
      this.pushTop();
      this.headerText( "Check Overscan settings" );

      if ( !this.overscan.isValid() )
         this.error( "Invalid overscan region(s) defined" );
      else if ( this.overscan.enabled && !this.overscan.hasOverscanRegions() )
         this.warning( "Overscan correction has been enabled, but no overscan regions have been defined" );

      this.cleanUp();
      this.popTop();
   }

   // ----------------------------------------

   // Reference frame
   if ( this.hasLightFrames( WBPPGroupingMode.POST ) && !engine.reuseLastReferenceFrames )
   {
      this.pushTop();
      this.headerText( "Check reference frame settings" );

      // best reference frame checks
      if ( this.imageRegistration )
      {
         if ( this.bestFrameReferenceMethod == WBPPBestReferenceMethod.MANUAL )
         {
            if ( isEmptyString( this.referenceImage ) )
               this.error( "No registration reference image has been specified." );
            else if ( !File.exists( this.referenceImage ) )
               this.error( "The specified registration reference file does not exist: " + this.referenceImage );
         }
         else
         {
            let keywords = engine.keywords.keywordsForMode( WBPPGroupingMode.POST );
            if ( keywords.length > 0 && this.bestFrameReferenceMethod == WBPPBestReferenceMethod.AUTO_SINGLE  && engine.groupingKeywordsEnabled )
            {
               let keywordsList = keywords.map( k => k.name ).join( ", " );
               let kwDesc = keywords.length > 1 ? "s " + keywordsList + "s <b>" : " <b>" + keywordsList + "</b>";
               this.warning( "Master Light frames will be grouped using the keyword" + kwDesc + " and the registration mode is <b>auto</b>: " +
                  "<u>All frames will be aligned on the same reference frame</u>. Consider selecting <b>\"auto by\"</b> registration mode " +
                  "if you want to register these groups separately by keyword" );
            }
         }
      }

      this.cleanUp();
      this.popTop();
   }

   // ----------------------------------------
   this.pushTop();
   this.headerText( "Check Autocrop configuration" );

   if ( this.autocrop && !this.generateRejectionMaps )
   {
      this.warning( "The global '<i>Generate rejection maps</i>' option will be ignored because the <b>Autocrop</b> feature has been selected, which depends on rejection maps" );
   }

   this.cleanUp();
   this.popTop();

   // ----------------------------------------

   // Pipeline script
   if ( engine.usePipelineScript && engine.pipelineScriptFile != "" )
   {
      this.headerText( "Checking pipeline script at path " + engine.pipelineScriptFile );
      let installResult = engine.operationQueue.installEventScript( engine.pipelineScriptFile );
      if ( typeof installResult != "boolean" )
         this.warning( "Check failed with error: " + installResult );
      else if ( installResult === false )
         this.warning( "Check failed" );
      else
         this.note( "Success" );
   }

   // ----------------------------------------
   // report the required space

   this.pushTop();
   this.headerText( "Check disk space availability" );

   // non zero-size keys
   let keys = Object.keys( engine.WS ).filter( key => ( engine.WS[ key ].size > 0 ) );
   // total space
   let WStot = keys.reduce( ( acc, key ) => ( acc + engine.WS[ key ].size ), 0 );
   // length of the longes non null label
   let pad = keys.reduce( ( acc, key ) => ( Math.max( acc, engine.WS[ key ].label.length ) ), 0 ) + 3;
   // generate the size report for each non null sized key
   keys.forEach( key =>
   {
      this.note( paddedLabel( engine.WS[ key ].label, pad ) + readableSize( engine.WS[ key ].size ) );
   } );

   // check if available space is enough
   if ( !hasOutputDirectoryIssue )
   {
      let availableSpace = File.getAvailableSpace( this.outputDirectory );
      let readableWS = readableSize( WStot ).trim();
      let readableAvailableSpace = readableSize( availableSpace ).trim();
      if ( WStot > availableSpace )
         this.warning( "The required working space (" + readableWS + ") is more than the available space (" + readableAvailableSpace + ")" );
      else if ( WStot >= 0.95 * availableSpace )
         this.warning( "The required working space (" + readableWS + ") is close to the available space (" + readableAvailableSpace + ")" );
      else
         this.note( "The required working space is " + readableWS + " (available " + readableAvailableSpace + ")" );
   }

   this.cleanUp();
   this.popTop();
};

// ----------------------------------------------------------------------------

/**
 * Returns true if diagnostic is empty.
 *
 * @returns
 */
StackEngine.prototype.hasDiagnosticMessages = function()
{
   return this.diagnosticMessages.length > 0;
};

// ----------------------------------------------------------------------------

/**
 * Returns true if diagnostic contains error messages.
 *
 * @returns
 */
StackEngine.prototype.hasErrorMessages = function()
{
   for ( let i = 0; i < this.diagnosticMessages.length; ++i )
      if ( this.diagnosticMessages[ i ].contains( "<b>Error</b>: " ) )
         return true;
   return false;
};

// ----------------------------------------------------------------------------

/**
 * Removes all diagnostic messages.
 *
 */
StackEngine.prototype.clearDiagnosticMessages = function()
{
   this.diagnosticMessages = new Array;
};

// ----------------------------------------------------------------------------

/**
 * Diagnostic messages dialog.
 *
 * @param {[String]} messages The messages array to be displayed.
 * @param {Boolean} cancelButton True if a Cancel button should be displayed.
 */
function DiagnosticInformationDialog( messages, cancelButton, generateScreenshots )
{
   this.__base__ = Dialog;
   this.__base__();

   let messagesCount = 0;
   let info = "<html>";
   for ( let i = 0; i < messages.length; ++i )
   {
      let line = messages[ i ].trim();
      info += line + "<br/>";
      if ( line.length > 0 )
         if ( !line.contains( "====" ) )
            messagesCount += 1;
   }
   info += "</html><beg>";

   this.infoLabel = new Label( this );
   this.infoLabel.text = format( "%d message(s):", messagesCount );
   this.infoLabel.adjustToContents();
   this.infoLabel.setMaxHeight( this.infoLabel.height );

   this.infoBoxContainer = new Control( this );

   this.infoBox = new TextBox( this );
   this.infoBox.readOnly = true;
   this.infoBox.styleSheet = "pi--PixInsightConsole { font-family: Hack, DejaVu Sans Mono, Monospace; font-size: 10pt; background: white; }";
   this.infoBox.setScaledMinSize( 800, 300 );
   this.infoBox.text = info;

   this.okButton = new PushButton( this );
   this.okButton.defaultButton = true;
   this.okButton.text = cancelButton ? "Continue" : "OK";
   this.okButton.icon = this.scaledResource( ":/icons/ok.png" );
   this.okButton.onClick = function()
   {
      this.dialog.done( StdDialogCode_Ok );
   };

   if ( cancelButton )
   {
      this.cancelButton = new PushButton( this );
      this.cancelButton.defaultButton = true;
      this.cancelButton.text = "Cancel";
      this.cancelButton.icon = this.scaledResource( ":/icons/cancel.png" );
      this.cancelButton.onClick = function()
      {
         this.dialog.done( StdDialogCode_Cancel );
      };
   }

   if ( generateScreenshots )
   {
      this.screenshotsButton = new PushButton( this );
      this.screenshotsButton.defaultButton = true;
      this.screenshotsButton.text = "Generate Screenshots";
      this.screenshotsButton.icon = this.scaledResource( ":/icons/picture-export.png" );
      this.screenshotsButton.onClick = function()
      {
         this.dialog.done( StdDialogCode_GenerateScreenshots );
      };
   }

   this.buttonsSizer = new HorizontalSizer;
   this.buttonsSizer.addStretch();
   this.buttonsSizer.add( this.okButton );
   if ( cancelButton )
   {
      this.buttonsSizer.addSpacing( 8 );
      this.buttonsSizer.add( this.cancelButton );
   }
   if ( generateScreenshots )
   {
      this.buttonsSizer.addSpacing( 8 );
      this.buttonsSizer.add( this.screenshotsButton );
   }

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.add( this.infoLabel );
   this.sizer.addSpacing( 4 );
   this.sizer.add( this.infoBox );
   this.sizer.addSpacing( 8 );
   this.sizer.add( this.buttonsSizer );

   this.ensureLayoutUpdated();
   this.adjustToContents();
   this.setMinSize();

   this.windowTitle = "Diagnostic Messages";
};

DiagnosticInformationDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------

/**
 * Shows the diagnostic messages dialog, optionally with a cancel button.
 *
 * @param {Boolean} cancelButton true if a cancel button needs to be shown
 * @returns StdDialogCode_Ok if OK button is pressed, StdDialogCode_Cancel
 *          dialog is closed or cancel button is pressed
 */
StackEngine.prototype.showDiagnosticMessages = function( cancelButton, generateScreenshots )
{
   if ( this.hasErrorMessages() )
   {
      ( new DiagnosticInformationDialog( this.diagnosticMessages, false /*cancelButton*/ , generateScreenshots ) ).execute();
      return false;
   }

   return ( new DiagnosticInformationDialog( this.diagnosticMessages, cancelButton, generateScreenshots ) ).execute();
};

StackEngine.prototype.postGroupStatus = () =>
{
};

// ----------------------------------------------------------------------------

/**
 * Constructor of the Process Logger dialog.
 *
 * @param {ProcessLogger} processLogger the instance of the process logger containing the messages
 */
function ProcessLogDialog( processLogger )
{
   this.__base__ = Dialog;
   this.__base__();

   let info = processLogger.toString();

   this.infoLabel = new Label( this );
   this.infoLabel.text = format( "WBPP steps:" );

   this.infoBox = new TextBox( this );
   this.infoBox.useRichText = true;
   this.infoBox.readOnly = true;
   this.infoBox.styleSheet = this.scaledStyleSheet( "QWidget { font-family: Hack, DejaVu Sans Mono, monospace; font-size: 10pt; color: #0066ff; padding: 4px;" );
   this.infoBox.setScaledMinSize( 800, 300 );
   this.infoBox.text = info;

   this.saveButton = new PushButton( this );
   this.saveButton.defaultButton = true;
   this.saveButton.text = "Save";
   this.saveButton.icon = this.scaledResource( ":/icons/save.png" );
   this.saveButton.onClick = () =>
   {
      // save content to a text file
      var save = new SaveFileDialog;
      save.caption = "Process Dialog Output File";
      save.initialPath = engine.outputDirectory + "/logs/ProcessLogger.txt";
      save.overwritePrompt = true;
      save.filters = [
         [ "*.txt", "*.*" ]
      ];

      if ( save.execute() )
         processLogger.writeToFile( save.fileName );
   };

   this.okButton = new PushButton( this );
   this.okButton.defaultButton = true;
   this.okButton.text = "DONE";
   this.okButton.icon = this.scaledResource( ":/icons/ok.png" );
   this.okButton.onClick = function()
   {
      this.dialog.ok();
   };

   this.buttonsSizer = new HorizontalSizer;
   this.buttonsSizer.addStretch();
   this.buttonsSizer.add( this.saveButton );
   this.buttonsSizer.addScaledSpacing( 8 );
   this.buttonsSizer.add( this.okButton );

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.add( this.infoLabel );
   this.sizer.addSpacing( 4 );
   this.sizer.add( this.infoBox );
   this.sizer.addSpacing( 8 );
   this.sizer.add( this.buttonsSizer );

   this.ensureLayoutUpdated();
   this.adjustToContents();
   this.setMinSize();

   this.windowTitle = "Smart Report";
}

ProcessLogDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------

/**
 * Shows the process logger dialog.
 */
StackEngine.prototype.showProcessLogs = function()
{
   let dialog = new ProcessLogDialog( this.processLogger );
   dialog.execute();
};

// ----------------------------------------------------------------------------

/**
 * Removes all messages from the process logger.
 */
StackEngine.prototype.cleanProcessLog = function()
{
   this.processLogger.clean();
};

// ----------------------------------------------------------------------------

/**
 * Constructor of the execution monitor Dialog.
 */
let ExecutionMonitorDialog = function( asReport, executionCancelRequestCallback, operations, onTerminationRequest )
{
   this.__base__ = Dialog;
   this.__base__();

   let
   {
      elapsedTimeToString
   } = WBPPUtils.shared();

   this.columnSizesAdjusted = false
   this.cancelRequested = false;

   let readyIcon = ":/bullets/bullet-circle-void.svg";
   let doneIcon = ":/bullets/bullet-check-green.svg";
   let doneWithWarningsIcon = ":/bullets/bullet-circle-purple.svg";
   let runningIcon = ":/bullets/bullet-triangle-blue.svg";
   let failedIcon = ":/bullets/bullet-circle-red.svg";
   let cancelIcon = ":/bullets/bullet-cross.svg";

   this.updateWithOperations = ( operations ) =>
   {
      // ensure we're not invoking this function after the interruption of a timer
      if ( this.updatingWithOperations )
         return;
      this.updatingWithOperations = true;

      let activeNode;
      this.infoTreeBox.clear();
      for ( let i = 0; i < operations.length; ++i )
      {
         let node = new TreeBoxNode;
         // operation index
         node.setText( 0, operations[ i ].name );
         // group info
         node.setText( 1, operations[ i ].groupDescription );
         // elapsed
         if ( operations[ i ].status == OperationBlockStatus.READY )
            node.setText( 2, "" );
         else if ( operations[ i ].status == OperationBlockStatus.RUNNING )
            node.setText( 2, elapsedTimeToString( operations[ i ].elapsed.value ) );
         else
            node.setText( 2, elapsedTimeToString( operations[ i ].executionTime ) );
         // status
         switch ( operations[ i ].status )
         {
            case OperationBlockStatus.READY:
               operations[ i ].updateGroupDescription();
               node.setIcon( 3, readyIcon );
               break;
            case OperationBlockStatus.RUNNING:
               activeNode = node;
               if ( this.cancelRequested )
                  node.setText( 3, "canceling..." );
               else
                  node.setText( 3, "running" );
               node.setIcon( 3, runningIcon );
               break;
            case OperationBlockStatus.DONE:
               node.setText( 3, "success" );
               if ( operations[ i ].hasWarnings )
                  node.setIcon( 3, doneWithWarningsIcon );
               else
                  node.setIcon( 3, doneIcon );
               break;
            case OperationBlockStatus.FAILED:
               node.setText( 3, "failed" );
               node.setIcon( 3, failedIcon );
               break;
            case OperationBlockStatus.CANCELED:
               node.setText( 3, "canceled" );
               node.setIcon( 3, cancelIcon );
               break;
         }
         // notes
         if ( operations[ i ].status != OperationBlockStatus.RUNNING )
         {
            node.setText( 4, operations[ i ].statusMessage );
         }

         this.infoTreeBox.add( node );

         if ( activeNode )
         {
            activeNode.setTextColor( 0, 0x0000BB );
            activeNode.setTextColor( 1, 0x0000BB );
            activeNode.setTextColor( 2, 0x0000BB );
            if ( this.cancelRequested )
               activeNode.setTextColor( 3, 0xBB0000 );
            else
               activeNode.setTextColor( 3, 0x0000BB );
         }
      }
      if ( !this.columnSizesAdjusted )
      {
         this.columnSizesAdjusted = true;
         this.infoTreeBox.adjustColumnWidthToContents( 0 );
         this.infoTreeBox.adjustColumnWidthToContents( 1 );

      }
      // elapsed always get adjusted
      this.infoTreeBox.adjustColumnWidthToContents( 2 );
      // status always get adjusted
      this.infoTreeBox.adjustColumnWidthToContents( 3 );


      // scroll follows the current active node in automation mode
      if ( activeNode )
         this.infoTreeBox.setNodeIntoView( activeNode );

      this.updatingWithOperations = false;
   };

   this.updateRunningOperation = ( operations ) =>
   {
      if ( this.updatingWithOperations )
         return;
      this.updatingWithOperations = true;

      for ( let i = 0; i < operations.length; ++i )
         if ( operations[ i ].status == OperationBlockStatus.RUNNING )
            if ( i < this.infoTreeBox.numberOfChildren )
            {
               let node = this.infoTreeBox.child( i );
               node.setText( 2, elapsedTimeToString( operations[ i ].elapsed.value ) );
               if ( this.cancelRequested )
               {
                  node.setText( 3, "canceling..." );
                  node.setTextColor( 3, 0xBB0000 );
                  this.infoTreeBox.adjustColumnWidthToContents( 3 );
               }
               break;
            }
      this.updatingWithOperations = false;
   };

   this.infoBoxContainer = new Control( this );
   this.infoBoxContainer.setVariableHeight();
   this.infoBoxContainer.setVariableWidth();

   this.infoTreeBox = new TreeBox( this );
   this.infoTreeBox.rootDecoration = false;
   this.infoTreeBox.headerVisible = true;
   this.infoTreeBox.headerSorting = false;
   this.infoTreeBox.alternateRowColor = true;
   this.infoTreeBox.numberOfColumns = 5;

   this.infoTreeBox.setHeaderText( 0, "Operation" );
   this.infoTreeBox.setHeaderText( 1, "Group involved" );
   this.infoTreeBox.setHeaderText( 2, "Elapsed" );
   this.infoTreeBox.setHeaderText( 3, "Status" );
   this.infoTreeBox.setHeaderText( 4, "Note" );

   if ( executionCancelRequestCallback )
   {
      this.cancelButton = new PushButton( this );
      this.cancelButton.defaultButton = true;
      this.cancelButton.text = "Cancel";
      this.cancelButton.icon = this.scaledResource( ":/icons/cancel.png" );
      this.cancelButton.onClick = () =>
      {
         if ( this.cancelRequested )
         {
            this.ok();
         }
         else
         {
            if ( onTerminationRequest )
               onTerminationRequest();
            executionCancelRequestCallback();
            this.cancelRequested = true;
         }
      };
   }

   if ( asReport )
   {
      this.okButton = new PushButton( this );
      this.okButton.defaultButton = true;
      this.okButton.text = "DONE";
      this.okButton.onClick = () =>
      {
         this.ok();
      };
   }

   //

   this.noteLabel = new Label( this );
   let f = this.noteLabel.font;
   f.bold = true;
   this.noteLabel.font = f;
   this.noteLabel.textAlignment = TextAlign_Left | TextAlign_VertCenter;

   //

   this.buttonsSizer = new HorizontalSizer;
   this.buttonsSizer.add( this.noteLabel );
   this.buttonsSizer.addStretch();
   if ( executionCancelRequestCallback )
   {
      this.buttonsSizer.addSpacing( 8 );
      this.buttonsSizer.add( this.cancelButton );
   }
   if ( asReport )
   {
      this.buttonsSizer.addSpacing( 8 );
      this.buttonsSizer.add( this.okButton );
   }

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.add( this.infoTreeBox );
   this.sizer.addSpacing( 8 );
   this.sizer.add( this.buttonsSizer );

   this.ensureLayoutUpdated();
   this.adjustToContents();
   this.setMinSize();

   this.windowTitle = (engine.fastMode ? "FBPP" : "WBPP") + " Execution Monitor";

   // init with operations
   if ( operations )
      this.updateWithOperations( operations );

   // -

   this.onReturn = ( retVal ) =>
   {
      this.storeDialogFrame();
      if ( retVal == 0 && executionCancelRequestCallback )
      {
         /* dialog dismissed without pressing CANCEL button */
         if ( onTerminationRequest )
            onTerminationRequest();
         executionCancelRequestCallback();
      }
   };

   this.adjustDialogFrame = () =>
   {
      // frame has already been adjusted once
      if ( ExecutionMonitorDialog.prototype.__frame__ )
         return;

      // get the rect of the last node
      let width = 680;
      let height = 300;
      if ( this.infoTreeBox.numberOfChildren > 0 )
      {
         let lastNode = this.infoTreeBox.child( this.infoTreeBox.numberOfChildren - 1 );
         let frame = this.infoTreeBox.nodeRect( lastNode );
         width = frame.x1;
         height = frame.y1;
      }

      width = Math.min( this.availableScreenRect.width - 100, width + 120 );
      height = Math.min( this.availableScreenRect.height - 200, height + 200 );

      let x = ( this.availableScreenRect.width - width ) / 2;
      let y = ( this.availableScreenRect.height - height ) / 2;
      this.position = new Point( x, y );
      this.width = width;
      this.height = height;

      this.storeDialogFrame();
   };

   this.storeDialogFrame = () =>
   {
      let frame = {
         position: new Point( this.position ),
         width: this.width,
         height: this.height
      };
      ExecutionMonitorDialog.prototype.__frame__ = frame;
   };

   this.restoreDialogFrame = () =>
   {
      let frame = ExecutionMonitorDialog.prototype.__frame__;
      if ( frame )
      {
         this.position = new Point( frame.position );
         this.width = frame.width;
         this.height = frame.height;
      }
   };

   this.restoreDialogFrame();
};

ExecutionMonitorDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------
// FILE ITEM MANAGEMENT
// ----------------------------------------------------------------------------

/**
 *  Finds the group matching the provided criteria. Virtual groups are excluded from the result.
 *
 * @param {ImageType} imageType
 * @param {String} filter
 * @param {Numeric} binning
 * @param {Numeric} exposureTime
 * @param {Object} size {width:Numeric, height:Numeric}
 * @param {Boolean} isMaster
 * @param {Boolean} isCFA
 * @param {Numeric} darkExposureTolerance
 * @param {Numeric} lightExposureTolerance
 * @param {{String: String}} keywords { key: value } keywords object
 * @param {Boolean} strictKeywordsMatching
 * @param {WBPPGroupingMode} mode
 * @returns
 */
StackEngine.prototype.findGroup = function( imageType, filter, binning, exposureTime, size, isMaster, isCFA, darkExposureTolerance, lightExposureTolerance, keywords, strictKeywordsMatching, mode )
{
   // memoization check to speed up the match
   let keywordsForMode = engine.keywords.filterKeywordsForMode( keywords, mode );
   let memoizationKey = "" + imageType + filter + binning + exposureTime + size.width + size.height + isMaster + isCFA + darkExposureTolerance + lightExposureTolerance + JSON.stringify( keywordsForMode ) + JSON.stringify( keywords ) + strictKeywordsMatching + mode;
   let memoizedGroup = engine.findGroupMemoization[ memoizationKey ];
   if ( memoizedGroup != undefined )
      return memoizedGroup;

   // NOTE: since there could be more than one group matching the same parameters but a different
   // number of keywords, we loop through all groups and we collect all groups that matches.
   // If there is more than one matching group then we do a final loop with the results and
   // we select the group that has the highest number of matching keywords
   let groupIndx = [];
   let groups = this.groupsManager.groupsForMode( mode );
   for ( let i = 0; i < groups.length; ++i )
   {
      // in case we're searching a group for a master frame then the tolerance is reduced to the maximum
      // precision in case the current group has a master too.
      // We do this because the tolerance has a meaning only when adding dark frames or matching
      // dark frame groups that contains only dark frames.
      let tolerance = isMaster ? CONST_MIN_EXPOSURE_TOLERANCE : darkExposureTolerance;
      if ( !groups[ i ].isVirtual() && groups[ i ].sameParameters( imageType, filter, binning, exposureTime, size, isCFA, tolerance, lightExposureTolerance, mode ) )
         groupIndx.push( i );
   }
   // return the group that best matches the keywords
   let bestGroupIndex = this.bestGroupMatchingKeywordsIndex( groups, exposureTime, groupIndx, keywordsForMode, strictKeywordsMatching );
   let group = bestGroupIndex == -1 ? undefined : groups[ bestGroupIndex ];
   engine.findGroupMemoization[ memoizationKey ] = group;
   return group;
};

// ----------------------------------------------------------------------------

/**
 * Implements the strategy to select the best candidate group accordingly to the
 * provided keywords. If more than one group is found that matches the same number of
 * keywords then the groups are sorted following the keywords order and the
 * first gets selected. If the same keywords are matched from more than one group
 * then the group with the closest duration is selected
 *
 * @param {[FrameGroup]} groups
 * @param {Numeric} exposureTime the group exposure time
 * @param {Numeric} groupIndx indexes of candidate groups in groups array
 * @param {{String: String}} keywords { key: value } keywords object
 * @param {Boolean} strictKeywordsMatching if true keywords values must match exactly, including
 *                                         the keywords that have no values
 * @returns
 */
StackEngine.prototype.bestGroupMatchingKeywordsIndex = function( groups, exposureTime, groupIndx, keywords, strictKeywordsMatching )
{
   // return -1 if no group matches
   if ( groupIndx.length == 0 )
      return -1;
   // find the groups with the highest matching count
   let maxMatch = 0;
   let matchingIndexes = [];
   groupIndx.forEach( i =>
   {
      let matchCount = groups[ i ].keywordsMatchCount(
         keywords,
         strictKeywordsMatching, /* strictDirectMatching */
         strictKeywordsMatching /* strictInverseMatching */
      );

      if ( matchCount > maxMatch )
      {
         maxMatch = matchCount;
         matchingIndexes = [ i ];
      }
      else if ( matchCount == maxMatch )
         matchingIndexes.push( i );
   } );

   if ( matchingIndexes.length == 0 )
      return -1; // no matching groups

   if ( matchingIndexes.length == 1 )
      return matchingIndexes[ 0 ]; // one matching group

   // initialize an array with matching groups and index
   let groupsWithIndex = groups.map( ( group, index ) => (
   {
      priority: 0,
      keywords: Object.keys( group.keywords ),
      group: group,
      index: index
   } ) );

   // update the index of a group
   engine.keywords.names().forEach( ( name, index ) =>
   {
      groupsWithIndex.forEach( ( g ) =>
      {
         if ( g.keywords.indexOf( name ) != -1 )
         {
            group.priority += 1 << ( index + 1 );
         }
      } );
   } );
   // return the group with the lowest index
   groupsWithIndex.sort( ( a, b ) => {
      if ( a.priority > b.priority )
         return -1;
      else if ( a.priority < b.priority )
         return 1;
      else
      {
         let A = Math.abs( a.group.exposureTime - exposureTime );
         let B = Math.abs( b.group.exposureTime - exposureTime );
         if ( A < B )
            return -1;
         else if ( A > B )
            return 1;
         else
            return 0;
      }
   } );

   return groupsWithIndex[ 0 ].index;
};

// ----------------------------------------------------------------------------
/**
 * Returns the list of groups linked to the given parent group ID. Optionally,
 * a specified group can be excluded from the list.
 * @param {*} parentGroupID the parent group ID that all returned groups are linked to
 * @param {*} excludedGroup the group to be excluded from the returned list
 * @returns
 */
StackEngine.prototype.getLinkedGroups = function( parentGroupID, excludedGroup )
{
   // expect to find 3 linked groups
   let groups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST );

   return groups.reduce( ( acc, group ) =>
      {
         if ( group.linkedGroupID == parentGroupID )
            if ( excludedGroup == undefined || ( group.id != excludedGroup.id ) )
               acc.push( group );
         return acc;
      },
      [] );
}

// ----------------------------------------------------------------------------

/**
 * Performs a sanity check on the file at the given filePath.
 *
 * @param {String} filePath
 * @returns
 */
StackEngine.prototype.checkFile = function( filePath )
{
   let
   {
      isEmptyString
   } = WBPPUtils.shared();

   // path must not be an empty string
   if ( isEmptyString( filePath ) )
      return {
         success: false,
         message: "Empty file path"
      };

   // file must exist
   if ( !File.exists( filePath ) )
      return {
         success: false,
         message: "File not found: " + filePath
      }

   // file must not be already added. By default the grouping mode WBPPGroupingMode = .pre is used for this check
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );
   for ( let i = 0; i < groups.length; ++i )
      for ( let j = 0; j < groups[ i ].fileItems.length; ++j )
         if ( groups[ i ].fileItems[ j ].filePath == filePath )
            return {
               success: false,
               message: "File " + filePath + " has already been added as " + StackEngine.imageTypeToString( groups[ i ].imageType ) + " frame"
            }

   return {
      success: true
   };
};

// ----------------------------------------------------------------------------
/**
 * Resets the automatic fast imaging mode groups.
 *
 * This function resets the list of the automatic fast imaging mode groups.
 * It is used to list the groups for which FastImaging has been automatically activated
 * when many frame items have been added to those groups.
 */
StackEngine.prototype.resetAutomaticFastImagingModeGroups = function()
{
   this.automaticFIgroups = [];
}

// ----------------------------------------------------------------------------

StackEngine.prototype.findFileInSearchPath = function( filePath )
{
   if ( this.fileSearchRootPath == undefined || typeof this.fileSearchRootPath != "string" || this.fileSearchRootPath.length == 0 )
      return undefined;

   let pathComponents = filePath.split("/");
   for ( let i = 0; i < pathComponents.length; i++ )
   {
      let searchPath = this.fileSearchRootPath + "/" + pathComponents.slice( i ).join("/");
      if ( File.exists( searchPath ) )
         return searchPath;
   }
   return undefined;
}

/**
 * Adds a new file with the given properties.
 *
 * @param {String} filePath
 * @param {ImageType} imageType
 * @param {String} filter
 * @param {Numeric} binning
 * @param {Numeric} exposureTime
 * @param {Object} overrideSize {width: Numeric, height: Numeric}, optional
 * @param {Boolean} overrideCFA
 * @param {*} customModes
 * @returns
 */
StackEngine.prototype.addFile = function( filePath, imageType, filter, binning, exposureTime, overrideSize, overrideCFA, customModes )
{
   let
   {
      keywords,
      smartNaming
   } = WBPPUtils.shared();

   filePath = filePath.trim();

   let checkResult = engine.checkFile( filePath );
   if ( !checkResult.success )
   {
      if ( engine.fileSearchRootPath && engine.fileSearchRootPath != "" )
      {
         // in automation mode we may have to find the file looking into the fileSarchRootPath
         // The startegy is to match the file
         filePath = engine.findFileInSearchPath( filePath )
         if ( !filePath )
         {
            return {
               success: false,
               message: "File not found"
            };
         }
      }
      else
         return checkResult;
   }

   let forcedType = imageType != undefined && imageType != ImageType.UNKNOWN;
   if ( !forcedType )
      imageType = ImageType.UNKNOWN;

   if ( filter == "?" ) // ### see Add Custom Frames dialog
      filter = undefined;
   let forcedFilter = filter != undefined;

   let forcedBinning = binning != undefined && binning > 0;
   if ( !forcedBinning )
      binning = 0;

   let forcedExposureTime = imageType == ImageType.BIAS || exposureTime != undefined && exposureTime > 0;
   if ( !forcedExposureTime || imageType == ImageType.BIAS )
      exposureTime = 0;

   // initially assume that the file is not a master file
   let isMaster = false;
   let forcedMaster = false;

   let result = keywords.readFileInfos( filePath, this.inputHints() );
   if ( !result.success )
      return result;

   // ### TODO: issue a warning if ( forcedType && result.imageType != imageType )

   if ( !forcedType )
   {
      forcedType = forcedMaster = true;
      switch ( result.imageType )
      {
      case ImageType_Bias:
         imageType = ImageType.BIAS;
         break;
      case ImageType_Dark:
         imageType = ImageType.DARK;
         break;
      case ImageType_Flat:
         imageType = ImageType.FLAT;
         break;
      case ImageType_Light:
      case ImageType_MasterLight: // light frames are never used as masters
         imageType = ImageType.LIGHT;
         break;
      case ImageType_MasterBias:
         imageType = ImageType.BIAS;
         isMaster = true;
         break;
      case ImageType_MasterDark:
         imageType = ImageType.DARK;
         isMaster = true;
         break;
      case ImageType_MasterFlat:
         imageType = ImageType.FLAT;
         isMaster = true;
         break;
      default:
      case ImageType_Unknown:
         imageType = ImageType.UNKNOWN;
         forcedType = forcedMaster = false;
         break;
      }
   }

   let solverParams = {
      observationDate: "",
      timestamp: 0,
      ra: NaN,
      dec: NaN,
      pixelSize: NaN,
      focalLength: NaN
   };

   if ( result.timeStart != null )
      if ( result.timeStart != '' )
      {
         solverParams.observationDate = result.timeStart;
         solverParams.timestamp = (new Date( result.timeStart )).getTime();
      }

   if ( result.centerRA != null )
      if ( isFinite( result.centerRA ) )
         solverParams.ra = result.centerRA;

   if ( result.centerDec != null )
      if ( isFinite( result.centerDec ) )
         solverParams.dec = result.centerDec;

   if ( result.pixelSize != null )
      if ( result.pixelSize > 0 )
         solverParams.pixelSize = result.pixelSize;

   if ( result.focalLength != null )
      if ( result.focalLength > 0 )
         solverParams.focalLength = result.focalLength;

   if ( !forcedExposureTime && imageType != ImageType.BIAS )
      if ( result.exposureTime != null )
      {
         exposureTime = result.exposureTime;
         forcedExposureTime = exposureTime > 0;
      }

   if ( !forcedBinning )
      if ( result.binning != null )
      {
         binning = result.binning;
         forcedBinning = binning > 0;
      }

   if ( !forcedFilter )
      if ( result.filterName != null )
      {
         filter = result.filterName.trim();
         forcedFilter = filter != '';
      }

   let isCFA = undefined;
   if ( overrideCFA !== undefined )
      isCFA = overrideCFA;
   else if ( result.bayerpat != null )
      isCFA = true;

   // set the item size
   let size = (overrideSize != undefined) ? overrideSize : { width: result.width, height: result.height };
   if ( !size || size.width == undefined || size.height == undefined )
      return {
         success: false,
         message: "Unable to detect image dimensions"
      };

   if ( engine.smartNamingOverride )
   {
      for ( let i = 0; i < result.keywords.length; ++i )
      {
         let keywordName = result.keywords[ i ].name;
         let value = smartNaming.getCustomKeyValueFromPath( keywordName, filePath );
         if ( value != undefined )
            result.keywords[ i ].value = value;
      }

      // append the relevant keywords that are found in the file
      let injectedKeywords = [ "IMAGETYP", "FILTER", "INSFLNAM", "XBINNING", "BINNING", "CCDBINX", "EXPTIME", "EXPOSURE", "BAYERPAT" ];
      for ( let i = 0; i < injectedKeywords.length; ++i )
      {
         let value = smartNaming.getCustomKeyValueFromPath( injectedKeywords[ i ], filePath );
         if ( value != undefined )
         {
            // a relevant keyword has found in the filePath, append it to the list
            result.keywords.push(
            {
               name: injectedKeywords[ i ],
               value: value,
               strippedValue: value
            } );
         }
      }
   }

   // initialize the keywords extracting them from the path

   let overscan = new Overscan();
   let preovscw;
   let preovsch;
   let fileKeywords = {};

   for ( let i = 0; i < result.keywords.length; ++i )
   {
      let name = result.keywords[ i ].name;
      if ( name == "HISTORY" )
         continue;

      let value = result.keywords[ i ].strippedValue.trim();
      // save the file keywords in the file item
      fileKeywords[ name ] = value;

      switch ( name )
      {
      case "IMAGETYP":
         if ( !forcedType )
            imageType = StackEngine.imageTypeFromKeyword( value );
         if ( !forcedMaster )
            isMaster = StackEngine.isMasterFromKeyword( value );
         break;
      case "FILTER":
      case "INSFLNAM":
         if ( !forcedFilter )
            filter = value;
         break;
      case "XBINNING":
      case "BINNING":
      case "CCDBINX":
         if ( !forcedBinning )
            binning = parseInt( value );
         break;
      case "EXPTIME":
      case "EXPOSURE":
         if ( !forcedExposureTime && imageType != ImageType.BIAS )
            exposureTime = parseFloat( value );
         break;
      case "BAYERPAT":
         if ( isCFA === undefined )
            isCFA = true;
         break;
      case "PREOVSCW": // the master flat pre-overscan width
         preovscw = parseFloat( value );
         break;
      case "PREOVSCH": // the master flat pre-overscan height
         preovsch = parseFloat( value );
         break;
      case "DATE-OBS": // observation time (UTC)
         if ( solverParams.observationDate == "" )
         {
            // let's add the postfix Z if needed to ensure this is interpreted as an UTC date
            let utcValue = value + ((value[value.length-1] == "Z") ? "" : "Z");
            let d = new Date( utcValue );
            let timestamp = d.getTime();
            if ( isFinite( d.getTime() ) ) // if the ISO 8601 representation is valid
            {
               solverParams.timestamp = timestamp;
               solverParams.observationDate = d.toISOString();
            }
         }
         break;
      case "RA": // right ascension of the center of the image
         if ( isNaN( solverParams.ra ) )
            solverParams.ra = parseFloat( value );
         break;
      case "DEC": // declination of the center of the image
         if ( isNaN( solverParams.dec ) )
            solverParams.dec = parseFloat( value );
         break;
      case "OBJCTRA": // right ascension of the center of the image (compatibility)
         if ( isNaN( solverParams.ra ) ) // RA takes precedence
         {
            let angle = DMSangle.FromString( value, 0, 24 );
            if ( angle != null )
               solverParams.ra = 15 * angle.GetValue();
         }
         break;
      case "OBJCTDEC": // declination of the center of the image (compatibility)
         if ( isNaN( solverParams.dec ) ) // DEC takes precedence
         {
            let angle = DMSangle.FromString( value, 0, 90 );
            if ( angle != null )
               solverParams.dec = angle.GetValue();
         }
         break;
      case "XPIXSZ": // pixel size in micron, including binning
         if ( isNaN( solverParams.pixelSize ) )
         {
            let parsedValue = parseFloat( value );
            if ( !isNaN( parsedValue ) && parsedValue > 0 )
               solverParams.pixelSize = parsedValue;
         }
         break;
      case "FOCALLEN": // focal length in mm
         if ( isNaN( solverParams.focalLength ) )
         {
            let parsedValue = parseFloat( value );
            if ( !isNaN( parsedValue ) && parsedValue > 0 )
               solverParams.focalLength = parsedValue;
         }
         break;
      default:
         // handle overscan
         overscan.updateWithKeyword( name, value );
      }
   }

   // smart naming: extract type binning, filter and duration from filePath if needed
   if ( imageType == ImageType.UNKNOWN )
   {
      imageType = smartNaming.geImageTypeFromPath( filePath );
      if ( imageType == ImageType.UNKNOWN )
      {
         this.diagnosticMessages.push( "Unable to determine frame type; assuming LIGHT frame: " + filePath );
         imageType = ImageType.LIGHT;
      }
   }

   if ( !forcedBinning && binning <= 0 )
      binning = smartNaming.getBinningFromPath( filePath );
   if ( !forcedFilter && filter == undefined )
      filter = smartNaming.getFilterFromPath( filePath ) || "NoFilter";
   if ( !forcedExposureTime && imageType !== ImageType.BIAS && exposureTime <= 0 )
      exposureTime = smartNaming.getExposureTimeFromPath( filePath );

   // if master was not found then check if file is a master from the filePath
   if ( !forcedMaster )
      if ( !isMaster )
         if ( imageType != ImageType.LIGHT )
         {
            let searchPath = this.detectMasterIncludingFullPath ? filePath : File.extractName( filePath );
            isMaster = smartNaming.isMasterFromPath( searchPath );
         }

   // light frames are never used as masters
   if ( imageType == ImageType.LIGHT )
      isMaster = false;

   // Reject any master file which does not have a .xisf extension
   if ( isMaster && File.extractExtension( filePath ).toLowerCase() != ".xisf" )
      return {
         success: false,
         message: ("Master file " + filePath + " rejected: master calibration files must be in XISF format.")
      };

   if ( isCFA === undefined )
      isCFA = false;

   // set the custom matching sizes for the master flat
   let matchingSizes = {};
   if ( isMaster && imageType == ImageType.FLAT && preovscw != undefined && preovsch != undefined )
      matchingSizes[ WBPPGroupingMode.PRE ] = {
         width: preovscw,
         height: preovsch
      };

   let item = new FileItem( filePath,
                            imageType,
                            filter,
                            binning,
                            exposureTime,
                            fileKeywords,
                            size,
                            matchingSizes,
                            isCFA,
                            isMaster,
                            undefined, /* item keywords are not defined yet */
                            solverParams,
                            overscan );

   // once created we can update the keywords
   item.updateKeywords();

   this.groupsManager.addFileItem( item, undefined /* custom keywords */ , customModes );

   return {
      success: true
   };
};

// ----------------------------------------------------------------------------

/**
 * Adds a bias Frame.
 *
 * @param {String} filePath
 * @returns
 */
StackEngine.prototype.addBiasFrame = function( filePath )
{
   return this.addFile( filePath, ImageType.BIAS );
};

// ----------------------------------------------------------------------------

/**
 * Adds a dark frame.
 *
 * @param {String} filePath
 * @returns
 */
StackEngine.prototype.addDarkFrame = function( filePath )
{
   return this.addFile( filePath, ImageType.DARK );
};

// ----------------------------------------------------------------------------

/**
 * Adds a flat frame.
 *
 * @param {String} filePath
 * @returns
 */
StackEngine.prototype.addFlatFrame = function( filePath )
{
   return this.addFile( filePath, ImageType.FLAT );
};

// ----------------------------------------------------------------------------

/**
 * Adds a light frame.
 *
 * @param {String} filePath
 * @returns
 */
StackEngine.prototype.addLightFrame = function( filePath )
{
   return this.addFile( filePath, ImageType.LIGHT );
};

// ----------------------------------------------------------------------------
/**
 * Rebuilds groups and regenerate the execution pipeline.
 *
 */
StackEngine.prototype.rebuild = function()
{
   // engine rebuild is always performed with in-memory data
   this.reconstructGroups( true /* from cache */ );
   this.buildExecutionPipeline();
};

/**
 * Reconstruction is performed by scanning all files in pre-processing groups
 * and re-add them one by one. Before reconstructing, all properties for each
 * group is saved and after the reconstruction the properties are restored
 * if groups with same ID have been created.
 *
 * @param {*} formCache true if file item properties have to be read from memory
 *                      instead of being read from disk.
 */
StackEngine.prototype.reconstructGroups = function( formCache )
{
   // reset the memoization cache
   engine.findGroupMemoization = {};

   // clean up null values
   this.removePurgedElements();

   // save the current group properties to be restored for the unchanged groups
   this.groupsManager.cacheGroupsProperties();

   // get all groups in pre-processing state, these contain all files added to WBPP
   let fileItems = {};
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   // flatten files, ensure uniqueness
   let filePaths = [];
   for ( let i = 0; i < groups.length; ++i )
      for ( let j = 0; j < groups[ i ].fileItems.length; ++j )
      {
         let fileItem = groups[ i ].fileItems[ j ];
         // ensure uniqueness
         if ( fileItems[ fileItem.filePath ] == undefined )
         {
            // first add the masters then add the other files
            if ( fileItem.isMaster )
               filePaths.unshift( fileItem.filePath );
            else
               filePaths.push( fileItem.filePath );
            fileItems[ fileItem.filePath ] = fileItem;
         }
      }

   // remove all groups
   this.groupsManager.clear();

   // re-add files one by one
   for ( let i = 0; i < filePaths.length; ++i )
   {
      let filePath = filePaths[ i ];
      let fileItem = fileItems[ filePath ];
      if ( fileItem )
      {
         if ( formCache )
         {
            // update the keywords in case they have changed
            fileItem.updateKeywords();
            this.groupsManager.addFileItem( fileItem, undefined /* custom keywords */ , [ WBPPGroupingMode.PRE ] /* customModes */ );
         }
         else
         {
            this.addFile(
               fileItem.filePath,
               fileItem.imageType,
               fileItem.filter,
               fileItem.binning,
               fileItem.exposureTime,
               fileItem.size,
               undefined, /* override CFA */
               [ WBPPGroupingMode.PRE ] /* customModes */
            );
         }
      }
   }

   // ready to restore the group properties
   this.groupsManager.restoreGroupsPropertiesFromCache( WBPPGroupingMode.PRE );

   // reconstruct post process groups after PRE has been completed
   this.reconstructPostProcessGroups();

   // ready to restore the group properties
   this.groupsManager.restoreGroupsPropertiesFromCache( WBPPGroupingMode.POST );

   // we need to know which frames will belong to post-calibration groups
   // integrated with fast integration data. This
   this.updateFastIntegrationData();

   // sort files by name
   this.groupsManager.groups.forEach( g =>
   {
      if ( g.mode == WBPPGroupingMode.PRE )
      {
         // sort but keep the master file in first position
         let master;
         if ( g.hasMaster )
         {
            master = g.fileItems[ 0 ];
            g.fileItems.shift();
         }
         g.fileItems.sort( ( a, b ) =>
         {
            return a.filePath.localeCompare( b.filePath );
         } )
         if ( master )
         {
            g.fileItems.unshift( master );
         }
      }
   } );

   // aux: returns the list of groups with the given type and mode, adding the
   // counter property
   let _getGroups = ( type, mode ) =>
   {
      // get the list of sorted groups
      let groups = engine.getSortedGroupsOfType( type, mode );
      // add the counter property (GUI purposes)
      groups.forEach( ( g, i ) =>
      {
         g.__counter__ = i + 1;
      } );
      return groups;
   }

   // sorted groups
   let bias = _getGroups( ImageType.BIAS, WBPPGroupingMode.PRE );
   let dark = _getGroups( ImageType.DARK, WBPPGroupingMode.PRE );
   let flat = _getGroups( ImageType.FLAT, WBPPGroupingMode.PRE );
   let light_pre = _getGroups( ImageType.LIGHT, WBPPGroupingMode.PRE );
   let light_post = _getGroups( ImageType.LIGHT, WBPPGroupingMode.POST );
   // clear and re-add groups in sorted order
   this.groupsManager.clear();
   bias.forEach( g => this.groupsManager.groups.push( g ) );
   dark.forEach( g => this.groupsManager.groups.push( g ) );
   flat.forEach( g => this.groupsManager.groups.push( g ) );
   light_pre.forEach( g => this.groupsManager.groups.push( g ) );
   light_post.forEach( g => this.groupsManager.groups.push( g ) );
}; /** StackEngine.prototype.reconstructGroups */

/**
 * Reconstruction is performed by scanning all files in pre-processing groups
 * and re-add them one by one. Before reconstructing, all properties for each
 * group is saved and after the reconstruction the properties are restored
 * if groups with same ID have been created.
 */
StackEngine.prototype.reconstructPostProcessGroups = function()
{
   // clean up null values
   this.removePurgedElements();

   // get all groups in pre-processing state, these contain all files added to WBPP
   let fileItems = [];
   let preGroups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   // flatten files and store the overridden CFA property
   for ( let i = 0; i < preGroups.length; ++i )
      if ( preGroups[ i ].imageType == ImageType.LIGHT )
      {
         for ( let j = 0; j < preGroups[ i ].fileItems.length; ++j )
            fileItems.push(
            {
               fileItem: preGroups[ i ].fileItems[ j ],
               isCFA: preGroups[ i ].isCFA
            } );
      }

   // purge post-process groups
   let postGroups = this.groupsManager.groupsForMode( WBPPGroupingMode.POST );
   postGroups.forEach( g =>
   {
      g.__purged__ = true;
   } );
   this.removePurgedElements();

   // re-add file items one by one.
   for ( let i = 0; i < fileItems.length; ++i )
   {
      let fileItem = fileItems[ i ].fileItem;
      if ( fileItem )
      {
         // override the POST matching size depending on the overscan settings
         if ( this.overscan.enabled )
         {
            fileItem.matchingSizes[ WBPPGroupingMode.POST ] = {
               width: engine.overscan.imageRect.x1 - engine.overscan.imageRect.x0,
               height: engine.overscan.imageRect.y1 - engine.overscan.imageRect.y0
            }
         }
         else
            fileItem.matchingSizes[ WBPPGroupingMode.POST ] = fileItem.matchingSizes[ WBPPGroupingMode.PRE ];

         let isCFA = fileItems[ i ].isCFA;
         this.groupsManager.addFileItem(
            fileItem,
            undefined /* custom keywords */ ,
            [ WBPPGroupingMode.POST ],
            isCFA /* custom CFA */
         );
      }
   }

   // retrieve the groups again and insert the associated groups for the separated RGB channels
   let updatedPostGroups = this.groupsManager.groups.reduce( ( acc, group ) =>
   {
      // let preprocessing groups unchanged
      if ( group.mode == WBPPGroupingMode.PRE )
      {
         acc.push( group );
         return acc;
      }

      // POST processing group: if the group is CFA then it remains if the debayer mode is combined RGB channels or both
      if ( !group.isCFA || this.debayerOutputMethod == WBPPDebayerOutputMode.COMBINED )
      {
         acc.push( group );
         return acc;
      }

      // hide and deactivate the post RGB group if only the separated RGB channels are generated

      if ( this.debayerOutputMethod == WBPPDebayerOutputMode.SEPARATED )
      {
         group.isHidden = true;
         group.isActive = false;
      }
      acc.push( group );
      let associatedChannels = [];
      if ( engine.debayerActiveChannelR )
         associatedChannels.push( WBPPAssociatedChannel.R );
      if ( engine.debayerActiveChannelG )
         associatedChannels.push( WBPPAssociatedChannel.G );
      if ( engine.debayerActiveChannelB )
         associatedChannels.push( WBPPAssociatedChannel.B );

      associatedChannels.forEach( associatedRGBchannel =>
      {
         let associatedGroup = new FrameGroup(
            group.imageType,
            group.filter,
            group.binning,
            group.exposureTime,
            group.size,
            false,      // isCFA: group is managed as MONO
            null,       // firstItem: group is initially empty
            false,      // hasMaster
            group.keywords,
            group.mode,
            associatedRGBchannel,
            group.id,  // the parent linked group
            undefined, // drizzle data
            undefined, // fast integration data
            undefined  // cosmetic correction data
         );
         associatedGroup.exposureTimes = group.exposureTimes.splice();
         // associate the same files, the function "activeFrames()" will return the associated file paths
         // for the given RGB channel
         associatedGroup.fileItems = group.fileItems;
         acc.push( associatedGroup );
      } );

      // recombination is performed only if all channels are active
      if ( engine.recombineRGB && associatedChannels.length == 3 && engine.integrate )
      {
         let associatedGroup = new FrameGroup(
            group.imageType,
            group.filter,
            group.binning,
            group.exposureTime,
            group.size,
            true, /* isCFA: group is managed as CFA */
            null, /* firstItem: group is initially empty */
            false, /* hasMaster */
            group.keywords,
            group.mode,
            WBPPAssociatedChannel.COMBINED_RGB,
            group.id /* the parent linked group */
         );
         associatedGroup.isHidden = false;
         associatedGroup.exposureTimes = group.exposureTimes.splice();
         acc.push( associatedGroup );
      }

      return acc;
   }, [] );

   this.groupsManager.groups = updatedPostGroups;
};

// ----------------------------------------------------------------------------

/**
 * Propagates the information about fast integration into the file items.
 * To schedule an optimized pipeline we need to know if a file item
 * belongs to a post-calibration group integrated with fast integration.
 */
StackEngine.prototype.updateFastIntegrationData = function()
{
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.POST );
   for ( let i = 0; i < groups.length; ++i )
   {
      let fileItems = groups[i].fileItems;
      let fi = groups[i].fastIntegrationData.enabled;
      for ( let j = 0; j < fileItems.length; ++j )
      {
         let value = fileItems[j].__fastIntegration;
         fileItems[j].__fastIntegration = (value || true) & fi;
      }
   }
};

// ----------------------------------------------------------------------------

/**
 * Apply the provided CFA settings to the pre-processing groups.
 *
 * @param {ImageType} imageType
 * @param {Boolean} isCFA
 * @param {Debayer.prototype} CFAPattern
 * @param {Debayer.prototype} debayerMethod
 */
StackEngine.prototype.applyCFASettings = function( imageType, isCFA, CFAPattern, debayerMethod )
{
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   for ( let i = 0; i < groups.length; ++i )
      if ( groups[ i ].imageType == imageType )
      {
         groups[ i ].isCFA = isCFA;
         groups[ i ].CFAPattern = CFAPattern;
         groups[ i ].debayerMethod = debayerMethod;
      }
};

// ----------------------------------------------------------------------------

/**
 * Apply the provided output pedestal value to the pre-processing light frame groups.
 *
 * @param {Float} limit the output pedestal limit
 */
StackEngine.prototype.applyOutputPedestalLimit = function( mode, pedestal, limit )
{
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   for ( let i = 0; i < groups.length; ++i )
      if ( groups[ i ].imageType == ImageType.LIGHT )
      {
         groups[ i ].lightOutputPedestalMode = mode;
         groups[ i ].lightOutputPedestal = pedestal;
         groups[ i ].lightOutputPedestalLimit = limit;
      }
};

// ----------------------------------------------------------------------------

/**
 * Apply the provided Cosmetic Correction template icon to all pre-processing light frame groups.
 *
 * @param {String} templateIconName CosmeticCorrection template icon
 */
StackEngine.prototype.applyCCData = function( ccData )
{
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   for ( let i = 0; i < groups.length; ++i )
      if ( groups[ i ].imageType == ImageType.LIGHT )
         groups[ i ].setCcData( ccData );
};

// ----------------------------------------------------------------------------

/**
 * Apply the drizzle configuration of the given group to all post-calibration groups
 *
 * @param {String} referenceGroup The group containing the drizzle data to be
 *                                applied to all groups
 */
StackEngine.prototype.applyDrizzleConfiguration = function( referenceGroup )
{
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.POST );

   for ( let i = 0; i < groups.length; ++i )
   {
      // skip virtual groups since they are
      groups[ i ].setDrizzleData( referenceGroup.drizzleData )
   }
};

// ----------------------------------------------------------------------------

/**
 * Apply the fast integration configuration of the given group to all post-calibration groups
 *
 * @param {String} referenceGroup The group containing the fast integration data to be
 *                                applied to all groups
 */
StackEngine.prototype.applyFastIntegrationConfiguration = function( referenceGroup )
{
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.POST );

   for ( let i = 0; i < groups.length; ++i )
   {
      // skip virtual groups
      if ( !groups[i].isVirtual() )
         groups[ i ].setFastIntegrationData( referenceGroup.fastIntegrationData )
   }
};

// ----------------------------------------------------------------------------

/**
 * Returns the FITS coordinate convention hints given the current global option status
 *
 * @return {*}
 */
StackEngine.prototype.coordinateConventionHints = function()
{
   if ( this.fitsCoordinateConvention != 0 )
      return (this.fitsCoordinateConvention == 1) ? " up-bottom" : " bottom-up";
   return "";
};

/**
 * Returns the input hits to keep the white balance convention if the global setting is set
 * to do so.
 */
StackEngine.prototype.whiteBalanceHints = function()
{
   if ( this.preserveWhiteBalance )
      return " camera-white-balance";
   return "";
}

/**
 * Provides default input file hints.
 *
 * @returns
 */
StackEngine.prototype.inputHints = function()
{
   // Input format hints:
   // * XISF: fits-keywords normalize only-first-image
   // * FITS: only-first-image signed-is-physical use-roworder-keywords up-bottom|bottom-up
   // * DSLR_RAW: raw cfa
   return "fits-keywords normalize only-first-image raw cfa use-roworder-keywords signed-is-physical" + this.coordinateConventionHints() + this.whiteBalanceHints();
};

// ----------------------------------------------------------------------------

/**
 * Provides default output file hints.
 *
 * @returns
 */
StackEngine.prototype.outputHints = function()
{
   // Output format hints:
   // * XISF: properties fits-keywords no-compress-data block-alignment 4096 max-inline-block-size 3072 no-embedded-data no-resolution
   // * FITS: up-bottom|bottom-up

   return "properties fits-keywords no-compress-data block-alignment 4096 max-inline-block-size 3072 no-embedded-data no-resolution " +
      this.coordinateConventionHints();
};

// ----------------------------------------------------------------------------

/**
 * Opens the image at filePath and returns the generated Window object.
 *
 * @param {String} filePath
 * @returns
 */
StackEngine.prototype.readImage = function( filePath )
{
   let
   {
      isEmptyString
   } = WBPPUtils.shared();

   let ext = File.extractExtension( filePath );
   let F = new FileFormat( ext, true /*toRead*/ , false /*toWrite*/ );
   if ( F.isNull )
      throw new Error( "No installed file format can read \'" + ext + "\' files." ); // shouldn't happen

   let f = new FileFormatInstance( F );
   if ( f.isNull )
      throw new Error( "Unable to instantiate file format: " + F.name );

   let d = f.open( filePath, this.inputHints() );
   if ( d.length < 1 )
      throw new Error( "Unable to open file: " + filePath );
   if ( d.length > 1 )
      throw new Error( "Multi-image files are not supported by this script: " + filePath );

   let window = new ImageWindow( 1, 1, 1, /*numberOfChannels*/ 32, /*bitsPerSample*/ true /*floatSample*/ );

   let view = window.mainView;
   view.beginProcess( UndoFlag_NoSwapFile );

   if ( !f.readImage( view.image ) )
      throw new Error( "Unable to read file: " + filePath );

   if ( F.canStoreImageProperties )
      if ( F.supportsViewProperties )
      {
         let info = view.importProperties( f );
         if ( !isEmptyString( info ) )
            console.criticalln( "<end><cbr>*** Error reading image properties:<br>" + info );
      }

   if ( F.canStoreKeywords )
      window.keywords = f.keywords;

   view.endProcess();

   f.close();

   return window;
};

// ----------------------------------------------------------------------------

/**
 * Writes an image to file.
 *
 * @param {String} filePath
 * @param {[ImageWindow]} imageWindows an array of image windows
 * @param {[String]} identifiers an optional array of identifiers
 * @param {[String]} imageHistory an optional string of XML source code representation of the image history
 */
StackEngine.prototype.writeImage = function( filePath, imageWindows, identifiers, imageHistory )
{
   if ( imageWindows.length == 0 )
   {
      console.warning( "*** StackEngine.writeImage(): empty image array provided, no image will be saved at path ", filePath );
      return;
   }
   if ( imageWindows.length != identifiers.length )
   {
      console.warning( "*** StackEngine.writeImage(): the number of images and identifiers must match, no image will be saved at path ", filePath );
      return;
   }

   let F = new FileFormat( ".xisf", false /*toRead*/ , true /*toWrite*/ );
   if ( F.isNull )
      throw new Error( "No installed file format can write " + ".xisf" + " files." ); // shouldn't happen

   let f = new FileFormatInstance( F );
   if ( f.isNull )
      throw new Error( "Unable to instantiate file format: " + F.name );

   if ( !f.create( filePath, this.outputHints() ) )
      throw new Error( "Error creating output file: " + filePath );

   let filename_id = imageIdFromFileName( File.extractName( filePath ) );

   let d = new ImageDescription;
   d.bitsPerSample = 32;
   d.ieeefpSampleFormat = true;

   for ( let i = 0; i < imageWindows.length; ++i )
      if ( imageWindows[ i ] != null )
      {
         d.imageType = imageWindows[ i ].imageType;
         if ( !f.setOptions( d ) )
            throw new Error( "Unable to set output file options: " + filePath );

         f.keywords = imageWindows[ i ].keywords;

         imageWindows[ i ].mainView.exportProperties( f );

         let history = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ProcessingHistory version=\"1.0\">";
         let pc = imageWindows[ i ].mainView.initialProcessing;
         for ( let i = 0; i < pc.length; i++)
            history += pc.at( i ).toSource( "XPSM 1.0" );
         history += "</ProcessingHistory>";
         f.writeImageProperty( "PixInsight:ProcessingHistory", history );

         f.setImageId( identifiers[ i ].replace( filename_id, "" ) );

         if ( !f.writeImage( imageWindows[ i ].mainView.image ) )
            throw new Error( "Error writing output file: " + filePath );
      }

   f.close();
};

// ----------------------------------------------------------------------------

StackEngine.rejectionMethods = [
{
   name: "Percentile Clipping",
   rejection: ImageIntegration.prototype.PercentileClip
},
{
   name: "Winsorized Sigma Clipping",
   rejection: ImageIntegration.prototype.WinsorizedSigmaClip
},
{
   name: "Linear Fit Clipping",
   rejection: ImageIntegration.prototype.LinearFit
},
{
   name: "Generalized Extreme Studentized Deviate",
   rejection: ImageIntegration.prototype.Rejection_ESD
},
{
   name: "Robust Chauvenet Rejection",
   rejection: ImageIntegration.prototype.Rejection_RCR
},
{
   name: "Auto",
   rejection: ImageIntegration.prototype.auto
} ];

// ----------------------------------------------------------------------------

StackEngine.prototype.rejectionNames = function()
{
   return StackEngine.rejectionMethods.map( item => item.name );
};

// ----------------------------------------------------------------------------

StackEngine.prototype.rejectionFromIndex = function( index )
{
   return StackEngine.rejectionMethods[ index ].rejection;
};

// ----------------------------------------------------------------------------

StackEngine.prototype.rejectionName = function( rejection )
{
   for ( let i = 0; i < StackEngine.rejectionMethods.length; ++i )
      if ( StackEngine.rejectionMethods[ i ].rejection === rejection )
         return StackEngine.rejectionMethods[ i ].name;
   return StackEngine.rejectionMethods[ StackEngine.rejectionMethods.length - 1 ].name;
};

// ----------------------------------------------------------------------------

StackEngine.prototype.rejectionIndex = function( rejection )
{
   for ( let i = 0; i < StackEngine.rejectionMethods.length; ++i )
      if ( StackEngine.rejectionMethods[ i ].rejection === rejection )
         return i;
   return StackEngine.rejectionMethods.length - 1;
};

// ----------------------------------------------------------------------------

/*
 * min/max values ot FWHM, eccentricity and SNR are computed.
 * These values will be used to compute the final weights of the light images.
 */
StackEngine.prototype.getMinMaxDescriptorsValues = function( imagesDescriptors )
{
   let FWHM = imagesDescriptors.map( descriptor => descriptor.FWHM );
   let eccentricity = imagesDescriptors.map( descriptor => descriptor.eccentricity );
   let SNR = imagesDescriptors.map( descriptor => descriptor.SNR );
   let noise = imagesDescriptors.map( descriptor => descriptor.noise );
   let stars = imagesDescriptors.map( descriptor => descriptor.numberOfStars );
   let PSFSignalWeight = imagesDescriptors.map( descriptor => descriptor.PSFSignalWeight );
   let PSFSNR = imagesDescriptors.map( descriptor => descriptor.PSFSNR );

   let FWHM_min = Math.min.apply( null, FWHM );
   let FWHM_max = Math.max.apply( null, FWHM );
   let eccentricity_min = Math.min.apply( null, eccentricity );
   let eccentricity_max = Math.max.apply( null, eccentricity );
   let SNR_min = Math.min.apply( null, SNR );
   let SNR_max = Math.max.apply( null, SNR );
   let noise_min = Math.min.apply( null, noise );
   let noise_max = Math.max.apply( null, noise );
   let stars_min = Math.min.apply( null, stars );
   let stars_max = Math.max.apply( null, stars );
   let PSFSignalWeight_min = Math.min.apply( null, PSFSignalWeight );
   let PSFSignalWeight_max = Math.max.apply( null, PSFSignalWeight );
   let PSFSNR_min = Math.min.apply( null, PSFSNR );
   let PSFSNR_max = Math.max.apply( null, PSFSNR );

   return {
      FWHM_min: FWHM_min,
      FWHM_max: FWHM_max,
      eccentricity_min: eccentricity_min,
      eccentricity_max: eccentricity_max,
      SNR_min: SNR_min,
      SNR_max: SNR_max,
      noise_min: noise_min,
      noise_max: noise_max,
      stars_min: stars_min,
      stars_max: stars_max,
      PSFSignalWeight_min: PSFSignalWeight_min,
      PSFSignalWeight_max: PSFSignalWeight_max,
      PSFSNR_min: PSFSNR_min,
      PSFSNR_max: PSFSNR_max
   };
};

// ----------------------------------------------------------------------------

/*
 * Find the best frame as reference for registration across all images
 */
StackEngine.prototype.findRegistrationReferenceFileItem = function( groups )
{
   // extract descriptors for all active frames (double check that a descriptor exists)
   let fileItems = groups.reduce( ( acc, g ) =>
      {
         return acc.concat( g.activeFrames() );
      },
      [] ).filter( fileItem => fileItem.descriptor != undefined )

   // find the lowest binning of frames
   let binningForRegistration = fileItems.reduce( ( acc, fileItem ) =>
   {
      return Math.min( acc, fileItem.binning );
   }, 256 );

   // extract only the frames with the lowest binning
   fileItems = fileItems.filter( item => ( item.binning == binningForRegistration ) );

   // get the best frame
   let descriptors = fileItems.map( fileItem => fileItem.descriptor );
   let maxVal = 0;
   let bestFrame = undefined;

   // compute the absolute mix/max ranges
   let flatDescriptorsMinMax = this.getMinMaxDescriptorsValues( descriptors );

   // compute all images weight and find the best
   for ( let i = 0; i < fileItems.length; ++i )
   {
      // select the frame with the highest number of stars
      let weight = this.computeWeightForLight(
         fileItems[ i ].descriptor,
         flatDescriptorsMinMax,
         0, /*FWHMWeight*/
         0, /*eccentricityWeight*/
         0, /*SNRWeight*/
         1, /*starsWeight*/
         0, /*PSF Signal Weight*/
         0, /*PSF SNR Weight*/
         1 /*pedestal*/
      );
      if ( weight && isFinite( weight ) )
         if ( weight > maxVal )
         {
            maxVal = weight;
            bestFrame = fileItems[ i ];
         }
   }

   // check in case light images are all the same
   return bestFrame;
};

// ----------------------------------------------------------------------------

StackEngine.prototype.computeWeightForLight = function( descriptor, descriptorMinMax, FWHMWeight, eccentricityWeight, SNRWeight, starsWeight, PSFSignalWeight, PSFSNRWeight, pedestal, normalizationFactor, printToConsole )
{
   if ( descriptor == undefined )
      return undefined;

   normalizationFactor = normalizationFactor || 1;
   printToConsole = printToConsole || false;

   let
   {
      paddedStringNumber
   } = WBPPUtils.shared();

   let FWHM = descriptor.FWHM;
   let FWHM_min = descriptorMinMax.FWHM_min;
   let FWHM_max = descriptorMinMax.FWHM_max;
   let eccentricity = descriptor.eccentricity;
   let eccentricity_min = descriptorMinMax.eccentricity_min;
   let eccentricity_max = descriptorMinMax.eccentricity_max;
   let SNR = descriptor.SNR;
   let SNR_min = descriptorMinMax.SNR_min;
   let SNR_max = descriptorMinMax.SNR_max;
   // let noise = descriptor.noise;
   // let noise_min = descriptorMinMax.noise_min;
   // let noise_max = descriptorMinMax.noise_max;
   let stars = descriptor.numberOfStars;
   let stars_min = descriptorMinMax.stars_min;
   let stars_max = descriptorMinMax.stars_max;
   let PSFSignal = descriptor.PSFSignalWeight;
   // let PSFSignal_min = descriptorMinMax.PSFSignalWeight_min;
   let PSFSignal_max = descriptorMinMax.PSFSignalWeight_max;
   let PSFSNR = descriptor.PSFSNR;
   // let PSFSNR_min = descriptorMinMax.PSFSNR_min;
   let PSFSNR_max = descriptorMinMax.PSFSNR_max;

   let a = FWHM_max - FWHM_min == 0 ? 0 : 1 - ( FWHM - FWHM_min ) / ( FWHM_max - FWHM_min );
   let b = eccentricity_max - eccentricity_min == 0 ? 0 : 1 - ( eccentricity - eccentricity_min ) / ( eccentricity_max - eccentricity_min );
   let c = SNR_max - SNR_min == 0 ? 0 : ( SNR - SNR_min ) / ( SNR_max - SNR_min );
   let s = stars_max - stars_min == 0 ? 0 : ( stars - stars_min ) / ( stars_max - stars_min );
   let ps = PSFSignal_max == 0 ? 0 : PSFSignal / PSFSignal_max;
   let psfsnr = PSFSNR_max == 0 ? 0 : PSFSNR / PSFSNR_max;
   let score = pedestal + a * FWHMWeight + b * eccentricityWeight + c * SNRWeight + s * starsWeight + ps * PSFSignalWeight + psfsnr * PSFSNRWeight;
   let weight = score / normalizationFactor;
   if ( printToConsole )
   {
      console.noteln( 'Weights of image: ', descriptor.filePath );
      console.noteln( "--------------------------------" );
      console.noteln( 'FWHM         : ', isFinite( a ) ? paddedStringNumber( format( "%.02f %%", a * 100 ), 4 ) + " [ " + paddedStringNumber( format( "%i", FWHMWeight ), 3 ) + " ] " : '-' );
      console.noteln( 'eccentricity : ', isFinite( b ) ? paddedStringNumber( format( "%.02f %%", b * 100 ), 4 ) + " [ " + paddedStringNumber( format( "%i", eccentricityWeight ), 3 ) + " ] " : '-' );
      console.noteln( 'SNR          : ', isFinite( c ) ? paddedStringNumber( format( "%.02f %%", c * 100 ), 4 ) + " [ " + paddedStringNumber( format( "%i", SNRWeight ), 3 ) + " ] " : '-' );
      console.noteln( 'stars        : ', isFinite( s ) ? paddedStringNumber( format( "%.02f %%", s * 100 ), 4 ) + " [ " + paddedStringNumber( format( "%i", starsWeight ), 3 ) + " ] " : '-' );
      console.noteln( 'PSF Signal   : ', isFinite( ps ) ? paddedStringNumber( format( "%.02f %%", ps * 100 ), 4 ) + " [ " + paddedStringNumber( format( "%i", PSFSignalWeight ), 3 ) + " ] " : '-' );
      console.noteln( 'PSF SNR      : ', isFinite( psfsnr ) ? paddedStringNumber( format( "%.02f %%", psfsnr * 100 ), 4 ) + " [ " + paddedStringNumber( format( "%i", PSFSNRWeight ), 3 ) + " ] " : '-' );
      console.noteln( 'Pedestal     : ', paddedStringNumber( format( "%i", pedestal ), 4 ) );
      console.noteln();
      console.noteln( 'Score        : ', isFinite( score ) ? paddedStringNumber( format( "%.03f", score ), 4 ) : '-' );
      console.noteln( 'Image weight : ', isFinite( weight ) ? paddedStringNumber( format( "%.03f", weight ), 4 ) : '-' );
      console.noteln( "--------------------------------" );
      console.flush();
   }
   return weight;
};

// -------------------------------------------------------- --------------------

/**
 * Writes the weights for each provided groups,
 *
 * @param {*} groups
 */
StackEngine.prototype.writeWeightsWithDescriptors = function( groups )
{
   let
   {
      existingDirectory,
      getLastModifiedDate
   } = WBPPUtils.shared();

   // compute weights for all groups
   for ( let i = 0; i < groups.length; ++i )
   {
      let activeFrames = groups[ i ].activeFrames();
      let virtualGroup = groups[ i ].isVirtual();

      for ( let j = 0; j < activeFrames.length; ++j )
      {
         processEvents();
         gc();

         let descriptor = activeFrames[ j ].descriptor;
         if ( descriptor && descriptor.formulaWeight != undefined && descriptor.imageWeight != undefined )
         {
            // avoid to overwrite unprocessed files (potentially original data)
            let targetFname;
            if ( virtualGroup || activeFrames[ j ].isProcessed() )
            {
               targetFname = activeFrames[ j ].current;
            }
            else
            {
               let subfolder = groups[ i ].folderName();
               let measuredDirectory = existingDirectory( this.outputDirectory + "/measured/" + subfolder );
               targetFname = measuredDirectory + "/" + activeFrames[ j ].currentFileName();
            }

            // check if this weight has already been cached for the source and target frames
            let key = activeFrames[ j ].current + "_" + targetFname + "_" + JSON.stringify( descriptor );
            let cacheKey = engine.executionCache.keyFor( key );
            if (
               engine.executionCache.isFileUnmodified( cacheKey, activeFrames[ j ].current ) &&
               engine.executionCache.isFileUnmodified( cacheKey, targetFname )
            )
            {
               console.noteln( "descriptor, source and target frame are unchanged, no need to write the weights into the target file" )
               activeFrames[ j ].processingSucceeded( WBPPFrameProcessingStep.WRITE_WEIGHTS, targetFname );
            }
            else
            {

               let imageWindow = this.readImage( activeFrames[ j ].current, "" );
               if ( imageWindow === null )
               {
                  console.warningln( "** Warning: Unable to open file to write weights: <raw>" + activeFrames[ j ].current + "</raw>; light frame will be discarded." );
                  this.processLogger.addWarning( "Unable to open file to write weights: " + activeFrames[ j ].current + "; light frame will be discarded." );
                  // can't read the file, discard it
                  activeFrames[ j ].processingFailed();
               }
               else
               {

                  if ( isFinite( descriptor.formulaWeight ) )
                  {
                     imageWindow.keywords = imageWindow.keywords.filter( keyword =>
                     {
                        return keyword.name != CONST_MEASUREMENT_FWHM &&
                           keyword.name != CONST_MEASUREMENT_ECCENTRICITY &&
                           keyword.name != CONST_MEASUREMENT_NOISE &&
                           keyword.name != CONST_MEASUREMENT_SNRWEIGHT &&
                           keyword.name != CONST_MEASUREMENT_STARS &&
                           keyword.name != CONST_MEASUREMENT_PSFSIGNAL &&
                           keyword.name != CONST_MEASUREMENT_PSFSNR &&
                           keyword.name != CONST_MEASUREMENT_SCORE &&
                           keyword.name != WEIGHT_KEYWORD;
                     } ).concat(
                        new FITSKeyword(
                           CONST_MEASUREMENT_FWHM,
                           format( "%.5e", descriptor.FWHM ).replace( "e", "E" ),
                           "WBPP Measurement: FWHM"
                        ) ).concat(
                        new FITSKeyword(
                           CONST_MEASUREMENT_ECCENTRICITY,
                           format( "%.5e", descriptor.eccentricity ).replace( "e", "E" ),
                           "WBPP Measurement: eccentricity"
                        ) ).concat(
                        new FITSKeyword(
                           CONST_MEASUREMENT_NOISE,
                           format( "%.5e", descriptor.noise ).replace( "e", "E" ),
                           "WBPP Measurement: noise"
                        ) ).concat(
                        new FITSKeyword(
                           CONST_MEASUREMENT_SNRWEIGHT,
                           format( "%.5e", descriptor.SNR ).replace( "e", "E" ),
                           "WBPP Measurement: SNR Weight"
                        ) ).concat(
                        new FITSKeyword(
                           CONST_MEASUREMENT_STARS,
                           format( "%i", descriptor.numberOfStars ),
                           "WBPP Measurement: number of stars found"
                        ) ).concat(
                        new FITSKeyword(
                           CONST_MEASUREMENT_PSFSIGNAL,
                           format( "%.5e", descriptor.PSFSignalWeight ).replace( "e", "E" ),
                           "WBPP Measurement: PSF Signal Weight"
                        ) ).concat(
                        new FITSKeyword(
                           CONST_MEASUREMENT_PSFSNR,
                           format( "%.5e", descriptor.PSFSNR ).replace( "e", "E" ),
                           "WBPP Measurement: PSF SNR"
                        ) ).concat(
                        new FITSKeyword(
                           CONST_MEASUREMENT_SCORE,
                           format( "%.5e", descriptor.formulaWeight ).replace( "e", "E" ),
                           "WBPP Measurement: Image score"
                        ) ).concat(
                        new FITSKeyword(
                           WEIGHT_KEYWORD,
                           format( "%.3e", descriptor.imageWeight ).replace( "e", "E" ),
                           "Subframe weight"
                        ) );

                     // get the current LMD of the source file before writing, file could be overwritten so we need to update the
                     // current LMD value in the cache
                     let previousLMD = getLastModifiedDate( activeFrames[ j ].current );

                     // write the file
                     imageWindow.saveAs( targetFname, false, false, false, false );
                     imageWindow.forceClose();

                     engine.executionCache.cacheFileLMD( cacheKey, targetFname );
                     engine.executionCache.cacheFileLMD( cacheKey, activeFrames[ j ].current );
                     // handle the overwriting of the file by updating the target frame LMD value in the cache
                     if ( targetFname == activeFrames[ j ].current )
                     {
                        // files are overwritten, update the LMD of the target
                        let newLMD = getLastModifiedDate( targetFname );
                        engine.executionCache.updateLMD( targetFname, previousLMD, newLMD );
                     }
                     activeFrames[ j ].processingSucceeded( WBPPFrameProcessingStep.WRITE_WEIGHTS, targetFname );
                  }
                  else
                  {
                     console.warningln( "** Warning: Unable to open file to write weights: <raw>" + activeFrames[ j ].current + "</raw>; light frame will be discarded." );
                     this.processLogger.addWarning( "Unable to open file to write weights: " + activeFrames[ j ].current + "; light frame will be discarded." );
                     // can't read the file, discard it
                     activeFrames[ j ].processingFailed();
                  }
               }
            }
         }
         else
         {
            console.warningln( "** Warning: Measurement not found for image: <raw>" + activeFrames[ j ].current + "</raw>; light frame will be discarded." );
            this.processLogger.addWarning( "Measurement not found for image: " + activeFrames[ j ].current + "; light frame will be discarded." );
            // can't find the descriptor for the file item
            activeFrames[ j ].processingFailed();
         }
      }
   }
};

// ----------------------------------------------------------------------------

/**
 * Measure the provided file items and store the measurements into each
 * measured item. The measure is performed by means of the SubframeSelector
 * Process.
 *
 * @param {[FileItem]} fileItems
 * @returns the number of frames successfully measured
 */
StackEngine.prototype.computeDescriptors = function( fileItems )
{
   let nCached = 0;
   let nMeasured = 0;
   let nFailed = 0;

   if ( !fileItems || fileItems.length == 0 )
      return {
         nCached: nCached,
         nMeasured: nMeasured,
         nFailed: nFailed
      };

   let
   {
      enableTargetFrames,
      paddedStringNumber
   } = WBPPUtils.shared();

   let subframes = fileItems.map( item => item.current );

   var SS = new SubframeSelector;
   SS.routine = SubframeSelector.prototype.MeasureSubframes;
   SS.nonInteractive = true; // ### since core version 1.8.8-8
   SS.cameraResolution = SubframeSelector.prototype.Bits16;
   SS.scaleUnit = SubframeSelector.prototype.ArcSeconds;
   SS.dataUnit = SubframeSelector.prototype.DataNumber;
   SS.fileCache = true;
   SS.noNoiseAndSignalWarnings = true;

   /**
    * Use the cached data for the unchanged files
    */
   let subframesToMeasure = [];
   let cachedDescriptors = {};

   let isDescriptorValid = function( descriptor )
   {
      let failed = !isFinite( descriptor.FWHM ) ||
         !isFinite( descriptor.eccentricity ) ||
         !isFinite( descriptor.numberOfStars ) ||
         !isFinite( descriptor.PSFSignalWeight ) ||
         !isFinite( descriptor.PSFSNR ) ||
         !isFinite( descriptor.SNR ) ||
         !isFinite( descriptor.median ) ||
         !isFinite( descriptor.mad ) ||
         !isFinite( descriptor.Mstar ) ||
         descriptor.numberOfStars <= 0;
      return !failed;
   };

   // The cache is a map between the filePath and its descriptor
   let SSCache = {};
   let SScacheKey = engine.executionCache.keyFor( "SubframeSelector" );
   console.writeln();
   console.noteln( "SS check for cached data" );
   if ( engine.executionCache.hasCacheForKey( SScacheKey ) )
   {
      console.noteln( "SS has cached data for key ", SScacheKey );
      SSCache = engine.executionCache.cacheForKey( SScacheKey );
   }
   else
      console.noteln( "SS no cached data available for key ", SScacheKey );

   for ( let i = 0; i < subframes.length; ++i )
   {
      let filePath = subframes[ i ];
      let cachedDescriptor = SSCache[ filePath ];
      if ( cachedDescriptor != undefined && this.executionCache.isFileUnmodified( SScacheKey, filePath ) )
      {
         // we have a valid cached data for that file
         console.noteln( "SS will use cached descriptor for ", filePath );
         cachedDescriptors[ filePath ] = cachedDescriptor;
         continue;
      }
      console.noteln( "SS will measure ", filePath );
      // non existent or old cache found, we need to measure the file
      subframesToMeasure.push( filePath );
   }

   // Put the process into the process container if needed
   // We set the full list of files to be measured, disregarding the ones that have not been measured because of
   // the cache

   SS.nonInteractive = false;
   SS.subframes = enableTargetFrames( subframes, 2 );
   engine.processContainer.add( SS );
   SS.nonInteractive = true;

   // Set the frames to be measured and perform the measurements
   let success = true;
   SS.subframes = enableTargetFrames( subframesToMeasure, 2 );
   if ( subframesToMeasure.length > 0 )
      success = SS.executeGlobal();

   // NB: fixed indexes that need to be aligned with the process implementation
   let iIndex = 0;
   let iFilePath = 3;
   let iFWHM = 5;
   let iEccentricity = 6;
   let iPSFSignalWeight = 7;
   let iSNREstimate = 9;
   let iMedian = 10;
   let iMad = 11;
   let iNoise = 12;
   let iStars = 14;
   let iMstar = 26;
   let iPSFSNR = 28;

   // create a table of  descriptors, the cached descriptors are stored immediately,
   // the others are filled by scanning the SS results
   let descriptors = [];
   for ( let i = 0; i < subframes.length; ++i )
   {
      if ( cachedDescriptors[ subframes[ i ] ] != undefined )
      {
         descriptors.push( cachedDescriptors[ subframes[ i ] ] );
         nCached++;
      }
      else
      {
         descriptors.push(
         {
            filePath: subframes[ i ],
            failed: true
         } );
      }
   }

   if ( success )
   {
      // extract successful measurements
      for ( let i = 0; i < SS.measurements.length; ++i )
      {
         let index = SS.measurements[ i ][ iIndex ];
         let filePath = SS.measurements[ i ][ iFilePath ];
         let FWHM = SS.measurements[ i ][ iFWHM ];
         let eccentricity = SS.measurements[ i ][ iEccentricity ];
         let noise = SS.measurements[ i ][ iNoise ];
         let numberOfStars = SS.measurements[ i ][ iStars ];
         let PSFSignalWeight = SS.measurements[ i ][ iPSFSignalWeight ];
         let PSFSNR = SS.measurements[ i ][ iPSFSNR ];
         let SNR = SS.measurements[ i ][ iSNREstimate ];
         let median = SS.measurements[ i ][ iMedian ];
         let mad = SS.measurements[ i ][ iMad ];
         let Mstar = SS.measurements[ i ][ iMstar ];

         let descriptor = {
            filePath: filePath,
            FWHM: FWHM,
            eccentricity: eccentricity,
            noise: noise,
            numberOfStars: numberOfStars,
            PSFSignalWeight: PSFSignalWeight,
            PSFSNR: PSFSNR,
            SNR: SNR,
            median: median,
            mad: mad,
            Mstar: Mstar
         };

         descriptor.failed = !isDescriptorValid( descriptor );

         // find the index of the corresponding descriptor data in the descriptor array
         let j = 0;
         while ( descriptors[ j ].filePath != filePath )
            j++;

         // update the descriptor
         descriptors[ j ] = descriptor;

         if ( descriptor.failed )
            nFailed++;
         else
            nMeasured++;
      }
   }
   else
   {
      // SS failed, no measure is returned
      nFailed = subframesToMeasure.length;
   }

   // store the result in the execution cache, only valid descriptors are saved
   for ( let i = 0; i < descriptors.length; ++i )
   {
      if ( !descriptors[ i ].failed )
      {
         let filePath = descriptors[ i ].filePath;
         engine.executionCache.cacheFileLMD( SScacheKey, filePath );
         SSCache[ descriptors[ i ].filePath ] = descriptors[ i ];
      }
   }
   engine.executionCache.setCache( SScacheKey, SSCache );

   // gather measurements in file items
   for ( let i = 0; i < descriptors.length; ++i )
   {
      let descriptor = descriptors[ i ];
      console.writeln();
      console.writeln( "<end><cbr><raw>" + descriptor.filePath + "</raw>" );
      if ( descriptor.failed )
      {
         console.noteln( "Descriptor failed: " );
         console.noteln( "FWHM            : ", descriptor.FWHM );
         console.noteln( "eccentricity    : ", descriptor.eccentricity );
         console.noteln( "numberOfStars   : ", descriptor.numberOfStars );
         console.noteln( "PSFSignalWeight : ", descriptor.PSFSignalWeight );
         console.noteln( "PSFSNR          : ", descriptor.PSFSNR );
         console.noteln( "SNR             : ", descriptor.SNR );
         console.noteln( "median          : ", descriptor.median );
         console.noteln( "mad             : ", descriptor.mad );
         console.noteln( "Mstar           : ", descriptor.Mstar );

         fileItems[ i ].processingFailed();
         console.warningln( "** Warning: Failed to measure frame: <raw>" + descriptor.filePath + "</raw>; image will be ignored." );
         this.processLogger.addWarning( "Failed to measure frame: " + descriptor.filePath + "; image will be ignored." );
         continue;
      }

      fileItems[ i ].setDescriptor( descriptor );

      let padding = descriptor.failed ? 10 : Math.floor( Math.max( 1, Math.log10( Math.max.apply( null, [ descriptor.FWHM, descriptor.eccentricity, descriptor.SNR, descriptor.numberOfStars, descriptor.PSFSignalWeight, descriptor.PSFSNR, descriptor.median * 65535, descriptor.Mstar * 65535 ] ) ) ) ) + 1;

      console.noteln( "--------------------------" + "-".repeat( padding ) );
      console.noteln( "FWHM              : ", isFinite( descriptor.FWHM ) ? paddedStringNumber( format( "%0.3f", descriptor.FWHM ), padding ) + ' [px]' : "NaN" );
      console.noteln( "Eccentricity      : ", isFinite( descriptor.eccentricity ) ? paddedStringNumber( format( "%0.3f", descriptor.eccentricity ), padding ) : "NaN" );
      console.noteln( "Number of stars   : ", isFinite( descriptor.numberOfStars ) ? paddedStringNumber( format( "%i", descriptor.numberOfStars ), padding ) : "NaN" );
      console.noteln( "PSF Signal Weight : ", isFinite( descriptor.PSFSignalWeight ) ? paddedStringNumber( format( "%0.3f", descriptor.PSFSignalWeight ), padding ) : "NaN" );
      console.noteln( "PSF SNR           : ", isFinite( descriptor.PSFSNR ) ? paddedStringNumber( format( "%0.3f", descriptor.PSFSNR ), padding ) : "NaN" );
      console.noteln( "SNR               : ", isFinite( descriptor.SNR ) ? paddedStringNumber( format( "%0.3f", descriptor.SNR ), padding ) : "NaN" );
      console.noteln( "Median (ADU)      : ", isFinite( descriptor.median ) ? paddedStringNumber( format( "%0.3f", descriptor.median * 65535 ), padding ) : "NaN" );
      console.noteln( "MAD (ADU)         : ", isFinite( descriptor.mad ) ? paddedStringNumber( format( "%0.3f", descriptor.mad * 65535 ), padding ) : "NaN" );
      console.noteln( "Mstar (ADU)       : ", isFinite( descriptor.Mstar ) ? paddedStringNumber( format( "%0.3f", descriptor.Mstar * 65535 ), padding ) : "NaN" );
      console.noteln( "--------------------------" + "-".repeat( padding ) );

      if ( i % 50 == 0 )
      {
         console.flush();
         processEvents();
         gc();
      }
   }

   // return the file count
   return {
      nCached: nCached,
      nMeasured: nMeasured,
      nFailed: nFailed
   };
};

// ----------------------------------------------------------------------------
/**
 * Returns the auto crop region for an image.
 * The autocrop region is computed analyzing the low rejection map and the crop region represents the
 * the largest rectangle that includes pixels with low rejection values greather than 0.5.
 * The method assumes to find the low rejection map store in the image file, if this is not the case
 * it attempts to load the file with the postfix "_low_rejection". If none is found then no crop region
 * is computed.
 *
 * @param {*} filePath the file path of the image
 * @param {*} keepTheImageOpen if TRUE, the main image window remains open once the function returns
 * @returns the Rect defining the crop region of the image, undefined in case of errors.
 */

StackEngine.prototype.getAutocropRegion = function( filePath, returnTheWorkingImages )
{
   if ( !File.exists( filePath ) )
      return {
         success: false,
         message: "unable to compute the autocrop region, file not found at path: " + filePath
      };

   let windows = ImageWindow.open( filePath );
   // ensure that data has been loaded
   if ( windows == undefined || windows.length == 0 )
      return {
         success: false,
         message: "unable to compute the autocrop region, file not loaded at path: " + filePath
      };

   // expect to find the main window and, optionally, the rejection high and low ones
   let mainWindow = windows[ 0 ];
   let rejLowWindow;
   let rejHighWindow;

   // Search for the low and high rejection maps within the loaded images.
   // N.B.: We must be robust to the current state of the "use filenames as
   // image identifiers" global preferences setting.
   for ( let i = 1; i < windows.length; ++i )
   {
      if ( windows[ i ].mainView.id.indexOf( "rejection_low" ) >= 0 )
         rejLowWindow = windows[ i ];
      if ( windows[ i ].mainView.id.indexOf( "rejection_high" ) >= 0 )
         rejHighWindow = windows[ i ];
   }

   // If rejection low is not present, the autocrop operation cannot continue.
   if ( rejLowWindow == undefined )
   {
      for ( let i = 0; i < windows.length; ++i )
         windows[ i ].forceClose();
      return {
         success: false,
         message: "unable to compute the autocrop region, low rejection map not found at path: " + filePath
      };
   }

   // compute the crop region
   let image = rejLowWindow.mainView.image;
   // memoization of the bottom and top Y coordinate available for each x coordinate
   let bottomY = new Array( image.width );
   let upperY = new Array( image.width );
   let rightX = new Array( image.height );
   let leftX = new Array( image.height );

   // crop coordinates
   let cx0 = 0;
   let cx1 = 0;
   let cy0 = 0;
   let cy1 = 0;

   // ROWS SCAN
   let maxArea = 0;
   let TOLERANCE = 0.25;

   for ( let row = 0; row < image.height; row++ )
   {
      // find the (x0,x1) extremes of the row
      let x0 = 0;
      while ( x0 < image.width && image.sample( x0, row ) > TOLERANCE )
         x0++;
      if ( x0 == image.width )
         continue;

      let x1 = image.width - 1;
      while ( image.sample( x1, row ) > TOLERANCE )
         x1--;

      // skip if the max possible area is lower than the current max
      if ( ( x1 - x0 ) * ( image.height - row ) <= maxArea && ( x1 - x0 ) * row <= maxArea )
         continue;

      // find the bottom Y values for each column
      let Ybottom = [ x0, x1 ].map( x =>
      {
         // check memoization
         if ( bottomY[ x ] != undefined )
            return bottomY[ x ];
         else
         {
            let y = image.height - 1;
            while ( y > row && image.sample( x, y ) > TOLERANCE )
               y--;
            bottomY[ x ] = y;
            return y;
         }
      } );
      // the bottom Y coordinate is the lower one
      let yb = Math.min( Ybottom[ 0 ], Ybottom[ 1 ] );

      // find the top Y values for each column
      let Ytop = [ x0, x1 ].map( x =>
      {
         // check memoization
         if ( upperY[ x ] != undefined )
            return upperY[ x ];
         else
         {
            let y = 0;
            while ( y < row && image.sample( x, y ) > TOLERANCE )
               y++;
            upperY[ x ] = y;
            return y;
         }
      } );
      // the top Y coordinate is the higher one
      let yt = Math.max( Ytop[ 0 ], Ytop[ 1 ] );

      // check the bottom area
      let area = ( x1 - x0 ) * ( yb - row );
      if ( area > maxArea )
      {
         maxArea = area;
         cx0 = x0;
         cy0 = row;
         cx1 = x1;
         cy1 = yb;
      }
      // check the top area
      area = ( x1 - x0 ) * ( row - yt );
      if ( area > maxArea )
      {
         maxArea = area;
         cx0 = x0;
         cy0 = yt;
         cx1 = x1;
         cy1 = row;
      }
   }

   for ( let col = 0; col < image.width; col++ )
   {
      // find the (y0,y1) extremes of the column
      let y0 = 0;
      while ( y0 < image.height && image.sample( col, y0 ) > TOLERANCE )
         y0++;
      if ( y0 == image.height )
         continue;

      let y1 = image.height - 1;
      while ( image.sample( col, y1 ) > TOLERANCE )
         y1--;

      // skip if the max possible area is lower than the current max
      if ( ( y1 - y0 ) * ( image.width - col ) <= maxArea && ( y1 - y0 ) * col <= maxArea )
         continue;

      // find the right X values for each row
      let Xright= [ y0, y1 ].map( y =>
      {
         // check memoization
         if ( rightX[ y ] != undefined )
            return rightX[ y ];
         else
         {
            let x = image.width - 1;
            while ( x > col && image.sample( x, y ) > TOLERANCE )
               x--;
            rightX[ y ] = x;
            return x;
         }
      } );
      // the right X coordinate is the lower one
      let xr = Math.min( Xright[ 0 ], Xright[ 1 ] );

      // find the left X values for each row
      let Xleft = [ y0, y1 ].map( y =>
      {
         // check memoization
         if ( leftX[ y ] != undefined )
            return leftX[ y ];
         else
         {
            let x = 0;
            while ( x < col && image.sample( x, y ) > TOLERANCE )
               x++;
            leftX[ y ] = x;
            return x;
         }
      } );
      // the top Y coordinate is the higher one
      let xl = Math.max( Xleft[ 0 ], Xleft[ 1 ] );

      // check the bottom area
      let area = (y1 - y0) * (xr - col);
      if ( area > maxArea )
      {
         maxArea = area;
         cx0 = col;
         cy0 = y0;
         cx1 = xr;
         cy1 = y1;
      }
      // check the top area
      area = ( y1 - y0 ) * ( col - xl );
      if ( area > maxArea )
      {
         maxArea = area;
         cx0 = xl;
         cy0 = y0;
         cx1 = col;
         cy1 = y1;
      }
   }

   // build the returned object
   let retVal = {
      success: true,
      rect: new Rect( cx0, cy0, cx1, cy1 )
   };

   if ( returnTheWorkingImages )
   {
      retVal.mainWindow = mainWindow;
      retVal.rejLowWindow = rejLowWindow;
      retVal.rejHighWindow = rejHighWindow;
   }
   else
   {
      if ( mainWindow )
         mainWindow.forceClose();
      if ( rejLowWindow )
         rejLowWindow.forceClose();
      if ( rejHighWindow )
         rejHighWindow.forceClose();
   }
   return retVal;
}

// ----------------------------------------------------------------------------

StackEngine.prototype.doLinearPatternSubtraction = function( groups )
{
   let
   {
      existingDirectory,
      isEmptyString
   } = WBPPUtils.shared();

   // first step: we need to generate the reference image. This image needs to be generated
   // for each pair binning/image size amongst the calibration groups.
   // For each binning/size we use as reference the group that has the
   // longest total exposure time since it is supposed to have the highest SNR once integrated.

   // identify the set of binning/size pairs
   let groupsByBinningAndSize = groups.filter( g =>
   {
      let isLight = g.imageType == ImageType.LIGHT;
      let isMono = !g.isCFA;
      let isPRE = g.mode == WBPPGroupingMode.PRE;
      return isLight && isMono && isPRE;
   } ).reduce( ( acc, group ) =>
   {
      // generate an unique key that is based on binning and image size
      let key = group.binning + " (" + group.sizeString() + ")";
      if ( !acc[ key ] )
         acc[ key ] = [ group ];
      else
         acc[ key ].push( group );

      return acc;
   },
   {} );

   // iterate through all binning/size values
   let binningAndSizes = Object.keys( groupsByBinningAndSize );

   let nCached = 0;
   let nGenerated = 0;
   let nFailed = 0;

   if ( binningAndSizes.length == 0 )
   {
      console.warning( "** Warning: No monochrmoatic groups have been found, linear defects correction will be skipped." );
      this.processLogger.addWarning( "No monochrmoatic groups have been found, linear defects correction will be skipped." );
   }
   else
   {
      // configure the LDD and LPS engines

      let LDD = new LDDEngine();
      LDD.detectionThreshold = engine.linearPatternSubtractionRejectionLimit;
      LDD.closeFormerWorkingImages = true;

      let LPS = new LPSEngine();
      LPS.targetIsActiveImage = false;
      LPS.rejectionLimit = engine.linearPatternSubtractionRejectionLimit;
      LPS.globalRejectionLimit = Math.max( 5.0, engine.linearPatternSubtractionRejectionLimit );
      LPS.closeFormerWorkingImages = true;

      let detectModes;
      switch ( engine.linearPatternSubtractionMode )
      {
         case 0: // columns
            detectModes = [
            {
               correctColumns: true,
               postfix: "_lps"
            } ];
            break;
         case 1: // rows
            detectModes = [
            {
               correctColumns: false,
               postfix: "_lps"
            } ];
            break;
         case 2: // columns and rows
            detectModes = [
            {
               correctColumns: true,
               postfix: "_lps"
            },
            {
               correctColumns: false,
               postfix: "" // on the second execution we overwrite existing files
            } ];
            break;
      }

      // execute a correction cycle for each binning/size
      for ( let i = 0; i < binningAndSizes.length; ++i )
      {
         let binning = binningAndSizes[ i ];
         let groups = groupsByBinningAndSize[ binning ];

         // sort groups by the total active duration
         let groupsSortedByDuration = groups
            .filter( g => ( g.activeFrames().length >= 3 ) )
            .map( g => (
            {
               group: g,
               tet: g.totalExposureTime( true /* only active frames */ )
            } ) )
            .sort( ( a, b ) =>
            {
               if ( b.tet > a.tet )
                  return 1;
               if ( a.tet > b.tet )
                  return -1;
               return 0;
            } )
            .map( g => ( g.group ) );

         // check if a reference group has been found
         if ( groupsSortedByDuration.length == 0 )
         {
            console.warningln( "** Warning: Unable to generate the linear defect detection reference frame." +
               " No groups with at least 3 grayscale light frames found. Light frames with binning " + binning +
               " will not be corrected." );
            this.processLogger.addError( "Unable to generate the linear defect detection reference frame." +
               " No groups with at least 3 grayscale light frames found. Light frames with binning " + binning +
               " will not be corrected." );
            // count the number of failed frames for the current binning
            nFailed += groups.reduce( ( acc, group ) => ( acc + group.activeFrames().length ), 0 );
            continue;
         }

         // the reference group has the longest duration
         let referenceGroup = groupsSortedByDuration[ 0 ];

         console.noteln( "LDD generate the reference frame for binning ", binning );
         // integrate the reference group to generate the reference frame for the current binninb/size value
         let
         {
            masterFilePath
         } = this.doIntegrate(
            referenceGroup,
            undefined, /* custom prefix */
            "_LDD_REFERENCE_FRAME", /* custom postfix */
            false, /* custom generate rejection maps */
            false, /* custom generate drizzle files */
            undefined, /* desired master file name */
            {
               /* II overridden parameters */
               combination: ImageIntegration.prototype.Average,
               weightMode: ImageIntegration.prototype.DontCare,
               evaluateSNR: false
            },
            undefined /* FITS keywords */
         );

         // integration check
         if ( isEmptyString( masterFilePath ) )
         {
            console.warningln( "** Warning: Failed to generate the linear defect detection reference frame." +
               " Light frames with binning " + binning + " will not be corrected." );
            this.processLogger.addError( "Failed to generate the linear defect detection reference frame for" +
                     " binning " + binning + "; light frames with this binning will not be corrected." );
            // count the number of failed frames for the current binning
            nFailed += groups.reduce( ( acc, group ) => ( acc + group.activeFrames().length ), 0 );
            continue;
         }

         // perform the detection for the linear defects for each mode (rows, columns or both) and generate the corresponding
         // defects list files
         let LDDdefectsFileNames = [];
         let imageReferenceWidth = 0;
         let imageReferenceHeight = 0;
         let LDDsuccess = true;
         for ( let d = 0; d < detectModes.length && LDDsuccess; ++d )
         {
            let dm = detectModes[ d ];
            LDD.detectColumns = dm.correctColumns;

            let columnOrRow = LDD.detectColumns ? "Col" : "Row";
            let LDDdefectsFileName = File.appendToName( masterFilePath, "_defects_list_" + columnOrRow );
            LDDdefectsFileName = File.changeExtension( LDDdefectsFileName, ".txt" );
            LDDdefectsFileNames[ d ] = LDDdefectsFileName;

            // generate the list of defects
            console.writeln();
            console.noteln( "Perform linear defect detection on " + columnOrRow + "s for binning ", binning );
            this.processLogger.addSuccess( "Linear defect detection reference frame", masterFilePath );

            console.noteln( "LDD.columnOrRow  ................... ", columnOrRow );
            console.noteln( "LDD.detectColumns .................. ", LDD.detectColumns );
            console.noteln( "LDD.detectPartialLines ............. ", LDD.detectPartialLines );
            console.noteln( "LDD.imageShift ..................... ", LDD.imageShift );
            console.noteln( "LDD.closeFormerWorkingImages ....... ", LDD.closeFormerWorkingImages );
            console.noteln( "LDD.layersToRemove ................. ", LDD.layersToRemove );
            console.noteln( "LDD.rejectionLimit ................. ", LDD.rejectionLimit );
            console.noteln( "LDD.detectionThreshold ............. ", LDD.detectionThreshold );
            console.noteln( "LDD.partialLineDetectionThreshold .. ", LDD.partialLineDetectionThreshold );

            // Check if valid cache is already present for this configuration
            let LDDcacheKey = engine.executionCache.keyFor( "LDD_" + engine.linearPatternSubtractionRejectionLimit + "_" + masterFilePath );

            // cached data must exist for the current configuration
            // the reference image must be unchanged
            // the defect list file must exist and be unchanged
            console.writeln();
            console.noteln( "* LDD check for cached data." );
            if ( engine.executionCache.hasCacheForKey( LDDcacheKey ) &&
                 engine.executionCache.isFileUnmodified( LDDcacheKey, masterFilePath ) &&
                 engine.executionCache.isFileUnmodified( LDDcacheKey, LDDdefectsFileName ) )
            {
               // valid cache exists
               let cachedData = engine.executionCache.cacheForKey( LDDcacheKey );
               imageReferenceWidth = cachedData.imageReferenceWidth;
               imageReferenceHeight = cachedData.imageReferenceHeight;
               console.noteln( "* LDD valid cached data exists, defect list file is: <raw>" + LDDdefectsFileName + "</raw>" );
               console.writeln();
            }
            else
            {
               // no valid cache exists, generate the defect list
               if ( !engine.executionCache.hasCacheForKey( LDDcacheKey ) )
                  console.noteln( "* LDD has no cached data for key ", LDDcacheKey );
               else if ( !engine.executionCache.isFileUnmodified( LDDcacheKey, masterFilePath ) )
                  console.noteln( "* LDD has cache but the reference frame has changed: ", masterFilePath );
               else
                  console.noteln( "* LDD has cache but the defect list file has changed: <raw>" + LDDdefectsFileName + "</raw>" );
               console.writeln();

               // open the reference frame
               let refrenceFrameImageWindow = ImageWindow.open( masterFilePath );
               if ( refrenceFrameImageWindow.length > 0 )
               {
                  refrenceFrameImageWindow = refrenceFrameImageWindow[ 0 ];
                  refrenceFrameImageWindow.show();
               }
               else
               {
                  LDDsuccess = false;
                  console.warningln( "** Warning: Failed to open the linear defect detection reference frame." +
                     " Light frames with binning " + binning + " will not be corrected." );
                  this.processLogger.addError( "Failed to open the linear defect detection reference frame for" +
                     " binning " + binning + "; light frames with this binning will not be corrected." );
                  break;
               }

               imageReferenceWidth = refrenceFrameImageWindow.mainView.image.width;
               imageReferenceHeight = refrenceFrameImageWindow.mainView.image.height;

               // perform LDD on the active frame
               LDD.execute();
               refrenceFrameImageWindow.forceClose();
               console.noteln( "Linear defect detection completed." );

               // write the defective lines
               let LDDDefectFile = File.createFileForWriting( LDDdefectsFileName );
               if ( LDDDefectFile )
               {
                  console.noteln( "Linear defect detection writing defect file: <raw>" + LDDdefectsFileName + "</raw>" );
                  for ( let i = 0; i < LDD.detectedColumnOrRow.length; ++i )
                  {
                     let line = columnOrRow + " " +
                        LDD.detectedColumnOrRow[ i ] + " " +
                        LDD.detectedStartPixel[ i ] + " " +
                        LDD.detectedEndPixel[ i ];
                     LDDDefectFile.outTextLn( line );
                     console.noteln( line );
                  }
                  LDDDefectFile.close();

                  // update the cache. The cache is epmty but we need to save it anyway to remember that LDD with this configuratio has
                  // already been performed
                  let cachedData = {
                     imageReferenceWidth: imageReferenceWidth,
                     imageReferenceHeight: imageReferenceHeight
                  };
                  console.writeln();
                  engine.executionCache.setCache( LDDcacheKey, cachedData );
                  engine.executionCache.cacheFileLMD( LDDcacheKey, masterFilePath );
                  engine.executionCache.cacheFileLMD( LDDcacheKey, LDDdefectsFileName );
                  console.writeln();
               }
               else
               {
                  LDDsuccess = false;
                  console.warningln( "** Warning: Linear defect detection error creating the defect file: <raw>" + LDDdefectsFileName + "</raw>" );
                  this.processLogger.addError( "Warning: Linear defect detection error for binning " + binning + "; cannot create " +
                     "the defect file: " + LDDdefectsFileName );
               }
            }
         }

         // we ignore the correction if LDD has not been succesfully executed
         if ( !LDDsuccess )
         {
            // count the number of failed frames for the current binning
            nFailed += groups.reduce( ( acc, group ) => ( acc + group.activeFrames().length ), 0 );
            continue;
         }

         // now that the list of defects has been succesfully generated for the current modes, we proceed correcting all frames of
         // all groups with the current binning/size
         for ( let ig = 0; ig < groups.length; ig++ )
         {
            // common mode LPS parameters
            LPS.outputDir = existingDirectory( this.outputDirectory + "/ldd_lps/" + groups[ ig ].folderName() );
            LPS.backgroundReferenceWidth = imageReferenceWidth;
            LPS.backgroundReferenceHeight = imageReferenceHeight;

            // generate the list of active frames associated to the current group
            let activeFrames = groups[ ig ].activeFrames();

            // check for cached data
            let LPScacheKey = engine.executionCache.keyFor(
               "LPS_" +
               engine.linearPatternSubtractionRejectionLimit + "_" +
               LPS.backgroundReferenceWidth + "_" +
               LPS.backgroundReferenceHeight + "_" +
               LPS.outputDir + "_" +
               // include the defects file names in the key
               LDDdefectsFileNames.join( "_" )
            );

            // check the cache and skip the files that are unmodified
            let filesToCorrect = activeFrames;
            // inject an auxiliary property to track the execution of LPS
            filesToCorrect = filesToCorrect.map( item =>
            {
               // assume files are processed succesfully by LPS
               item.__lps_success__ = true;
               item.__lps_input__ = item.current;
               return item;
            } );
            let LPScacheData = {};
            // prerequisite before checking: we must have a saved cache and the defect files must me unmodified
            console.writeln();
            console.noteln( "LPS check for cached data" );
            if ( engine.executionCache.hasCacheForKey( LPScacheKey ) )
            {
               console.noteln( "LPS has cache data for key ", LPScacheKey );

               // check if defect files are unmodified
               let defectFilesUnchanged = LDDdefectsFileNames.reduce( ( acc, defectFilePath ) =>
               {
                  // skip the check if a file has already changes
                  if ( !acc )
                     return acc;
                  // check the current defect file for modifications
                  let unmodified = engine.executionCache.isFileUnmodified( LPScacheKey, defectFilePath );
                  if ( !unmodified )
                     console.noteln( "LPS cache found but the defect file has changed: ", defectFilePath );
                  return acc && unmodified;
               }, true );

               if ( defectFilesUnchanged )
               {
                  // cached data is valid
                  // the cache is a map between input files and corrected files
                  LPScacheData = engine.executionCache.cacheForKey( LPScacheKey );
                  filesToCorrect = [];

                  // get the list of cached files
                  for ( let j = 0; j < activeFrames.length; ++j )
                  {
                     let activeFrame = activeFrames[ j ];
                     let outputFile = LPScacheData[ activeFrame.current ];
                     if ( outputFile != undefined &&
                        engine.executionCache.isFileUnmodified( LPScacheKey, activeFrame.current ) &&
                        engine.executionCache.isFileUnmodified( LPScacheKey, outputFile ) )
                     {
                        // input and output files are unmodified, proceed without processing the input file again
                        activeFrame.processingSucceeded( WBPPFrameProcessingStep.LPS, outputFile );
                        nCached++;
                        console.noteln( "LPS cache found for ", activeFrame.current );
                     }
                     else
                     {
                        // put the file to be corrected into the list and store its LMD
                        filesToCorrect.push( activeFrame );
                        engine.executionCache.cacheFileLMD( LPScacheKey, activeFrame.current );
                        if ( outputFile == undefined )
                           console.noteln( "LPS no cached data for ", File.extractNameAndExtension( activeFrame.current ) );
                        else if ( !engine.executionCache.isFileUnmodified( LPScacheKey, activeFrame.current ) )
                           console.noteln( "LPS input file is modified ", File.extractNameAndExtension( activeFrame.current ) );
                        else
                           console.noteln( "LPS output file is modified ", outputFile );
                        console.noteln( "LPS will correct ", activeFrame.current );
                     }
                  }
               }
               else
               {
                  console.noteln( "LPS defect files changed, LPS will be executed on all frames" );
               }
            }
            else
            {
               console.noteln( "LPS has no cache data for key ", LPScacheKey );
            }
            console.writeln();

            if ( filesToCorrect.length > 0 )
            {

               // execute the correction of the group for each correction mode
               // perform the correction passes (rows, columns or both)
               let LPSsuccess = true;
               for ( let d = 0; d < detectModes.length && LPSsuccess; d++ )
               {
                  let dm = detectModes[ d ];
                  let columnOrRow = LDD.detectColumns ? "Col" : "Row";

                  // get the defects list file name for the given mode
                  let LDDdefectsFileName = LDDdefectsFileNames[ d ];

                  // keep only the files that has been succesfully processed or not yet processed
                  filesToCorrect = filesToCorrect.filter( item => item.__lps_success__ );

                  // configure LPS
                  LPS.postfix = dm.postfix;
                  LPS.correctColumns = dm.correctColumns;
                  LPS.postfix = dm.postfix;
                  LPS.defectTableFilePath = LDDdefectsFileName;

                  console.noteln( "Executing Linear Pattern Subtraction for group ", groups[ ig ].toString() );
                  console.noteln( "LPS.defectTableFilePath ........ ", LPS.defectTableFilePath );
                  console.noteln( "LPS.postfix .................... ", LPS.postfix );
                  console.noteln( "LPS.targetIsActiveImage ........ ", LPS.targetIsActiveImage );
                  console.noteln( "LPS.correctColumns ............. ", LPS.correctColumns );
                  console.noteln( "LPS.correctEntireImage ......... ", LPS.correctEntireImage );
                  console.noteln( "LPS.layersToRemove ............. ", LPS.layersToRemove );
                  console.noteln( "LPS.rejectionLimit ............. ", LPS.rejectionLimit );
                  console.noteln( "LPS.globalRejection ............ ", LPS.globalRejection );
                  console.noteln( "LPS.globalRejectionLimit ....... ", LPS.globalRejectionLimit );
                  console.noteln( "LPS.closeFormerWorkingImages ... ", LPS.closeFormerWorkingImages );
                  console.noteln( "LPS.backgroundReferenceLeft .... ", LPS.backgroundReferenceLeft );
                  console.noteln( "LPS.backgroundReferenceTop ..... ", LPS.backgroundReferenceTop );
                  console.noteln( "LPS.backgroundReferenceWidth ... ", LPS.backgroundReferenceWidth );
                  console.noteln( "LPS.backgroundReferenceHeight .. ", LPS.backgroundReferenceHeight );

                  LPS.inputFiles = filesToCorrect.map( item => item.current );
                  LPS.execute();
                  console.noteln( "Linear Pattern Subtraction completed for group ", groups[ ig ].toString() );
                  // cache the defects list LMD
                  engine.executionCache.cacheFileLMD( LPScacheKey, LDDdefectsFileName );

                  // check output results
                  if ( LPS.output.length == 0 )
                  {
                     LPSsuccess = false;
                     filesToCorrect = filesToCorrect.map( item =>
                     {
                        item.__lps_success__ = false;
                        return item;
                     } );
                     nFailed += filesToCorrect.length;
                     // report the issue
                     console.warningln( "** Warning: Linear pattern subtraction " + columnOrRow + "s failed. Light frames for group " + groups[ ig ].toString() + " will not be corrected." );
                     this.processLogger.addError( "Linear pattern subtraction " + columnOrRow + "s failed. Light frames for group " + groups[ ig ].toString() + " will not be corrected." );
                  }
                  else
                  {
                     // create a support for output file matching
                     let LPSFilesData = LPS.output.reduce( ( acc, filePath ) =>
                     {
                        acc[ File.extractName( filePath ) ] = filePath;
                        return acc;
                     },
                     {} );

                     console.writeln();
                     for ( let c = 0; c < filesToCorrect.length; ++c )
                     {
                        let lpsFileName = File.extractName( filesToCorrect[ c ].current ) + dm.postfix;
                        let outputFilePath = LPSFilesData[ lpsFileName ];

                        if ( outputFilePath != undefined && outputFilePath.length > 0 )
                        {
                           if ( File.exists( outputFilePath ) )
                           {
                              // success
                              filesToCorrect[ c ].processingSucceeded( WBPPFrameProcessingStep.LPS, outputFilePath );
                              nGenerated++;
                           }
                           else
                           {
                              filesToCorrect[ c ].__lps_success__ = false;
                              console.warningln( "** Warning: File does not exist after linear pattern subtraction: <raw>" + filesToCorrect[ c ].current + "</raw>" );
                              this.processLogger.addWarning( "File does not exist after linear pattern subtraction: " + filesToCorrect[ c ].current );
                              nFailed++;
                           }
                        }
                        else
                        {
                           filesToCorrect[ c ].__lps_success__ = false;
                           console.warningln( "** Warning: Linear pattern subtraction failed for frame: <raw>" + filesToCorrect[ c ].current + "</raw>" );
                           this.processLogger.addWarning( "Linear pattern subtraction failed for frame: " + filesToCorrect[ c ].current );
                           nFailed++;
                        }
                     }
                  }
               }

               // cache the successfully processed files
               filesToCorrect = filesToCorrect.filter( item => item.__lps_success__ );
               for ( let c = 0; c < filesToCorrect.length; ++c )
               {
                  // update the cached input/output map
                  LPScacheData[ filesToCorrect[ c ].__lps_input__ ] = filesToCorrect[ c ].current;
                  // cache the final output file LMD
                  engine.executionCache.cacheFileLMD( LPScacheKey, filesToCorrect[ c ].current );
                  engine.executionCache.cacheFileLMD( LPScacheKey, filesToCorrect[ c ].__lps_input__ );
               }
            }
            else if ( activeFrames.length > 0 )
            {
               console.noteln( "LPS is skipped since all frames are already cached" );
            }
            else
            {
               console.noteln( "LPS no files to correct found" );
            }

            // end processing the groups
            engine.executionCache.setCache( LPScacheKey, LPScacheData );
         }
      }
   }

   return {
      nCached: nCached,
      nGenerated: nGenerated,
      nFailed: nFailed
   };
};

/**
 *  Generic function to plagesolve the image window.
 *
 * @param {*} imageWindow the image to be solved
 * @param {*} solverCfgOverrides optional object containing the key/value to configure the solver
 * @param {*} overrides an optional object containing a set of key/value metadata values to override
 * @returns {undefined|Solver} Returns undefined if the plate solve failed, or the instance of the solver on success.
 */
StackEngine.prototype.solveImage = function( referenceImageWindow, solverCfgOverrides, overrides )
{
   let
   {
      computeSTF
   } = WBPPUtils.shared();

   let solver = new ImageSolver();
   if ( InitializeSolver( solver, referenceImageWindow ) )
   {
      resetImageSolverConfiguration( solver );
      if ( overrides != undefined )
      {
         let keys = Object.keys( overrides );
         keys.forEach( k =>
         {
            solver.metadata[ k ] = overrides[ k ];
         } );
      }
      let stfApplied = false;
      if ( solverCfgOverrides )
      {
         let keys = Object.keys( solverCfgOverrides );
         for ( let i = 0; i < keys.length; ++i )
            solver.solverCfg[keys[i]] = solverCfgOverrides[keys[i]];
      }

      for ( ;; )
      {
         if ( solver.error )
            console.warningln( solver.error );

         if ( solver.solverCfg.distortionCorrection == false )
         {
            // warning: distortion correction must be enabled
            ( new MessageBox( "<p>Distortion correction must be enabled to compute an accurate astrometric solution.</p>",
               "Plate Solving",
               StdIcon_Warning,
               StdButton_Ok ) ).execute();
         }
         else
         {
            if ( SolveImage( solver, referenceImageWindow ) )
               // solved
               break;
            else if ( !engine.platesolveFallbackManual )
               // if manual fallback is not checked then we do not attempt to present the interactive GUI
               return undefined;
         }

         // solver failed, prepare the interactive mode
         engine.operationQueue.hideExecutionMonitorDialog();
         if ( !stfApplied )
         {
            referenceImageWindow.mainView.stf = computeSTF( referenceImageWindow.mainView, undefined /* descriptor */ , true /* for STF */ );
            referenceImageWindow.fitWindow();
            referenceImageWindow.position = new Point( 0, 0 );
            stfApplied = true;
         }

         referenceImageWindow.show();
         console.hide();
         let dialog = new ImageSolverDialog( solver.solverCfg, solver.metadata, false /*showTargetImage*/ );
         let res = dialog.execute();
         referenceImageWindow.hide();
         console.show();
         if ( res == 0 )
            return undefined;

         solver.solverCfg = dialog.solverCfg;
         solver.metadata = dialog.metadata;
      }

      return solver;
   }
   return undefined;
}

// ----------------------------------------------------------------------------

/**
 * Initialize the processing.
 * To be invoked before starting the pipeline.
 */
StackEngine.prototype.initializeExecution = function()
{
   let
   {
      existingDirectory,
      timestampString
   } = WBPPUtils.shared();

   this.groupsManager.initializeProcessing();
   this.processContainer = new ProcessContainer;
   this.executionTimestamp = timestampString();
   this.logsFileName = existingDirectory( this.outputDirectory + "/logs" ) + "/" + this.executionTimestamp + ".log";
   this.consoleLogger.initialize( this.title, this.version, this.logsFileName )
};

StackEngine.prototype.flushProcessContainer = function ()
{
   let
   {
      existingDirectory
      } = WBPPUtils.shared();

   // store the log timestamp
   let prefix = this.fastMode ? "F" : "W";
   let filePath = existingDirectory( this.outputDirectory + "/logs" ) + "/" + this.executionTimestamp + "_ProcessContainer.xpsm";
   let str = "<?xml version=\"1.0\"";
   str += "encoding=\"UTF-8\"?>\n<xpsm version=\"1.0\"";
   str += " xmlns=\"http://www.pixinsight.com/xpsm\"";
   str += " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
   str += " xsi:schemaLocation=\"http://www.pixinsight.com/xpsm";
   str += " http://pixinsight.com/xpsm/xpsm-1.0.xsd\">\n";
   str += this.processContainer.toSource( "XPSM 1.0", "BPP_ProcessContainer"/*varId*/,
                  0/*indentSize*/,
                  SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim() + "\n";
   str +="<icon id=\"" + prefix + "BPP_ProcessContainer_" + this.executionTimestamp + "\" instance=\"ProcessContainer_instance\" xpos=\"100\" ypos=\"100\" workspace=\"BPP\"/>\n</xpsm>";

   File.writeTextFile( filePath, str );
}

/**
 * Performs the post-execution actions.
 *
 */
StackEngine.prototype.postExecutionActions = function()
{
   console.writeln();
   console.writeln( "Smart report result" );
   console.writeln();
   console.writeln( this.processLogger.toString() );
   console.resetStatus();
};

// ----------------------------------------------------------------------------
//                                  OPERATIONS
// ----------------------------------------------------------------------------

StackEngine.prototype.calibrationOperation = function( frameGroup )
{
   this.__base__ = BPPOperationBlock;
   this.__base__( "Calibration", frameGroup, true /* trackable */ );

   this.spaceRequired = () =>
   {
      return frameGroup.groupSize();
   };

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Calibration",
      status: this.status,
      statusMessage: this.statusMessage,
      group: frameGroup
   } );

   let
   {
      resultCountToString
   } = WBPPUtils.shared();

   this._run = () =>
   {
      let groupType = StackEngine.imageTypeToString( frameGroup.imageType )

      // calibrate in two steps, first the frames that will be integrated with fast integration
      // and next the others
      let fastIntegrationFrames = [];
      let regularIntegrationFrames = [];
      {
         let activeFrames = frameGroup.activeFrames();
         if ( frameGroup.imageType == ImageType.LIGHT )
         {
            for ( let i = 0; i < activeFrames.length; ++i )
            {
               if ( activeFrames[i].__fastIntegration )
                  fastIntegrationFrames.push( activeFrames[i] );
               else
                  regularIntegrationFrames.push( activeFrames[i] );
            }
         }
         else
            regularIntegrationFrames = activeFrames;
      }

      // prepare the two dataset to calibrate
      let originalItems = frameGroup.fileItems;
      let calibrationDataSet = [{
         fileItems: fastIntegrationFrames.map(item => item.fileItem ),
         doMeasurements: false
      }, {
         fileItems: regularIntegrationFrames.map(item => item.fileItem ),
         doMeasurements: undefined
      }];

      let nCached = 0;
      let nGenerated = 0;
      let nFailed = 0;

      for ( let c = 0; c < calibrationDataSet.length; ++c )
      {
         frameGroup.fileItems = calibrationDataSet[c].fileItems;
         let activeFrames = frameGroup.activeFrames();
         if ( activeFrames.length == 0 )
            continue;

         // calibrate the remaining frames
         let
         {
            calibratedFiles,
            nCached: _nCached,
            nGenerated: _nGenerated
         } = engine.doCalibrate( frameGroup, calibrationDataSet[c].doMeasurements );
         if ( !calibratedFiles )
         {
            // calibration skipped
            frameGroup.fileItems = originalItems;
            return OperationBlockStatus.CANCELED;
         }
         else
         {
            // check which active frame has been successfully processed
            for ( let c = 0; c < activeFrames.length; ++c )
            {
               let inputFile = activeFrames[ c ].current
               let outputFile = calibratedFiles[ c ];

               if ( outputFile != undefined && outputFile.length > 0 )
               {
                  if ( File.exists( outputFile ) )
                  {
                     activeFrames[ c ].processingSucceeded( WBPPFrameProcessingStep.CALIBRATION, outputFile );
                     console.writeln( "Calibration frame " + c + ": <raw>" + inputFile + "</raw> ---> <raw>" + outputFile + "</raw>" );
                  }
                  else
                  {
                     // non-existing file can occur only with processed files
                     nFailed++;
                     _nGenerated--;
                     console.warningln( "** Warning: Calibration frame " + c + ": <raw>" + outputFile + "</raw> ---> [ FAILED: Calibrated file not found ]" );
                     engine.processLogger.addWarning( "File does not exist after image calibration: " + outputFile );
                     activeFrames[ c ].processingFailed();
                  }
               }
               else
               {
                  console.warningln( "** Warning: Calibration frame " + c + ": <raw>" + inputFile + "</raw> ---> [ FAILED ]" );
                  engine.processLogger.addWarning( "Calibration failed for image: " + inputFile );
                  activeFrames[ c ].processingFailed();
                  _nGenerated--;
                  nFailed++;
               }
            }
         }

         nGenerated += _nGenerated;
         nCached += _nCached;
      }
      // report how many frames exist after calibration
      frameGroup.fileItems = originalItems;
      let calibratedActiveFrames = frameGroup.activeFrames();
      if ( calibratedActiveFrames.length < 1 )
      {
         console.warningln( "** Warning: No " + groupType + " frames found after calibration." );
         engine.processLogger.addError( "No " + groupType + " frames found after calibration." );
         this.statusMessage = "no frames found after calibration";
         return OperationBlockStatus.FAILED;
      }

      engine.processLogger.addSuccess( "Calibration completed", calibratedActiveFrames.length + " " + groupType + " frame" + ( calibratedActiveFrames.length == 1 ? "" : "s" ) + " calibrated." );
      this.statusMessage = resultCountToString( nCached, nGenerated, nFailed, "calibrated" );
      this.hasWarnings = nFailed > 0;
      return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.calibrationOperation.prototype = new BPPOperationBlock;

StackEngine.prototype.LPSOperation = function()
{
   this.__base__ = BPPOperationBlock;
   this.__base__( "Linear Defects Correction ", undefined, true /* trackable */ );

   this.spaceRequired = () =>
   {
      let preprocessGroups = engine.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

      let size = 0;
      let bins = preprocessGroups.reduce( ( acc, group ) =>
      {
         acc[ group.binning ] = true;
         return acc
      },
      {} );

      Object.keys( bins ).forEach( bin =>
      {
         let firstFound = false;
         for ( let i = 0; i < preprocessGroups.length; ++i )
            if ( preprocessGroups[ i ].binning == bin && !preprocessGroups[ i ].isCFA )
            {
               size += preprocessGroups[ i ].groupSize();
               if ( !firstFound )
               {
                  // include the master generated for LPS execution
                  firstFound = true;
                  size += preprocessGroups[ i ].frameSize();
               }
            }
      } );

      return size;
   };

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "LPS",
      status: this.status,
      statusMessage: this.statusMessage
   } );

   let
   {
      resultCountToString
   } = WBPPUtils.shared();

   this._run = function()
   {
      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Apply linear defects correction to light frames" );
      console.noteln( SEPARATOR );

      let groupsPRE = engine.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

      let
      {
         nCached,
         nGenerated,
         nFailed
      } = engine.doLinearPatternSubtraction( groupsPRE );

      let resultString = resultCountToString( nCached, nGenerated, nFailed, "corrected" );
      this.statusMessage = ( nCached + nGenerated + nFailed ) + " frame(s)" + ( resultString.length > 0 ? ", " + resultString : "" );
      this.hasWarnings = nFailed > 0;

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* End of linear defects correction process" );
      console.noteln( SEPARATOR );

      return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.LPSOperation.prototype = new BPPOperationBlock;

StackEngine.prototype.CosmeticCorrectionOperation = function( frameGroup )
{
   this.__base__ = BPPOperationBlock;
   this.__base__( "Cosmetic Correction", frameGroup, true /* trackable */ );
   this.spaceRequired = () =>
   {
      return frameGroup.groupSize();
   }
   this.envForScript = () => ( {
      name: "Cosmetic Correction",
      status: this.status,
      statusMessage: this.statusMessage,
      group: frameGroup
   } );
   let
   {
      enableTargetFrames,
      existingDirectory,
      resultCountToString
   } = WBPPUtils.shared();
   this._run = function()
   {
      let cosmeticCorrectionTemplateId = frameGroup.ccData.CCTemplate;
      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Begin cosmetic correction of light frames" );
      console.noteln( SEPARATOR );
      frameGroup.log();
      let activeFrames = frameGroup.activeFrames();
      let filePaths = [];
      let nCached = 0;
      let nGenerated = 0;
      let nFailed = 0;
      let CC = ProcessInstance.fromIcon( cosmeticCorrectionTemplateId );
      if ( CC == null )
      {
         console.warningln( "** Warning: No such process icon: " + cosmeticCorrectionTemplateId + "; cosmetic correction will be skipped." );
         engine.processLogger.addWarning( "No such process icon: " + cosmeticCorrectionTemplateId + "; cosmetic correction will be skipped." );
         this.statusMessage = "process icon not found";
         canceled = true;
      }
      else if ( !( CC instanceof CosmeticCorrection ) )
      {
         console.warningln( "** Warning: The specified icon does not transport an instance " +
            "of CosmeticCorrection: " + cosmeticCorrectionTemplateId + "; cosmetic correction will be skipped." );
         engine.processLogger.addWarning( "The specified icon does not transport an instance " +
            "of CosmeticCorrection: " + cosmeticCorrectionTemplateId + "; cosmetic correction will be skipped." );
         this.statusMessage = "wrong process icon found";
         canceled = true;
      }
      else if ( activeFrames.length == 0 )
      {
         console.warningln( "** Warning: no active frames." );
         engine.processLogger.addWarning( "No active frames." );
         this.statusMessage = "no active frames";
         return OperationBlockStatus.CANCELED;
      }
      else
      {
         console.noteln( "Cosmetic Correction: applying " + cosmeticCorrectionTemplateId + " process icon." );
         engine.processLogger.addMessage( "Running <b>" + cosmeticCorrectionTemplateId + "</b> Cosmetic Correction process icon." );
         let subfolder = frameGroup.folderName();
         let cosmetizedDirectory = existingDirectory( engine.outputDirectory + "/cosmetized/" + subfolder );
         filePaths = activeFrames.map( activeFrame => activeFrame.current );
         CC.outputDir = cosmetizedDirectory;
         CC.outputExtension = ".xisf";
         CC.prefix = "";
         CC.postfix = "_cc";
         CC.overwrite = true;
         CC.cfa = frameGroup.isCFA;
         let CCSource = CC.toSource( "JavaScript", "CC" /*varId*/ , 0 /*indent*/ ,
            SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();
         console.writeln( SEPARATOR2 );
         console.writeln( CCSource );
         console.writeln( SEPARATOR2 );
         /**
          * Skip CC for files that have valid cached data. Cache is a map between an input file and an
          * output file:
          * [inputFile]: outputFile
          */
         let filesToProcess = filePaths;
         let cachedCount = 0;
         let cached = {};
         let CCCache = {};
         let CCcacheKey = engine.executionCache.keyFor( CCSource );
         if ( engine.executionCache.hasCacheForKey( CCcacheKey ) )
         {
            console.noteln( "Cosmetic correction has cached data for key ", CCcacheKey );
            if ( ( !CC.useMasterDark || ( CC.useMasterDark && engine.executionCache.isFileUnmodified( CCcacheKey, CC.masterDarkPath ) ) ) )
            {
               // Cosmetic Correction has already been executed with such configuration and the (eventually used) master
               // dark is unchanged.
               // We avoid to correct frames that have already been processed by this configuration.
               filesToProcess = [];
               CCCache = engine.executionCache.cacheForKey( CCcacheKey );
               for ( let i = 0; i < filePaths.length; ++i )
               {
                  let inputFile = filePaths[ i ];
                  let outputFile = CCCache[ inputFile ];
                  if ( outputFile != undefined &&
                     engine.executionCache.isFileUnmodified( CCcacheKey, inputFile ) &&
                     engine.executionCache.isFileUnmodified( CCcacheKey, outputFile )
                  )
                  {
                     console.noteln( "Cosmatic Correction: cache ", inputFile, " --> ", outputFile );
                     cached[ inputFile ] = outputFile;
                     cachedCount++;
                  }
                  else
                  {
                     console.noteln( "Cosmetic Correction: process ", inputFile );
                     filesToProcess.push( inputFile );
                  }
               }
            }
         }
         else
         {
            console.noteln( "Cosmetic correction has no cached data for key ", CCcacheKey );
         }
         // in process container we store the full CC files
         CC.targetFrames = enableTargetFrames( filePaths, 2 );
         engine.processContainer.add( CC );
         engine.flushProcessContainer();
         if ( filesToProcess.length > 0 )
         {
            CC.targetFrames = enableTargetFrames( filesToProcess, 2 );
            CC.executeGlobal();
         }
         else if ( cachedCount > 0 )
            console.noteln( "Cosmetic Correction: all ", filePaths.length, " files are cached, execution is skipped." )
         /*
          * ### FIXME: CosmeticCorrection should provide read-only output
          * data, including the full file path of each output image.
          */
         for ( let c = 0; c < filePaths.length; ++c )
         {
            let filePath = filePaths[ c ];
            let ccFilePath = cosmetizedDirectory + '/' + File.extractName( filePath ) + "_cc" + ".xisf";
            if ( cached[ filePath ] )
            {
               activeFrames[ c ].processingSucceeded( WBPPFrameProcessingStep.CC, ccFilePath );
               nCached++;
            }
            else
            {
               // we mark as successful the frames that succeeded but we don't mark as failed frames for which CC failed such that wa can continue
               // using the uncosmetized versions
               if ( File.exists( ccFilePath ) )
               {
                  activeFrames[ c ].processingSucceeded( WBPPFrameProcessingStep.CC, ccFilePath );
                  // cache the result
                  CCCache[ filePath ] = ccFilePath;
                  engine.executionCache.cacheFileLMD( CCcacheKey, filePath );
                  engine.executionCache.cacheFileLMD( CCcacheKey, ccFilePath );
                  nGenerated++;
               }
               else
               {
                  console.warningln( "** Warning: File does not exist after cosmetic correction: <raw>" + ccFilePath + "</raw>, the uncosmetized frame <raw>" + activeFrames[ c ].current + "</raw> will be used." );
                  engine.processLogger.addWarning( "File does not exist after cosmetic correction: " + ccFilePath + ", the uncosmetized frame " + activeFrames[ c ].current + " will be used." );
                  nFailed++;
               }
            }
         }
         if ( nFailed == activeFrames.length )
         {
            console.warningln( "** Warning: All cosmetic corrected light frame files have been removed or cannot be accessed. Uncosmetized frames will be used." );
            engine.processLogger.addWarning( "All cosmetic corrected light frame files have been removed or cannot be accessed. Uncosmetized frames will be used." );
            this.statusMessage = "no corrected frames found";
            this.hasWarnings = true;
         }
         else if ( nFailed == 0 )
         {
            engine.processLogger.addSuccess( "Cosmetic correction completed", activeFrames.length + " light frame" + ( activeFrames.length > 1 ? "s have" : "has" ) + " been calibrated." );
         }
         else
         {
            let successCount = activeFrames.length - nFailed;
            engine.processLogger.addSuccess( "Cosmetic correction completed", successCount + " light frame" + ( successCount > 1 ? "s have" : "has" ) + " been cosmetized." );
            this.statusMessage = nFailed + " frame" + ( nFailed > 1 ? "s" : "" ) + " failed";
            this.hasWarnings = true;
         }
         // update the cache
         engine.executionCache.setCache( CCcacheKey, CCCache );
         if ( CC.useMasterDark )
            engine.executionCache.cacheFileLMD( CCcacheKey, CC.masterDarkPath );
         console.noteln( "<end><cbr><br>", SEPARATOR );
         console.noteln( "* End cosmetic correction of light frames" );
         console.noteln( SEPARATOR );
      }
      this.statusMessage = resultCountToString( nCached, nGenerated, nFailed, "corrected" );
      this.hasWarnings = this.hasWarnings || nFailed > 0;
      // return the proper status
      return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.CosmeticCorrectionOperation.prototype = new BPPOperationBlock;

StackEngine.prototype.DebayerOperation = function( frameGroup )
{
   this.__base__ = BPPOperationBlock;
   this.__base__( "Debayer", frameGroup, true /* trackable */ );

   this.spaceRequired = () =>
   {
      let groupsize = frameGroup.groupSize();
      let size = 0;
      switch ( engine.debayerOutputMethod )
      {
         case WBPPDebayerOutputMode.COMBINED:
            size = groupsize * 3; // from mono to RGB
            break;
         case WBPPDebayerOutputMode.BOTH:
            size = groupsize * 6; // consider the combined RGB output (3x) + 3 mono channels
         case WBPPDebayerOutputMode.SEPARATED:
            if ( engine.debayerActiveChannelR )
               size += groupsize; // R only is Gray
            if ( engine.debayerActiveChannelG )
               size += groupsize; // G only is Gray
            if ( engine.debayerActiveChannelB )
               size += groupsize; // B only is Gray
      }

      return size;
   };

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Debayer",
      status: this.status,
      statusMessage: this.statusMessage,
      group: frameGroup
   } );

   let
   {
      enableTargetFrames,
      existingDirectory,
      resultCountToString
   } = WBPPUtils.shared();

   this._run = function()
   {
      let failed = false;

      if ( frameGroup.isCFA )
      {

         console.noteln( "<end><cbr><br>", SEPARATOR );
         console.noteln( "* Begin demosaicing of light frames" );
         console.noteln( SEPARATOR );

         let activeFrames = frameGroup.activeFrames();
         let nCached = 0;
         let nGenerated = 0;
         let nFailed = 0;

         if ( activeFrames.length == 0 )
         {
            // report the issue
            console.warningln( "** Warning: No active light frames in the group." );
            engine.processLogger.addError( "No active light frames in the group." );
            this.statusMessage = "no active frames";
            return OperationBlockStatus.CANCELED;
         }
         else
         {

            engine.processLogger.addSuccess( "Demosaicing with pattern", frameGroup.CFAPatternString() );
            frameGroup.log();
            console.writeln( "CFA pattern: ", frameGroup.CFAPatternString() );
            console.writeln( "Demosaicing method: ", frameGroup.debayerMethodString() );

            let DB = new Debayer;

            let subfolder = frameGroup.folderName();
            let debayerDirectory = existingDirectory( engine.outputDirectory + "/debayered/" + subfolder );

            // we separate the active frames in two groups. one contains frames that are integrated with fast integration,
            // the other contains the remaining frames in the group.
            let fastIntegrationFrames = [];
            let regularFrames = [];
            for ( let i = 0; i < activeFrames.length; ++i )
               if ( activeFrames[i].__fastIntegration )
                  fastIntegrationFrames.push( activeFrames[i] );
               else
                  regularFrames.push( activeFrames[i] );
            let debayreData = [
            {
               frames: fastIntegrationFrames,
               doMeasure: false
            },
            {
               frames: regularFrames,
               doMeasure: engine.subframeWeightingEnabled
            }];

            let channels = [];

            for ( let dd = 0; dd < debayreData.length; ++dd )
            {

               let localActiveFrames = debayreData[dd].frames;
               if ( localActiveFrames.length == 0 )
                  continue;

               let filePaths = localActiveFrames.map( activeFrame => activeFrame.current );
               DB.inputHints = engine.inputHints();
               DB.cfaPattern = frameGroup.CFAPattern;
               DB.debayerMethod = frameGroup.debayerMethod;
               // N.B. For CFAs, evaluate noise and signal with Debayer instead of ImageCalibration
               DB.evaluateNoise = DB.evaluateSignal = !this.fastMode && debayreData[dd].doMeasure;
               DB.outputDirectory = debayerDirectory;
               DB.outputExtension = ".xisf";
               DB.outputPostfix = "_d";
               DB.overwriteExistingFiles = false;

               /**
                * Check if some files are already cached and can be skipped
                */
               let DBSource = DB.toSource( "JavaScript", "DB" /*varId*/ , 0 /*indent*/ ,
                  SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();

               // we set the output configuration after creating the caching keys to share the cached files amongst all configurations
               DB.outputRGBImages = engine.debayerOutputMethod != WBPPDebayerOutputMode.SEPARATED;
               DB.outputSeparateChannels = engine.debayerOutputMethod != WBPPDebayerOutputMode.COMBINED;

               // define the channels involved
               switch ( engine.debayerOutputMethod )
               {
                  case WBPPDebayerOutputMode.COMBINED:
                     channels = [ "" ];
                     break;
                  case WBPPDebayerOutputMode.SEPARATED:
                     channels = [ WBPPAssociatedChannel.R, WBPPAssociatedChannel.G, WBPPAssociatedChannel.B ];
                     break;
                  default:
                     channels = [ "", WBPPAssociatedChannel.R, WBPPAssociatedChannel.G, WBPPAssociatedChannel.B ];
               }

               let filesToDebayer = filePaths;
               let cachedCount = 0;
               let cached = {};
               let DBCache = {};
               let DBcacheKey = engine.executionCache.keyFor( DBSource );
               if ( engine.executionCache.hasCacheForKey( DBcacheKey ) )
               {
                  console.noteln( "Debayer has cached data for key ", DBcacheKey );

                  // The cache is a map with the input file and the array of output files previously generated
                  DBCache = engine.executionCache.cacheForKey( DBcacheKey );
                  filesToDebayer = [];

                  for ( let i = 0; i < filePaths.length; ++i )
                  {
                     let inputFile = filePaths[ i ];
                     let outputFiles = DBCache[ inputFile ];

                     // output files is always an array of 4 file paths, RGB, R, G and B channels.
                     // depending on the debayer output mode, only the proper output files are checked

                     if ( outputFiles == undefined )
                        // we don't have any cached result for this input file
                        filesToDebayer.push( inputFile );
                     else
                     {
                        // the original file must be unchanged
                        let useCache = engine.executionCache.isFileUnmodified( DBcacheKey, inputFile );

                        // all output channel files must be unchanged
                        for ( let j = 0; ( j < channels.length ) && useCache; ++j )
                        {
                           let channel = channels[ j ];
                           let outputFile = outputFiles[ channel ];
                           useCache = engine.executionCache.isFileUnmodified( DBcacheKey, outputFile );
                        }

                        if ( useCache )
                        {
                           cached[ inputFile ] = outputFiles;
                           cachedCount++;
                           console.noteln( "Debayer will use cache for file: ", inputFile );
                        }
                        else
                        {
                           filesToDebayer.push( inputFile );
                           console.noteln( "Debayer will demosaic: ", inputFile );
                        }
                     }
                  }
               }
               else
               {
                  console.noteln( "Debayer has no cached data for key ", DBcacheKey );
               }

               // process is saved in container with the full list of files to be debayered
               {
                  DB.targetItems = enableTargetFrames( filePaths, 2 );

                  let DBSource = DB.toSource( "JavaScript", "DB" /*varId*/ , 0 /*indent*/ ,
                     SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();

                  console.writeln( SEPARATOR2 );
                  console.writeln( DBSource );
                  console.writeln( SEPARATOR2 );
                  engine.processContainer.add( DB );
                  engine.flushProcessContainer();
               }

               let debayeredFiles = [];
               let success = true;
               if ( filesToDebayer.length > 0 )
               {
                  DB.targetItems = enableTargetFrames( filesToDebayer, 2 );
                  success = DB.executeGlobal();
               }
               else
                  console.noteln( "Debayer has no files to process" );

               // track which output files are referenced by the cache
               let cachedInputFiles = {};

               let j = 0;
               filePaths.forEach( filePath =>
               {
                  let fileMap = {};

                  if ( cached[ filePath ] != undefined )
                  {
                     let cachedData = cached[ filePath ];
                     for ( let k = 0; k < channels.length; ++k )
                     {
                        let channel = channels[ k ];
                        fileMap[ channel ] = cachedData[ channel ];
                     }
                     cachedInputFiles[ filePath ] = true;
                  }
                  else
                  {
                     let outputRow = DB.outputFileData[ j++ ];
                     let lastIndx = outputRow.length - 1;
                     let DBoutputMap = {};
                     DBoutputMap[ "" ] = outputRow[ 0 ];
                     DBoutputMap[ WBPPAssociatedChannel.R ] = outputRow[ lastIndx - 2 ];
                     DBoutputMap[ WBPPAssociatedChannel.G ] = outputRow[ lastIndx - 1 ];
                     DBoutputMap[ WBPPAssociatedChannel.B ] = outputRow[ lastIndx ];

                     // update the cached data of the only the relevant channels
                     for ( let k = 0; k < channels.length; ++k )
                     {
                        let channel = channels[ k ];
                        fileMap[ channel ] = DBoutputMap[ channel ];
                     }
                     cachedInputFiles[ filePath ] = false;
                  }

                  debayeredFiles.push( fileMap );
               } );

               if ( ( !success || debayeredFiles.length == 0 ) && cachedCount == 0 )
               {
                  // mark all active frames as failed
                  localActiveFrames.forEach( activeFrame => activeFrame.processingFailed() );
                  // report the issue
                  console.warningln( "** Warning: Light frames demosaicing failed." );
                  engine.processLogger.addError( "Light frames demosaicing failed." );
                  nFailed += localActiveFrames.length;
               }
               else
               {
                  // check which acive frame has been successfully processed
                  for ( let c = 0; c < localActiveFrames.length; ++c )
                  {
                     let inputFilePath = localActiveFrames[ c ].current;
                     let currentBaseName = File.extractName( inputFilePath );
                     let outputFiles = debayeredFiles[ c ];
                     let isCached = cachedInputFiles[ inputFilePath ];

                     // process the results for each active channel
                     let frameFailed = false;
                     channels.forEach( channel =>
                     {

                        let outputFilePath = outputFiles[ channel ];

                        if ( outputFilePath != undefined && outputFilePath.length > 0 )
                        {
                           if ( File.exists( outputFilePath ) )
                           {
                              localActiveFrames[ c ].processingSucceeded( WBPPFrameProcessingStep.DEBAYER, outputFilePath, channel );
                              engine.executionCache.cacheFileLMD( DBcacheKey, inputFilePath );
                              engine.executionCache.cacheFileLMD( DBcacheKey, outputFilePath );
                              outputFiles[ channel ] = outputFilePath;
                           }
                           else
                           {
                              let channelName = channel.length > 0 ? " channel " + channel : "";
                              console.warningln( "** Warning: File does not exist after image demosaicing" + channelName + ": <raw>" + currentBaseName + "</raw>" );
                              engine.processLogger.addWarning( "File does not exist after image demosaicing " + channelName + ": " + currentBaseName );
                              localActiveFrames[ c ].processingFailed( channel );
                              frameFailed = true;
                              // invalidate the cache and the LMD for that file
                              outputFiles[ channel ] = undefined;
                              engine.executionCache.invalidateFileLMD( DBcacheKey, inputFilePath );
                              engine.executionCache.invalidateFileLMD( DBcacheKey, outputFilePath );
                           }
                        }
                        else
                        {
                           let channelName = channel.length > 0 ? "channel " + channel + " of " : "";
                           console.warningln( "** Warning: Debayer failed for " + channelName + "frame: <raw>" + currentBaseName + "</raw>" );
                           engine.processLogger.addWarning( "Debayer failed for " + channelName + "frame: " + currentBaseName );
                           localActiveFrames[ c ].processingFailed( channel );
                           frameFailed = true;
                           // invalidate the cache and  the LMD for that file
                           outputFiles[ channel ] = undefined;
                           engine.executionCache.invalidateFileLMD( DBcacheKey, inputFilePath );
                           engine.executionCache.invalidateFileLMD( DBcacheKey, outputFilePath );
                        }
                     } );

                     if ( frameFailed )
                        nFailed++;
                     else if ( isCached )
                        nCached++;
                     else
                        nGenerated++;

                     // update the list of output files for the given input file
                     DBCache[ inputFilePath ] = outputFiles;
                  }
                  // save the DB cache
                  engine.executionCache.setCache( DBcacheKey, DBCache );
               }
            }

            // report the status of the channels
            let emptyChannels = [];
            channels.forEach( channel =>
            {
               let debayerChannelName;
               switch ( channel )
               {
                  case "":
                     debayerChannelName = "combined";
                     break;
                  case WBPPAssociatedChannel.R:
                     debayerChannelName = "separated red";
                     break;
                  case WBPPAssociatedChannel.G:
                     debayerChannelName = "separated green";
                     break;
                  case WBPPAssociatedChannel.B:
                     debayerChannelName = "separated blue";
               }

               let debayeredActiveFrames = frameGroup.activeFrames( channel );
               if ( debayeredActiveFrames.length < 1 )
               {
                  msg = "** Warning: No frames found for " + debayerChannelName + " channel after demosaicing.";
                  console.warningln( msg );
                  engine.processLogger.addError( msg );
                  emptyChannels.push( debayerChannelName );
               }
               else
               {
                  engine.processLogger.addSuccess( "Demosaicing completed", debayeredActiveFrames.length + " light frame" + (debayeredActiveFrames.length == 1 ? "" : "s") + "  demosaiced for " + debayerChannelName + " channel." );
               }
            } )

            if ( emptyChannels.length > 0 )
            {
               let last = emptyChannels.pop();
               let commaSeparatedList = emptyChannels.join( ", " ) + ( emptyChannels.length > 0 ? " and " : "" ) + last;
               this.statusMessage += "\nno frames generated for " + commaSeparatedList + " channel";
            }

            // set the status message
            this.statusMessage = resultCountToString( nCached, nGenerated, nFailed, "demosaiced" );
            this.hasWarnings = nFailed > 0;
         }

         failed = nFailed == activeFrames.length;

         console.noteln( "<end><cbr><br>", SEPARATOR );
         console.noteln( "* End demosaicing of light frames" );
         console.noteln( SEPARATOR );
      }




      if ( failed )
      {
         this.statusMessage = "demosaicing failed";
         return OperationBlockStatus.FAILED;
      }
      else
         return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.DebayerOperation.prototype = new BPPOperationBlock;

StackEngine.prototype.ReferenceFrameDataPreparationOperation = function()
{
   this.__base__ = BPPOperationBlock;
   this.__base__( "Reference frame data preparation", undefined, false /* trackable */ );

   /**
    * data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Reference frame data preparation",
      status: this.status,
      statusMessage: this.statusMessage
   } );

   this._run = function( environment, requestInterruption )
   {
      // here we continue using the post-process groups
      let groups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST );

      // remove non-active groups
      groups = groups.filter( g => g.isActive );

      // returns immediatly if no post-process groups are given (this could be the case when WBPP is used to generate the master
      // bias / dark / flat only).
      if ( groups.length == 0 )
      {
         // no POST calibration groups, we can interrupt the whole processing
         requestInterruption();
         return OperationBlockStatus.CANCELED;
      }

      // ----------------------------------------------------------------------------
      // MEASURING FRAMES
      // ----------------------------------------------------------------------------

      // List of cases which requires to measure the images:
      // 1. if I choose to generate the weights
      // 2. if I do the registration and I want the best reference image to be auto-selcted
      // 3. local normalization is enabled
      let referenceFrameData = {};

      // assign the manual reference frame
      if ( engine.bestFrameReferenceMethod == WBPPBestReferenceMethod.MANUAL )
      {
         // assume that the file is not added to the session
         let currentReferenceImage = engine.referenceImage;
         // check if the reference frame is one frame in the session
         let referenceFrameFileItem = engine.groupsManager.getReferenceFrameFileItem( engine.referenceImage );
         if ( referenceFrameFileItem )
         {
            // precedence is given to the green channel if separate debayer channels has been generated for the reference frame
            if ( referenceFrameFileItem.current[ WBPPAssociatedChannel.G ] )
            {
               currentReferenceImage = referenceFrameFileItem.current[ WBPPAssociatedChannel.G ]
               referenceFrameFileItem.markAsReference( WBPPAssociatedChannel.G );
            }
            else if ( referenceFrameFileItem.current[ "default" ] )
            {
               currentReferenceImage = referenceFrameFileItem.current[ "default" ]
               referenceFrameFileItem.markAsReference( "default" );
            }
            else
            {
               currentReferenceImage = referenceFrameFileItem.filePath;
            }
         }
         // by convention assume to configure the "auto_single" mode with the manual reference file
         referenceFrameData[ "__manual__" ] = {
            /* no groups to be measured */
            groups: [],
            referenceImage: currentReferenceImage
         }
      }
      else if ( engine.bestFrameReferenceMethod == WBPPBestReferenceMethod.AUTO_SINGLE )
      {
         referenceFrameData[ "__single__" ] = {
            /* measure all groups  */
            groups: groups
         }
      }
      else
      {
         // aggregate groups by keyword
         referenceFrameData = groups.reduce( ( acc, group ) =>
         {
            let value = group.keywords[ engine.bestFrameReferenceKeyword ] || "__undefined__";
            if ( acc[ value ] )
               acc[ value ].groups.push( group );
            else
               acc[ value ] = {
                  groups: [ group ]
               };
            return acc;
         },
         {} );
      }

      // for each keyword store the lowest binning
      Object.keys( referenceFrameData ).forEach( key =>
      {
         let groups = referenceFrameData[ key ].groups;
         let lowestBinning = groups.reduce( ( acc, group ) =>
         {
            return Math.min( group.binning, acc );
         }, 256 );
         referenceFrameData[ key ].lowestBinning = lowestBinning;
      } );

      // store the reference frame data for further processing
      environment.referenceFrameData = referenceFrameData;
      return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.ReferenceFrameDataPreparationOperation.prototype = new BPPOperationBlock;

StackEngine.prototype.MeasurementOperation = function()
{
   this.__base__ = BPPOperationBlock;
   this.__base__( "Measurements", undefined, true /* trackable */ );

   /**
    * data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Measurements",
      status: this.status,
      statusMessage: this.statusMessage
   } );

   let
   {
      resultCountToString
   } = WBPPUtils.shared();

   this._run = function( environment )
   {
      let failed = false;

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Perform image measurements" );
      console.noteln( SEPARATOR );
      console.flush();
      engine.processLogger.newLine();
      engine.processLogger.addMessage( "<b>*****************</b> <i>IMAGE MEASUREMENTS</i> <b>**************</b>" );

      // get the reference frame data from the environment
      let referenceFrameData = environment.referenceFrameData;

      // using the active post-process groups
      let groups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST ).filter( g => g.isActive );

      // retrieve the whole list of frames to be measured
      let framesToMeasure = [];

      let generateSubframesWeights = engine.subframeWeightingEnabled && ( engine.subframesWeightsMethod == WBPPSubframeWeightsMethod.FORMULA );
      // we measure all frames only in WBPP
      if ( ( generateSubframesWeights || engine.localNormalization ) && !engine.fastMode )
      {
         // we measure only the first 5 frames in a fast integration group
         for ( let i = 0; i < groups.length; ++i )
         {
            let activeFrames = groups[ i ].activeFrames();
            if ( groups[i].fastIntegrationData.enabled )
               activeFrames.slice( 0, 5 );
            framesToMeasure = framesToMeasure.concat( groups[ i ].activeFrames() );
         }
      }
      else
      {
         // measurement is used only to find the reference frame
         // measure only the groups with the lowest binning for each keyword
         framesToMeasure = Object.keys( referenceFrameData ).reduce( ( acc, key ) =>
         {
            let lowBinningGroups = referenceFrameData[ key ].groups.filter( group => group.binning == referenceFrameData[ key ].lowestBinning );
            return acc.concat( lowBinningGroups );
         }, [] ).reduce( ( acc, group ) =>
         {
            let activeFrames = group.activeFrames();
            // we measure only the first 5 frames in a fast integration group
            if ( group.fastIntegrationData.enabled )
               activeFrames = activeFrames.slice( 0, 5 );
            return acc.concat( activeFrames );
         }, [] );
      }

      if ( framesToMeasure.length == 0 )
      {
         console.criticalln( "*** Error: No active frames to be measured." );
         engine.processLogger.addError( "No active frames to be measured." );
         this.statusMessage = "No active frames to be measured";
         this.hasWarnings = true;
         return OperationBlockStatus.CANCELED;
      }

      // measure the frames
      let
      {
         nCached,
         nMeasured,
         nFailed
      } = engine.computeDescriptors( framesToMeasure );

      console.noteln( "SS nCached   : ", nCached );
      console.noteln( "SS nMeasured : ", nMeasured );
      console.noteln( "SS nFailed   : ", nFailed );

      // report
      if ( ( nMeasured + nCached ) == framesToMeasure.length )
      {
         console.noteln( "All frame measurements completed successfully" );
         engine.processLogger.addSuccess( "Frames measurement", "completed successfully" );
      }
      else if ( nFailed < framesToMeasure.length )
      {
         let measuredCount = nMeasured + nCached;
         console.warningln( "** Warning: Measurements completed successfully for " + measuredCount + " light frame" + (measuredCount == 1 ? "" : "s") + " over " + framesToMeasure.length + "." );
         engine.processLogger.addSuccess( "Measurements completed successfully", measuredCount + " light frame" + (measuredCount == 1 ? "" : "s") + " over " + framesToMeasure.length + "." );
         this.statusMessage = "measurement failed on " + nFailed + " frame" + (nFailed > 1 ? "s" : "");
         this.hasWarnings = true;
      }
      else
      {
         console.criticalln( "*** Error: Frame measurement failed for all light frames." );
         engine.processLogger.addError( "Frame measurement failed for all light frames." );
         this.statusMessage = "measurement failed on all frames";
         failed = true;
      }

      let countString = resultCountToString( nCached, nMeasured, nFailed, "measured" );
      this.statusMessage = countString.length > 0 ? countString : framesToMeasure.length + " measured";
      this.hasWarnings = this.hasWarnings || nFailed > 0;

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* End generation of image descriptors" );
      console.noteln( SEPARATOR );
      console.flush();
      engine.processLogger.addMessage( "<b>" + SEPARATOR + "</b>" );
      engine.processLogger.newLine();

      if ( failed )
         return OperationBlockStatus.FAILED;
      else
         return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.MeasurementOperation.prototype = new BPPOperationBlock;

StackEngine.prototype.CustomFormulaWeightsGenerationOperation = function()
{
   this.__base__ = BPPOperationBlock;
   this.__base__( "Weights generation", undefined, true /* trackable */ );

   /**
    * data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Weights generation",
      status: this.status,
      statusMessage: this.statusMessage
   } );

   let
   {
      resultCountToString
   } = WBPPUtils.shared();

   this._run = function()
   {
      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Generate the custom formula weights" );
      console.noteln( SEPARATOR );
      console.flush();
      engine.processLogger.newLine();
      engine.processLogger.addMessage( "<b>*****************</b> <i>CUSTOM WEIGHTS GENERATION</i> <b>**************</b>" );

      // scan all groups and generate the formula weights, we skip fast integration groups
      let groups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST ).filter( g => g.isActive && !g.fastIntegrationData.enabled );

      let nGenerated = 0;
      let nFailed = 0;
      for ( let i = 0; i < groups.length; ++i )
      {
         let activeFrames = groups[ i ].activeFrames().filter( af => af.descriptor );
         let descriptors = activeFrames.map( frame => frame.descriptor ).filter( Boolean );
         let descriptorMinMax = engine.getMinMaxDescriptorsValues( descriptors );

         // pre-compute non normalized weights
         let scores = activeFrames.map( af =>
         {
            return engine.computeWeightForLight(
               af.descriptor,
               descriptorMinMax,
               engine.FWHMWeight,
               engine.eccentricityWeight,
               engine.SNRWeight,
               engine.starsWeight,
               engine.PSFSignalWeight,
               engine.PSFSNRWeight,
               engine.pedestal,
               1 /*normalization factor*/ ,
               false /* print to console */ );
         } );

         let normalizationFactor = Math.max.apply( null, scores.filter( score => isFinite( score ) ) );

         for ( let j = 0; j < activeFrames.length; ++j )
         {

            let descriptor = activeFrames[ j ].descriptor;

            let weight = engine.computeWeightForLight(
               descriptor,
               descriptorMinMax,
               engine.FWHMWeight,
               engine.eccentricityWeight,
               engine.SNRWeight,
               engine.starsWeight,
               engine.PSFSignalWeight,
               engine.PSFSNRWeight,
               engine.pedestal,
               normalizationFactor,
               true /* print to console */ );


            if ( isFinite( weight ) )
            {
               descriptor.imageWeight = weight;
               descriptor.formulaWeight = weight * normalizationFactor;
               nGenerated++;
            }
            else
            {
               console.warningln( "** Warning: Custom formula failed for frame: <raw>" + activeFrames[ j ].current + "</raw>" );
               activeFrames[ j ].processingFailed();
               nFailed++;
            }
         }
      }

      this.statusMessage = resultCountToString( 0 /* nCached */ , nGenerated, nFailed, "weight(s) generated" );
      this.hasWarnings = nFailed > 0;

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* End generation of image weights" );
      console.noteln( SEPARATOR );
      console.flush();


      if ( nFailed == 0 )
         engine.processLogger.addSuccess( "Success", nGenerated + " weight(s) generated" );
      else
         engine.processLogger.addWarning( this.statusMessage );
      engine.processLogger.addMessage( "<b>" + SEPARATOR + "</b>" );
      engine.processLogger.newLine();

   };
};
StackEngine.prototype.CustomFormulaWeightsGenerationOperation.prototype = new BPPOperationBlock;

StackEngine.prototype.BadFramesRejectionOperation = function()
{
   this.__base__ = BPPOperationBlock;
   this.__base__( "Bad frames rejection", undefined, true /* trackable */ );

   /**
    * data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Bad frames rejection",
      status: this.status,
      statusMessage: this.statusMessage
   } );

   let _weightForFrame = function( activeFrame )
   {
      let descriptor = activeFrame.descriptor;
      if ( descriptor == undefined )
         return undefined;

      switch ( engine.subframesWeightsMethod )
      {
         case WBPPSubframeWeightsMethod.PSFSignal:
            return descriptor.PSFSignalWeight;
         case WBPPSubframeWeightsMethod.PSFSNR:
            return descriptor.PSFSNR;
         case WBPPSubframeWeightsMethod.PSFScaleSNR:
            return undefined;
         case WBPPSubframeWeightsMethod.SNREstimate:
            return descriptor.SNR;
         case WBPPSubframeWeightsMethod.FORMULA:
            return descriptor.imageWeight;
      }
      return undefined;
   }

   this._run = function()
   {

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Bad frames rejection" );
      console.noteln( SEPARATOR );
      engine.processLogger.newLine();
      engine.processLogger.addMessage( "<b>*****************</b> <i>BAD FRAMES REJECTION</i> <b>**************</b>" );

      // we exclude fast integration groups
      let groups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST ).filter( g => g.isActive && !g.fastIntegrationData.enabled );
      let discardedFrames = 0;

      if ( engine.subframesWeightsMethod != WBPPSubframeWeightsMethod.PSFScaleSNR )
      {
         // using the active post-process groups

         for ( let i = 0; i < groups.length; ++i )
         {
            let group = groups[ i ];

            // we don't implement any frame rejection if fast integration is enabled
            if ( group.fastIntegrationData.enabled )
               continue;

            let activeFrames = group.activeFrames();

            // find the max value of the current weighting method
            let maxValue = activeFrames.reduce( ( value, frame ) =>
            {
               let weight = _weightForFrame( frame );

               if ( weight == undefined )
                  return value;

               return Math.max( weight, value );
            }, 0 );

            // if all weights are invalid then skip the group filtering
            if ( maxValue == 0 )
               continue;

            for ( let j = 0; j < activeFrames.length; ++j )
            {
               let normalizedWeight = ( _weightForFrame( activeFrames[ j ] ) || 0 ) / maxValue;
               let info = File.extractNameAndExtension( activeFrames[ j ].current ) +
                  " - " +
                  format( "%.3f", normalizedWeight ) +
                  ( normalizedWeight < engine.minWeight ? " < " : " > " ) +
                  format( "%.3f", engine.minWeight ) +
                  " | " +
                  ( normalizedWeight < engine.minWeight ? "rejected" : "accepted" );

               console.noteln( "[Frames rejection] ", info );
               if ( normalizedWeight < engine.minWeight )
               {
                  engine.processLogger.addWarning( "frame rejected [" + info + "]: " + activeFrames[ j ].current );
                  activeFrames[ j ].processingFailed();
                  discardedFrames++;
               }
            }
         }
      }

      this.statusMessage = discardedFrames + " rejected";

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* End of bad frames rejection" );
      console.noteln( SEPARATOR );

      engine.processLogger.addMessage( this.statusMessage );
      engine.processLogger.addMessage( "<b>" + SEPARATOR + "</b>" );
      engine.processLogger.newLine();
   };
};
StackEngine.prototype.BadFramesRejectionOperation.prototype = new BPPOperationBlock;

StackEngine.prototype.ReferenceFrameSelectionOperation = function()
{
   this.__base__ = BPPOperationBlock;
   this.__base__( "Reference frame selection", undefined, true /* trackable */ );

   /**
    * data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Reference frame selection",
      status: this.status,
      statusMessage: this.statusMessage
   } );

   this._run = function( environment, requestInterruption )
   {
      let criticalErrorOccurred = false;

      engine.processLogger.newLine();
      engine.processLogger.addMessage( "<b>**********</b> <i>BEST REFERENCE FRAME FOR REGISTRATION</i> <b>***********</b>" );

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Begin selection of the best reference frame" );
      console.noteln( SEPARATOR );
      console.flush();

      // get the reference frame data from the environment
      let referenceFrameData = environment.referenceFrameData;

      // using the active post-process groups
      let groups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST ).filter( g => g.isActive );

      // initialize the reference frames per group ( only if mode is not cached, otherwise we keep it to reuse the same reference frame )
      if ( !engine.reuseLastReferenceFrames )
         for ( let i = 0; i < groups.length; ++i )
            groups[ i ].__reference_frame__ = undefined;
      if ( !engine.reuseLastLNReferenceFrames )
         for ( let i = 0; i < groups.length; ++i )
            groups[ i ].__ln_reference_frame__ = undefined;


      let nSelected = 0;
      let nFailed = 0;

      if ( engine.bestFrameReferenceMethod == WBPPBestReferenceMethod.MANUAL )
      {
         // search in all files the selected frame, in case take as reference the
         // current processed file name
         console.noteln( "Best reference frame, manually selected: " + referenceFrameData[ "__manual__" ].referenceImage );
         engine.processLogger.addSuccess( "Best reference frame", " manually selected " + referenceFrameData[ "__manual__" ].referenceImage );

         // save reference frame for all groups
         for ( let i = 0; i < groups.length; ++i )
            groups[ i ].__reference_frame__ = referenceFrameData[ "__manual__" ].referenceImage;
         this.statusMessage = "Manual";
      }
      else
      {
         if ( engine.bestFrameReferenceMethod == WBPPBestReferenceMethod.AUTO_SINGLE )
         {
            let actualReferenceImage = engine.findRegistrationReferenceFileItem( referenceFrameData[ "__single__" ].groups );
            if ( actualReferenceImage )
            {
               console.noteln( "Best reference frame for registration - auto selection completed: " + actualReferenceImage.current );
               engine.processLogger.addSuccess( "Best reference frame", " (auto selection) " + actualReferenceImage.current );

               // save reference frame for all groups
               for ( let i = 0; i < groups.length; ++i )
                  groups[ i ].__reference_frame__ = actualReferenceImage.current;

               // mark the frame as reference
               actualReferenceImage.markAsReference();
               nSelected++;
            }
            else
            {
               // mark all frames as failed
               for ( let i = 0; i < groups.length; ++i )
               {
                  let activeFrames = groups[ i ].activeFrames();
                  for ( let j = 0; j < activeFrames.length; ++j )
                     activeFrames[ j ].processingFailed();
               }

               console.criticalln( "*** Error: Unable to detect the best reference frame." );
               engine.processLogger.addError( "Unable to detect the best reference frame." );
               this.statusMessage = "failed to detect the reference frame";
               criticalErrorOccurred = true;
            }
         }
         else
         {
            let keywordsFailed = [];
            let referenceFrameDataKeys = Object.keys( referenceFrameData );
            referenceFrameData = referenceFrameDataKeys.reduce( ( acc, keywordValue ) =>
            {
               let data = referenceFrameData[ keywordValue ];
               let actualReferenceImage = engine.findRegistrationReferenceFileItem( data.groups );
               console.noteln( "<end><cbr><br>" );
               if ( actualReferenceImage )
               {
                  console.noteln( "Best reference frame for " + engine.bestFrameReferenceKeyword + " = " + keywordValue + " : " + actualReferenceImage.current );
                  engine.processLogger.addSuccess( "Best reference frame for " + engine.bestFrameReferenceKeyword + " = " + keywordValue, actualReferenceImage.current );

                  // store the reference image in the current groups per keyword
                  for ( let i = 0; i < data.groups.length; ++i )
                     data.groups[ i ].__reference_frame__ = actualReferenceImage.current;

                  // mark the frame as reference
                  actualReferenceImage.markAsReference();
                  nSelected++;
               }
               else
               {
                  // mark all frames in the current goups per keyword as failed
                  for ( let i = 0; i < data.groups.length; ++i )
                  {
                     let activeFrames = data.groups[ i ].activeFrames();
                     for ( let j = 0; j < activeFrames.length; ++j )
                        activeFrames[ j ].processingFailed();
                  }

                  let msg = "Unable to detect the best reference frame for " + engine.bestFrameReferenceKeyword + " = " + keywordValue +
                     ". Groups with this key/value will not be registered.";
                  console.criticalln( "*** Error: " + msg );
                  engine.processLogger.addError( msg );
                  keywordsFailed.push( keywordValue );
                  nFailed++;
               }
               return acc;
            },
            {} );

            if ( keywordsFailed.length == referenceFrameDataKeys.length )
            {
               this.statusMessage = "failed to detect all reference frames";
               criticalErrorOccurred = true;
            }
            else if ( keywordsFailed.length > 0 )
            {
               let last = keywordsFailed.pop();
               let commaSeparatedList = keywordsFailed.join( ", " ) + ( keywordsFailed.length > 0 ? " and " : "" ) + last;
               this.statusMessage = "failed to assign a reference frame for " + commaSeparatedList + " keyword" + ( keywordsFailed.length > 1 ? "s" : "" );
               this.hasWarnings = true;
            }

            console.noteln( "<end><cbr><br>", SEPARATOR );
            console.noteln( "* End selection of the best reference frame" );
            console.noteln( "<end><cbr><br>", SEPARATOR );
            console.flush();

            this.statusMessage = nSelected + " frame(s) selected";
            if ( nFailed > 0 )
               this.statusMessage += ", " + nFailed + " failed";
         }
      }

      engine.processLogger.addMessage( "<b>" + SEPARATOR + "</b>" );
      engine.processLogger.newLine();

      if ( criticalErrorOccurred )
      {
         requestInterruption();
         return OperationBlockStatus.CANCELED;
      }

      return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.ReferenceFrameSelectionOperation.prototype = new BPPOperationBlock;

function resetImageSolverConfiguration( solver )
{
   /*
    * Default parameters for ImageSolver version 6.3.1 (core version 1.9.2)
    */
   solver.solverCfg.useActive = true;
   solver.solverCfg.files = [];
   solver.solverCfg.catalogMode = CatalogMode.prototype.Automatic;
   solver.solverCfg.vizierServer = "https://vizier.cds.unistra.fr/";
   solver.solverCfg.magnitude = 12;
   solver.solverCfg.maxIterations = 100;
   solver.solverCfg.structureLayers = 5;
   solver.solverCfg.minStructureSize = 0;
   solver.solverCfg.hotPixelFilterRadius = 1;
   solver.solverCfg.noiseReductionFilterRadius = 0;
   solver.solverCfg.sensitivity = 0.5;
   solver.solverCfg.peakResponse = 0.5;
   solver.solverCfg.brightThreshold = 3.0;
   solver.solverCfg.maxStarDistortion = 0.6;
   solver.solverCfg.autoPSF = false;
   solver.solverCfg.generateErrorImg = false;
   solver.solverCfg.showStars = false;
   solver.solverCfg.catalog = "PPMXL";
   solver.solverCfg.autoMagnitude = true;
   solver.solverCfg.showStarMatches = false;
   solver.solverCfg.showSimplifiedSurfaces = false;
   solver.solverCfg.showDistortion = false;
   solver.solverCfg.distortionCorrection = true;
   solver.solverCfg.rbfType = WCS_DEFAULT_RBF_TYPE;
   solver.solverCfg.maxSplinePoints = WCS_DEFAULT_MAX_SPLINE_POINTS;
   solver.solverCfg.splineOrder = 2;
   solver.solverCfg.splineSmoothing = 0.005;
   solver.solverCfg.enableSimplifier = true;
   solver.solverCfg.simplifierRejectFraction = 0.10;
   solver.solverCfg.outlierDetectionRadius = 160;
   solver.solverCfg.outlierDetectionMinThreshold = 4.0;
   solver.solverCfg.outlierDetectionSigma = 5.0;
   solver.solverCfg.generateDistortModel = false;
   solver.solverCfg.outSuffix = "_ast";
   solver.solverCfg.projection = 0;
   solver.solverCfg.projectionOriginMode = 0;
   solver.solverCfg.restrictToHQStars = false;
   solver.solverCfg.intersectionMode = IntersectionMode.prototype.Automatic;
   solver.solverCfg.tryApparentCoordinates = true;
   solver.solverCfg.tryExhaustiveInitialAlignment = false;
}

function InitializeSolver( solver, imageWindow )
{
   function defaults()
   {
      solver.metadata.dec = engine.imageSolverDec;
      solver.metadata.epoch = engine.imageSolverEpoch;
      solver.metadata.focal = engine.imageSolverFocalLength;
      solver.metadata.ra = engine.imageSolverRa;
      solver.metadata.referenceSystem = "ICRS";
      solver.metadata.topocentric = false;
      solver.metadata.useFocal = true;
      solver.metadata.xpixsz = engine.imageSolverPixelSize;
      // derived
      solver.metadata.resolution = isNaN( solver.metadata.focal ) ? 0 : solver.metadata.xpixsz / solver.metadata.focal * 0.18 / Math.PI;
      solver.metadata.SaveParameters();
   }

   try
   {
      // always init the default values
      defaults();
      solver.Init( imageWindow, engine.imageSolverForceDefaults );
      return true;
   }
   catch ( e )
   {
      console.warningln( e );
      return false;
   }
}

function SolveImage( solver, image )
{
   try
   {
      let result = solver.SolveImage( image );
      if ( result )
      {
         // save configuration and metadata on success
         solver.solverCfg.SaveSettings();
         solver.solverCfg.SaveParameters();
         solver.metadata.SaveSettings();
         solver.metadata.SaveParameters();

         engine.imageSolverEpoch = solver.metadata.epoch;
         engine.imageSolverDec = solver.metadata.dec;
         engine.imageSolverRa = solver.metadata.ra;
         engine.imageSolverFocalLength = solver.metadata.focal;
         engine.imageSolverPixelSize = solver.metadata.xpixsz;
      }
      return result;
   }
   catch ( e )
   {
      console.warningln( e );
      return false;
   }
}

StackEngine.prototype.RegistrationOperation = function( frameGroup )
{
   this.__base__ = BPPOperationBlock;
   this.__base__( "Registration", frameGroup, true /* trackable */ );

   this.spaceRequired = () =>
   {
      return frameGroup.groupSize();
   };

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Registration",
      status: this.status,
      statusMessage: this.statusMessage,
      group: frameGroup
   } );

   let
   {
      enableTargetFrames,
      existingDirectory,
      resultCountToString
   } = WBPPUtils.shared();

   this._run = function()
   {
      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Begin registration of light frames" );
      console.noteln( SEPARATOR );
      frameGroup.log();
      console.flush();

      let activeFrames = frameGroup.activeFrames();

      if ( activeFrames.length == 0 )
      {
         console.warningln( "** Warning: No active light frames to be registered." );
         engine.processLogger.addError( "No active light frames to be registered." );
         this.statusMessage = "no active frames";
         return OperationBlockStatus.CANCELED;
      }

      console.noteln( "<br>Reference image: ", frameGroup.__reference_frame__ );
      engine.processLogger.addSuccess( "Reference image", frameGroup.__reference_frame__ );

      let subfolder = frameGroup.folderName();
      let registerDirectory = existingDirectory( engine.outputDirectory + "/registered/" + subfolder );
      let filePaths = activeFrames.map( item => item.current );

      let SA = new StarAlignment;
      SA.inputHints = engine.inputHints();
      SA.outputHints = engine.outputHints();
      SA.referenceImage = frameGroup.__reference_frame__;
      SA.referenceIsFile = true;
      SA.outputDirectory = registerDirectory;
      SA.generateDrizzleData = true;
      SA.pixelInterpolation = engine.pixelInterpolation;
      SA.clampingThreshold = engine.clampingThreshold;
      SA.structureLayers = engine.structureLayers;
      SA.hotPixelFilterRadius = engine.hotPixelFilterRadius;
      SA.noiseReductionFilterRadius = engine.noiseReductionFilterRadius;
      SA.minStructureSize = engine.minStructureSize;
      SA.sensitivity = engine.sensitivity;
      SA.peakResponse = engine.peakResponse;
      SA.brightThreshold = engine.brightThreshold;
      SA.maxStarDistortion = engine.maxStarDistortion;
      SA.allowClusteredSources = engine.allowClusteredSources;
      SA.useTriangles = engine.useTriangleSimilarity;
      SA.outputExtension = ".xisf";
      SA.outputPrefix = "";
      SA.outputPostfix = "_r";
      SA.outputSampleFormat = StarAlignment.prototype.f32;
      SA.overwriteExistingFiles = false;
      SA.inheritAstrometricSolution = true;
      SA.rigidTransformations = engine.rigidTransformations;

      // Override star alignment parameters if the current group is a separated RGB channel
      if ( engine.distortionCorrection || frameGroup.associatedRGBchannel != undefined )
      {
         SA.distortionCorrection = true;
         SA.rbfType = StarAlignment.prototype.DDMThinPlateSpline;
         SA.maxSplinePoints = 4000;
         SA.splineOrder = 2;
      }
      else
         SA.maxStars = engine.maxStars;

      if ( frameGroup.associatedRGBchannel != undefined )
         SA.noiseReductionFilterRadius = Math.max( 2, engine.noiseReductionFilterRadius );

      let SASource = SA.toSource( "JavaScript", "SA" /*varId*/ , 0 /*indent*/ ,
         SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();
      console.writeln( SEPARATOR2 );
      console.writeln( SASource );
      console.writeln( SEPARATOR2 );

      /*
       * Skip alignment for files that have valid cached data. Cache is a map between an input file and an
       * output file:
       * [inputFile]: outputFile
       */
      let filesToRegister = filePaths;
      let cachedCount = 0;
      let cached = {};
      let SACache = {};
      let SAcacheKey = engine.executionCache.keyFor( SASource );
      if ( engine.executionCache.hasCacheForKey( SAcacheKey ) )
      {
         console.noteln( "StarAlignment: cache data for key ", SAcacheKey );

         if ( engine.executionCache.isFileUnmodified( SAcacheKey, frameGroup.__reference_frame__ ) )
         {
            // Star Alignment has already been executed with such configuration and the registered frame is unchanged.
            // We avoid to align frames that have already been processed by this configuration.
            // The cache data is a map between input and output (registered) file, if both are unchanged then
            // we remove them from the list of the files to be aligned.
            filesToRegister = [];
            SACache = engine.executionCache.cacheForKey( SAcacheKey );
            for ( let i = 0; i < filePaths.length; ++i )
            {
               let inputFile = filePaths[ i ];
               let outputFile = SACache[ inputFile ];
               let drizzleFilePath = outputFile != undefined ? File.changeExtension( outputFile, ".xdrz" ) : "";

               if ( outputFile != undefined &&
                  engine.executionCache.isFileUnmodified( SAcacheKey, inputFile ) &&
                  engine.executionCache.isFileUnmodified( SAcacheKey, outputFile ) &&
                  engine.executionCache.isFileUnmodified( SAcacheKey, drizzleFilePath )
               )
               {
                  console.noteln( "StarAlignment: cache ", inputFile, " --> ", outputFile );
                  cached[ inputFile ] = outputFile;
                  cachedCount++;
               }
               else
               {
                  console.noteln( "StarAlignment: align ", inputFile );
                  filesToRegister.push( inputFile );
               }
            }
         }
         else
         {
            console.noteln( "StarAlignment: reference frame has changed, ignore cache and register all frames." );
         }
      }
      else
      {
         console.noteln( "StarAlignment: no cache data for key ", SAcacheKey );
      }

      // in process container we store the full SA measured files
      SA.targets = enableTargetFrames( filePaths, 3 );
      engine.processContainer.add( SA );
      engine.flushProcessContainer();

      // set the files to be measured and proceed
      let success = true;
      if ( filesToRegister.length > 0 )
      {
         SA.targets = enableTargetFrames( filesToRegister, 3 );
         success = SA.executeGlobal();
         engine.executionCache.cacheFileLMD( SAcacheKey, frameGroup.__reference_frame__ );
      }

      if ( !success && cachedCount == 0 )
      {
         // critical error, the output data must always have the same size of the input data
         // mark all active frames as failed
         activeFrames.forEach( activeFrame => activeFrame.processingFailed() );
         // report the issue
         console.warningln( "** Warning: Error registering light frames. This group will be skipped." );
         engine.processLogger.addError( "Error registering light frames. This group will be skipped." );
         this.statusMessage = "registration failed";
         return OperationBlockStatus.FAILED;
      }

      if ( filesToRegister.length > 0 && cachedCount == 0 )
         if ( !SA.outputData || SA.outputData.length != filesToRegister.length )
         {
            // critical error, the output data must always have the same size of the input data
            // mark all active frames as failed
            activeFrames.forEach( activeFrame => activeFrame.processingFailed() );
            // report the issue
            console.warningln( "** Warning: Light frames registration failed: the output data size mismatches the input data size." );
            engine.processLogger.addError( "Light frames registration failed: output data size mismatch." );
            this.statusMessage = "a critical error occurred (data size mismatch).";
            return OperationBlockStatus.FAILED;
         }

      // create a support for output file matching
      let registeredFiles = SA.outputData.map( outputItem => outputItem[ 0 ] );

      let nCached = 0;
      let nRegistered = 0;
      let nFailed = 0;

      // scan all input files and detect the corresponding registered version
      let j = 0;
      for ( let c = 0; c < activeFrames.length; ++c )
      {
         let inputFile = filePaths[ c ];
         let outputFile;
         let cachedFile = false;

         // input file may be mapped in the cache or picked from the SA output list (if not cached)
         if ( cached[ inputFile ] != undefined )
         {
            cachedFile = true;
            outputFile = cached[ inputFile ];
         }
         else
            // just aligned
            outputFile = registeredFiles[ j++ ];

         let errorMsg = "";
         let success = false;

         if ( outputFile )
         {
            if ( outputFile.length > 0 )
            {
               if ( File.exists( outputFile ) )
               {
                  success = true;
                  activeFrames[ c ].processingSucceeded( WBPPFrameProcessingStep.REGISTRATION, outputFile );

                  // store the drizzle file if present
                  let drizzleFilePath = File.changeExtension( outputFile, ".xdrz" );
                  if ( File.exists( drizzleFilePath ) )
                     activeFrames[ c ].addDrizzleFile( drizzleFilePath );

                  // cache the input and output file data if not already cached
                  if ( !cachedFile )
                  {
                     SACache[ inputFile ] = outputFile;
                     engine.executionCache.cacheFileLMD( SAcacheKey, inputFile );
                     engine.executionCache.cacheFileLMD( SAcacheKey, outputFile );
                     engine.executionCache.cacheFileLMD( SAcacheKey, drizzleFilePath );
                  }

                  if ( cachedFile )
                     nCached++;
                  else
                     nRegistered++;
               }
               else
               {
                  errorMsg = ": Registered frame not found " + outputFile;
                  engine.processLogger.addWarning( "File does not exist after image registration: " + outputFile );
                  activeFrames[ c ].processingFailed();
                  nFailed++;
               }
            }
            else
            {
               errorMsg = ": output file name is an empty string";
               engine.processLogger.addWarning( "Registration failed for image: " + inputFile );
               activeFrames[ c ].processingFailed();
               nFailed++;
            }
         }
         else
         {
            errorMsg = ": Empty output file name";
            engine.processLogger.addWarning( "Registration failed for image: " + inputFile );
            activeFrames[ c ].processingFailed();
            nFailed++;
         }

         if ( success )
            console.writeln( "Registered frame " + c + ": <raw>" + inputFile + "</raw> ---> <raw>" + outputFile + "</raw>" );
         else
            console.warningln( "** Warning: Registered frame " + c + ": <raw>" + inputFile + "</raw> ---> [ FAILED" + errorMsg + " ]" );
      }

      // update the cache for this Star Alignment configuration
      engine.executionCache.setCache( SAcacheKey, SACache );

      let registeredFrames = frameGroup.activeFrames();
      if ( registeredFrames.length < 1 )
      {
         console.warningln( "** Warning: No light frames found after registration." );
         engine.processLogger.addError( "No light frames found after registration." );
         this.statusMessage = "no light frames found after registration";
         return OperationBlockStatus.FAILED;
      }

      engine.processLogger.addSuccess( "Registration completed", registeredFrames.length + " images out of " + activeFrames.length + " successfully registered." );

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* End registration of light frames" );
      console.noteln( SEPARATOR );
      console.flush();

      this.statusMessage = resultCountToString( nCached, nRegistered, nFailed, "registered" );
      this.hasWarnings = nFailed > 0;

      return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.RegistrationOperation.prototype = new BPPOperationBlock;

StackEngine.prototype.LocalNormalizationReferenceFrameSelectionOperation = function( frameGroup )
{
   this.__base__ = BPPOperationBlock;
   let title = "";
   let trackable = false;
   if ( engine.localNormalizationInteractiveMode )
   {
      title = "LN reference [interactive]";
      trackable = true;
   }
   else if ( engine.localNormalizationReferenceFrameGenerationMethod == WBPPLocalNormalizationReferenceFrameMethod.INTEGRATION_BEST_FRAMES )
   {
      title = "LN reference generation";
      trackable = true;
   }
   else
   {
      trackable = false;
   }
   this.__base__( title, frameGroup, trackable );

   this.spaceRequired = () =>
   {
      return frameGroup.frameSize();
   };

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: title,
      status: this.status,
      statusMessage: this.statusMessage,
      group: frameGroup
   } );

   this._run = function( environment )
   {
      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Local normalization reference frame selection" );
      console.noteln( SEPARATOR );
      frameGroup.log();
      console.flush();

      let activeFrames = frameGroup.activeFrames();

      if ( activeFrames.length == 0 )
      {
         console.warningln( "** Warning: No active light frames to be locally normalized." );
         engine.processLogger.addError( "No active light frames to be locally normalized." );
         this.statusMessage = "no active frames";
         return OperationBlockStatus.CANCELED;
      }

      if ( engine.localNormalizationInteractiveMode )
      {
         console.noteln( "-" );
         console.noteln( "STARTING Local Normalization Interactive Session" );
         console.noteln( "-" );
         engine.operationQueue.hideExecutionMonitorDialog();
         let LNReferenceFrameSelectionWindow = new WBPPLocalNormalizationReferenceSelector( frameGroup );
         LNReferenceFrameSelectionWindow.execute();
         console.noteln( "-" );
         console.noteln( "Local Normalization Interactive Session TERMINATED" );
         console.noteln( "-" );
         let lnReferenceFrame = LNReferenceFrameSelectionWindow.referenceFrame;


         if ( lnReferenceFrame == undefined )
         {
            console.warningln( "** Warning: Unable to find the reference frame generated during the interactive mode." );
            engine.processLogger.addError( "Unable to find the reference frame generated during the interactive mode." );
            this.statusMessage = "no reference frame defined";
            return OperationBlockStatus.FAILED;
         }

         console.noteln( "<end><cbr><br>", SEPARATOR );
         console.noteln( "* End selection of local normalization reference frame" );
         console.noteln( SEPARATOR );
         console.flush();

         // inject the reference frame in the environment
         frameGroup.__ln_reference_frame__ = lnReferenceFrame;
         return OperationBlockStatus.DONE;
      }
      else
      {
         // select the reference frame
         let
         {
            lnReferenceFilePath,
            cached
         } = engine.generateLNReference( frameGroup );

         if ( lnReferenceFilePath == undefined )
         {
            console.warningln( "** Warning: Unable to determine the local normalization reference frame. " +
               "Local normalization will be skipped for this group." );
            engine.processLogger.addError( "Unable to determine the local normalization reference frame. " +
               "Local normalization will be skipped for this group." );
            this.statusMessage = "unable to find a local normalization reference frame";
            return OperationBlockStatus.FAILED;
         }

         console.noteln( "<end><cbr><br>", SEPARATOR );
         console.noteln( "* End selection of local normalization reference frame" );
         console.noteln( SEPARATOR );
         console.flush();

         // inject the reference frame in the environment
         frameGroup.__ln_reference_frame__ = lnReferenceFilePath;
         if ( cached )
            this.statusMessage = "cached";
         return OperationBlockStatus.DONE;
      }
   };
};
StackEngine.prototype.LocalNormalizationReferenceFrameSelectionOperation.prototype = new BPPOperationBlock;

StackEngine.prototype.LocalNormalizationOperation = function( frameGroup )
{
   this.__base__ = BPPOperationBlock;
   this.__base__( "Local Normalization", frameGroup, true /* trackable */ );

   this.spaceRequired = () =>
   {
      if ( engine.localNormalizationGenerateImages )
      {
         return frameGroup.groupSize();
      }
      else
      {
         return 0;
      }
   };

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Local Normalization",
      status: this.status,
      statusMessage: this.statusMessage,
      group: frameGroup
   } );

   let
   {
      enableTargetFrames,
      getImageSize,
      resultCountToString
   } = WBPPUtils.shared();

   this._run = function( environment )
   {
      let failed = false;

      let activeFrames = frameGroup.activeFrames();

      if ( activeFrames.length == 0 )
      {
         console.warningln( "** Warning: No active light frames to be locally normalized." );
         engine.processLogger.addError( "No active light frames to be locally normalized." );
         this.statusMessage = "no active frames";
         return OperationBlockStatus.CANCELED;
      }

      // select the reference frame
      let lnReferenceFrame = frameGroup.__ln_reference_frame__;

      if ( lnReferenceFrame == undefined )
      {
         console.warningln( "** Warning: Unable to determine the local normalization reference frame. " +
            "Local normalization will be skipped for this group." );
         engine.processLogger.addError( "Unable to determine the local normalization reference frame. " +
            "Local normalization will be skipped for this group." );
         this.statusMessage = "unable to find a local normalization reference frame";
         return OperationBlockStatus.FAILED;
      }

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Begin local normalization of light frames" );
      console.noteln( SEPARATOR );
      frameGroup.log();
      console.flush();

      console.noteln( "* Local normalization reference image: <raw>" + lnReferenceFrame + "</raw>" );
      engine.processLogger.addSuccess( "Local normalization reference image", lnReferenceFrame );

      let referenceImageSize = getImageSize( lnReferenceFrame );
      let imageRefrenceDimension = Math.min( referenceImageSize.width, referenceImageSize.height );

      let LN = new LocalNormalization;
      LN.overwriteExistingFiles = true;
      let filePaths = activeFrames.map( item => item.current );
      LN.referencePathOrViewId = lnReferenceFrame;
      LN.referenceIsView = false;
      LN.scale = imageRefrenceDimension / engine.localNormalizationGridSize;
      LN.referenceRejectionThreshold = 3.00;
      LN.targetRejectionThreshold = 3.20;
      LN.psfMaxStars = engine.localNormalizationPsfMaxStars;
      LN.psfMinSNR = engine.localNormalizationPsfMinSNR;
      LN.psfAllowClusteredSources = engine.localNormalizationPsfAllowClusteredSources;
      LN.lowClippingLevel = engine.localNormalizationLowClippingLevel;
      LN.highClippingLevel = engine.localNormalizationHighClippingLevel;
      LN.scaleEvaluationMethod = ( engine.localNormalizationMethod == 0 ) ?
         LocalNormalization.prototype.ScaleEvaluationMethod_PSFSignal :
         LocalNormalization.prototype.ScaleEvaluationMethod_MultiscaleAnalysis;
      LN.psfType = [
         LocalNormalization.prototype.PSFType_Gaussian,
         LocalNormalization.prototype.PSFType_Moffat15,
         LocalNormalization.prototype.PSFType_Moffat4,
         LocalNormalization.prototype.PSFType_Moffat6,
         LocalNormalization.prototype.PSFType_Moffat8,
         LocalNormalization.prototype.PSFType_MoffatA,
         LocalNormalization.prototype.PSFType_Auto
      ][ engine.localNormalizationPsfType ];
      LN.psfGrowth = engine.localNormalizationPsfGrowth;

      if ( engine.localNormalizationGenerateImages )
         LN.generateNormalizedImages = LocalNormalization.prototype.GenerateNormalizedImages_Always;

      // Since core version 1.8.9-1, LocalNormalization can generate .xnml
      // tagged as invalid when relative scale factor evaluation fails. These
      // special files are recognized by ImageIntegration and the corresponding
      // images are excluded from the integration. This allows us to be
      // tolerant of normalization errors.
      LN.generateInvalidData = true;

      let LNSource = LN.toSource( "JavaScript", "LN" /*varId*/ , 0 /*indent*/ ,
         SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();

      // Check if valid cached data is present
      let filesToNormalize = filePaths;
      let cachedCount = 0;
      let cached = {};
      let LNCache = {};
      let LNcacheKey = engine.executionCache.keyFor( LNSource );
      if ( engine.executionCache.hasCacheForKey( LNcacheKey ) )
      {
         console.noteln( "Local Normalization has cached data for key ", LNcacheKey );

         if ( engine.executionCache.isFileUnmodified( LNcacheKey, LN.referencePathOrViewId ) )
         {
            // valid cache found. The cache is a map between the input file and the output LN file
            console.noteln( "Local Normalization has cached data for key ", LNcacheKey );

            LNCache = engine.executionCache.cacheForKey( LNcacheKey );
            filesToNormalize = [];

            for ( let i = 0; i < filePaths.length; ++i )
            {
               let inputFile = filePaths[ i ];
               let lnFile = LNCache[ inputFile ];
               if ( lnFile != undefined &&
                  engine.executionCache.isFileUnmodified( LNcacheKey, inputFile ) &&
                  engine.executionCache.isFileUnmodified( LNcacheKey, lnFile ) )
               {
                  cached[ inputFile ] = lnFile;
                  cachedCount++;
                  console.noteln( "Local Normalization cache for input found: ", inputFile, " --> ", lnFile );
               }
               else
               {
                  filesToNormalize.push( inputFile );
                  console.noteln( "Local Normalization input file will be normalized: ", inputFile );
               }
            }
         }
      }
      else
      {
         console.noteln( "Local Normalization has no cached data for key ", LNcacheKey );
      }

      // write the process into the console
      console.writeln( SEPARATOR2 );
      console.writeln( LNSource );
      console.writeln( SEPARATOR2 );

      // process is saved in container with the full list of files to be normalized
      LN.targetItems = enableTargetFrames( filePaths, 2 );
      engine.processContainer.add( LN );
      engine.flushProcessContainer();

      // perform LN if there are files to normalize
      let success = true;
      if ( filesToNormalize.length > 0 )
      {
         LN.targetItems = enableTargetFrames( filesToNormalize, 2 );
         success = LN.executeGlobal();
         engine.executionCache.cacheFileLMD( LNcacheKey, LN.referencePathOrViewId );
      }

      let nCached = 0;
      let nSuccess = 0;
      let nFailed = 0;

      if ( !success && cachedCount == 0 )
      {
         console.warningln( "** Warning: Error applying local normalization to light frames. This group will be skipped." );
         engine.processLogger.addError( "Error applying local normalization to light frames. This group will be skipped." );
         failed = true;
      }
      else if ( filesToNormalize.length > 0 && cachedCount == 0 && ( !LN.outputData || LN.outputData.length != filesToNormalize.length ) )
      {
         // skip local normalization
         console.warningln( "** Warning: Local normalization issue occurred. Local normalization will not be applied." );
         engine.processLogger.addWarning( "Local normalization issue occurred. Local normalization will not be applied" );
         failed = true;
      }
      else
      {
         let lnFiles = [];
         let j = 0;
         for ( let i = 0; i < filePaths.length; ++i )
         {
            let inputFile = filePaths[ i ];
            if ( cached[ inputFile ] != undefined )
            {
               lnFiles.push(
               {
                  path: cached[ inputFile ],
                  valid: true,
                  cached: true
               } );
            }
            else
            {
               let lnFile = LN.outputData[ j++ ];
               lnFiles.push(
               {
                  path: lnFile[ 0 ] || "",
                  valid: lnFile[ 5 ] || false,
                  cached: false
               } );
            }
         }

         // ensure that valid LN files have been created for each file
         for ( let k = 0; k < lnFiles.length; ++k )
         {
            if ( lnFiles[ k ].path.length == 0 )
            {
               console.warningln( "** Warning: Local normalization generation failed for file: <raw>" + activeFrames[ k ].current + "</raw>" );
               engine.processLogger.addWarning( "Local normalization failed for file: " + activeFrames[ k ].current );
               activeFrames[ k ].processingFailed();
               nFailed++;
            }
            else if ( !File.exists( lnFiles[ k ].path ) )
            {
               console.warningln( "** Warning: Local normalization data file not found for file: <raw>" + activeFrames[ k ].current + "</raw>" );
               engine.processLogger.addWarning( "Local normalization data file not found for file: " + activeFrames[ k ].current );
               activeFrames[ k ].processingFailed();
               nFailed++;
            }
            else
            {
               if ( !lnFiles[ k ].valid )
               {
                  console.warningln( "** Warning: Invalid local normalization data generated for file: <raw>" + activeFrames[ k ].current + "</raw>" );
                  engine.processLogger.addWarning( "Invalid local normalization data generated for file: " + activeFrames[ k ].current );
                  activeFrames[ k ].processingFailed();
                  nFailed++;
               }
               else
               {
                  // valid LN files are cached
                  LNCache[ filePaths[ k ] ] = lnFiles[ k ].path;
                  engine.executionCache.cacheFileLMD( LNcacheKey, filePaths[ k ] );
                  engine.executionCache.cacheFileLMD( LNcacheKey, lnFiles[ k ].path );
                  activeFrames[ k ].addLocalNormalizationFile( lnFiles[ k ].path );
                  if ( lnFiles[ k ].cached )
                     nCached++;
                  else
                     nSuccess++;
               }
            }
         }
         // save the updated cache
         engine.executionCache.setCache( LNcacheKey, LNCache );

      }

      engine.processLogger.addSuccess( "Local normalization", "completed." );
      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* End local normalization of light frames" );
      console.noteln( SEPARATOR );
      console.flush();

      this.statusMessage = resultCountToString( nCached, nSuccess, nFailed, "completed" );
      this.hasWarnings = nFailed > 0;
      if ( failed )
         return OperationBlockStatus.FAILED;
      else
         return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.LocalNormalizationOperation.prototype = new BPPOperationBlock;

StackEngine.prototype.ImageIntegrationOperation = function( frameGroup )
{
   this.__base__ = BPPOperationBlock;
   this.__base__( "Integration", frameGroup, true /* trackable */ );

   this.spaceRequired = () =>
   {
      return engine.generateRejectionMaps ? frameGroup.frameSize() * 3 : frameGroup.frameSize();
   }

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Integration",
      status: this.status,
      statusMessage: this.statusMessage,
      group: frameGroup
   } );

   let
   {
      getLastModifiedDate,
      isEmptyString
   } = WBPPUtils.shared();

   this._run = function()
   {
      let activeFrames = frameGroup.activeFrames();

      if ( activeFrames.length == 0 )
      {
         console.warningln( "** Warning: No active frames to be integrated." );
         engine.processLogger.addError( "No active frames to be integrated." );
         this.statusMessage = "no active frames";
         return OperationBlockStatus.CANCELED;
      }

      if ( activeFrames.length < 3 )
      {
         console.warningln( "** Warning: Less than 3 frames to integrated." );
         engine.processLogger.addError( "Less than 3 frames to integrated." );
         this.statusMessage = "only " + activeFrames.length + " active frames";
         return OperationBlockStatus.CANCELED;
      }

      let groupType = StackEngine.imageTypeToString( frameGroup.imageType );

      // we store the xnml and xdrz file LMD to check if they have been updated after the integration, if needed
      let LMDs = {};

      if ( frameGroup.isDrizzleEnabled() )
         for ( let i=0; i < activeFrames.length; ++i )
         {
            let drizzleFile = activeFrames[i].drizzleFile;
            if ( drizzleFile != undefined )
               LMDs[drizzleFile] = getLastModifiedDate( drizzleFile );
         }

      // do integrate
      // overscan-specific intervention: when we integrate a master flat and the overscan is enabled we
      // store two custon keywords to remind the original frame sizes, this will help the matching and the addition
      // of this special cropped master
      let keywords = [];
      if ( frameGroup.imageType == ImageType.FLAT && engine.overscan.enabled )
      {
         keywords.push( new FITSKeyword( "PREOVSCW", format( "%d", frameGroup.size.width ), "Width of the original flat frames before overscan" ) );
         keywords.push( new FITSKeyword( "PREOVSCH", format( "%d", frameGroup.size.height ), "Height of the original flat frames before overscan" ) );
      }
      let
      {
         masterFilePath,
         cached,
         numberOfImages
      } = engine.doIntegrate(
         frameGroup,
         undefined /* custom prefix */ ,
         undefined /* custom postfix */ ,
         undefined /* customGenerateRejectionMaps */ ,
         undefined /* customGenerateDrizzle */ ,
         undefined /* desiredFileName */ ,
         {} /* overrideIIparameters */ ,
         keywords
      );

      // check the result
      if ( isEmptyString( masterFilePath ) )
      {
         console.warningln( "** Warning: Master " + groupType + " file was not generated." );
         engine.processLogger.addError( "Warning: Master " + groupType + " file was not generated." );
         this.statusMessage = "master file not generated";
         return OperationBlockStatus.FAILED;
      }

      if ( frameGroup.imageType != ImageType.LIGHT )
      {
         // add the created master file
         console.writeln( "Add the master file: <raw>" + masterFilePath + "</raw>" );
         engine.addFile( masterFilePath );
      }
      else
      {
         // store the master file associated to the group ID into the environment for further processing
         // this info is used, for example, by channel recombination
         console.writeln( "Set the group's master file: <raw>" + masterFilePath + "</raw>" );
         frameGroup.setMasterFileName( masterFilePath );

         // we disable the light frame if the xnml files are supposed to be updated but are unchanged after integration
         // but drizzle integration is enabled, in this case the drizzle integration would complain that no
         // normalization information is present, so we set the light frame as failed.
         // this may only happen if the frame has not been integrated because of the low weight.
         if ( !cached && frameGroup.isDrizzleEnabled() )
            for ( let i=0; i < activeFrames.length; ++i )
            {
               let drizzleFile = activeFrames[i].drizzleFile;
               if ( LMDs[drizzleFile] == getLastModifiedDate( drizzleFile ) )
               {
                  activeFrames[i].processingFailed();
                  console.writeln( "Drizzle data for frame <raw>" + activeFrames[i].fileItem.filePath + "</raw> has not been updated." )
               }
            }
      }

      engine.processLogger.addSuccess( "Integration completed", "master " + groupType + " saved at path " + masterFilePath );
      this.statusMessage = "" + numberOfImages + ( cached ? " cached" : " integrated" );
      console.noteln( "numberOfImages: ", numberOfImages );
      if ( numberOfImages < activeFrames.length )
         this.statusMessage += (this.statusMessage.length > 0 ? ", " : "") + (activeFrames.length - numberOfImages) + " rejected";
      return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.ImageIntegrationOperation.prototype = new BPPOperationBlock;

StackEngine.prototype.FastIntegrationOperation = function( frameGroup )
{
   this.__base__ = BPPOperationBlock;
   this.__base__( "Fast Integration", frameGroup, true /* trackable */ );

   this.spaceRequired = () =>
   {
      if ( frameGroup.fastIntegrationSaveImageEnabled() )
         return frameGroup.frameSize() * frameGroup.fileItems.length;
      else
         return 0;
   };

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "FastIntegration",
      status: this.status,
      statusMessage: this.statusMessage,
      group: frameGroup
   } );

   let
   {
      getLastModifiedDate,
      isEmptyString
   } = WBPPUtils.shared();

   this._run = function()
   {
      let activeFrames = frameGroup.activeFrames();

      if ( activeFrames.length == 0 )
      {
         console.warningln( "** Warning: No active frames to be integrated." );
         engine.processLogger.addError( "No active frames to be integrated." );
         this.statusMessage = "no active frames";
         return OperationBlockStatus.CANCELED;
      }

      let groupType = StackEngine.imageTypeToString( frameGroup.imageType );

      // we store the xnml and xdrz file LMD to check if they have been updated after the integration, if needed
      let LMDs = {};

      if ( frameGroup.isDrizzleEnabled() )
         for ( let i=0; i < activeFrames.length; ++i )
         {
            let drizzleFile = activeFrames[i].drizzleFile;
            if ( drizzleFile != undefined )
               LMDs[drizzleFile] = getLastModifiedDate( drizzleFile );
         }

      let
      {
         masterFilePath,
         cached,
         numberOfImages
      } = engine.doFastIntegration(
         frameGroup,
         frameGroup.__reference_frame__
      );

      // check the result
      if ( isEmptyString( masterFilePath ) )
      {
         console.warningln( "** Warning: Master " + groupType + " file was not generated." );
         engine.processLogger.addError( "Warning: Master " + groupType + " file was not generated." );
         this.statusMessage = "master file not generated";
         return OperationBlockStatus.FAILED;
      }

      // store the master file associated to the group ID into the environment for further processing
      // this info is used, for example, by channel recombination
      console.writeln( "Set the group's master file: <raw>" + masterFilePath + "</raw>" );
      frameGroup.setMasterFileName( masterFilePath );

      engine.processLogger.addSuccess( "Fast integration completed (" + numberOfImages + " of " + activeFrames.length + " integrated)", "master " + groupType + " saved at path " + masterFilePath );
      this.statusMessage = "" + numberOfImages + ( cached ? " cached" : " integrated" );
      console.noteln( "numberOfImages: ",numberOfImages );
      if ( numberOfImages < activeFrames.length )
         this.statusMessage += (this.statusMessage.length > 0 ? ", " : "") + (activeFrames.length - numberOfImages) + " failed";
      this.hasWarnings = activeFrames.length != numberOfImages;
      return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.FastIntegrationOperation.prototype = new BPPOperationBlock;


StackEngine.prototype.DrizzleIntegrationOperation = function( frameGroup )
{
   this.__base__ = BPPOperationBlock;
   this.__base__( "Drizzle Integration (" + frameGroup.drizzleScale() + "x)" + ( frameGroup.drizzleFast() ? " Fast" : "" ), frameGroup, true /* trackable */ );

   this.spaceRequired = () =>
   {
      return frameGroup.frameSize() * frameGroup.drizzleScale() * frameGroup.drizzleScale();
   };

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Drizzle Integration",
      status: this.status,
      statusMessage: this.statusMessage,
      group: frameGroup
   } );

   let
   {
      isEmptyString
   } = WBPPUtils.shared();

   this._run = function( environment )
   {
      let activeFrames = frameGroup.activeFrames();

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Begin drizzle integration of ", StackEngine.imageTypeToString( frameGroup.imageType ) + " frames" );
      console.noteln( SEPARATOR );

      if ( activeFrames.length == 0 )
      {
         console.warningln( "** Warning: No active frames; drizzle integration skipped." );
         engine.processLogger.addError( "No active frames; drizzle integration skipped." );
         this.statusMessage = "no active frames";
         return OperationBlockStatus.CANCELED;
      }
      else
      {
         // we proceed with drizzle integration only if image integration has succeeded
         let masterFileName = frameGroup.getMasterFileName();
         if ( masterFileName == undefined || !File.exists( masterFileName ) )
         {
            console.warningln( "** Warning: No active master light has been generated. Drizzle integration is skipped." );
            engine.processLogger.addError( "No active master light has been generated. Drizzle integration is skipped." );
            this.statusMessage = "no master light found";
            return OperationBlockStatus.CANCELED;
         }
      }

      let groupType = StackEngine.imageTypeToString( frameGroup.imageType );

      // apply drizzle integration

      let
      {
         masterFilePath,
         cached
      } = engine.doDrizzleIntegration(
         frameGroup,
         frameGroup.drizzleFast() /* fast */ ,
         frameGroup.drizzleScale() /* scale */ ,
         frameGroup.drizzleDropShrink() /* shrink */ ,
         frameGroup.drizzleFunction() /* kernel */ ,
         undefined /* custom prefix */ ,
         "_drizzle_" + frameGroup.drizzleScale() + "x" /* custom postfix */ ,
         undefined /* desiredFileName */ ,
         undefined /* overrideDIparameters */ ,
         [] /* keywords */
      );

      // check the result
      if ( isEmptyString( masterFilePath ) || !File.exists( masterFilePath ) )
      {
         console.warningln( "** Warning: Master " + groupType + " file was not generated." );
         engine.processLogger.addError( "Warning: Master " + groupType + " file was not generated." );
         this.statusMessage = "master file not generated";
         return OperationBlockStatus.FAILED;
      }

      if ( frameGroup.imageType != ImageType.LIGHT )
         // add the created master file
         engine.addFile( masterFilePath );
      else
         // store the master file associated to the group ID into the environment for further processing
         // this info is used, for example, by channel recombination
         frameGroup.setMasterFileName( masterFilePath, WBPPMasterType.DRIZZLE );


      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* End drizzle integration of " + StackEngine.imageTypeToString( frameGroup.imageType ) + " frames" );
      console.noteln( SEPARATOR );

      engine.processLogger.addSuccess( "Drizzle Integration completed", "master " + groupType + " saved at path " + masterFilePath );
      this.statusMessage = cached ? "cached" : "";
      return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.DrizzleIntegrationOperation.prototype = new BPPOperationBlock;

StackEngine.prototype.AutoCropOperation = function()
{
   this.__base__ = BPPOperationBlock;
   this.__base__( "Autocrop", undefined, true /* trackable */ );

   let
   {
      keywords,
      resultCountToString
   } = WBPPUtils.shared();

   this.spaceRequired = () =>
   {
      // in the average case, we expect the cropped version to have a slightly smaller size than the original file.
      // We assume 95 % as the heuristic factor.
      let groups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST ).filter( g => !g.isHidden );
      return groups.reduce( ( a, g ) =>
      {
         let size = g.frameSize();
         if ( g.isDrizzleEnabled() )
            size += g.drizzledFrameSize();
         return a + size;
      }, 0 );
   };

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Autocrop",
      status: this.status,
      statusMessage: this.statusMessage
   } );

   this._run = function( environment )
   {

      let totalToCrop = 0;
      let nSuccess = 0;
      let nCached = 0;
      let nFailed = 0;

      let allPostGroups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST ).filter( g => g.isActive );
      console.writeln( "Autocrop: active groups to be processed = ", allPostGroups.length );

      // we collect groups that shares the same reference frame. For each reference frame, all
      // groups will be cropped on the same region
      let refGroupsMap = allPostGroups.reduce( ( a, g ) =>
      {
         // we ignore the recombined rgb channels group
         if ( g.__reference_frame__ != undefined )
         {
            if ( g.associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB )
            {
               if ( a[ g.__reference_frame__ ] == undefined )
                  a[ g.__reference_frame__ ] = [];
               a[ g.__reference_frame__ ].push( g );
            }
         }
         else
         {
            console.writeln( "Autocrop: group has no reference frame: ", g.toShortString() );
         }
         return a;
      },
      {} );
      let refFrames = Object.keys( refGroupsMap );
      console.writeln( "Autocrop: reference frames = ", refFrames.length );

      // the categories of master files to be cropped are
      // 1. ImageIntegration master files
      // 2. DrizzleIntegration master files
      // for each category a list of cropping groups are crated, in each cropping group all masters
      // shares the same reference frame
      let croppingGroups = [];

      // add the image integration masters files. We pick the master files names from the environment as they have to be
      // created by the execution of ImageIntegration first in the pipeline.
      // If ImageIntegration did fail, the group may remain active but no master file name is stored into the environment,
      // in that case we need to filter out these groups.
      [
      {
         desc: "image integration master light",
         isDrizzle: false
      },
      {
         desc: "drizzle master light",
         isDrizzle: true
      } ].forEach( item =>
      {
         refFrames.forEach( referenceFrame =>
         {
            let groups = refGroupsMap[ referenceFrame ];
            let filteredGroups = [];

            // we first filter the groups that has a valid master file name assocaited for both
            // master lights and drizzled master lights.
            groups.forEach( g =>
            {
               if ( !item.isDrizzle || ( item.isDrizzle && g.isDrizzleEnabled() ) )
               {
                  if ( g.getMasterFileName( item.isDrizzle ? WBPPMasterType.DRIZZLE : WBPPMasterType.MASTER_LIGHT ) != undefined )
                     filteredGroups.push( g );
                  else
                     console.warningln( "** Warning: Autocrop: ", item.desc, " not available for group ", g.toShortString() );
               }
            } );

            if ( filteredGroups.length > 0 )
            {

               let type = item.isDrizzle ? WBPPMasterType.DRIZZLE : WBPPMasterType.MASTER_LIGHT;
               let filePaths = filteredGroups.map( g => ( g.getMasterFileName( type ) ) );
               totalToCrop += filteredGroups.length;
               croppingGroups.push(
               {
                  groups: filteredGroups,
                  filePaths: filePaths,
                  referenceFrame: referenceFrame,
                  isDrizzle: item.isDrizzle
               } );
            }
         } );
      } );
      croppingGroups.filter( cg => cg.groups.length > 0 );

      // log the crop groups
      croppingGroups.forEach( ( cg ) =>
      {
         console.noteln( "Autocrop: masters group for reference frame ", File.extractNameAndExtension( cg.referenceFrame ) );
         cg.filePaths.forEach( ( f, j ) =>
         {
            console.noteln( "Autocrop:     ", j, ". ", File.extractNameAndExtension( f ) );
         } );
      } );

      console.writeln( "Autocrop: the active crop groups count is ", croppingGroups.length );

      // loop through all aggregated groups and
      //    determine the common crop region
      //    crop all frames in the same group using the common crop region
      for ( let i = 0; i < croppingGroups.length; ++i )
      {
         let groups = croppingGroups[ i ].groups;
         let filePaths = croppingGroups[ i ].filePaths;
         let referenceFrame = croppingGroups[ i ].referenceFrame;
         let isDrizzle = croppingGroups[ i ].isDrizzle;

         console.writeln();
         console.writeln( "Autocrop: ---------------------------------------------" );
         console.writeln( "Autocrop: crop group ", i );
         console.writeln( "Autocrop: crop the following masters on reference frame: <raw>" + referenceFrame + "</raw>" );
         filePaths.forEach( f =>
         {
            console.writeln( "Autocrop: <raw>" + f + "</raw>" );
         } )
         console.writeln( "Autocrop: ---------------------------------------------" );
         console.writeln();

         // we check if the reference frame has already been cropped, if this is the case then
         // no crop needs to be applied.
         // This is the case where we selected an already cropped frame as reference frame; when this
         // happens the aligned frame already has the same frame size of the reference frame.
         let isAutocrop = keywords.readFileKeyword( referenceFrame, AUTOCROP_KEYWORD );
         if ( isAutocrop )
         {
            nSuccess == groups.length;
            console.writeln( "Autocrop: reference frame has already been cropped by WBPP; skip the cropping." );
            engine.processLogger.addMessage( "reference frame " + referenceFrame + " has already been cropped by WBPP; skip cropping " + filePaths.length + " master" + ( filePaths.length == 1 ? "" : "s" ) + " using it as the reference." );
            continue;
         }

         // check if cache exists
         // the cache key is generated from the concatenated sorted list of masters to be cropped
         let ACcacheKey = engine.executionCache.keyFor( filePaths.join( "|" ) );
         let ACCache = {
            previouslyFailed: false,
            cropRects:
            {}
         };
         let inputDataIsUnchanged = true;
         let outputDataIsUnchanged = true;

         // cache handling
         // we skip processing the whole grop if
         // - input files are unchanged
         // - the previous execution failed, so we already know that re-executing the autocrop will fail again
         // - the previous execution did not fail and the output files are unchanged
         if ( engine.executionCache.hasCacheForKey( ACcacheKey ) )
         {
            ACCache = engine.executionCache.cacheForKey( ACcacheKey );
            if ( ACCache.cropRects == undefined )
            {
               console.noteln( "Autocrop: missing crop rectangles cached data. Reprocess the master files." );
               ACCache.cropRects = {};
            }
            else
            {
               console.noteln( "Autocrop: has cached data for the key ", ACcacheKey );
               console.noteln( "Autocrop: check input files " );
               // all input files must be unchanged
               for ( let j = 0; inputDataIsUnchanged && j < filePaths.length; ++j )
               {
                  inputDataIsUnchanged = engine.executionCache.isFileUnmodified( ACcacheKey, filePaths[ j ] );
                  if ( !inputDataIsUnchanged )
                  {
                     console.noteln( "Autocrop: file has changed since last autocrop execution, ", filePaths[ j ] );
                     break;
                  }
                  else
                     console.noteln( "Autocrop: file is unmodified since last autocrop execution, ", filePaths[ j ] );
               }

               if ( inputDataIsUnchanged && ACCache.previouslyFailed )
               {
                  console.noteln( "Autocrop: input data is unchanged but the previous execution failed. Autocrop will be skipped." );
                  nFailed += groups.length;
                  continue;
               }
               else if ( inputDataIsUnchanged && !ACCache.previouslyFailed )
               {
                  console.noteln( "Autocrop: check output files " );
                  for ( let j = 0; outputDataIsUnchanged && j < filePaths.length; ++j )
                  {
                     let croppedFilePath = File.appendToName( filePaths[ j ], "_autocrop" );
                     outputDataIsUnchanged = engine.executionCache.isFileUnmodified( ACcacheKey, croppedFilePath );
                     if ( !outputDataIsUnchanged )
                     {
                        console.noteln( "Autocrop: file has changed since last autocrop execution, ", filePaths[ j ] );
                        break;
                     }
                     else
                        console.noteln( "Autocrop: file is unmodified since last autocrop execution, ", filePaths[ j ] );
                  }
               }

               // if data is unchanged if input files are unchanged and the previous execution failed or did not failed and output files are unchanged
               if ( inputDataIsUnchanged && outputDataIsUnchanged )
               {
                  // we have cached data with unchanged inputs and outputs that succeded in the previous execution.
                  // skip any opration and inject into the environment the crop rects to feed the drizzle integration in case.
                  let validCache = true;
                  let groupIDs = Object.keys( ACCache.cropRects );
                  groupIDs.forEach( key =>
                  {
                     let rect = ACCache.cropRects[ key ];
                     if ( rect != undefined )
                        try
                        {
                           environment[ key ] = new Rect( rect.x0, rect.y0, rect.x1, rect.y1 );
                        }
                     catch ( e )
                     {
                        validCache = false;
                     }
                     else
                        validCache = false;
                  } );
                  if ( !validCache )
                  {
                     console.noteln( "Autocrop: invalid crop rectangles cached data. Reprocess the master files." );
                  }
                  else
                  {
                     // set the autocrop master file into the groups
                     for ( let j = 0; j < groups.length; ++j )
                     {
                        let croppedFilePath = File.appendToName( filePaths[ j ], "_autocrop" );
                        groups[ j ].setMasterFileName( croppedFilePath, isDrizzle ? WBPPMasterType.DRIZZLE : WBPPMasterType.MASTER_LIGHT, WBPPMasterVariant.CROPPED );
                        engine.processLogger.addSuccess( "Autocrop: cached master", croppedFilePath);
                     }
                     console.noteln( "Autocrop: successfully cached cropped file found. Autocrop will be skipped." );
                     nCached += groups.length;
                     continue;
                  }
               }
            }
         }

         // detect the crop region for all masters first, then the unique crop
         // region applied to all of them will be the intersection of all individual crop regions
         let cropRectangles = [];
         for ( let j = 0; j < groups.length; ++j )
         {
            let masterFilePath = croppingGroups[ i ].filePaths[ j ];

            // cache the LMD for all input filepaths
            engine.executionCache.cacheFileLMD( ACcacheKey, masterFilePath );
            let result = {
               success: false
            };

            let group = groups[ j ];
            console.writeln( "Autocrop: process master: <raw>" + masterFilePath + "</raw>" );
            if ( isDrizzle )
            {
               // each group that is drizzled must have been processed once already to crop
               // the master light, so the crop region is available in the environment
               let cropRect = environment[ "crop_region_" + group.id ];
               if ( cropRect != undefined )
               {
                  let scale = group.drizzleScale();
                  cropRect.x0 = cropRect.x0 * scale;
                  cropRect.y0 = cropRect.y0 * scale;
                  cropRect.x1 = cropRect.x1 * scale;
                  cropRect.y1 = cropRect.y1 * scale;
                  result = {
                     success: true,
                     rect: cropRect
                  };
               }
               else
               {
                  console.warningln( "** Warning: Autocrop: crop region not found for drizzle master ", group.toString() );
               }
            }
            else
            {
               // open the master light and compute the crop region
               result = engine.getAutocropRegion( masterFilePath, true /* returnTheWorkingImages */ );
               if ( !result.success )
               {
                  console.warningln( result.message );
                  engine.processLogger.addError( result.message );
                  console.warningln( "** Warning: Autocrop: failed to determine the crop region for file: <raw>" + masterFilePath + "</raw>" );
               }
               else
               {
                  let id = "crop_region_" + group.id;
                  environment[ id ] = result.rect;
                  ACCache.cropRects[ id ] = {
                     x0: result.rect.x0,
                     x1: result.rect.x1,
                     y0: result.rect.y0,
                     y1: result.rect.y1
                  };
               }
            }
            cropRectangles.push( result );
         }

         // report the result
         for ( let j = 0; j < groups.length; ++j )
         {
            let masterFilePath = filePaths[ j ];
            if ( cropRectangles[ j ].success )
            {
               let cropRect = cropRectangles[ j ].rect;
               let rectString = "(" + cropRect.x0 + "," + cropRect.y0 + "), (" + cropRect.x1 + "," + cropRect.y1 + ")";
               console.noteln( "* Autocrop: crop region ", rectString, " successflly computed for file: <raw>" + masterFilePath + "</raw>" );
            }
            else
            {
               console.warningln( "** Warning: Autocrop: failed to compute crop region for file: <raw>" + masterFilePath + "</raw>" );
               engine.processLogger.addWarning( "Failed to compute crop region for the file " + masterFilePath );
            }
         }

         // -------------------------------------------------
         // ensure that at least one crop region is available
         let oneOreMoreCropRegionsAvailable = false;
         for ( let j = 0; j < cropRectangles.length; ++j )
            if ( cropRectangles[ j ].success )
            {
               oneOreMoreCropRegionsAvailable = true;
               break;
            }

         if ( !oneOreMoreCropRegionsAvailable )
         {
            nFailed += groups.length;
            // update the cache
            ACCache.previouslyFailed = true;
            engine.executionCache.setCache( ACcacheKey, ACCache );
            // log
            let msg = "No autocrop regions defined. Autocrop will be skipped for master frames " +
               "aligned on reference frame " + referenceFrame;
            console.warningln( "** Warning: Autocrop: " + msg );
            engine.processLogger.addError( msg );
            continue;
         }

         // -------------------------------------------------
         // the final crop recion is computed as the intersection of all crop regions
         let cropRect = new Rect( -1, -1, 1e6, 1e6 );
         for ( let j = 0; j < cropRectangles.length; ++j )
         {
            if ( !cropRectangles[ j ].success )
               continue;
            let c = cropRectangles[ j ].rect;
            cropRect.intersect( new Rect( c.x0, c.y0, c.x1, c.y1 ) );
         }
         let rectString = "(" + cropRect.x0 + "," + cropRect.y0 + "), (" + cropRect.x1 + "," + cropRect.y1 + ")";
         console.noteln( "Autocrop: the applied crop region is ", rectString );

         // check the intersection result
         if ( cropRect.width == 0 || cropRect.height == 0 )
         {
            nFailed += groups.length;
            // update the cache
            ACCache.previouslyFailed = true;
            engine.executionCache.setCache( ACcacheKey, ACCache );
            // log
            console.warningln( "** Warning: Autocrop: crop region has null size " + cropRect.width + "x" + cropRect.height + "; crop will be skipped." );
            engine.processLogger.addError( "Crop region has null size " + cropRect.width + "x" + cropRect.height + "; crop will be skipped for master " + masterFilePath );
            continue;
         }

         // -------------------------------------------------
         // now we need to apply the crop to all master frames
         for ( let j = 0; j < groups.length; ++j )
         {
            let masterFilePath = filePaths[ j ];
            let croppedFilePath = File.appendToName( masterFilePath, "_autocrop" );

            console.writeln( "Autocrop: cropping file: <raw>" + masterFilePath + "</raw>" );

            let windows = ImageWindow.open( masterFilePath );
            if ( windows == undefined || windows.length == 0 )
            {
               console.warningln( "** Warning: Autocrop: failed to open master file: <raw>" + masterFilePath + "</raw>" );
               engine.processLogger.addError( "Failed to open master file: " + masterFilePath );
               nFailed++;
               continue;
            }
            let window = windows[ 0 ];
            // close the rejection maps
            let k = 1;
            while ( k < windows.length )
            {
               windows[ k ].forceClose();
               k++;
            }
            windows = [ window ];

            console.writeln( "Autocrop: generating the crop region image." );
            // generate the cropping mask
            let pm = new PixelMath;
            pm.expression = "iif(" +
               "(x()==" + cropRect.x0 + " && y()>=" + cropRect.y0 + " && y()<=" + cropRect.y1 + ") || " +
               "(x()==" + cropRect.x1 + " && y()>=" + cropRect.y0 + " && y()<=" + cropRect.y1 + ") || " +
               "(y()==" + cropRect.y0 + " && x()>=" + cropRect.x0 + " && x()<=" + cropRect.x1 + ") || " +
               "(y()==" + cropRect.y1 + " && x()>=" + cropRect.x0 + " && x()<=" + cropRect.x1 + ")" +
               ",1,0)";
            pm.createNewImage = true;
            pm.showNewImage = false;
            pm.newImageId = "crop_mask";
            pm.newImageColorSpace = PixelMath.prototype.Gray;
            pm.newImageSampleFormat = PixelMath.prototype.i8;
            pm.executeOn( window.mainView );
            let maskImage = ImageWindow.windowById( "crop_mask" );

            // crop the image
            window.mainView.beginProcess();
            window.mainView.image.cropTo( cropRect );
            window.mainView.endProcess();

            // append the crop mask to the set of images stored into the XISF file
            windows.push( maskImage );

            window.keywords = window.keywords.concat(
               new FITSKeyword(
                  AUTOCROP_KEYWORD,
                  format( "(%d,%d)x(%d,%d)", cropRect.x0, cropRect.y0, cropRect.x1, cropRect.y1 ),
                  "WBPP Autocrop" )
            );

            engine.writeImage(
               croppedFilePath,
               windows,
               [ "integration_autocrop",
                  "crop_mask"
               ] );

            // store the autocrop file name into the environment
            groups[ j ].setMasterFileName( croppedFilePath, isDrizzle ? WBPPMasterType.DRIZZLE : WBPPMasterType.MASTER_LIGHT, WBPPMasterVariant.CROPPED );

            windows.forEach( win =>
            {
               if ( win )
                  win.forceClose();
            } );
            console.noteln( "Autocrop: cropped master file saved at ", croppedFilePath );
            engine.processLogger.addSuccess( "Autocrop: cropped master file saved at", croppedFilePath );
            engine.executionCache.cacheFileLMD( ACcacheKey, croppedFilePath );
            nSuccess++;
         }

         // update the cache
         ACCache.previouslyFailed = false;
         engine.executionCache.setCache( ACcacheKey, ACCache );
      }

      engine.processLogger.addSuccess( "Autocrop completed", totalToCrop + " master file" + ( totalToCrop > 1 ? "s" : "" ) + " processed, " + ( nSuccess + nCached ) + " cropped." );
      this.statusMessage = resultCountToString( nCached, nSuccess, nFailed, "cropped" );
      if ( this.statusMessage == "" && nSuccess > 0 )
         this.statusMessage = nSuccess + " cropped";
      this.hasWarnings = nFailed > 0;
      if (totalToCrop > 0 && nFailed == totalToCrop )
         return OperationBlockStatus.FAILED;
      else
         return OperationBlockStatus.DONE;
   }
}
StackEngine.prototype.AutoCropOperation.prototype = new BPPOperationBlock;

StackEngine.prototype.RGBRecombinationOperation = function( frameGroup )
{
   this.__base__ = BPPOperationBlock;
   this.__base__( "RGB Combination", frameGroup, true /* trackable */ );

   let
   {
      resultCountToString
   } = WBPPUtils.shared();

   this.spaceRequired = () =>
   {
      // the group size if autocrop is not ebabled, otherwise both the uncropped and cropped files
      // will be recombined
      return frameGroup.frameSize() * ( engine.autocrop ? 2 : 1 );
   };
   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "RGB Combination",
      status: this.status,
      statusMessage: this.statusMessage,
      group: frameGroup
   } );

   this._run = function()
   {
      // get the recombined parent group, the R, G and B associated groups must have the same parent
      let parentGroupID = frameGroup.linkedGroupID;
      let singleChannelGroups = engine.getLinkedGroups( parentGroupID, frameGroup );

      if ( singleChannelGroups.length != 3 )
      {
         // something is wrong, the number of associated channels must be 3 at this point
         this.statusMessage = "Number of assocaited channels is " + singleChannelGroups.length + ", expected 3.";
         this.hasWarnings = true;
         return OperationBlockStatus.FAILED;
      }

      // check if drizzle is enabled for the first group, this means that it should be enabled for all groups and we have to
      // recombine the drizzle channels too
      let recombineDrizzle = singleChannelGroups[ 0 ].isDrizzleEnabled();
      let drizzlePostfix = "_" + singleChannelGroups[ 0 ].drizzleScale() + "x";

      // construct the list of master variants to recombined. Ignore the drizzle for separated channel since it;s currently disabled.
      let types = [
      {
         type: WBPPMasterType.MASTER_LIGHT,
         variant: WBPPMasterVariant.REGULAR,
         postfix: ""
      } ];

      if ( recombineDrizzle )
      {
         types.push(
         {
            type: WBPPMasterType.DRIZZLE,
            variant: WBPPMasterVariant.REGULAR,
            postfix: "_drizzle" + drizzlePostfix
         } );
      }

      if ( engine.autocrop )
      {
         types.push(
         {
            type: WBPPMasterType.MASTER_LIGHT,
            variant: WBPPMasterVariant.CROPPED,
            postfix: "_autocrop"
         } );
         if ( recombineDrizzle )
         {
            types.push(
            {
               type: WBPPMasterType.DRIZZLE,
               variant: WBPPMasterVariant.CROPPED,
               postfix: "_autocrop_drizzle" + drizzlePostfix
            } );
         }
      }

      let nSuccess = 0;
      let nFailed = 0;
      let nCached = 0;
      for ( let i = 0; i < types.length; ++i )
      {
         let type = types[ i ].type;
         let variant = types[ i ].variant;
         let postfix = types[ i ].postfix;

         // exctract the linkned groups ensuring that the correspondend master file is generated
         let linkedGroups = singleChannelGroups.reduce( ( acc, group ) =>
         {
            let filePath = group.getMasterFileName( type, variant );
            if ( filePath != undefined )
            {
               // store the file name that has been found in the environment associated to the group ID
               acc[ group.associatedRGBchannel ] = filePath;
            }
            return acc;
         },
         {} );

         if ( Object.keys( linkedGroups ).length < 3 )
         {
            let missingChannels = [ WBPPAssociatedChannel.R,
               WBPPAssociatedChannel.G,
               WBPPAssociatedChannel.B
            ].reduce( ( acc, ch ) =>
            {
               if ( linkedGroups[ ch ] == undefined )
                  acc.push( ch );
               return acc;
            }, [] );

            let message = "RGB combination not possible, " + postfix + " channel" +
               ( missingChannels.length > 1 ? "s " : " " ) + missingChannels.join( ", " ) + " not found.";
            console.warningln( "** Warning: " + message );
            engine.processLogger.addError( message );
            this.statusMessage = "missing " + missingChannels.join( ", " );
            nFailed++;
            continue;
         }

         // combine the channels
         let recombinedFileName = "master" + frameGroup.folderName().replace( " ", "_" ) + postfix + ".xisf";
         let
         {
            success,
            error,
            filePath,
            cached
         } = engine.combineRGB(
            linkedGroups[ WBPPAssociatedChannel.R ],
            linkedGroups[ WBPPAssociatedChannel.G ],
            linkedGroups[ WBPPAssociatedChannel.B ],
            recombinedFileName );

         // check the result
         if ( !success )
         {
            console.warningln( "** Warning: " + error );
            engine.processLogger.addError( "Warning: " + error );
            this.statusMessage = "combined RGB file not generated";
            nFailed++;
            continue;
         }

         // success
         frameGroup.setMasterFileName( filePath, type, variant );
         if ( cached )
            nCached++;
         else
            nSuccess++;

         engine.processLogger.addSuccess( "RGB Combination completed", "combined file saved at path " + filePath );
      };

      // done
      this.statusMessage = resultCountToString( nCached, nSuccess, nFailed, "recombined" );
      this.hasWarnings = nFailed > 0;
      return nCached + nSuccess > 0 ? OperationBlockStatus.DONE : OperationBlockStatus.FAILED;
   };
};
StackEngine.prototype.RGBRecombinationOperation.prototype = new BPPOperationBlock;

StackEngine.prototype.PlateSolveOperation = function( frameGroup )
{
   this.__base__ = BPPOperationBlock;
   this.__base__( "Astrometric solution", frameGroup, true /* trackable */ );

   this.spaceRequired = () => 0;

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Astrometric solution",
      status: this.status,
      statusMessage: this.statusMessage,
      group: frameGroup
   } );

   let
   {
      getLastModifiedDate,
      resultCountToString
   } = WBPPUtils.shared();

   this._run = function()
   {
      // the first requirement is that a regular master light has been successfully generated
      let masterLightFileName = frameGroup.getMasterFileName( WBPPMasterType.MASTER_LIGHT, WBPPMasterVariant.REGULAR );
      if ( masterLightFileName == undefined || !File.exists( masterLightFileName ) )
      {
         console.warningln( "** Warning: No master file found for group ", frameGroup.toShortString() );
         engine.processLogger.addError( "Warning: No master file generated." );
         this.statusMessage = "no master file generated";
         return OperationBlockStatus.CANCELED;
      }

      // list the masters to integrate, masters is an array of objects with the following properties:
      // fName: the file name of the master
      // type: the type of the master
      let masters = [];
      for ( let type = 0; type < WBPPMasterType.NOptions; ++type )
         for ( let variant = 0; variant < WBPPMasterVariant.NOptions; ++variant )
         {
            // push the master file name if it exists
            let fName = frameGroup.getMasterFileName( type, variant );
            if ( fName != undefined && File.exists( fName ) )
               masters.push( {
                  fName: fName,
                  type: type
               } );
         }

      // reuse the reference frame's metadata if available
      let metadata = undefined;

      // extract the cache center coordinates and pixel scale for the reference frame
      let AScacheKey = engine.executionCache.keyFor( "astrometry_" + frameGroup.__reference_frame__ );
      if ( engine.executionCache.hasCacheForKey( AScacheKey ) )
      {
         // extract the center coordinates
         metadata = engine.executionCache.cacheForKey( AScacheKey );
      }

      let nCached = 0;
      let nSuccess = 0;
      let nFailed = 0;

      // find the masters to solve, skipping the cached ones
      let mastersToSolve = masters.reduce( ( acc, masterInfo ) => {
         if ( engine.executionCache.isFileUnmodified( AScacheKey, masterInfo.fName ) )
            nCached++;
         else
            acc.push( masterInfo );
         return acc;
      }, [] );

      // solve each master
      for ( let i = 0; i < mastersToSolve.length; ++i )
      {
         let masterData = mastersToSolve[ i ];
         console.writeln( "Compute the astrometric solution for file: <raw>" + masterData.fName + "</raw>" );
         let windows = ImageWindow.open( masterData.fName );
         if ( windows.length > 0 )
         {
            let window = windows[ 0 ];
            try
            {
               let imageMetadata = {};
               // prepare the metadata, if available
               if ( metadata != undefined )
               {
                  imageMetadata.observationTime = metadata.observationTime;
                  imageMetadata.ra = metadata.ra;
                  imageMetadata.dec = metadata.dec;
                  let dzScale = (masterData.type == WBPPMasterType.DRIZZLE) ? frameGroup.drizzleScale() : 1;
                  imageMetadata.resolution = metadata.resolution / dzScale;
                  imageMetadata.xpixsz = metadata.xpixsz / dzScale;
                  imageMetadata.useFocal = false;
                  console.writeln( format( "* Using cached metadata: ra=%.8f deg dec=%+.8f deg xpixsz=%.2f um (scale=%d:1)",
                                           imageMetadata.ra, imageMetadata.dec, imageMetadata.xpixsz, dzScale ) );
               }
               // solve the image
               let solver = engine.solveImage( window, {}, imageMetadata );
               if ( solver != undefined )
               {
                  // save the metadata in cache
                  if ( metadata == undefined )
                  {
                     metadata = solver.metadata;
                     engine.executionCache.setCache( AScacheKey, metadata);
                  }
                  // update the last modified date
                  let previousLMD = getLastModifiedDate( masterData.fName );
                  engine.writeImage( masterData.fName, windows, windows.map( w => w.originalImageId ) );
                  let newLMD = getLastModifiedDate( masterData.fName );
                  engine.executionCache.cacheFileLMD( AScacheKey, masterData.fName );
                  engine.executionCache.updateLMD( masterData.fName, previousLMD, newLMD );
                  nSuccess++;
                  engine.processLogger.addSuccess( "Astrometric solution completed", masterData.fName );
               }
               else
               {
                  console.warningln( "** Warning: Astrometric solution failed: <raw>" + masterData.fName + "</raw>" );
                  nFailed++;
               }
            }
            catch ( e )
            {
               console.warningln( "** Warning: Astrometric solution failed: <raw>" + masterData.fName + "</raw> with error ", e );
               nFailed++;
            }
            window.forceClose();
         }
         else
         {
            console.warningln( "** Warning: No images found in master file: <raw>" + masterData.fName + "</raw>" );
            failed++;
            continue;
         }
      }

      // done
      this.statusMessage = resultCountToString( nCached, nSuccess, nFailed, "solved" );
      this.hasWarnings = nFailed > 0;
      return (nCached + nSuccess > 0) ? OperationBlockStatus.DONE : OperationBlockStatus.FAILED;
   }
}

StackEngine.prototype.PlateSolveOperation.prototype = new BPPOperationBlock;

/**
 * Generates the pipeline steps to process the bias frames.
 */
StackEngine.prototype.buildPipelineForBias = function()
{
   // process only pre-process groups
   let groupsPRE = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   for ( let i = 0; i < groupsPRE.length; ++i )
      if ( groupsPRE[ i ].imageType == ImageType.BIAS && !groupsPRE[ i ].hasMaster )
      {
         let logEnv = {
            processLogger: this.processLogger,
            group: groupsPRE[ i ]
         };

         // log header
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.addMessage( env.params.group.logStringHeader( 'MASTER BIAS GENERATION' ) );
         }, logEnv )
         // integration step
         let integrationOperation = new this.ImageIntegrationOperation( groupsPRE[ i ] );
         this.WS.masterBias.size += integrationOperation.spaceRequired();
         this.operationQueue.addOperation( integrationOperation );
         // log footer
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.addMessage( env.params.group.logStringFooter() );
            env.params.processLogger.newLine();
         }, logEnv )
      }
};

// ----------------------------------------------------------------------------

/**
 * Generates the pipeline steps to process the dark frames.
 */
StackEngine.prototype.buildPipelineForDark = function()
{
   // process only pre-process groups
   let groupsPRE = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   for ( let i = 0; i < groupsPRE.length; ++i )
      if ( groupsPRE[ i ].imageType == ImageType.DARK && !groupsPRE[ i ].hasMaster )
      {
         let logEnv = {
            processLogger: this.processLogger,
            group: groupsPRE[ i ]
         };

         // log header
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.addMessage( env.params.group.logStringHeader( 'MASTER DARK GENERATION' ) );
         }, logEnv );
         // integration step
         let integrationOperation = new this.ImageIntegrationOperation( groupsPRE[ i ] );
         this.WS.masterDark.size += integrationOperation.spaceRequired();
         this.operationQueue.addOperation( integrationOperation );
         // log footer
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.addMessage( env.params.group.logStringFooter() );
            env.params.processLogger.newLine();
         }, logEnv );
      }
};

// ----------------------------------------------------------------------------

/**
 * Generates the pipeline steps to process the flat frames.
 */
StackEngine.prototype.buildPipelineForFlat = function()
{

   // process only pre-process groups
   let groupsPRE = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   // flats calibration operations
   for ( let i = 0; i < groupsPRE.length; ++i )
      if ( groupsPRE[ i ].imageType == ImageType.FLAT && !groupsPRE[ i ].hasMaster )
      {
         let logEnv = {
            processLogger: this.processLogger,
            group: groupsPRE[ i ]
         };

         // log header
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.addMessage( env.params.group.logStringHeader( 'MASTER FLAT GENERATION' ) );
         }, logEnv );
         // calibration step (only if calibration masters are found)
         let cg = this.getCalibrationGroupsFor( groupsPRE[ i ] );
         if ( cg.masterBias || cg.masterDark || engine.overscan.enabled )
         {
            let calibrationOperation = new this.calibrationOperation( groupsPRE[ i ] );
            this.WS.calibrationFlat.size += calibrationOperation.spaceRequired();
            this.operationQueue.addOperation( calibrationOperation );
         }
         // integration step
         let integrationOperation = new this.ImageIntegrationOperation( groupsPRE[ i ] );
         this.WS.masterFlat.size += integrationOperation.spaceRequired();
         this.operationQueue.addOperation( integrationOperation );
         // log footer
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.addMessage( env.params.group.logStringFooter() );
            env.params.processLogger.newLine();
         }, logEnv );
      }
};

// ----------------------------------------------------------------------------

StackEngine.prototype.readableLNReferenceSelectionMethod = function( sanitized )
{
   sanitized = sanitized != undefined ? sanitized : false;
   switch ( this.localNormalizationBestReferenceSelectionMethod )
   {
      case WBPPLocalNormalizationReferenceFrameMetric.PSFSW:
         return sanitized ? "the_highest_PSF_Signal_Weight" : "the highest PSF Signal Weight";
      case WBPPLocalNormalizationReferenceFrameMetric.PSFSNR:
         return sanitized ? "the_highest_PSF_SNR" : "the highest PSF SNR";
      case WBPPLocalNormalizationReferenceFrameMetric.MSTAR:
         return sanitized ? "the_lowers_Mstar" : "the lowest M*";
      case WBPPLocalNormalizationReferenceFrameMetric.MEDIAN:
         return sanitized ? "the_lowest_median" : "the lowest median";
      case WBPPLocalNormalizationReferenceFrameMetric.STARS:
         return sanitized ? "the_highest_number_of-stars" : "the highest number of stars";
   }

   return "";
};

/**
 * Returns the best active frames or the set of best active frames to be integrated to
 * generate the local normalization reference frame accordingly
 *
 * @param {*} group
 * @return {*}
 */
StackEngine.prototype.sortFramesForLocalNormalizationReference = function( group )
{
   let activeFrames = group.activeFrames();

   // determine the measuring criteria
   let descriptorKey = "PSFSignalWeight";
   let sortByMaxVal = true;
   switch ( this.localNormalizationBestReferenceSelectionMethod )
   {
      case WBPPLocalNormalizationReferenceFrameMetric.PSFSW:
         descriptorKey = "PSFSignalWeight";
         break;
      case WBPPLocalNormalizationReferenceFrameMetric.PSFSNR:
         descriptorKey = "PSFSNR";
         break;
      case WBPPLocalNormalizationReferenceFrameMetric.MSTAR:
         descriptorKey = "Mstar";
         sortByMaxVal = false;
         break;
      case WBPPLocalNormalizationReferenceFrameMetric.MEDIAN:
         descriptorKey = "median";
         sortByMaxVal = false;
         break;
      case WBPPLocalNormalizationReferenceFrameMetric.STARS:
         descriptorKey = "numberOfStars";
         break;
   }

   // sort by measuring criteria
   activeFrames.sort( ( a, b ) =>
   {
      let aVal;
      let bVal;

      aVal = a.descriptor[ descriptorKey ];
      bVal = b.descriptor[ descriptorKey ];
      return sortByMaxVal ? bVal - aVal : aVal - bVal;
   } );

   let N = 1;
   // we exclude from the count the frames with the __integrated__ property used in LN interactive mode
   // to properly compute the number of best frmes to be integrated
   let filteredActiveFrames = activeFrames.filter( f => ( f.__integrated__ == undefined ) )
   if ( filteredActiveFrames.length >= 3 &&
      engine.localNormalizationReferenceFrameGenerationMethod != WBPPLocalNormalizationReferenceFrameMethod.SINGLE_BEST )
      // determine the number of frames to be integrated
      N = Math.max( 3, Math.min( engine.localNormalizationMaxIntegratedFrames, Math.floor( filteredActiveFrames.length / 3 ) ) );

   return {
      descriptorKey: descriptorKey,
      N: N,
      activeFrames: activeFrames
   };
};

/**
 * Generates the local normalization reference frame and returns the file path.
 *
 * @param {*} group
 * @param {*} bestFrames
 * @param {*} logEnabled
 * @param {*} desiredFileName
 */
StackEngine.prototype.generateLNReference = function( group, bestFrames, logEnabled, desiredFileName )
{
   let activeFrames = group.activeFrames();
   if ( logEnabled == undefined )
      logEnabled = true;

   let
   {
      enableTargetFrames,
      existingDirectory,
      getImageSize,
      isEmptyString
   } = WBPPUtils.shared();

   let lbl = this.readableLNReferenceSelectionMethod();
   let dk;

   if ( bestFrames == undefined )
   {
      let
      {
         descriptorKey,
         N,
         activeFrames
      } = engine.sortFramesForLocalNormalizationReference( group );
      dk = descriptorKey;
      bestFrames = activeFrames.slice( 0, N );
      console.noteln( "* Selecting the best reference frames for Local Normalization using ", dk, " metric." );
   }

   // if 1 frame hes been selected then return it, otherwise proceed with the integration
   if ( engine.localNormalizationReferenceFrameGenerationMethod == WBPPLocalNormalizationReferenceFrameMethod.SINGLE_BEST )
   {
      console.noteln( "* Local normalization: using the single best frame as reference." );
      if ( logEnabled )
         this.processLogger.addSuccess( "Local normalization", "using the single frame with ", dk, " as reference." );
      return {
         lnReferenceFilePath: bestFrames[ 0 ].current,
         cached: false
      };
   }
   else if ( bestFrames.length < 3 )
   {
      console.warningln( "** Warning: Local normalization: not enough best frames found; using the single best frame as reference." );
      if ( logEnabled )
         this.processLogger.addWarning( "Local normalization: ", "not enough best frames found; using the single best frame as reference." );
      return {
         lnReferenceFilePath: bestFrames[ 0 ].current,
         cached: false
      };
   }

   console.noteln( "Local normalization: generate the reference frame selecting " + bestFrames.length + " frames with " + lbl + " amongst " + activeFrames.length + " frames" );
   // do LN and Integration on a temporary group overriding the method to MEDIAN
   let integrationGroup = group.cloneWithActiveItems( bestFrames );

   // perform local normalization using the best frame as reference
   let integratedFrames = integrationGroup.activeFrames();
   let LN = new LocalNormalization;

   let subfolder = integrationGroup.folderName();
   LN.outputDirectory = existingDirectory( engine.outputDirectory + "/registered/" + subfolder + "/ln_reference_frame_data" );

   // read the current reference frame size

   let referenceImageSize = getImageSize( integratedFrames[ 0 ].current );
   let imageRefrenceDimension = Math.min( referenceImageSize.width, referenceImageSize.height );

   LN.referencePathOrViewId = integratedFrames[ 0 ].current;
   LN.referenceIsView = false;
   LN.scale = imageRefrenceDimension / engine.localNormalizationGridSize;
   LN.referenceRejection = true;
   LN.referenceRejectionThreshold = 3.00;
   LN.targetRejectionThreshold = 3.20;
   LN.psfMaxStars = engine.localNormalizationPsfMaxStars;
   LN.psfMinSNR = engine.localNormalizationPsfMinSNR;
   LN.psfAllowClusteredSources = engine.localNormalizationPsfAllowClusteredSources;
   LN.lowClippingLevel = engine.localNormalizationLowClippingLevel;
   LN.highClippingLevel = engine.localNormalizationHighClippingLevel;
   LN.scaleEvaluationMethod = this.localNormalizationMethod == 0 ?
      LocalNormalization.prototype.ScaleEvaluationMethod_PSFSignal :
      LocalNormalization.prototype.ScaleEvaluationMethod_MultiscaleAnalysis;
   LN.psfType = [
      LocalNormalization.prototype.PSFType_Gaussian,
      LocalNormalization.prototype.PSFType_Moffat15,
      LocalNormalization.prototype.PSFType_Moffat4,
      LocalNormalization.prototype.PSFType_Moffat6,
      LocalNormalization.prototype.PSFType_Moffat8,
      LocalNormalization.prototype.PSFType_MoffatA,
      LocalNormalization.prototype.PSFType_Auto
   ][ this.localNormalizationPsfType ];
   LN.psfGrowth = engine.localNormalizationPsfGrowth;
   LN.overwriteExistingFiles = false;

   let LNSource = LN.toSource( "JavaScript", "LN" /*varId*/ , 0 /*indent*/ ,
      SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();
   console.writeln( SEPARATOR2 );
   console.writeln( LNSource );
   console.writeln( SEPARATOR2 );

   // Check if valid cached data is present
   let fileItemsToNormalize = integratedFrames;
   let LNCache = {};
   let LNcacheKey = engine.executionCache.keyFor( "LNReference" + LNSource );
   if ( engine.executionCache.hasCacheForKey( LNcacheKey ) )
   {

      console.noteln( "LN Reference frame generation has cached data for key ", LNcacheKey );

      // the cache is the map between each input file and the correspondent output xnml file
      LNCache = engine.executionCache.cacheForKey( LNcacheKey );

      // check if the reference file is unchanged
      if ( engine.executionCache.isFileUnmodified( LNcacheKey, LN.referencePathOrViewId ) )
      {
         fileItemsToNormalize = [];
         // check which input file is unchanged
         for ( let i = 0; i < integratedFrames.length; ++i )
         {
            let inputFile = integratedFrames[ i ].current;
            let lnFile = LNCache[ inputFile ];
            if ( engine.executionCache.isFileUnmodified( LNcacheKey, inputFile ) &&
               engine.executionCache.isFileUnmodified( LNcacheKey, lnFile ) )
            {
               // the cached ln file is valid, associate it
               integratedFrames[ i ].addLocalNormalizationFile( lnFile );
               console.noteln( "LN file is cached: [", inputFile, "] -> [", lnFile, "]" );
            }
            else
            {
               fileItemsToNormalize.push( integratedFrames[ i ] );
               console.noteln( "LN file will be generated for: [", inputFile, "]" );
            }
         }
      }
   }
   else
   {
      console.noteln( "LN Reference frame generation has no cache date for key ", LNcacheKey );
   }

   let filePaths = fileItemsToNormalize.map( item => item.current );
   // process is saved in container with the full list of files to be normalized
   LN.targetItems = enableTargetFrames( filePaths, 2 );
   engine.processContainer.add( LN );
   engine.flushProcessContainer();

   // perform LN if there are files to normalize
   let lnSuccess = true;
   if ( filePaths.length > 0 )
   {
      LN.targetItems = enableTargetFrames( filePaths, 2 );
      lnSuccess = LN.executeGlobal();
   }

   // ignore any result if something went wrong. We accept LN data only if normalized files have been gerenrated for
   // all input files provided
   let useLN = true;

   /* AUX: delete all normalized files generated by the provided LN instance */
   let cleanLNFiles = function( LN )
   {
      if ( !LN.outputData )
         return

      let lnFiles = LN.outputData.map( item => ( item[ 0 ] || "" ) );
      for ( let k = 0; k < lnFiles.length; ++k )
         File.remove( lnFiles[ k ] )
   }

   // clean the generated ln files and disable LN if something went wrong
   let lnFiles = [];
   if ( !lnSuccess )
      useLN = false;
   else if ( fileItemsToNormalize.length > 0 )
   {
      if ( !LN.outputData || LN.outputData.length != fileItemsToNormalize.length )
         useLN = false;
      else
      {
         lnFiles = LN.outputData.map( item => ( item[ 0 ] || "" ) );
         // ensure that LN files have been created for each file
         for ( let k = 0; k < lnFiles.length; ++k )
            if ( lnFiles[ k ].length == 0 || !File.exists( lnFiles[ k ] ) )
            {
               useLN = false;
               break;
            }
      }
   }

   // in case of success then integrate the input files along with the corresponding local normalization files
   if ( useLN )
   {
      // merge cached and generated normalized files
      for ( let i = 0; i < filePaths.length; ++i )
      {
         let inputFile = filePaths[ i ];
         let lnFile = lnFiles[ i ];
         fileItemsToNormalize[ i ].addLocalNormalizationFile( lnFile );
         console.noteln( "associate the local normalization file: [" + fileItemsToNormalize[ i ].current + "] -> [" + lnFile + "]" );

         // cache the result
         LNCache[ inputFile ] = lnFile;
         engine.executionCache.cacheFileLMD( LNcacheKey, inputFile );
         engine.executionCache.cacheFileLMD( LNcacheKey, lnFile );
         LNCache[ inputFile ] = lnFile;
      }
      // save the updated cache
      engine.executionCache.setCache( LNcacheKey, LNCache );

      // integrate the reference frame
      let
      {
         masterFilePath,
         cached
      } = this.doIntegrate(
         integrationGroup, /* frameGroup */
         "LN_Reference_", /* customPrefix */
         "", /* customPostfix */
         undefined, /* customGenerateRejectionMaps */
         false, /* customGenerateDrizzle */
         desiredFileName, /* desired master file name */
         {
            /* II overridden parameters */
            combination: ImageIntegration.prototype.Average,
            rejection: integrationGroup.bestRejectionMethod(),
            normalization: ImageIntegration.prototype.AdditiveWithScaling,
            rejectionNormalization: ImageIntegration.prototype.LocalRejectionNormalization,
            weightMode: ImageIntegration.prototype.PSFSignalWeight,
            rangeClipLow: true,
            rangeLow: 0,
            generateRejectionMaps: false,
            minWeight: 0
         },
         undefined /* FITS keywords */
      );

      // if integration failed then return the best frame
      if ( isEmptyString( masterFilePath ) )
      {
         console.warningln( "** Warning: Local normalization: integration failed; using the best frame as reference." );
         if ( logEnabled )
            this.processLogger.addWarning( "Local normalization: ", "integration failed; using the best frame as reference." );
         return {
            lnReferenceFilePath: integratedFrames[ 0 ].current,
            cached: false
         };
      }
      console.noteln( "* Local normalization: reference frame generated by integrating " + integratedFrames.length + " frames." );
      if ( logEnabled )
         this.processLogger.addSuccess( "Local normalization", "reference frame generated by integrating " + integratedFrames.length + " frames" );
      return {
         lnReferenceFilePath: masterFilePath,
         cached: cached
      };
   }
   else
   {
      cleanLNFiles( LN );
      console.warningln( "** Warning: Local normalization, local normalizaton of best frames failed; using the best frame as reference" );
      if ( logEnabled )
         this.processLogger.addWarning( "Local normalization: ", "local normalizaton of best frames failed; using the best frame as reference" );
      return {
         lnReferenceFilePath: integratedFrames[ 0 ].current,
         cached: false
      };
   }
};

// ----------------------------------------------------------------------------

/**
 * Generates the pipeline steps to process the light frames.
 */
StackEngine.prototype.buildPipelineForLight = function()
{
   // pre-process groups first
   let groupsPRE = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   // CALIBRATION
   for ( let i = 0; i < groupsPRE.length; ++i )
      if ( groupsPRE[ i ].imageType == ImageType.LIGHT )
      {
         let logEnv = {
            processLogger: this.processLogger,
            group: groupsPRE[ i ]
         };

         // log header
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.newLine();
            env.params.processLogger.addMessage( env.params.group.logStringHeader( 'LIGHT FRAMES CALIBRATION' ) );
         }, logEnv );

         // calibration step (only if calibration masters are found)
         let cg = this.getCalibrationGroupsFor( groupsPRE[ i ] );
         if ( cg.masterBias || cg.masterDark || cg.masterFlat )
         {
            let calibrationOperation = new this.calibrationOperation( groupsPRE[ i ] );
            this.WS.calibrationLight.size += calibrationOperation.spaceRequired();
            this.operationQueue.addOperation( calibrationOperation );
         }

         // log footer
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.addMessage( env.params.group.logStringFooter() );
            env.params.processLogger.newLine();
         }, logEnv );
      }

   // LINEAR PATTERN SUBTRACTION
   if ( this.linearPatternSubtraction )
   {
      let logEnv = {
         processLogger: this.processLogger
      };

      // log header
      this.operationQueue.addOperationBlock( ( env ) =>
      {
         env.params.processLogger.newLine();
         env.params.processLogger.addMessage( "<b>********************</b> <i>LINEAR DEFECTS CORRECTION</i> <b>********************</b>" );
      }, logEnv );

      let LPSOperation = new this.LPSOperation();
      this.WS.LPS.size += LPSOperation.spaceRequired();
      this.operationQueue.addOperation( LPSOperation );

      // log footer
      this.operationQueue.addOperationBlock( ( env ) =>
      {
         env.params.processLogger.addMessage( "<b>" + SEPARATOR + "</b>" );
         env.params.processLogger.newLine();
      }, logEnv );
   }

   // COSMETIC CORRECTON AND DEBAYER

   for ( let i = 0; i < groupsPRE.length; ++i )
      if ( groupsPRE[ i ].imageType == ImageType.LIGHT )
      {
         let logEnv = {
            processLogger: this.processLogger,
            group: groupsPRE[ i ]
         };

         let needsCC = groupsPRE[ i ].ccData.CCTemplate && groupsPRE[ i ].ccData.CCTemplate.length > 0;
         let needsDebayer = groupsPRE[ i ].isCFA;

         if ( !needsCC && !needsDebayer )
            continue;

         // log header
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.newLine();
            env.params.processLogger.addMessage( env.params.group.logStringHeader( ( needsCC ? "COSMETIZATION" : "" ) + ( needsCC && needsDebayer ? " and " : "" ) + (needsDebayer ? "DEBAYERING" : "" ) ) );
         }, logEnv );

         // cosmetic correction
         if ( needsCC )
         {
            let cosmeticCorretcionOperation = new this.CosmeticCorrectionOperation( groupsPRE[ i ] );
            this.WS.cosmeticCorrection.size += cosmeticCorretcionOperation.spaceRequired();
            this.operationQueue.addOperation( cosmeticCorretcionOperation );
         }

         // debayer
         if ( needsDebayer )
         {
            let debayerOperation = new this.DebayerOperation( groupsPRE[ i ] );
            this.WS.debayer.size += debayerOperation.spaceRequired();
            this.operationQueue.addOperation( debayerOperation );
         }

         // log footer
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.addMessage( env.params.group.logStringFooter() );
            env.params.processLogger.newLine();
         }, logEnv );
      }

   // REFERENCE FRAME DATA PREPARATION
   if ( !engine.reuseLastReferenceFrames )
      this.operationQueue.addOperation( new this.ReferenceFrameDataPreparationOperation() );

   // POST-PROCESS GROUPS
   let groupsPOST = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST ).filter( g => g.isActive );

   // MEASUREMENT OPERATION
   let generateSubframesWeights = this.subframeWeightingEnabled && ( ( this.subframesWeightsMethod == WBPPSubframeWeightsMethod.FORMULA ) || this.integrate );
   let measureForBestFrameSelection = ( this.imageRegistration || this.fastMode ) && this.bestFrameReferenceMethod != WBPPBestReferenceMethod.MANUAL && !engine.reuseLastReferenceFrames;
   let measureImages = generateSubframesWeights || measureForBestFrameSelection || this.localNormalization;
   if ( !engine.groupsManager.isEmpty() && measureImages )
   {
      this.operationQueue.addOperation( new this.MeasurementOperation() );
      if ( engine.subframeWeightingEnabled && ( engine.subframesWeightsMethod == WBPPSubframeWeightsMethod.FORMULA ) )
         this.operationQueue.addOperation( new this.CustomFormulaWeightsGenerationOperation() );
      if ( ( engine.subframesWeightsMethod != WBPPSubframeWeightsMethod.PSFScaleSNR ) && this.integrate && engine.groupsManager.regularIntegrationGroupExists() )
         this.operationQueue.addOperation( new this.BadFramesRejectionOperation() );
   }

   // SET THE REFERENCE FRAME (for both post calibration group with or without fast integration )
   let regularRegistrationGropuExists = this.imageRegistration && engine.groupsManager.regularIntegrationGroupExists();
   if ( !engine.groupsManager.isEmpty() && ( regularRegistrationGropuExists || ( engine.groupsManager.fastIntegrationGroupExists() && engine.integrate ) ) )
      if ( !engine.reuseLastReferenceFrames )
         this.operationQueue.addOperation( new this.ReferenceFrameSelectionOperation() );

   // exit if no registration, local normalization and integration needs to be performed
   if ( !generateSubframesWeights && !this.imageRegistration && !this.localNormalization && !this.integrate )
      return;

   // POST-PROCESS GROUPS - REGISTRATION
   if ( this.imageRegistration )
      for ( let i = 0; i < groupsPOST.length; ++i )
      {
         let standardPostCalibrationProcessing = groupsPOST[ i ].associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB && !groupsPOST[ i ].fastIntegrationData.enabled;

         if ( standardPostCalibrationProcessing )
         {
            let logEnv = {
               processLogger: this.processLogger,
               group: groupsPOST[ i ]
            };

            // log header
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringHeader( 'IMAGE REGISTRATION' ) );
            }, logEnv )

            let registrationOperation = new this.RegistrationOperation( groupsPOST[ i ] );
            this.WS.registration.size += registrationOperation.spaceRequired();
            this.operationQueue.addOperation( registrationOperation );

            // log footer
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringFooter() );
               env.params.processLogger.newLine();
            }, logEnv )
         }
      }

   // POST-PROCESS GROUPS - LOCAL NORMALIZATION REFERENCE FRAME SELECTION
   if ( this.localNormalization )
      for ( let i = 0; i < groupsPOST.length; ++i )
      {
         let standardPostCalibrationProcessing = groupsPOST[ i ].associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB && !groupsPOST[ i ].fastIntegrationData.enabled;

         if ( standardPostCalibrationProcessing )
         {
            let logEnv = {
               processLogger: this.processLogger,
               group: groupsPOST[ i ]
            };

            // log header
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringHeader( 'LOCAL NORMALIZATION - REFERENCE FRAME SELECTION' ) );
            }, logEnv );

            if ( !engine.reuseLastLNReferenceFrames )
            {
               let lnReferenceFrameSelectionOperation = new this.LocalNormalizationReferenceFrameSelectionOperation( groupsPOST[ i ] );
               this.WS.localNormalization.size += lnReferenceFrameSelectionOperation.spaceRequired();
               this.operationQueue.addOperation( lnReferenceFrameSelectionOperation );
            }

            // log footer
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringFooter() );
               env.params.processLogger.newLine();
            }, logEnv );
         }
      }

   // POST-PROCESS GROUPS - LOCAL NORMALIZATION
   if ( this.localNormalization )
      for ( let i = 0; i < groupsPOST.length; ++i )
      {
         let standardPostCalibrationProcessing = groupsPOST[ i ].associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB && !groupsPOST[ i ].fastIntegrationData.enabled;

         if ( standardPostCalibrationProcessing )
         {
            let logEnv = {
               processLogger: this.processLogger,
               group: groupsPOST[ i ]
            };

            // log header
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringHeader( 'LOCAL NORMALIZATION' ) );
            }, logEnv );

            let lnOperation = new this.LocalNormalizationOperation( groupsPOST[ i ] );
            this.WS.localNormalization.size += lnOperation.spaceRequired();
            this.operationQueue.addOperation( lnOperation );

            // log footer
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringFooter() );
               env.params.processLogger.newLine();
            }, logEnv );
         }
      }

   // POST-PROCESS GROUPS - IMAGE INTGEGRATION

   if ( this.integrate )
   {
      // REGULAR INTEGRATION

      for ( let i = 0; i < groupsPOST.length; ++i )
      {
         let standardPostCalibrationProcessing = groupsPOST[ i ].associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB;

         if ( standardPostCalibrationProcessing && !groupsPOST[ i ].fastIntegrationData.enabled )
         {

            // Image Integration
            let logEnv = {
               processLogger: this.processLogger,
               group: groupsPOST[ i ]
            };

            // log header
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringHeader( 'IMAGE INTEGRATION' ) );
            }, logEnv );

            let imageIntegrationOperation = new this.ImageIntegrationOperation( groupsPOST[ i ] );
            this.WS.integration.size += imageIntegrationOperation.spaceRequired();
            this.operationQueue.addOperation( imageIntegrationOperation );

            // log footer
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringFooter() );
               env.params.processLogger.newLine();
            }, logEnv );
         }
      }

      // FAST INTEGRATION

      for ( let i = 0; i < groupsPOST.length; ++i )
      {
         let standardPostCalibrationProcessing = groupsPOST[ i ].associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB;

         if ( standardPostCalibrationProcessing && groupsPOST[ i ].fastIntegrationData.enabled )
         {
            // Fast Integration
            let logEnv = {
               processLogger: this.processLogger,
               group: groupsPOST[ i ]
            };

            // log header
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringHeader( 'FAST INTEGRATION' ) );
            }, logEnv );


            let fastIntegrationOperation = new this.FastIntegrationOperation( groupsPOST[ i ] );
            this.WS.fastIntegration.size += fastIntegrationOperation.spaceRequired();
            let imageIntegrationOperation = new this.ImageIntegrationOperation( groupsPOST[ i ] );
            this.WS.integration.size += imageIntegrationOperation.spaceRequired();
            this.operationQueue.addOperation( fastIntegrationOperation );

            // log footer
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringFooter() );
               env.params.processLogger.newLine();
            }, logEnv );
         }
      }

      // DRIZZLE INTEGRATION

      for ( let i = 0; i < groupsPOST.length; ++i )
      {
         let standardPostCalibrationProcessing = groupsPOST[ i ].associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB;

         if ( standardPostCalibrationProcessing && groupsPOST[ i ].isDrizzleEnabled() )
         {
            // Fast Integration
            let logEnv = {
               processLogger: this.processLogger,
               group: groupsPOST[ i ]
            };

            // log header
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringHeader( 'DRIZZLE INTEGRATION' ) );
            }, logEnv );

            let drizzleIntegrationOperation = new this.DrizzleIntegrationOperation( groupsPOST[ i ] );
            this.WS.integration.size += drizzleIntegrationOperation.spaceRequired();
            this.operationQueue.addOperation( drizzleIntegrationOperation );

            // log footer
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringFooter() );
               env.params.processLogger.newLine();
            }, logEnv );
         }
      }

      // AUTO CROP

      if ( this.integrate && this.autocrop && groupsPOST.length > 0)
      {
         let logEnv = {
            processLogger: this.processLogger
         };

         // log header
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            console.writeln();
            env.params.processLogger.addMessage( "<b>*********************** <i>AUTO CROP</i> ***********************</b>" );
            console.writeln( SEPARATOR );
            console.writeln( "* Begin autocrop of master light frames." );
            console.writeln( SEPARATOR );
         }, logEnv );

         let autocropOperation = new this.AutoCropOperation();
         this.WS.integration.size += autocropOperation.spaceRequired();
         this.operationQueue.addOperation( autocropOperation );

         // log footer
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.addMessage( "<b>" + SEPARATOR + "</b>" );
            env.params.processLogger.newLine();
            console.writeln( SEPARATOR );
            console.writeln( "* End autocrop of master light frames." );
            console.writeln( SEPARATOR );
         }, logEnv );
      }

      // process the RGB recombination groups, this needs to be done at the end to ensure that all
      // gray R,G and B masters have been integrated
      for ( let i = 0; i < groupsPOST.length; ++i )
      {
         let logEnv = {
            processLogger: this.processLogger,
            group: groupsPOST[ i ]
         };

         // RGB RECOMBINATION
         if ( groupsPOST[ i ].associatedRGBchannel == WBPPAssociatedChannel.COMBINED_RGB )
         {
            // log header
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringHeader( 'RGB COMBINATION' ) );
            }, logEnv );

            // RGB Recombination
            let recombinationOperation = new this.RGBRecombinationOperation( groupsPOST[ i ] );
            this.WS.recombination.size += recombinationOperation.spaceRequired();
            this.operationQueue.addOperation( recombinationOperation );

            // log footer
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringFooter() );
               env.params.processLogger.newLine();
            }, logEnv );
         }
      }

      // Plate solve
      if ( this.integrate && this.platesolve )
      {

         for ( let i = 0; i < groupsPOST.length; ++i )
         {
            // Image Integration
            let logEnv = {
               processLogger: this.processLogger,
               group: groupsPOST[ i ]
            };

            // log header
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringHeader( 'ASTROMETRIC SOLUTION' ) );
            }, logEnv );

            let plateSolveOperation = new this.PlateSolveOperation( groupsPOST[ i ] );
            this.operationQueue.addOperation( plateSolveOperation );

            this.operationQueue.addOperationBlock( ( env ) =>
               {
                  env.params.processLogger.addMessage( env.params.group.logStringFooter( 'COMPLETED' ) );
               }, logEnv );
         }
      }
   }
};

StackEngine.prototype.resetRequiredSpace = function()
{
   // Space required
   this.WS = {
      masterBias:
      {
         label: "Master bias",
         size: 0
      },
      masterDark:
      {
         label: "Master darks",
         size: 0
      },
      masterFlat:
      {
         label: "Master flats",
         size: 0
      },
      calibrationFlat:
      {
         label: "Calibrated flats",
         size: 0
      },
      calibrationLight:
      {
         label: "Calibrated lights",
         size: 0
      },
      LPS:
      {
         label: "Linear pattern subtraction",
         size: 0
      },
      cosmeticCorrection:
      {
         label: "Cosmetic correction",
         size: 0
      },
      debayer:
      {
         label: "Debayer",
         size: 0
      },
      imageWeighting:
      {
         label: "Image Weighting",
         size: 0,
      },
      registration:
      {
         label: "Registration",
         size: 0
      },
      localNormalization:
      {
         label: "Local normalization",
         size: 0
      },
      integration:
      {
         label: "Master lights",
         size: 0
      },
      fastIntegration:
      {
         label: "Fast integration",
         size: 0
      },
      recombination:
      {
         label: "Channel recombination",
         size: 0
      }
   }
};

// ----------------------------------------------------------------------------

StackEngine.prototype.buildExecutionPipeline = function()
{
   this.operationQueue.clear();
   this.resetRequiredSpace();
   this.buildPipelineForBias();
   this.buildPipelineForDark();
   this.buildPipelineForFlat();
   // we bild the defaul pipeline for lights if no pipeline builder is defined
   // or if its execution fails
   this.pipelineBuilderError = "";
   if ( !this.runPipelineBuilder() )
      this.buildPipelineForLight();
};

StackEngine.prototype.runPipeline = function()
{
   this.saveRunningConfiguration();

   if ( engine.usePipelineScript )
      this.operationQueue.installEventScript( engine.pipelineScriptFile );
   else
      this.operationQueue.uninstallEventScript();

   this.operationQueue.run();
};

StackEngine.prototype.runPipelineBuilder = function ()
{
   if ( !this.usePipelineBuilderScript )
      return false;

   if ( this.pipelineBuilderScriptFile == "" )
      return false;

   if ( !File.exists(this.pipelineBuilderScriptFile) )
   {
      engine.pipelineBuilderError = "Script file not found";
      return false;
   }

   let scriptCode = File.readTextFile(this.pipelineBuilderScriptFile);
   if ( scriptCode == "" )
   {
      engine.pipelineBuilderError = "The script file is empty";
      return false;
   }

   let code = "let success = false; { " + scriptCode + " }; success = true; success";
   let success = false;
   try
   {
      success = eval(code);
   }
   catch (e)
   {
      success = false;
      engine.pipelineBuilderError = "Script execution failed.";
      console.warningln( "** Warning: Pipeline builder script failed." );
      console.warningln( e.name );
      console.warningln( e.message );
      console.warningln( e.stack );
   }
   return success;
};

// ----------------------------------------------------------------------------

StackEngine.prototype.generateSignatureProperty = function( window )
{
   window.mainView.setPropertyValue( "PCL:Signature:Preprocessing",
                                     "process=" + engine.id +
                                     ",version=" + engine.version +
                                     ",timestamp=" + (new Date).toISOString(),
                                     PropertyType_IsoString,
                                     PropertyAttribute_Storable | PropertyAttribute_Permanent );
};

// ----------------------------------------------------------------------------

/**
 * Integrates the provided group.
 *
 * @param {*} frameGroup
 * @param {String} customPrefix custom prefix to be added at the end of the master frame (default is "master")
 * @param {String} customPostfix custom postfix to be added at the end of the master frame
 * @param {Boolean} customGenerateRejectionMaps optionally overrides the rejection maps setting
 * @param {Boolean} customGenerateDrizzle optionally override the drizzle files generation
 * @param {String} desiredFileName optionally requires a master file name
 * @param {Boolean} overrideIIparameters to override ImageIntegration's parameters, needs to be an object with key/values
 * @param {Array} FITSKeywords optional list of FITS keyword to inject into the integrated image before saving
 *
 */
StackEngine.prototype.doIntegrate = function( frameGroup,
                                              customPrefix,
                                              customPostfix,
                                              customGenerateRejectionMaps,
                                              customGenerateDrizzle,
                                              desiredFileName,
                                              overrideIIparameters,
                                              FITSKeywords )
{
   let
   {
      enableTargetFrames,
      existingAndUniqueFileName,
      existingDirectory,
      getLastModifiedDate,
   } = WBPPUtils.shared();


   let filePath = "";
   let imageType = frameGroup.imageType;

   console.noteln( "<end><cbr><br>", SEPARATOR );
   console.noteln( "* Begin integration of ", StackEngine.imageTypeToString( imageType ) + " frames" );
   console.noteln( SEPARATOR );
   frameGroup.log();

   let useCache = false;
   let numberOfImages = -1;
   let failedFrames = [];

   let activeFrames = frameGroup.activeFrames();
   if ( activeFrames.length < 3 )
   {
      console.warningln( "** Warning: Cannot integrate less than three frames." );
      this.processLogger.addWarning( "Cannot integrate less than three frames." );
   }
   else
   {
      // Image Integration

      let selectedRejection = ( this.rejection[ imageType ] == ImageIntegration.prototype.auto ) ?
         frameGroup.bestRejectionMethod() : this.rejection[ imageType ];

      if ( this.rejection[ imageType ] == ImageIntegration.prototype.auto )
      {
         console.noteln( "Rejection method auto-selected: ", engine.rejectionName( selectedRejection ) );
         this.processLogger.addMessage( "<b>Rejection method auto-selected:</b> " + engine.rejectionName( selectedRejection ) );
      }
      else
      {
         console.noteln( "<b>Rejection method:</b> ", engine.rejectionName( selectedRejection ) );
      }

      // Drizzle is generated only for Light frames
      let generateDrizzle = imageType == ImageType.LIGHT && ( ( customGenerateDrizzle != undefined ) ? customGenerateDrizzle : true );

      // ensure that drizzle files exists otherwise we disable the drizzle handling
      if ( imageType == ImageType.LIGHT )
         for ( let i = 0; i < activeFrames.length; ++i )
         {
            let sanitizedFileName = activeFrames[ i ].drizzleFile || "";
            let valid = sanitizedFileName.length > 0;

            if ( !valid || !File.exists( sanitizedFileName ) )
            {
               generateDrizzle = false;
               console.warningln( "** Warning: Drizzle file not found: <raw>" + activeFrames[ i ].drizzleFile + "</raw>" );
               console.warningln( "** Disabling the update of the drizzle files." );
               break;
            }
         }

      // add local normalization files if xnml files are found for all frames
      let useLN = activeFrames.reduce( ( acc, item ) => ( acc && ( item.localNormalizationFile != undefined ) ), true );
      let embedRejectionMaps = ( customGenerateRejectionMaps != undefined ) ? customGenerateRejectionMaps : this.generateRejectionMaps;

      let II = new ImageIntegration;
      II.inputHints = this.inputHints();
      II.bufferSizeMB = 16;
      II.stackSizeMB = 1024;
      II.autoMemorySize = true;
      II.autoMemoryLimit = 0.75;
      II.images = enableTargetFrames( activeFrames, 2, generateDrizzle, useLN );
      // FIX: local normalization files are associated to the file item but can have a differnt path,
      // we need to override the file path generated by the enableTargetFrames function
      II.combination = this.combination[ imageType ];
      II.rejection = selectedRejection;
      II.generateRejectionMaps = embedRejectionMaps || this.autocrop;
      II.generateDrizzleData = generateDrizzle;
      II.pcClipLow = this.percentileLow[ imageType ];
      II.pcClipHigh = this.percentileHigh[ imageType ];
      II.sigmaLow = this.sigmaLow[ imageType ];
      II.sigmaHigh = this.sigmaHigh[ imageType ];
      II.winsorizationCutoff = 5.0;
      II.linearFitLow = this.linearFitLow[ imageType ];
      II.linearFitHigh = this.linearFitHigh[ imageType ];
      II.esdOutliersFraction = this.ESD_Outliers[ imageType ];
      II.esdAlpha = this.ESD_Significance[ imageType ];
      II.rcrLimit = this.RCR_Limit[ imageType ];
      II.clipLow = true;
      II.clipHigh = true;
      II.largeScaleClipLow = false;
      II.largeScaleClipHigh = false;
      II.subtractPedestals = false;
      II.truncateOnOutOfRange = true;
      II.generate64BitResult = false;
      II.useFileThreads = true;
      II.fileThreadOverload = 1.00;
      II.weightScale = ImageIntegration.prototype.WeightScale_BWMV;

      switch ( imageType )
      {
         case ImageType.LIGHT:
            II.minWeight = this.minWeight; // since core 1.8.9-1
            II.normalization = ImageIntegration.prototype.AdditiveWithScaling;
            II.rejectionNormalization = ImageIntegration.prototype.Scale;
            II.largeScaleClipHigh = this.lightsLargeScaleRejectionHigh;
            II.largeScaleClipHighProtectedLayers = this.lightsLargeScaleRejectionLayersHigh;
            II.largeScaleClipHighGrowth = this.lightsLargeScaleRejectionGrowthHigh;
            II.largeScaleClipLow = this.lightsLargeScaleRejectionLow;
            II.largeScaleClipLowProtectedLayers = this.lightsLargeScaleRejectionLayersLow;
            II.largeScaleClipLowGrowth = this.lightsLargeScaleRejectionGrowthLow;
            II.subtractPedestals = true;
            break;
         case ImageType.FLAT:
            II.normalization = ImageIntegration.prototype.Multiplicative;
            II.rejectionNormalization = ImageIntegration.prototype.EqualizeFluxes;
            II.largeScaleClipHigh = this.flatsLargeScaleRejection;
            II.largeScaleClipHighProtectedLayers = this.flatsLargeScaleRejectionLayers;
            II.largeScaleClipHighGrowth = this.flatsLargeScaleRejectionGrowth;
            break;
         default:
            II.normalization = ImageIntegration.prototype.NoNormalization;
            II.rejectionNormalization = ImageIntegration.prototype.NoRejectionNormalization;
            break;
      }

      switch ( imageType )
      {
         case ImageType.LIGHT:
            if ( this.subframeWeightingEnabled )
            {
               II.weightMode = [
                  ImageIntegration.prototype.PSFSignalWeight,
                  ImageIntegration.prototype.PSFSNR,
                  ImageIntegration.prototype.PSFScaleSNR,
                  ImageIntegration.prototype.SNREstimate,
                  ImageIntegration.prototype.CSVWeightsFile
               ][ this.subframesWeightsMethod ];
               II.weightKeyword = WEIGHT_KEYWORD;
            }
            else
            {
               II.weightMode = ImageIntegration.prototype.DontCare;
            }

            II.evaluateSNR = true;
            II.rangeClipLow = true;
            II.rangeLow = 0;
            II.rangeClipHigh = false;
            II.truncateOnOutOfRange = false;
            II.useCache = true;
            break;
         default:
            II.weightMode = ImageIntegration.prototype.DontCare;
            II.evaluateSNR = false;
            II.rangeClipLow = false;
            II.rangeClipHigh = false;
            II.useCache = false;
            break;
      }

      // finally enable local normalization if normalization files have been provided
      if ( useLN )
      {
         II.normalization = ImageIntegration.prototype.LocalNormalization;
         II.rejectionNormalization = ImageIntegration.prototype.LocalRejectionNormalization;
         // ### N.B. LN is incompatible with subtraction of pedestals. This is
         // because the LN functions have been computed with the pedestals
         // added. This applies to both rejection and output normalizations.
         II.subtractPedestals = false;
      }

      // override the II parameters
      if ( overrideIIparameters )
      {
         Object.keys( overrideIIparameters ).forEach( key =>
         {
            // must be a valid key
            if ( II[ key ] != undefined )
               II[ key ] = overrideIIparameters[ key ];
         } );
      }

      let csvWeightsFileIsUsed = false;
      let csvWeightsFileIsUnmodified = false;
      let customFormulaPostfix = "";
      // create the weights file if needed
      if ( II.weightMode == ImageIntegration.prototype.CSVWeightsFile )
      {
         csvWeightsFileIsUsed = true;
         let newWeightsFileContents;
         if ( frameGroup.isCFA )
            newWeightsFileContents =
                  activeFrames.map( frame => [frame.current, frame.descriptor.imageWeight,
                                              frame.descriptor.imageWeight, frame.descriptor.imageWeight].join( ", " ) ).join( "\n" );
         else
            newWeightsFileContents = activeFrames.map( frame => [frame.current, frame.descriptor.imageWeight].join( ", " ) ).join( "\n" );
         let outputDirectory = existingDirectory( engine.outputDirectory + "/weights" );
         customFormulaPostfix =
         ( engine.PSFSignalWeight     > 0 ? "_PSFSW-"   + engine.PSFSignalWeight      : "" ) +
         ( engine.PSFSNRWeight        > 0 ? "_PSFSNR-"  + engine.PSFSNRWeight         : "" ) +
         ( engine.SNRWeight           > 0 ? "_SNRW-"    + engine.SNRWeight            : "" ) +
         ( engine.FWHMWeight          > 0 ? "_FWHM-"    + engine.FWHMWeight           : "" ) +
         ( engine.eccentricityWeight  > 0 ? "_ECC-"     + engine.eccentricityWeight   : "" ) +
         ( engine.starsWeight         > 0 ? "_STARS-"   + engine.starsWeight          : "" ) +
         "_PED-" + engine.pedestal;

         let filePath = outputDirectory + "/" + frameGroup.folderName() + customFormulaPostfix + ".csv";
         if ( File.exists( filePath ) )
         {
            let existingWeights = File.readTextFile( filePath );
            if ( existingWeights != newWeightsFileContents )
            {
               File.remove( filePath );
               File.writeTextFile( filePath, newWeightsFileContents );
            }
            else
               csvWeightsFileIsUnmodified = true;
         }
         else
            File.writeTextFile( filePath, newWeightsFileContents );
         II.csvWeightsFilePath = filePath;
      }

      /**
       * Check if valid cached data can be used, generate rejection maps is a key and has to be chcked separately since
       * the masters will change depending on this value while the II.generateRejectionMaps property may have not changed
       * since it also depends on the autocrop option.
       */

      let IISource = II.toSource( "JavaScript", "II" /*varId*/ , 0 /*indent*/ ,
         SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();

      let IIcacheKey = engine.executionCache.keyFor( IISource + "_" + customFormulaPostfix + "_" + this.generateRejectionMaps );
      console.writeln();
      if ( engine.executionCache.hasCacheForKey( IIcacheKey ) )
      {
         console.noteln( "ImageIntegration has cached data for key ", IIcacheKey )
         // until version 2.6.1 Image Integration cache consists in the integrated image filePath
         // from 2.6.2 it become an object
         let cachedObject = engine.executionCache.cacheForKey( IIcacheKey );
         let IICacheOutputFilePath;

         if ( typeof cachedObject == typeof {} )
         {
            IICacheOutputFilePath = cachedObject.IICacheOutputFilePath;
            numberOfImages = cachedObject.numberOfImages;
            failedFrames = cachedObject.failedFrames;
         }

         // we can use the cache only if weights file is used and has not changed
         useCache = !csvWeightsFileIsUsed || ( csvWeightsFileIsUnmodified && csvWeightsFileIsUnmodified );

         // we can use the cache only if there is an unmodified master file
         useCache = useCache && engine.executionCache.isFileUnmodified( IIcacheKey, IICacheOutputFilePath );

         // the cache is valid only if all input files and the integrated file are unchanged
         for ( let i = 0; ( i < activeFrames.length ) && useCache; ++i )
         {
            // source images must be unchanged
            if ( !engine.executionCache.isFileUnmodified( IIcacheKey, activeFrames[ i ].current ) )
               useCache = false;
            if ( useCache && generateDrizzle && !engine.executionCache.isFileUnmodified( IIcacheKey, activeFrames[ i ].drizzleFile ) )
               useCache = false;
            // if using the local normalization then the source images must be unchanged
            if ( useCache && useLN && !engine.executionCache.isFileUnmodified( IIcacheKey, activeFrames[ i ].localNormalizationFile ) )
               useCache = false;
            // keep the status monitor refreshing
            if ( i % 100 == 0 )
               processEvents();
         }

         // determine if using cache or not
         if ( useCache )
         {
            filePath = IICacheOutputFilePath;
            console.noteln( "ImageIntegration the cache is valid, skip the integration and use the cached result at path ", IICacheOutputFilePath );
         }
         else
            console.noteln( "ImageIntegration the cache is not valid, proceed with the integration" );
      }
      else
      {
         console.noteln( "ImageIntegration has no cache data for key ", IIcacheKey );
      }
      console.writeln();

      // cache the drizzle file LMD to be updated if needed
      let drizzleFileLMD = useCache ?
      {} : activeFrames.reduce( ( acc, item ) =>
      {
         if ( item.drizzleFile && item.drizzleFile.length > 0 )
            acc[ item.drizzleFile ] = getLastModifiedDate( item.drizzleFile );
         return acc;
      },
      {} );

      // PROCEED
      console.writeln( SEPARATOR2 );
      console.writeln( IISource );
      console.writeln( SEPARATOR2 );

      engine.processContainer.add( II );
      engine.flushProcessContainer();

      let ok = true;
      if ( useCache )
      {
         console.noteln( "** Using cached data for ImageIntegration." );
         console.noteln( "<end><cbr><br>* master " + StackEngine.imageTypeToString( imageType ) + " frame:" );
         console.noteln( "<raw>" + filePath + "</raw>" );
      }
      else
      {
         II.showImages = false;
         ok = II.executeGlobal();
         II.showImages = true;
         numberOfImages = II.numberOfImages;
      }

      if ( !ok )
      {
         console.warningln( "** Warning: ImageIntegration failed." );
         this.processLogger.addWarning( "ImageIntegration failed." );
      }
      else if ( !useCache )
      {
         // Write master frame FITS keywords
         // Build the file name postfix

         let keywords = new Array;
         if ( FITSKeywords )
            for ( let i = 0; i < FITSKeywords.length; ++i )
               keywords.push( FITSKeywords[ i ] );

         keywords.push( new FITSKeyword( "COMMENT", "", "PixInsight image preprocessing pipeline" ) );
         keywords.push( new FITSKeyword( "COMMENT", "", "Master frame generated with " + engine.title + " v" + engine.version ) );

         keywords.push( new FITSKeyword( "IMAGETYP", StackEngine.imageTypeToMasterKeywordValue( imageType ), "Type of image" ) );

         keywords.push( new FITSKeyword( "XBINNING", format( "%d", frameGroup.binning ), "Binning factor, horizontal axis" ) );
         keywords.push( new FITSKeyword( "YBINNING", format( "%d", frameGroup.binning ), "Binning factor, vertical axis" ) );

         keywords.push( new FITSKeyword( "FILTER", frameGroup.filter, "Filter used when taking image" ) );

         keywords.push( new FITSKeyword( "EXPTIME", format( "%.3f", frameGroup.exposureTime ), "Exposure time in seconds" ) );

         //  inject the overscan area configuration to any master BIAS, DARK and FLATS
         if ( imageType != ImageType.LIGHT && engine.overscan.enabled )
         {
            keywords.push( new FITSKeyword( "OSIR0X0", format( "%d", this.overscan.imageRect.x0 ), "Custom WBPP Info: overscan image rect x0" ) );
            keywords.push( new FITSKeyword( "OSIR0Y0", format( "%d", this.overscan.imageRect.y0 ), "Custom WBPP Info: overscan image rect y0" ) );
            keywords.push( new FITSKeyword( "OSIR0X1", format( "%d", this.overscan.imageRect.x1 ), "Custom WBPP Info: overscan image rect x1" ) );
            keywords.push( new FITSKeyword( "OSIR0Y1", format( "%d", this.overscan.imageRect.y1 ), "Custom WBPP Info: overscan image rect y1" ) );

            for ( let i = 0; i < 4; ++i )
               if ( this.overscan.overscan[ i ].enabled )
               {
                  keywords.push( new FITSKeyword( "OSSR" + i + "X0", format( "%d", this.overscan.overscan[ i ].sourceRect.x0 ), "Custom WBPP Info: overscan source rect x0" ) );
                  keywords.push( new FITSKeyword( "OSSR" + i + "Y0", format( "%d", this.overscan.overscan[ i ].sourceRect.y0 ), "Custom WBPP Info: overscan source rect y0" ) );
                  keywords.push( new FITSKeyword( "OSSR" + i + "X1", format( "%d", this.overscan.overscan[ i ].sourceRect.x1 ), "Custom WBPP Info: overscan source rect x1" ) );
                  keywords.push( new FITSKeyword( "OSSR" + i + "Y1", format( "%d", this.overscan.overscan[ i ].sourceRect.y1 ), "Custom WBPP Info: overscan source rect y1" ) );
                  keywords.push( new FITSKeyword( "OSTR" + i + "X0", format( "%d", this.overscan.overscan[ i ].targetRect.x0 ), "Custom WBPP Info: overscan target rect x0" ) );
                  keywords.push( new FITSKeyword( "OSTR" + i + "Y0", format( "%d", this.overscan.overscan[ i ].targetRect.y0 ), "Custom WBPP Info: overscan target rect y0" ) );
                  keywords.push( new FITSKeyword( "OSTR" + i + "X1", format( "%d", this.overscan.overscan[ i ].targetRect.x1 ), "Custom WBPP Info: overscan target rect x1" ) );
                  keywords.push( new FITSKeyword( "OSTR" + i + "Y1", format( "%d", this.overscan.overscan[ i ].targetRect.y1 ), "Custom WBPP Info: overscan target rect y1" ) );
               }
         }

         // concatenate the image keywords filtering out the keywords already added that have to remain unique
         let uniqueKeywords = [ "IMAGETYP", "XBINNING", "YBINNING", "FILTER", "EXPTIME" ];
         let window = ImageWindow.windowById( II.integrationImageId );
         this.generateSignatureProperty( window );
         window.keywords = keywords.concat( window.keywords.filter( k => uniqueKeywords.indexOf( k.name ) == -1 ) );

         // for masterFlat if overscan is enabled we temporarily set the group size to the overscan region to generate the proper master file
         let fileName = "";
         if ( desiredFileName == undefined &&
            ( frameGroup.imageType == ImageType.FLAT || frameGroup.imageType == ImageType.LIGHT ) &&
            engine.overscan.enabled )
         {
            let W = frameGroup.size.width;
            let H = frameGroup.size.height;
            frameGroup.size.width = engine.overscan.imageRect.x1 - engine.overscan.imageRect.x0;
            frameGroup.size.height = engine.overscan.imageRect.y1 - engine.overscan.imageRect.y0;
            fileName = desiredFileName || frameGroup.folderName( false /* sanitized */ );
            frameGroup.size.width = W;
            frameGroup.size.height = H;
         }
         else
         {
            fileName = desiredFileName || frameGroup.folderName( false /* sanitized */ );
         }
         let prefix = customPrefix != undefined ? customPrefix : "master";
         let fullFileName = prefix + fileName + ( customPostfix || "" ) + ".xisf";
         // ensure file name uniqueness
         filePath = existingAndUniqueFileName( this.outputDirectory + "/master", fullFileName );

         console.noteln( "<end><cbr><br>* Writing master " + StackEngine.imageTypeToString( imageType ) + " frame:" );
         console.noteln( "<raw>" + filePath + "</raw>" );

         // extract the rejection map windows
         let rejectionLowWindow = null;
         let rejectionHighWindow = null;

         if ( II.generateRejectionMaps )
         {
            if ( II.clipLow )
               rejectionLowWindow = ImageWindow.windowById( II.lowRejectionMapImageId );
            if ( II.clipHigh && embedRejectionMaps )
               rejectionHighWindow = ImageWindow.windowById( II.highRejectionMapImageId );

            this.writeImage( filePath,
               [ window, rejectionLowWindow, rejectionHighWindow ],
               [ "integration", "rejection_low", "rejection_high" ] );

            if ( rejectionLowWindow != null && !rejectionLowWindow.isNull )
               rejectionLowWindow.forceClose();
            if ( rejectionHighWindow != null && !rejectionHighWindow.isNull )
               rejectionHighWindow.forceClose();
         }
         else
         {
            this.writeImage( filePath, [ window ], [ "integration" ] );
         }

         window.forceClose();

         // store the cached data
         if ( File.exists( filePath ) )
         {
            console.writeln();
            engine.executionCache.cacheFileLMD( IIcacheKey, filePath );
            let failedFrames = [];
            for ( let i = 0; i < activeFrames.length; ++i )
            {
               // cache the current input frames
               engine.executionCache.cacheFileLMD( IIcacheKey, activeFrames[ i ].current );
               if ( generateDrizzle )
                  engine.executionCache.cacheFileLMD( IIcacheKey, activeFrames[ i ].drizzleFile );
               if ( useLN )
                  engine.executionCache.cacheFileLMD( IIcacheKey, activeFrames[ i ].localNormalizationFile );

               // update the cache for the drizzle file since ImageIntegration modified it (registration will keep it cached)
               let lastLMD = drizzleFileLMD[ activeFrames[ i ].drizzleFile ];
               if ( generateDrizzle && lastLMD )
               {
                  let newLMD = getLastModifiedDate( activeFrames[ i ].drizzleFile );
                  if ( newLMD == lastLMD )
                  {
                     // drizzle file has not changed, we assume that it was excluded from the integration because of low weight.
                     activeFrames[ i ].processingFailed();
                     failedFrames.push( i );
                  }
                  else
                     engine.executionCache.updateLMD( activeFrames[ i ].drizzleFile, lastLMD, newLMD );
               }
            }
            console.writeln();

            // set the cache
            engine.executionCache.setCache( IIcacheKey, {
               IICacheOutputFilePath: filePath,
               numberOfImages: numberOfImages,
               failedFrames: failedFrames
            } );
         }
      } else {
         // we pick from the cache the failed frames and make them fail. This is needed because in the cached
         // integration execution some frames may have been discarded because of the low weight, so we need
         // to replicate this failure also for the cached frames
         for ( let i=0; i < failedFrames.length; ++i )
            activeFrames[failedFrames[i]].processingFailed();
      }
   }

   console.noteln( "<end><cbr><br>", SEPARATOR );
   console.noteln( "* End integration of " + StackEngine.imageTypeToString( imageType ) + " frames" );
   console.noteln( SEPARATOR );

   return {
      masterFilePath: filePath,
      cached: useCache,
      numberOfImages: numberOfImages
   };
};

// ----------------------------------------------------------------------------

/**
 * Performs the fast integration of a group.
 *
 * @param {*} frameGroup
 * @param {*} referenceFramePath
 * @return {*}
 */
StackEngine.prototype.doFastIntegration = function( frameGroup, referenceFramePath )
{
   let
   {
      enableTargetFrames,
      existingAndUniqueFileName,
      existingDirectory,
   } = WBPPUtils.shared();

   console.noteln( "<end><cbr><br>", SEPARATOR );
   console.noteln( "* Begin fast integration of " + StackEngine.imageTypeToString( frameGroup.imageType ) + " frames" );
   console.noteln( SEPARATOR );
   frameGroup.log();

   let useCache = false;
   let numberOfImages = -1;
   let failedFrames = [];
   let filePath="";

   let activeFrames = frameGroup.activeFrames();
   let generateDrizzle = frameGroup.isDrizzleEnabled();
   let generateImages = frameGroup.fastIntegrationSaveImageEnabled();
   let outputDirectory = existingDirectory( engine.outputDirectory + "/fastIntegration/" + frameGroup.folderName() );
   let FI = new FastIntegration;
   FI.inputHints = this.inputHints();
   FI.targets = enableTargetFrames( activeFrames, 2 );
   FI.rejectionFluxRatio = 0.35;
   FI.referenceImage = referenceFramePath;
   FI.generateDrizzleData = generateDrizzle;
   FI.generateImages = generateImages;
   FI.generateRejectionMaps = this.generateRejectionMaps || this.autocrop;
   FI.outputDirectory = outputDirectory;
   FI.overwriteExistingFiles = true;
   FI.preciseAlignmentEnabled = false;
   FI.useROI = false;
   FI.maxStarSearchIterations = 2;
   FI.weightingEnabled = frameGroup.fastIntegrationData.weightingScheme != WBPPFastIntegrationWeightScheme.NONE;
   FI.weightingAlgorithm = Math.max( 0, frameGroup.fastIntegrationData.weightingScheme - 1 );

   // Waiting for the "auto" mode of Fast Integration where the batch size will be computed automatically
   // estimate the memory occupation and adjust accordingly

   let ns = frameGroup.frameSize() / 4; // number of samples

   let targetBytes = ns * ( 32 / 8 );
   let rejectionBytes = ns * ( 8 / 8 );
   let registeredBytes = ns * ( 32 / 8 );
   let integratedImage = ns * ( 64 / 8 );
   let stackCountImage = ns * ( 32 / 8 );

   let fsize = targetBytes + registeredBytes + rejectionBytes;
   let availableMemory = physicalMemoryStatus().availableBytes // get the available physical memory
   // we assume to use 90% of the free memory
   let maxFrames = Math.floor( (availableMemory * 0.9 - integratedImage - stackCountImage)  / fsize );
   let batchSize;
   let prefetchSize;
   if ( maxFrames < 20 )
      batchSize = maxFrames;
   else if ( maxFrames < 40 )
      batchSize = 20;
   else
      batchSize = Math.min( 100, maxFrames / 2 );
   batchSize = Math.max( 10, batchSize );
   prefetchSize = Math.min( 100, Math.max( 0, maxFrames - batchSize ) );

   FI.integrationBatchSize = 10;
   FI.integrationPrefetchSize = 10;

   //

   /**
    * Check if valid cached data can be used, generate rejection maps is a key and has to be checked separately since
    * the masters will change depending on this value while the II.generateRejectionMaps property may have not changed
    * since it also depends on the autocrop option.
    */

   let FISourceForKey = FI.toSource( "JavaScript", "FI" /*varId*/ , 0 /*indent*/ ,
   SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();
   let FIcacheKey = engine.executionCache.keyFor( FISourceForKey );
   console.writeln();

   // these values are set after we generate the cache key otherwise the key could change any time depending
   // on the current memory available
   FI.integrationBatchSize = batchSize;
   FI.integrationPrefetchSize = prefetchSize;

   if ( engine.executionCache.hasCacheForKey( FIcacheKey ) )
   {
      console.noteln( "Fast Integration has cached data for key ", FIcacheKey )
      let cachedObject = engine.executionCache.cacheForKey( FIcacheKey );
      let FICacheOutputFilePath;

      if ( typeof cachedObject == typeof {} )
      {
         FICacheOutputFilePath = cachedObject.FICacheOutputFilePath;
         numberOfImages = cachedObject.numberOfImages;
         failedFrames = cachedObject.failedFrames;
      }

      useCache = FICacheOutputFilePath != undefined;

      // the cache is valid only if all input and output files and the integrated file are unchanged
      for ( let i = 0; ( i < activeFrames.length ) && useCache; ++i )
      {
         let current = activeFrames[ i ].current;
         let drizzleFilePath;
         // source images must be unchanged
         if ( !engine.executionCache.isFileUnmodified( FIcacheKey, current ) )
            useCache = false;
         if ( generateDrizzle )
         {
            let fname = File.extractNameAndExtension( current );
            fname = File.appendToName( fname, "_r" );
            let drizzleFileName = File.changeExtension( fname, ".xdrz" );
            drizzleFilePath = outputDirectory + "/" + drizzleFileName;
            if ( !File.exists( drizzleFilePath ) || !engine.executionCache.isFileUnmodified( FIcacheKey, drizzleFilePath ) )
               useCache = false;
         }
         if ( generateImages )
         {
            let registeredImage = FI.outputDirectory + "/" + File.extractNameAndExtension(activeFrames[ i ].current );
            registeredImage = File.appendToName( registeredImage, "_r" );
            registeredImage = File.changeExtension( registeredImage, ".xisf" );
            if ( drizzleFilePath && File.exists( drizzleFilePath ) && !engine.executionCache.isFileUnmodified( FIcacheKey, registeredImage ) )
               useCache = false;
         }
         // keep the status monitor refreshing
         if ( i % 100 == 0 )
            processEvents();
      }

      // source images must be unchanged
      if ( useCache )
         useCache = engine.executionCache.isFileUnmodified( FIcacheKey, FICacheOutputFilePath );

      // determine if using cache or not
      if ( useCache )
      {
         filePath = FICacheOutputFilePath;
         console.noteln( "Fast Integration the cache is valid, skip the integration and use the cached result at path ", FICacheOutputFilePath );
      }
      else
         console.noteln( "Fast Integration the cache is not valid, proceed with the integration" );
   }
   else
   {
      console.noteln( "Fast Integration has no cache data for key ", FIcacheKey );
   }
   console.writeln();

   // PROCEED
   let FISource = FI.toSource( "JavaScript", "FI" /*varId*/ , 0 /*indent*/ ,
      SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();
   console.writeln( SEPARATOR2 );
   console.writeln( FISource );
   console.writeln( SEPARATOR2 );

   engine.processContainer.add( FI );
   engine.flushProcessContainer();

   let ok = true;
   if ( useCache )
   {
      console.noteln( "** Using cached data for Fast Integration." );
      console.noteln( "<end><cbr><br>* master " + StackEngine.imageTypeToString( frameGroup.imageType ) + " frame:" );
      console.noteln( "<raw>" + filePath + "</raw>" );
   }
   else
   {
      FI.showImages = false;
      console.noteln("Using batch size of ", FI.integrationBatchSize, " and prefetch size of ", FI.integrationPrefetchSize );
      ok = FI.executeGlobal();
      FI.showImages = true;
      numberOfImages = FI.numberOfImages;
   }

   if ( !ok )
   {
      console.warningln( "** Warning: FastIntegration failed." );
      this.processLogger.addWarning( "FastIntegration failed." );
   }
   else if ( !useCache )
   {
      // Write master frame FITS keywords
      // Build the file name postfix

      let keywords = new Array;

      keywords.push( new FITSKeyword( "COMMENT", "", "PixInsight image preprocessing pipeline" ) );
      keywords.push( new FITSKeyword( "COMMENT", "", "Master frame generated with " + engine.title + " v" + engine.version ) );

      keywords.push( new FITSKeyword( "IMAGETYP", StackEngine.imageTypeToMasterKeywordValue( frameGroup.imageType ), "Type of image" ) );

      keywords.push( new FITSKeyword( "XBINNING", format( "%d", frameGroup.binning ), "Binning factor, horizontal axis" ) );
      keywords.push( new FITSKeyword( "YBINNING", format( "%d", frameGroup.binning ), "Binning factor, vertical axis" ) );

      keywords.push( new FITSKeyword( "FILTER", frameGroup.filter, "Filter used when taking image" ) );

      keywords.push( new FITSKeyword( "EXPTIME", format( "%.3f", frameGroup.exposureTime ), "Exposure time in seconds" ) );

      // concatenate the image keywords filtering out the keywords already added that have to remain unique
      let uniqueKeywords = [ "IMAGETYP", "XBINNING", "YBINNING", "FILTER", "EXPTIME" ];
      let window = ImageWindow.windowById( FI.integrationImageId );
      this.generateSignatureProperty( window );
      window.keywords = keywords.concat( window.keywords.filter( k => uniqueKeywords.indexOf( k.name ) == -1 ) );

      let fileName = frameGroup.folderName( false /* sanitized */ );
      let fullFileName = "master" + fileName + "_fastIntegration.xisf";
      // ensure file name uniqueness
      filePath = existingAndUniqueFileName( this.outputDirectory + "/master", fullFileName );

      console.noteln( "<end><cbr><br>* Writing master " + StackEngine.imageTypeToString( frameGroup.imageType ) + " frame:" );
      console.noteln( "<raw>" + filePath + "</raw>" );

      // extract the rejection map windows
      if ( FI.generateRejectionMaps )
      {
         let rejectionLowWindow = ImageWindow.windowById( FI.lowRejectionMapImageId );
         let rejectionHighWindow = ImageWindow.windowById( FI.highRejectionMapImageId );

         this.writeImage( filePath,
            [ window, rejectionLowWindow, rejectionHighWindow ],
            [ "integration", "rejection_low", "rejection_high" ] );

         if ( rejectionLowWindow != null && !rejectionLowWindow.isNull )
            rejectionLowWindow.forceClose();
         if ( rejectionHighWindow != null && !rejectionHighWindow.isNull )
            rejectionHighWindow.forceClose();
      }
      else
      {
         this.writeImage( filePath, [ window ], [ "integration" ] );
      }

      window.forceClose();

      // store the cached data
      if ( File.exists( filePath ) )
      {
         console.writeln();
         engine.executionCache.cacheFileLMD( FIcacheKey, filePath );
         engine.executionCache.cacheFileLMD( FIcacheKey, referenceFramePath );
         let failedFrames = [];
         // cache the current input frames
         for ( let i = 0; i < activeFrames.length; ++i )
         {
            let current = activeFrames[ i ].current;
            engine.executionCache.cacheFileLMD( FIcacheKey, current );
            if ( generateDrizzle )
            {
               let fname = File.extractNameAndExtension( current );
               fname = File.appendToName( fname, "_r" );
               let drizzleFileName = File.changeExtension( fname, ".xdrz" );
               let drizzleFilePath = outputDirectory + "/" + drizzleFileName;
               if ( File.exists( drizzleFilePath ) )
               {
                  engine.executionCache.cacheFileLMD( FIcacheKey, drizzleFilePath );
                  activeFrames[ i ].addDrizzleFile( drizzleFilePath );
               }
            }
            if ( generateImages )
            {
               let registeredImage = FI.outputDirectory + "/" + File.extractNameAndExtension( current );
               registeredImage = File.appendToName( registeredImage, "_r" );
               registeredImage = File.changeExtension( registeredImage, ".xisf" );
               engine.executionCache.cacheFileLMD( FIcacheKey, registeredImage );
            }
         }
         console.writeln();

         // set the cache
         engine.executionCache.setCache( FIcacheKey, {
            FICacheOutputFilePath: filePath,
            numberOfImages: numberOfImages,
            failedFrames: failedFrames
         } );
      }
      else
      {
         // master file does not exist at path
         filePath = undefined;
      }
   }
   else
   {
      // we pick from the cache the failed frames and make them fail. This is needed because in the cached
      // integration execution some frames may have been discarded because of the low weight, so we need
      // to replicate this failure also for the cached frames
      for ( let i = 0; i < failedFrames.length; ++i )
         activeFrames[failedFrames[i]].processingFailed();
         // update the drizzle files from the cache
      for ( let i = 0; i < activeFrames.length; ++i )
      {
         let current = activeFrames[ i ].current;
         if ( generateDrizzle )
         {
            let fname = File.extractNameAndExtension( current );
            fname = File.appendToName( fname, "_r" );
            let drizzleFileName = File.changeExtension( fname, ".xdrz" );
            let drizzleFilePath = outputDirectory + "/" + drizzleFileName;
            if ( File.exists( drizzleFilePath ) )
               activeFrames[ i ].addDrizzleFile( drizzleFilePath );
         }
      }

   }

   console.noteln( "<end><cbr><br>", SEPARATOR );
   console.noteln( "* End fast integration of " + StackEngine.imageTypeToString(frameGroup.imageType ) + " frames" );
   console.noteln( SEPARATOR );

   return {
      masterFilePath: filePath,
      cached: useCache,
      numberOfImages: numberOfImages
   };
}

// ----------------------------------------------------------------------------

/**
 * Performs the drizzle integration.
 *
 * @param {*} frameGroup
 * @param {*} fast
 * @param {*} scale
 * @param {*} shrink
 * @param {*} kernel
 * @param {*} customPrefix custom prefix to be added at the end of the master frame (default is "master")
 * @param {*} customPostfix custom postfix to be added at the end of the master frame
 * @param {*} desiredFileName
 * @param {*} overrideDIparameters
 * @param {*} FITSKeywords optional list of FITS keyword to inject into the integrated image before saving
 * @return {*}
 */
StackEngine.prototype.doDrizzleIntegration = function( frameGroup, fast, scale, shrink, kernel, customPrefix, customPostfix, desiredFileName, overrideIIparameters, FITSKeywords )
{
   let
   {
      existingAndUniqueFileName
   } = WBPPUtils.shared();

   let filePath = "";
   let imageType = frameGroup.imageType;

   frameGroup.log();

   let useCache = false;

   let activeFrames = frameGroup.activeFrames().filter( f => ( f.drizzleFile != undefined && f.drizzleFile.length > 0 ) );
   if ( activeFrames.length < 3 )
   {
      console.warningln( "** Warning: Cannot apply drizzle integration to less than three frames." );
      this.processLogger.addWarning( "Cannot apply drizzle integration to less than three frames." );

      console.noteln( "active frames:\n" )
      console.noteln( JSON.stringify( activeFrames, null, 2 ) );
   }
   else
   {
      if ( activeFrames.length < 15 )
      {
         let msg = "** Warning: it is recommended to perform drizzle integration with a set of frames higher than 15 frames " +
            "(current is " + activeFrames.length + ").";
         console.warningln( "** Warning: " + msg );
         this.processLogger.addWarning( msg );
      }

      let useLN = activeFrames.reduce( ( acc, item ) => ( acc && ( item.localNormalizationFile != undefined ) ), true );

      let DI = new DrizzleIntegration;

      DI.inputData = activeFrames.map( f => ( [ true, f.drizzleFile, useLN ? f.localNormalizationFile : "" ] ) )
      DI.useLUT = fast;
      DI.scale = scale;
      DI.dropShrink = shrink;
      DI.kernelFunction = kernel;
      DI.enableCFA = frameGroup.isCFA || ( frameGroup.associatedRGBchannel == WBPPAssociatedChannel.R ) || ( frameGroup.associatedRGBchannel == WBPPAssociatedChannel.G ) || ( frameGroup.associatedRGBchannel == WBPPAssociatedChannel.B );

      // override the II parameters
      if ( overrideIIparameters )
         Object.keys( overrideIIparameters ).forEach( key =>
         {
            // must be a valid key
            if ( DI[ key ] != undefined )
               DI[ key ] = overrideIIparameters[ key ];
         } );

      /**
       * Check if valid cached data can be used
       */

      let DISource = DI.toSource( "JavaScript", "DI" /*varId*/ , 0 /*indent*/ ,
         SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();

      let DIcacheKey = engine.executionCache.keyFor( DISource );
      console.writeln();
      if ( engine.executionCache.hasCacheForKey( DIcacheKey ) )
      {
         console.noteln( "DrizzleIntegration has cached data for key ", DIcacheKey )
         // Drizzle Integration cache consists in the integrated image filePath
         let DICacheOutputFilePath = engine.executionCache.cacheForKey( DIcacheKey );

         useCache = DICacheOutputFilePath != undefined;

         // the cache is valid only if all input files and the integrated file are unchanged
         for ( let i = 0; i < activeFrames.length && useCache; ++i )
         {
            // source images must be unchanged
            if ( !engine.executionCache.isFileUnmodified( DIcacheKey, activeFrames[ i ].current ) )
               useCache = false;
            // drizzle files must be unchanged
            if ( !engine.executionCache.isFileUnmodified( DIcacheKey, activeFrames[ i ].drizzleFile ) )
               useCache = false;
            // if using the local normalization then the source images must be unchanged
            if ( useCache && useLN && !engine.executionCache.isFileUnmodified( DIcacheKey, activeFrames[ i ].localNormalizationFile ) )
               useCache = false;
         }

         // source images must be unchanged
         if ( useCache )
            useCache = engine.executionCache.isFileUnmodified( DIcacheKey, DICacheOutputFilePath )

         // determine if using cache or not
         if ( useCache )
         {
            filePath = DICacheOutputFilePath;
            console.noteln( "Drizzle Integration the cache is valid, skip the drizzle integration and use the cached result" );
         }
         else
            console.noteln( "Drizzle Integration the cache is not valid, proceed with the drizzle integration" );
      }
      else
      {
         console.noteln( "Drizzle Integration has no cache data for key ", DIcacheKey );
      }
      console.writeln();

      // PROCEED
      console.writeln( SEPARATOR2 );
      console.writeln( DISource );
      console.writeln( SEPARATOR2 );

      engine.processContainer.add( DI );
      engine.flushProcessContainer();

      let ok = true;
      if ( useCache )
      {
         console.noteln( "** Using cached data for Drizzle Integration." );
         console.noteln( "<end><cbr><br>* master " + StackEngine.imageTypeToString( imageType ) + " frame:" );
         console.noteln( "<raw>" + filePath + "</raw>" );
      }
      else
      {
         DI.showImages = false;
         ok = DI.executeGlobal();
         DI.showImages = true;
      }

      if ( !ok )
      {
         console.warningln( "** Warning: DrizzleIntegration failed." );
         this.processLogger.addWarning( "DrizzleIntegration failed." );
      }
      else if ( !useCache )
      {
         // Write master frame FITS keywords
         // Build the file name postfix

         let keywords = new Array;
         if ( FITSKeywords )
            for ( let i = 0; i < FITSKeywords.length; ++i )
               keywords.push( FITSKeywords[ i ] );

         keywords.push( new FITSKeyword( "COMMENT", "", "PixInsight image preprocessing pipeline" ) );
         keywords.push( new FITSKeyword( "COMMENT", "", "Master frame generated with " + engine.title + " v" + engine.version ) );

         keywords.push( new FITSKeyword( "IMAGETYP", StackEngine.imageTypeToMasterKeywordValue( imageType ), "Type of image" ) );

         keywords.push( new FITSKeyword( "XBINNING", format( "%d", frameGroup.binning ), "Binning factor, horizontal axis" ) );
         keywords.push( new FITSKeyword( "YBINNING", format( "%d", frameGroup.binning ), "Binning factor, vertical axis" ) );

         keywords.push( new FITSKeyword( "FILTER", frameGroup.filter, "Filter used when taking image" ) );

         keywords.push( new FITSKeyword( "EXPTIME", format( "%.3f", frameGroup.exposureTime ), "Exposure time in seconds" ) );

         // concatenate the image keywords filtering out the keywords already added that have to remain unique
         let uniqueKeywords = [ "IMAGETYP", "XBINNING", "YBINNING", "FILTER", "EXPTIME" ];
         let window = ImageWindow.windowById( DI.integrationImageId );
         this.generateSignatureProperty( window );
         let weightImage = ImageWindow.windowById( DI.weightImageId );
         window.keywords = keywords.concat( window.keywords.filter( k => uniqueKeywords.indexOf( k.name ) == -1 ) );

         // for masterFlat if overscan is enabled we temporarily set the group size to the overscan region to generate the proper master file
         let fileName = desiredFileName || frameGroup.folderName( false /* sanitized */ );
         let prefix = customPrefix != undefined ? customPrefix : "master";
         let fullFileName = prefix + fileName + ( customPostfix || "" ) + ".xisf";
         // ensure file name uniqueness
         filePath = existingAndUniqueFileName( this.outputDirectory + "/master", fullFileName );

         console.noteln( "<end><cbr><br>* Writing master " + StackEngine.imageTypeToString( imageType ) + " frame:" );
         console.noteln( "<raw>" + filePath + "</raw>" );

         this.writeImage( filePath, [ window, weightImage ], [ "drizzle_integration", "drizzle_weights" ] );

         window.forceClose();
         if ( weightImage )
            weightImage.forceClose();

         // store the cached data
         if ( File.exists( filePath ) )
         {
            console.writeln();
            engine.executionCache.setCache( DIcacheKey, filePath );
            engine.executionCache.cacheFileLMD( DIcacheKey, filePath );
            for ( let i = 0; i < activeFrames.length; ++i )
            {
               engine.executionCache.cacheFileLMD( DIcacheKey, activeFrames[ i ].current );
               engine.executionCache.cacheFileLMD( DIcacheKey, activeFrames[ i ].drizzleFile );
               if ( useLN )
                  engine.executionCache.cacheFileLMD( DIcacheKey, activeFrames[ i ].localNormalizationFile );
            }
            console.writeln();
         }
      }
   }

   return {
      masterFilePath: filePath,
      cached: useCache
   };
};

// ----------------------------------------------------------------------------

/**
 * Calibrate the provided frame group.
 *
 * @param {*} frameGroup
 * @param {*} doMeasurements optionally enable/disable the measurements
 * @returns
 */
StackEngine.prototype.doCalibrate = function( frameGroup, doMeasurements )
{
   let
   {
      enableTargetFrames,
      existingDirectory,
      isEmptyString
   } = WBPPUtils.shared();

   console.noteln( "<end><cbr><br>", SEPARATOR );
   console.noteln( "* Begin calibration of " + StackEngine.imageTypeToString( frameGroup.imageType ) + " frames" );
   console.noteln( SEPARATOR );

   frameGroup.log();

   let activeFrames = frameGroup.activeFrames();
   let cg = this.getCalibrationGroupsFor( frameGroup );

   // -------------------------------
   // get the matching MASTER BIAS
   // -------------------------------
   let masterBias = cg.masterBias;
   let masterBiasPath = masterBias ? masterBias.fileItems[ 0 ].filePath : "";
   let masterBiasEnabled = !isEmptyString( masterBiasPath );

   // -------------------------------
   // get the matching MASTER DARK
   // -------------------------------
   let masterDark = cg.masterDark;
   let masterDarkPath = masterDark ? masterDark.fileItems[ 0 ].filePath : "";
   let masterDarkEnabled = !isEmptyString( masterDarkPath );

   if ( !frameGroup.forceNoDark )
   {
      if ( frameGroup.overrideDark )
      {
         this.processLogger.addMessage( 'Master Dark manually assigned.' )
      }
      else
      {
         console.noteln( "Master Dark automatic match" );
         this.processLogger.addMessage( 'Master Dark automatic match.' );
      }
   }
   else
   {
      this.processLogger.addMessage( 'Master Dark manually disabled.' );
   }

   // -------------------------------
   // get the matching MASTER FLAT
   // -------------------------------
   // flats are enabled only when calibrating light frames
   let masterFlat = cg.masterFlat;
   let masterFlatPath = masterFlat ? masterFlat.fileItems[ 0 ].filePath : "";
   let masterFlatEnabled = !isEmptyString( masterFlatPath );

   if ( !frameGroup.forceNoFlat )
   {
      if ( frameGroup.overrideFlat )
      {
         console.noteln( "Master Flat manually assigned" );
         this.processLogger.addMessage( 'Master Flat manually assigned.' );
      }
      else if ( frameGroup.imageType == ImageType.LIGHT )
      {

         console.noteln( "Master Flat automatic match" );
         this.processLogger.addMessage( 'Master Flat automatic match.' );
      }
   }
   else
   {
      console.noteln( "Master Flat manually disabled" );
      this.processLogger.addMessage( 'Master Flat manually disabled.' );
   }

   // LOG
   this.processLogger.addMessage( '<ul>' );
   if ( masterBiasEnabled )
   {
      console.noteln( "Master bias: " + masterBiasPath );
      this.processLogger.addMessage( "<li>Master bias: " + masterBiasPath + '</li>' );
   }
   else
   {
      console.noteln( "Master bias: none" );
      this.processLogger.addMessage( "<li>Master bias: none</li>" );
   }

   if ( masterDarkEnabled )
   {
      console.noteln( "* Master dark: " + masterDarkPath );
      this.processLogger.addMessage( "<li>Master dark: " + masterDarkPath + '</li>' );
   }
   else
   {
      console.noteln( " Master dark: none" );
      this.processLogger.addMessage( "<li>Master dark: none</li>" );
   }

   if ( masterFlatEnabled )
   {
      console.noteln( "* Master flat: " + masterFlatPath );
      this.processLogger.addMessage( "<li>Master flat: " + masterFlatPath + '</li>' );
   }
   else
   {
      console.noteln( " Master flat: none" );
      this.processLogger.addMessage( "<li>Master flat: none</li>" );
   }

   this.processLogger.addMessage( '<ul>' );

   if ( !engine.overscan.enabled && !masterBiasEnabled && !masterDarkEnabled && !masterFlatEnabled )
   {
      console.warningln( "** Warning: Image calibration skipped for " + StackEngine.imageTypeToString( frameGroup.imageType ) + " of duration " + frameGroup.exposureTime + 's.' );
      this.processLogger.addWarning( "Image calibration skipped for " + StackEngine.imageTypeToString( frameGroup.imageType ) + " of duration " + frameGroup.exposureTime + 's.' );
      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* End calibration of " + StackEngine.imageTypeToString( frameGroup.imageType ) + " frames" );
      console.noteln( SEPARATOR );
      return undefined; /* mark the calibration skipped by returning undefined */
   }

   if ( frameGroup.optimizeMasterDark )
      this.processLogger.addMessage( "Master Dark is optimized." );

   // PREPARE CALIBRATION
   let pedestalMode = function( mode )
   {
      if ( mode >= 0 && mode < WBPPPedestalModeIC.length )
         return WBPPPedestalModeIC[ mode ];
      return ImageCalibration.prototype.OutputPedestal_Auto;
   };

   let IC = new ImageCalibration;

   IC.enableCFA = frameGroup.isCFA
   if ( frameGroup.isCFA )
      IC.cfaPattern = frameGroup.CFAPattern; // ### N.B. Debayer and IC define compatible enumerated parameters for CFA patterns
   IC.inputHints = this.inputHints();
   IC.outputHints = this.outputHints();
   IC.masterBiasEnabled = false;
   IC.masterDarkEnabled = false;
   IC.masterFlatEnabled = false;
   IC.calibrateBias = true; // relevant if we define overscan areas
   IC.calibrateDark = this.overscan.enabled || masterBiasEnabled; // compatibility with pre-calibrated master dark has been removed
   IC.calibrateFlat = false; // assume we have calibrated each individual flat frame
   IC.optimizeDarks = frameGroup.optimizeMasterDark;
   IC.darkOptimizationLow = this.darkOptimizationLow;
   IC.darkOptimizationWindow = this.darkOptimizationWindow;
   IC.separateCFAFlatScalingFactors = masterFlat ? frameGroup.isCFA : false;
   IC.flatScaleClippingFactor = 0.05;
   IC.outputExtension = ".xisf";
   IC.outputPrefix = "";
   IC.outputPostfix = "_c";
   if ( doMeasurements != undefined )
      IC.evaluateNoise = IC.evaluateSignal = doMeasurements;
   else
   // N.B. For CFAs, evaluate noise and signal with Debayer instead of ImageCalibration
      IC.evaluateNoise = IC.evaluateSignal = frameGroup.imageType == ImageType.LIGHT && !frameGroup.isCFA && engine.subframeWeightingEnabled;
   IC.outputSampleFormat = ImageCalibration.prototype.f32;
   IC.overwriteExistingFiles = false;
   IC.onError = ImageCalibration.prototype.Continue;

   if ( frameGroup.imageType == ImageType.LIGHT )
   {
      let lightOutputPedestalLogMessage;
      if ( frameGroup.lightOutputPedestalMode == WBPPPedestalMode.AUTO )
         lightOutputPedestalLogMessage = format( "Light Output Pedestal: auto" );
      else
         lightOutputPedestalLogMessage = format( "Light Output Pedestal: %.0f", frameGroup.lightOutputPedestal );
      this.processLogger.addMessage( lightOutputPedestalLogMessage );
      console.noteln( lightOutputPedestalLogMessage );
      IC.outputPedestal = frameGroup.lightOutputPedestal;
      IC.outputPedestalMode = pedestalMode( frameGroup.lightOutputPedestalMode );
      IC.autoPedestalLimit = frameGroup.lightOutputPedestalLimit;

      // Cosmetic Correction enabling
      IC.cosmeticCorrectionHigh = frameGroup.ccData.enabled;
      IC.cosmeticHighSigma = frameGroup.ccData.highSigma;
   }

   if ( this.overscan.enabled )
   {
      IC.overscanEnabled = true;
      IC.overscanImageX0 = this.overscan.imageRect.x0;
      IC.overscanImageY0 = this.overscan.imageRect.y0;
      IC.overscanImageX1 = this.overscan.imageRect.x1;
      IC.overscanImageY1 = this.overscan.imageRect.y1;
      IC.overscanRegions = [ // enabled, sourceX0, sourceY0, sourceX1, sourceY1, targetX0, targetY0, targetX1, targetY1
         [ false, 0, 0, 0, 0, 0, 0, 0, 0 ],
         [ false, 0, 0, 0, 0, 0, 0, 0, 0 ],
         [ false, 0, 0, 0, 0, 0, 0, 0, 0 ],
         [ false, 0, 0, 0, 0, 0, 0, 0, 0 ]
      ];

      for ( let i = 0; i < 4; ++i )
         if ( this.overscan.overscan[ i ].enabled )
         {
            let M = IC.overscanRegions;
            M[ i ] = [
               true,
               this.overscan.overscan[ i ].sourceRect.x0,
               this.overscan.overscan[ i ].sourceRect.y0,
               this.overscan.overscan[ i ].sourceRect.x1,
               this.overscan.overscan[ i ].sourceRect.y1,
               this.overscan.overscan[ i ].targetRect.x0,
               this.overscan.overscan[ i ].targetRect.y0,
               this.overscan.overscan[ i ].targetRect.x1,
               this.overscan.overscan[ i ].targetRect.y1
            ];
            IC.overscanRegions = M;
         }
   }

   // Set master files
   IC.masterBiasEnabled = masterBiasEnabled;
   IC.masterBiasPath = masterBiasPath

   IC.masterDarkEnabled = masterDarkEnabled;
   IC.masterDarkPath = masterDarkPath;

   IC.masterFlatEnabled = masterFlatEnabled;
   IC.masterFlatPath = masterFlatPath;

   // Set output directories
   let subfolder = frameGroup.folderName();
   IC.outputDirectory = existingDirectory( this.outputDirectory + "/calibrated/" + subfolder );

   let calibratedFiles = [];
   let ICSource = IC.toSource( "JavaScript", "IC" /*varId*/ , 0 /*indent*/ ,
      SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();
   console.writeln( SEPARATOR2 );
   console.writeln( ICSource );
   console.writeln( SEPARATOR2 );

   /*
    * Check if valid cached data can be used. Cache must exist for the same Image Calibration configuration and bias, dark and flat masters
    * must be unchanged (if provided)
    */
   let inputFiles = activeFrames.map( item => item.current );
   let filesToCalibrate = inputFiles;

   let cached = {};
   let ICCache = {};
   let ICcacheKey = engine.executionCache.keyFor( ICSource );
   if ( engine.executionCache.hasCacheForKey( ICcacheKey ) )
   {
      console.noteln( "Image Calibration has cached data for key ", ICcacheKey );

      // the precondition for using the cache is that the calibratino masters have not changed
      let useCache = true;
      if ( masterBiasEnabled && !engine.executionCache.isFileUnmodified( ICcacheKey, masterBiasPath ) )
      {
         console.noteln( "Image Calibration master bias has changed, recalibrate all frames" );
         useCache = false;
      }
      if ( masterDarkEnabled && !engine.executionCache.isFileUnmodified( ICcacheKey, masterDarkPath ) )
      {
         console.noteln( "Image Calibration master dark has changed, recalibrate all frames" );
         useCache = false;
      }
      if ( masterFlatEnabled && !engine.executionCache.isFileUnmodified( ICcacheKey, masterFlatPath ) )
      {
         console.noteln( "Image Calibration master flat has changed, recalibrate all frames" );
         useCache = false;
      }

      if ( useCache )
      {
         ICCache = engine.executionCache.cacheForKey( ICcacheKey );
         filesToCalibrate = [];

         for ( let i = 0; i < inputFiles.length; ++i )
         {
            let inputFile = inputFiles[ i ];
            let outputFile = ICCache[ inputFile ];

            if ( outputFile != undefined &&
               engine.executionCache.isFileUnmodified( ICcacheKey, inputFile ) &&
               engine.executionCache.isFileUnmodified( ICcacheKey, outputFile ) )
            {
               cached[ inputFile ] = outputFile;
               console.noteln( "Image Calibration will use cache for file: ", File.extractNameAndExtension( inputFile ) );
            }
            else
            {
               console.noteln( "Image Calibration will calibrate: ", File.extractNameAndExtension( inputFile ) );
               filesToCalibrate.push( inputFile );
            }
         }
      }
   }
   else
      console.noteln( "Image Calibration has no cached data for key ", ICcacheKey );

   // in process container we store the full calibrated files
   IC.targetFrames = enableTargetFrames( inputFiles, 2 );
   engine.processContainer.add( IC );
   engine.flushProcessContainer();

   // set the files to be calibrated and proceed
   let success = true;
   if ( filesToCalibrate.length > 0 )
   {
      IC.targetFrames = enableTargetFrames( filesToCalibrate, 2 );
      success = IC.executeGlobal();
   }
   let nCached = 0;
   let nGenerated = 0;
   let nFailed = 0;

   // iterate through all input files and store cached data or the new generated ones
   let j = 0;
   for ( let i = 0; i < inputFiles.length; ++i )
   {
      let inputFile = inputFiles[ i ];
      if ( cached[ inputFile ] != undefined )
      {
         calibratedFiles.push( cached[ inputFile ] );
         nCached++;
      }
      else if ( success )
      {
         let outputFile = IC.outputData[ j++ ][ 0 ];

         console.noteln( "IC outputFile: [" + outputFile + "]" );

         if ( outputFile != undefined && outputFile.length > 0 )
         {
            if ( File.exists( outputFile ) )
            {
               calibratedFiles.push( outputFile );

               // update the LMD for the new generated files
               ICCache[ inputFile ] = outputFile;
               engine.executionCache.cacheFileLMD( ICcacheKey, inputFile );
               engine.executionCache.cacheFileLMD( ICcacheKey, outputFile );
               nGenerated++;
            }
            else
            {
               calibratedFiles.push( undefined );
               nFailed++;
            }
         }
         else
         {
            calibratedFiles.push( undefined );
            nFailed++;
         }

         console.noteln( "IC outputFile checked" );
      }
      else
      {
         calibratedFiles.push( undefined );
         nFailed++;
      }
   }
   // update the used master files LMD
   if ( masterBiasEnabled )
      engine.executionCache.cacheFileLMD( ICcacheKey, masterBiasPath );
   if ( masterDarkEnabled )
      engine.executionCache.cacheFileLMD( ICcacheKey, masterDarkPath );
   if ( masterFlatEnabled )
      engine.executionCache.cacheFileLMD( ICcacheKey, masterFlatPath );
   engine.executionCache.setCache( ICcacheKey, ICCache );
   console.noteln( nCached, " cached, ", nGenerated, " generated, ", nFailed, " failed." );

   processEvents();
   gc();

   console.noteln( "<end><cbr><br>", SEPARATOR );
   console.noteln( "* End calibration of " + StackEngine.imageTypeToString( frameGroup.imageType ) + " frames" );
   console.noteln( SEPARATOR );

   return {
      calibratedFiles: calibratedFiles,
      nCached: nCached,
      nGenerated: nGenerated,
      nFailed: nFailed
   };
};

// ----------------------------------------------------------------------------

StackEngine.prototype.combineRGB = function( RfilePath, GfilePath, BfilePath, fileName )
{
   let
   {
      existingAndUniqueFileName
   } = WBPPUtils.shared();

   let filePath = this.outputDirectory + "/master/" + fileName;

   // Check if cached data exists
   let idString = "CombinedRBG_" + RfilePath + "_" + GfilePath + "_" + BfilePath;
   let CRGBcacheKey = engine.executionCache.keyFor( idString );
   let CRBGCache = {};
   if ( engine.executionCache.hasCacheForKey( CRGBcacheKey ) )
   {

      CRBGCache = engine.executionCache.cacheForKey( CRGBcacheKey );
      if ( engine.executionCache.isFileUnmodified( CRGBcacheKey, RfilePath ) &&
         engine.executionCache.isFileUnmodified( CRGBcacheKey, GfilePath ) &&
         engine.executionCache.isFileUnmodified( CRGBcacheKey, BfilePath ) &&
         CRBGCache.outputFName != undefined && engine.executionCache.isFileUnmodified( CRGBcacheKey, CRBGCache.outputFName ) )
      {
         console.noteln( "RGB Combination success with cached data." );
         return {
            success: true,
            filePath: CRBGCache.outputFName,
            cached: true
         };
      }
   }

   // no cached data, create a unique file name
   filePath = existingAndUniqueFileName( this.outputDirectory + "/master", fileName );

   // load the imagaes
   let loopData = [
   {
      c: "R",
      p: RfilePath
   },
   {
      c: "G",
      p: GfilePath
   },
   {
      c: "B",
      p: BfilePath
   } ];
   let windows = [];
   for ( let i = 0; i < 3; ++i )
   {
      let w = ImageWindow.open( loopData[ i ].p );
      if ( w.length == 0 )
      {
         for ( j = 0; j, windows.length; ++j )
            windows[ j ].forceClose();

         return {
            error: "Unable to load " + loopData[ i ].c + " channel image at path " + loopData[ i ].p,
            success: false
         }
      }
      else
         windows.push( w[ 0 ] );
   }

   // run channel combination
   var CC = new ChannelCombination;
   CC.colorSpace = ChannelCombination.prototype.RGB;
   CC.channels = [ // enabled, id
      [ true, windows[ 0 ].mainView.id ],
      [ true, windows[ 1 ].mainView.id ],
      [ true, windows[ 2 ].mainView.id ]
   ];

   engine.processContainer.add( CC );
   engine.flushProcessContainer();

   let res = CC.executeGlobal();

   if ( !res )
   {
      res = {
         error: "Channel combination failed.",
         success: false
      };
   }
   else
   {
      let rgbWindow = ImageWindow.activeWindow;

      // inject all keywords from R image to the combined image
      rgbWindow.keywords = windows[ 0 ].keywords;

      // save the file
      this.writeImage( filePath, [ rgbWindow ], [ "RGB_combination" ] );

      rgbWindow.forceClose();

      // check the result
      if ( !File.exists( filePath ) )
         res = {
            error: "Error savning the combined RGB file.",
            success: false
         };
      else
      {
         CRBGCache.outputFName = filePath;
         engine.executionCache.setCache( CRGBcacheKey, CRBGCache );
         engine.executionCache.cacheFileLMD( CRGBcacheKey, RfilePath );
         engine.executionCache.cacheFileLMD( CRGBcacheKey, GfilePath );
         engine.executionCache.cacheFileLMD( CRGBcacheKey, BfilePath );
         engine.executionCache.cacheFileLMD( CRGBcacheKey, filePath );
         res = {
            success: true,
            filePath: filePath,
            cached: false
         };
      }
   }

   // close R,G,B images
   windows[ 0 ].forceClose();
   windows[ 1 ].forceClose();
   windows[ 2 ].forceClose();

   return res;
};

// ----------------------------------------------------------------------------

/**
 * Cleans up elements that has been deallocated but that are still in the groups
 * or file items lists.
 *
 */
StackEngine.prototype.removePurgedElements = function()
{
   let groups = this.groupsManager.groups;
   for ( let i = groups.length; --i >= 0; )
   {
      if ( !groups[ i ] || groups[ i ].__purged__ )
         this.groupsManager.removeGroupAtIndex( i );
      else
      {
         for ( let j = groups[ i ].fileItems.length; --j >= 0; )
            if ( !groups[ i ].fileItems[ j ] || groups[ i ].fileItems[ j ].__purged__ )
               groups[ i ].removeItem( j );
         if ( groups[ i ].fileItems.length == 0 )
            this.groupsManager.removeGroupAtIndex( i );
      }
   }
};

// ----------------------------------------------------------------------------

/**
 * Encode the JSON string of the list of groups.
 *
 * @returns
 */
StackEngine.prototype.groupsToStringData = function()
{
   // from version 2.1.3 manual groups matching overrides references
   // are replaced by the group ID before saving and restored once reloaded
   this.groupsManager.groups.forEach( ( group ) =>
   {
      if ( group.overrideDark && group.overrideDark.id )
         group.overrideDark = group.overrideDark.id;
      if ( group.overrideFlat && group.overrideFlat.id )
         group.overrideFlat = group.overrideFlat.id;
   } );
   let stringData = JSON.stringify( this.groupsManager.groups, null, 2 );
   this.relinkManualOverrides();
   // save files structure
   return stringData;
};

// ----------------------------------------------------------------------------

/**
 * Decode the list of groups from a JSON string.
 *
 * @param {*} data
 */
StackEngine.prototype.groupsFromStringData = function( data, version )
{
   try
   {
      let groupsData = JSON.parse( data );

      // save files structure
      if ( groupsData )
      {
         this.removePurgedElements();
         this.migrateGroupsData( groupsData, version );
         this.relinkManualOverrides();
         this.reconstructGroups();
      }
   }
   catch ( e )
   {
      console.noteln( e );
      console.noteln( "Error occurred while loading saved groups. Group list will be cleared." );
      this.groupsManager.clear();
   }
};

// ----------------------------------------------------------------------------

/**
 * Migrates old data versions to the current version
 *
 * @param {*} groupsData groups data to be migrated
 */
StackEngine.prototype.migrateGroupsData = function( groupsData, version )
{
   let
   {
      versionLT
   } = WBPPUtils.shared();

   // migration occurs with WBPP only
   if ( version == undefined || ( !this.fastMode && versionLT( version, "2.5.0" ) ) )
   {
      // braking change: group ID changed (fixed)
      console.noteln( "WBPP v2.5.0 group data is not compatible with earlier versions (", version, "). All group properties will be reset to default values." );

      let fileItems = [];
      groupsData.forEach( group =>
      {
         if ( group.mode == WBPPGroupingMode.PRE )
            for ( let j = 0; j < group.fileItems.length; ++j )
               fileItems.push( group.fileItems[ j ] );
      } );

      this.groupsManager.clear();
      this.groupsManager.clearCache();

      // re-add files one by one
      console.show();
      for ( let i = 0; i < fileItems.length; ++i )
      {
         // show progressing
         console.noteln( "reading [", i, "/", fileItems.length, "] ", fileItems[ i ].filePath );
         this.addFile( fileItems[ i ].filePath, fileItems[ i ].imageType );
      }
      console.hide();
   }

   // Migrate the cosmetic correction data. ccData has been added to groups, it contains the information about the CCTemplate name which now is stored at the group root level.
   // We need to move this informatino inside the ccData structure.
   // Valid for
   // - WBPP lower than 2.7.4
   if ( !this.fastMode && versionLT( version, "2.7.4" ) )
   {
      for ( let i = 0; i < groupsData.length; ++i )
         if ( groupsData[ i ].imageType == ImageType.LIGHT )
         {
            let newGroup = new FrameGroup( ImageType.LIGHT );
            groupsData[ i ].ccData = newGroup.ccData;
            groupsData[ i ].ccData.CCTemplate = groupsData[ i ].CCTemplate;
            if ( groupsData[ i ].ccData.CCTemplate )
               groupsData[ i ].ccData.enabled = false;
         }
   }

   this.groupsManager.groups = groupsData;
};

/**
 * Reconstruct the links between the groups that has been manually assigned.
 * When groups are saved, the links are replaced by the the group id string,
 * once reloaded these strings must be replaced by the link to the group.
 *
 * This function must be called when groups has been loaded, migrated and
 * finally assigned to the groups manager.
 */
StackEngine.prototype.relinkManualOverrides = function()
{
   this.groupsManager.groups.forEach( ( group ) =>
   {
      if ( typeof group.overrideDark == "string" )
         group.overrideDark = this.groupsManager.getGroupByID( group.overrideDark );
      if ( typeof group.overrideFlat == "string" )
         group.overrideFlat = this.groupsManager.getGroupByID( group.overrideFlat );
   } );
};

// ----------------------------------------------------------------------------

StackEngine.prototype.migrateKeywords = function( keywords, version )
{
   let migrated = keywords;

   // ---------------------------
   // no keywords are suppored before version [2.0.2]
   if ( version == undefined )
   {
      return [];
   }

   return keywords;
};

// ----------------------------------------------------------------------------

StackEngine.prototype.migrateFrom = function( dataVersion )
{
   if ( dataVersion == undefined || typeof dataVersion != typeof "" )
      dataVersion = "0.0.0";

   let
   {
      versionLT
   } = WBPPUtils.shared();

   // migration versions
   let migrationVersions = [
   {
      version: "2.4.0", // migrate from versions < 2.4.0
      migration: () =>
      {
         // PSF power has been replaced by PSFSNR, set it to PSFSignal in case
         if ( this.subframesWeightsMethod == undefined || this.subframesWeightsMethod == WBPPSubframeWeightsMethod.PSFSNR )
         {
            if ( this.subframesWeightsMethod )
               console.noteln( "* The weighting method PSF Power Weight has been removed and will be replaced by PSF Signal Weight" );
            this.subframesWeightsMethod = WBPPSubframeWeightsMethod.PSFSignal;
         }
         // Rejection methods list has been shortened, map unavailable rejection methods to auto
         for ( let i = 0; i < 4; ++i )
         {
            // index correspond to auto if the rejection method is not listed
            let index = this.rejectionIndex( this.rejection[ i ] )
            this.rejection[ i ] = this.rejectionFromIndex( index )
         }
      }
   },
   {
      version: "2.4.1", // migrate from versions < 2.4.1
      migration: () =>
      {
         // PSFScaleSNR has been introduced after PSFSNR, update the old enumeration
         if ( this.subframesWeightsMethod == undefined || this.subframesWeightsMethod > WBPPSubframeWeightsMethod.PSFSNR )
            this.subframesWeightsMethod = this.subframesWeightsMethod + 1;
      }
   },
   {
      version: "2.4.3", // migrate from versions < 2.4.3
      migration: () =>
      {
         // New StarDetector engine V2 in core 1.8.9-1: reset star detection parameters to default values.
         this.sensitivity = DEFAULT_SA_SENSITIVITY;
         this.peakResponse = DEFAULT_SA_PEAK_RESPONSE;
         this.maxStarDistortion = DEFAULT_SA_MAX_STAR_DISTORTION;
      }
   },
   {
      version: "2.5.0", // migrate from versions < 2.5.0
      migration: () =>
      {
         // fix the issue with the LN max stars out of range (set the default value if OOR)
         if ( this.localNormalizationPsfMaxStars > DEFAULT_LOCALNORMALIZATION_PSF_MAX_STARS ||
            this.localNormalizationPsfMaxStars < DEFAULT_LOCALNORMALIZATION_PSF_MIN_STARS )
            this.localNormalizationPsfMaxStars = DEFAULT_LOCALNORMALIZATION_PSF_MAX_STARS;
         // we always clear the cache when new version is installed
         this.executionCache.reset();
      }
   },
   {
      version: "2.6.2", // migrate from version < 2.6.2
      migration: () =>
      {
         // grid size minimum value is 3
         this.localNormalizationGridSize = Math.min( 3, this.localNormalizationGridSize );
      }

   } ];

   // perform the migrations (WBPP only)
   for ( let i = 0; i < migrationVersions.length; ++i )
      if ( !this.fastMode && versionLT( dataVersion, migrationVersions[ i ].version ) )
      {
         console.noteln( "* Migrating data from WBPP v", dataVersion, " to v", migrationVersions[ i ].version );
         // perform the migration
         migrationVersions[ i ].migration();
         dataVersion = migrationVersions[ i ].version;
      }
};

// ----------------------------------------------------------------------------
/**
 * Saves the current running configuratoin.
 *
 */
StackEngine.prototype.saveRunningConfiguration = function()
{
   // temporary save the parameters
   if ( !this.automationMode )
   {
      let JSON = this.exportParameters( true /* toJSON*/ );
      Settings.write( this.saveSettingsKeyBase + "runningConfiguration", DataType_UCString, JSON );
      Settings.write( this.saveSettingsKeyBase + "runningConfiguration_VERSION", DataType_UCString, this.version );
   }
}

/**
 * Checks if a running configuration is available. The presence of this information measnt hat WBPP has most probably crashed or PI has been
 * terminated during the previous execution.
 *
 */
StackEngine.prototype.hasRunningConfiguration = function()
{
   let hasConfiguration = Settings.read( this.loadSettingsKeyBase + "runningConfiguration", DataType_UCString );
   let version = Settings.read( this.loadSettingsKeyBase + "runningConfiguration_VERSION", DataType_UCString );
   // only configurations from the current version will be restored
   if ( version != this.version )
   {
      this.removeRunningConfiguration();
      return false;
   }
   return hasConfiguration != undefined;
}

/**
 * Restores the saved running configuration. This operation will update all paremeters, rebuild the engine and removes the saved
 * configuraiton.
 *
 */
StackEngine.prototype.restoreRunningConfiguration = function()
{
   if ( !this.hasRunningConfiguration() )
      return;

   let JSON = Settings.read( this.loadSettingsKeyBase + "runningConfiguration", DataType_UCString );
   if ( JSON )
   {
      this.importParameters( JSON );
      engine.rebuild();
   }
   this.removeRunningConfiguration();
}

/**
 * Removes the running configuration.
 *
 */
StackEngine.prototype.removeRunningConfiguration = function()
{
   Settings.remove( this.saveSettingsKeyBase + "runningConfiguration" );
   Settings.remove( this.saveSettingsKeyBase + "runningConfiguration_VERSION" );
}


/**
 * Load the persisted WBPP settings.
 *
 * param {bool} skipMigration - if true the migration will be skipped
 * @returns
 */
StackEngine.prototype.loadSettings = function( skipMigration )
{
   let
   {
      cacheFName,
      versionLT
   } = WBPPUtils.shared();

   function load( key, type )
   {
      return Settings.read( engine.loadSettingsKeyBase + key, type );
   }

   function loadIndexed( key, index, type )
   {
      return load( key + '_' + index.toString(), type );
   }

   let o;

   let dataVersion = undefined;
   if ( ( o = load( "VERSION", DataType_String ) ) != null )
      dataVersion = o;

   if ( ( o = load( "outputDirectory", DataType_String ) ) != null )
   {
      // from version 2.50 and above we use base64 representation to handle non utf8 characters
      if ( !this.loadInWBPP && ( dataVersion == undefined || ( !this.fastMode && versionLT( dataVersion, "2.5.0" ) ) ) )
         this.outputDirectory = o;
      else
      {
         try
         {
            this.outputDirectory = ByteArray.fromBase64( o ).toString();
         }
         catch ( e )
         {
            this.outputDirectory = "";
         }
      }
   }

   if ( ( o = load( "saveFrameGroups", DataType_Boolean ) ) != null )
      this.saveFrameGroups = o;
   if ( ( o = load( "smartNamingOverride", DataType_Boolean ) ) != null )
      this.smartNamingOverride = o;
   if ( ( o = load( "fitsCoordinateConvention", DataType_Int32 ) ) != null )
      this.fitsCoordinateConvention = o;
   if ( ( o = load( "detectMasterIncludingFullPath", DataType_Boolean ) ) != null )
      this.detectMasterIncludingFullPath = o;
   if ( ( o = load( "detectMasterIncludingFullPath", DataType_Boolean ) ) != null )
      this.detectMasterIncludingFullPath = o;
   if ( ( o = load( "generateRejectionMaps", DataType_Boolean ) ) != null )
      this.generateRejectionMaps = o;
   if ( ( o = load( "preserveWhiteBalance", DataType_Boolean ) ) != null )
      this.preserveWhiteBalance = o;
   if ( ( o = load( "groupingKeywordsEnabled", DataType_Boolean ) ) != null )
      this.groupingKeywordsEnabled = o;
   if ( ( o = load( "showAstrometricInfo", DataType_Boolean ) ) != null )
      this.showAstrometricInfo = o;

   if ( ( o = load( "darkOptimizationThreshold", DataType_Float ) ) != null )
      this.darkOptimizationThreshold = o;
   if ( ( o = load( "darkOptimizationLow", DataType_Float ) ) != null )
      this.darkOptimizationLow = o;
   if ( ( o = load( "darkExposureTolerance", DataType_Float ) ) != null )
      this.darkExposureTolerance = o;
   if ( ( o = load( "lightExposureTolerance", DataType_Float ) ) != null )
      this.lightExposureTolerance = o;
   if ( ( o = load( "lightExposureTolerancePost", DataType_Float ) ) != null )
      this.lightExposureTolerancePost = o;

   if ( ( o = load( "overscanEnabled", DataType_Boolean ) ) != null )
      this.overscan.enabled = o;
   for ( let i = 0; i < 4; ++i )
   {
      if ( ( o = loadIndexed( "overscanRegionEnabled", i, DataType_Boolean ) ) != null )
         this.overscan.overscan[ i ].enabled = o;
      if ( ( o = loadIndexed( "overscanSourceX0", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].sourceRect.x0 = o;
      if ( ( o = loadIndexed( "overscanSourceY0", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].sourceRect.y0 = o;
      if ( ( o = loadIndexed( "overscanSourceX1", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].sourceRect.x1 = o;
      if ( ( o = loadIndexed( "overscanSourceY1", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].sourceRect.y1 = o;
      if ( ( o = loadIndexed( "overscanTargetX0", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].targetRect.x0 = o;
      if ( ( o = loadIndexed( "overscanTargetY0", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].targetRect.y0 = o;
      if ( ( o = loadIndexed( "overscanTargetX1", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].targetRect.x1 = o;
      if ( ( o = loadIndexed( "overscanTargetY1", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].targetRect.y1 = o;
   }
   if ( ( o = load( "overscanImageX0", DataType_Int32 ) ) != null )
      this.overscan.imageRect.x0 = o;
   if ( ( o = load( "overscanImageY0", DataType_Int32 ) ) != null )
      this.overscan.imageRect.y0 = o;
   if ( ( o = load( "overscanImageX1", DataType_Int32 ) ) != null )
      this.overscan.imageRect.x1 = o;
   if ( ( o = load( "overscanImageY1", DataType_Int32 ) ) != null )
      this.overscan.imageRect.y1 = o;

   if ( ( o = load( "minWeight", DataType_Float ) ) != null )
      this.minWeight = o;

   for ( let i = 0; i < 4; ++i )
   {
      if ( ( o = loadIndexed( "combination", i, DataType_Int32 ) ) != null )
         this.combination[ i ] = o;
      if ( ( o = loadIndexed( "rejection", i, DataType_Int32 ) ) != null )
         this.rejection[ i ] = o;
      // compatibility from PI 1.8.7 and above
      if ( this.rejection[ i ] == ImageIntegration.CCDClip )
         this.rejection[ i ] = ImageIntegration.auto;
      if ( ( o = loadIndexed( "percentileLow", i, DataType_Float ) ) != null )
         this.percentileLow[ i ] = o;
      if ( ( o = loadIndexed( "percentileHigh", i, DataType_Float ) ) != null )
         this.percentileHigh[ i ] = o;
      if ( ( o = loadIndexed( "sigmaLow", i, DataType_Float ) ) != null )
         this.sigmaLow[ i ] = o;
      if ( ( o = loadIndexed( "sigmaHigh", i, DataType_Float ) ) != null )
         this.sigmaHigh[ i ] = o;
      if ( ( o = loadIndexed( "linearFitLow", i, DataType_Float ) ) != null )
         this.linearFitLow[ i ] = o;
      if ( ( o = loadIndexed( "linearFitHigh", i, DataType_Float ) ) != null )
         this.linearFitHigh[ i ] = o;
      if ( ( o = loadIndexed( "ESD_Outliers", i, DataType_Float ) ) != null )
         this.ESD_Outliers[ i ] = o;
      if ( ( o = loadIndexed( "ESD_Significance", i, DataType_Float ) ) != null )
         this.ESD_Significance[ i ] = o;
      if ( ( o = loadIndexed( "RCR_Limit", i, DataType_Float ) ) != null )
         this.RCR_Limit[ i ] = o;
   }

   if ( ( o = load( "flatsLargeScaleRejection", DataType_Boolean ) ) != null )
      this.flatsLargeScaleRejection = o;
   if ( ( o = load( "flatsLargeScaleRejectionLayers", DataType_Int32 ) ) != null )
      this.flatsLargeScaleRejectionLayers = o;
   if ( ( o = load( "flatsLargeScaleRejectionGrowth", DataType_Int32 ) ) != null )
      this.flatsLargeScaleRejectionGrowth = o;
   if ( ( o = load( "lightsLargeScaleRejectionHigh", DataType_Boolean ) ) != null )
      this.lightsLargeScaleRejectionHigh = o;
   if ( ( o = load( "lightsLargeScaleRejectionLayersHigh", DataType_Int32 ) ) != null )
      this.lightsLargeScaleRejectionLayersHigh = o;
   if ( ( o = load( "lightsLargeScaleRejectionGrowthHigh", DataType_Int32 ) ) != null )
      this.lightsLargeScaleRejectionGrowthHigh = o;
   if ( ( o = load( "lightsLargeScaleRejectionLow", DataType_Boolean ) ) != null )
      this.lightsLargeScaleRejectionLow = o;
   if ( ( o = load( "lightsLargeScaleRejectionLayersLow", DataType_Int32 ) ) != null )
      this.lightsLargeScaleRejectionLayersLow = o;
   if ( ( o = load( "lightsLargeScaleRejectionGrowthLow", DataType_Int32 ) ) != null )
      this.lightsLargeScaleRejectionGrowthLow = o;
   if ( ( o = load( "imageRegistration", DataType_Boolean ) ) != null )
      this.imageRegistration = o;

   if ( ( o = load( "linearPatternSubtraction", DataType_Boolean ) ) != null )
      this.linearPatternSubtraction = o;
   if ( ( o = load( "linearPatternSubtractionRejectionLimit", DataType_Int32 ) ) != null )
      this.linearPatternSubtractionRejectionLimit = o;
   if ( ( o = load( "linearPatternSubtractionMode", DataType_Int32 ) ) != null )
      this.linearPatternSubtractionMode = o;

   if ( ( o = load( "subframeWeightingEnabled", DataType_Boolean ) ) != null )
      this.subframeWeightingEnabled = o;
   if ( ( o = load( "subframeWeightingPreset", DataType_Int32 ) ) != null )
      this.subframeWeightingPreset = o;
   if ( ( o = load( "subframesWeightsMethod", DataType_Int32 ) ) != null )
      this.subframesWeightsMethod = o;

   if ( ( o = load( "FWHMWeight", DataType_Int32 ) ) != null )
      this.FWHMWeight = o;
   if ( ( o = load( "eccentricityWeight", DataType_Int32 ) ) != null )
      this.eccentricityWeight = o;
   if ( ( o = load( "SNRWeight", DataType_Int32 ) ) != null )
      this.SNRWeight = o;
   if ( ( o = load( "starsWeight", DataType_Int32 ) ) != null )
      this.starsWeight = o;
   if ( ( o = load( "PSFSignalWeight", DataType_Int32 ) ) != null )
      this.PSFSignalWeight = o;
   if ( ( o = load( "PSFSNRWeight", DataType_Int32 ) ) != null )
      this.PSFSNRWeight = o;
   if ( ( o = load( "pedestal", DataType_Int32 ) ) != null )
      this.pedestal = o;

   if ( ( o = load( "localNormalization", DataType_Boolean ) ) != null )
      this.localNormalization = o;
   if ( ( o = load( "localNormalizationInteractiveMode", DataType_Boolean ) ) != null )
      this.localNormalizationInteractiveMode = o;
   if ( ( o = load( "localNormalizationGenerateImages", DataType_Boolean ) ) != null )
      this.localNormalizationGenerateImages = o;
   if ( ( o = load( "localNormalizationMethod", DataType_Int32 ) ) != null )
      this.localNormalizationMethod = o;
   if ( ( o = load( "localNormalizationMaxIntegratedFrames", DataType_Int32 ) ) != null )
      this.localNormalizationMaxIntegratedFrames = o;
   if ( ( o = load( "localNormalizationBestReferenceSelectionMethod", DataType_Int32 ) ) != null )
      this.localNormalizationBestReferenceSelectionMethod = o;
   if ( ( o = load( "localNormalizationGridSize", DataType_Int32 ) ) != null )
      this.localNormalizationGridSize = o;
   if ( ( o = load( "localNormalizationReferenceFrameGenerationMethod", DataType_Int32 ) ) != null )
      this.localNormalizationReferenceFrameGenerationMethod = o;
   if ( ( o = load( "localNormalizationPsfType", DataType_Int32 ) ) != null )
      this.localNormalizationPsfType = o;
   if ( ( o = load( "localNormalizationPsfGrowth", DataType_Float ) ) != null )
      this.localNormalizationPsfGrowth = o;
   if ( ( o = load( "localNormalizationPsfMaxStars", DataType_Int32 ) ) != null )
      this.localNormalizationPsfMaxStars = o;
   if ( ( o = load( "localNormalizationPsfMinSNR", DataType_Float ) ) != null )
      this.localNormalizationPsfMinSNR = o;
   if ( ( o = load( "localNormalizationPsfAllowClusteredSources", DataType_Boolean ) ) != null )
      this.localNormalizationPsfAllowClusteredSources = o;
   if ( ( o = load( "localNormalizationLowClippingLevel", DataType_Float ) ) != null )
      this.localNormalizationLowClippingLevel = o;
   if ( ( o = load( "localNormalizationHighClippingLevel", DataType_Float ) ) != null )
      this.localNormalizationHighClippingLevel = o;
   if ( ( o = load( "reuseLastLNReferenceFrames", DataType_Boolean ) ) != null )
      this.reuseLastLNReferenceFrames = o;

   if ( ( o = load( "platesolve", DataType_Boolean ) ) != null )
      this.platesolve = o;
   if ( ( o = load( "platesolveFallbackManual", DataType_Boolean ) ) != null )
      this.platesolveFallbackManual = o;
   if ( ( o = load( "imageSolverRa", DataType_Double ) ) != null )
      this.imageSolverRa = o;
   if ( ( o = load( "imageSolverDec", DataType_Double ) ) != null )
      this.imageSolverDec = o;
   if ( ( o = load( "imageSolverEpoch", DataType_Double ) ) != null )
      this.imageSolverEpoch = o;
   if ( ( o = load( "imageSolverFocalLength", DataType_Float ) ) != null )
      this.imageSolverFocalLength = o;
   if ( ( o = load( "imageSolverPixelSize", DataType_Float ) ) != null )
      this.imageSolverPixelSize = o;
   if ( ( o = load( "imageSolverForceDefaults", DataType_Boolean ) ) != null )
      this.imageSolverForceDefaults = o;

   if ( ( o = load( "pixelInterpolation", DataType_Int32 ) ) != null )
      this.pixelInterpolation = o;
   if ( ( o = load( "clampingThreshold", DataType_Float ) ) != null )
      this.clampingThreshold = o;
   if ( ( o = load( "maxStars", DataType_Int32 ) ) != null )
      this.maxStars = o;
   if ( ( o = load( "distortionCorrection", DataType_Boolean ) ) != null )
      this.distortionCorrection = o;
   if ( ( o = load( "maxSplinePoints", DataType_Int32 ) ) != null )
      this.maxSplinePoints = o;
   if ( ( o = load( "rigidTransformations", DataType_Boolean ) ) != null )
      this.rigidTransformations = o;
   if ( ( o = load( "structureLayers", DataType_Int32 ) ) != null )
      this.structureLayers = o;
   if ( ( o = load( "hotPixelFilterRadius", DataType_Int32 ) ) != null )
      this.hotPixelFilterRadius = o;
   if ( ( o = load( "noiseReductionFilterRadius", DataType_Int32 ) ) != null )
      this.noiseReductionFilterRadius = o;
   if ( ( o = load( "minStructureSize", DataType_Int32 ) ) != null )
      this.minStructureSize = o;
   if ( ( o = load( "sensitivity", DataType_Float ) ) != null )
      this.sensitivity = o;
   if ( ( o = load( "peakResponse", DataType_Float ) ) != null )
      this.peakResponse = o;
   if ( ( o = load( "brightThreshold", DataType_Float ) ) != null )
      this.brightThreshold = o;
   if ( ( o = load( "maxStarDistortion", DataType_Float ) ) != null )
      this.maxStarDistortion = o;
   if ( ( o = load( "allowClusteredSources", DataType_Boolean ) ) != null )
      this.allowClusteredSources = o;
   if ( ( o = load( "useTriangleSimilarity", DataType_Boolean ) ) != null )
      this.useTriangleSimilarity = o;
   if ( ( o = load( "reuseLastReferenceFrames", DataType_Boolean ) ) != null )
      this.reuseLastReferenceFrames = o;

   if ( ( o = load( "integrate", DataType_Boolean ) ) != null )
      this.integrate = o;
   if ( ( o = load( "autocrop", DataType_Boolean ) ) != null )
      this.autocrop = o;
   if ( ( o = load( "autoIntegrationMode", DataType_Boolean ) ) != null )
      this.autoIntegrationMode = o;
   if ( ( o = load( "usePipelineScript", DataType_Boolean ) ) != null )
      this.usePipelineScript = o;
   if ( ( o = load( "pipelineScriptFile", DataType_String ) ) != null )
      this.pipelineScriptFile = o;

   if ( ( o = load( "usePipelineBuilderScript", DataType_Boolean ) ) != null )
      this.usePipelineBuilderScript = o;
   if ( ( o = load( "pipelineBuilderScriptFile", DataType_String ) ) != null )
      this.pipelineBuilderScriptFile = o;

   if ( ( o = load( "referenceImage", DataType_String ) ) != null )
      this.referenceImage = o;
   if ( ( o = load( "bestFrameReferenceKeyword", DataType_String ) ) != null )
      this.bestFrameReferenceKeyword = o;

   // migration from wbpp 2.7.8 and earlier
   if ( ( o = load( "bestFrameRefernceMethod", DataType_Int32 ) ) != null )
      this.bestFrameReferenceMethod = o;
   if ( ( o = load( "bestFrameReferenceMethod", DataType_Int32 ) ) != null )
      this.bestFrameReferenceMethod = o;


   if ( ( o = load( "debayerOutputMethod", DataType_Int32 ) ) != null )
      this.debayerOutputMethod = o;
   if ( ( o = load( "recombineRGB", DataType_Boolean ) ) != null )
      this.recombineRGB = o;
   if ( ( o = load( "debayerActiveChannelR", DataType_Boolean ) ) != null )
      this.debayerActiveChannelR = o;
   if ( ( o = load( "debayerActiveChannelG", DataType_Boolean ) ) != null )
      this.debayerActiveChannelG = o;
   if ( ( o = load( "debayerActiveChannelB", DataType_Boolean ) ) != null )
      this.debayerActiveChannelB = o;

   if ( ( o = load( "enableCompactGUI", DataType_Boolean ) ) != null )
      this.enableCompactGUI = o;

   if ( ( o = load( "keywords", DataType_String ) ) != null )
   {
      let keywords = JSON.parse( o );
      this.keywords.list = this.migrateKeywords( keywords, dataVersion );
   }
   if ( this.saveFrameGroups )
   {
      if ( ( o = load( "groups", DataType_String ) ) != null )
         this.groupsFromStringData( o, dataVersion );
      else if ( ( o = load( "frameGroups", DataType_String ) ) != null ) /* WBPP 2.0.2 */
         this.groupsFromStringData( o, dataVersion );
   }

   try
   {
      let cacheContnet = "";

      // compatibility with cache until WBPP 2.5.10
      if ( ( o = load( "executionCache", DataType_String ) ) != null ) {
         cacheContnet = o;
      }

      if ( cacheContnet.length == 0 )
      {
         let fname = cacheFName();
         cacheContnet = File.readFile( fname ).toString();
      }
      if ( cacheContnet.length > 0 )
      {
         // measure parsing performance
         let elapsed = new ElapsedTime;
         this.executionCache.fromString( cacheContnet );
         console.noteln( "* Parsed cache data in ", elapsed.text );
      }
   }
   catch ( e )
   {
      this.executionCache.reset();
   }

   if ( dataVersion && !skipMigration )
      this.migrateFrom( dataVersion );
};

// ----------------------------------------------------------------------------

/**
 * Persist the WBPP settings.
 *
 */
StackEngine.prototype.saveSettings = function()
{
   let
   {
      cacheFName
   } = WBPPUtils.shared();

   function save( key, type, value )
   {
      try
      {
         Settings.write( engine.saveSettingsKeyBase + key, type, value );
      }
      catch ( e )
      {
         console.warningln( "** Warning: Unable to save [", key, "] for type ", type, " with value ", value );
      }
   }

   function saveIndexed( key, index, type, value )
   {
      try
      {
         save( key + '_' + index.toString(), type, value );
      }
      catch ( e )
      {
         console.warningln( "** Warning: Unable to save [", key, "] for type", type, " with value ", value, " at index ", index );
      }
   }

   // base64 encoding to handle non utf8 chars
   save( "outputDirectory", DataType_String, new ByteArray( this.outputDirectory ).toBase64() );

   save( "saveFrameGroups", DataType_Boolean, this.saveFrameGroups );
   save( "smartNamingOverride", DataType_Boolean, this.smartNamingOverride );
   save( "detectMasterIncludingFullPath", DataType_Boolean, this.detectMasterIncludingFullPath );
   save( "fitsCoordinateConvention", DataType_Int32, this.fitsCoordinateConvention );
   save( "generateRejectionMaps", DataType_Boolean, this.generateRejectionMaps );
   save( "preserveWhiteBalance", DataType_Boolean, this.preserveWhiteBalance );
   save( "groupingKeywordsEnabled", DataType_Boolean, this.groupingKeywordsEnabled );
   save( "showAstrometricInfo", DataType_Boolean, this.showAstrometricInfo );

   save( "darkOptimizationLow", DataType_Float, this.darkOptimizationLow );
   save( "darkExposureTolerance", DataType_Float, this.darkExposureTolerance );
   save( "lightExposureTolerance", DataType_Float, this.lightExposureTolerance );
   save( "lightExposureTolerancePost", DataType_Float, this.lightExposureTolerancePost );

   save( "overscanEnabled", DataType_Boolean, this.overscan.enabled );
   for ( let i = 0; i < 4; ++i )
   {
      saveIndexed( "overscanRegionEnabled", i, DataType_Boolean, this.overscan.overscan[ i ].enabled );
      saveIndexed( "overscanSourceX0", i, DataType_Int32, this.overscan.overscan[ i ].sourceRect.x0 );
      saveIndexed( "overscanSourceY0", i, DataType_Int32, this.overscan.overscan[ i ].sourceRect.y0 );
      saveIndexed( "overscanSourceX1", i, DataType_Int32, this.overscan.overscan[ i ].sourceRect.x1 );
      saveIndexed( "overscanSourceY1", i, DataType_Int32, this.overscan.overscan[ i ].sourceRect.y1 );
      saveIndexed( "overscanTargetX0", i, DataType_Int32, this.overscan.overscan[ i ].targetRect.x0 );
      saveIndexed( "overscanTargetY0", i, DataType_Int32, this.overscan.overscan[ i ].targetRect.y0 );
      saveIndexed( "overscanTargetX1", i, DataType_Int32, this.overscan.overscan[ i ].targetRect.x1 );
      saveIndexed( "overscanTargetY1", i, DataType_Int32, this.overscan.overscan[ i ].targetRect.y1 );
   }
   save( "overscanImageX0", DataType_Int32, this.overscan.imageRect.x0 );
   save( "overscanImageY0", DataType_Int32, this.overscan.imageRect.y0 );
   save( "overscanImageX1", DataType_Int32, this.overscan.imageRect.x1 );
   save( "overscanImageY1", DataType_Int32, this.overscan.imageRect.y1 );

   save( "minWeight", DataType_Float, this.minWeight );

   for ( let i = 0; i < 4; ++i )
   {
      saveIndexed( "combination", i, DataType_Int32, this.combination[ i ] );
      saveIndexed( "rejection", i, DataType_Int32, this.rejection[ i ] );
      saveIndexed( "percentileLow", i, DataType_Float, this.percentileLow[ i ] );
      saveIndexed( "percentileHigh", i, DataType_Float, this.percentileHigh[ i ] );
      saveIndexed( "sigmaLow", i, DataType_Float, this.sigmaLow[ i ] );
      saveIndexed( "sigmaHigh", i, DataType_Float, this.sigmaHigh[ i ] );
      saveIndexed( "linearFitLow", i, DataType_Float, this.linearFitLow[ i ] );
      saveIndexed( "linearFitHigh", i, DataType_Float, this.linearFitHigh[ i ] );
      saveIndexed( "ESD_Outliers", i, DataType_Float, this.ESD_Outliers[ i ] );
      saveIndexed( "ESD_Significance", i, DataType_Float, this.ESD_Significance[ i ] );
      saveIndexed( "RCR_Limit", i, DataType_Float, this.RCR_Limit[ i ] );
   }

   save( "flatsLargeScaleRejection", DataType_Boolean, this.flatsLargeScaleRejection );
   save( "flatsLargeScaleRejectionLayers", DataType_Int32, this.flatsLargeScaleRejectionLayers );
   save( "flatsLargeScaleRejectionGrowth", DataType_Int32, this.flatsLargeScaleRejectionGrowth );
   save( "lightsLargeScaleRejectionHigh", DataType_Boolean, this.lightsLargeScaleRejectionHigh );
   save( "lightsLargeScaleRejectionLayersHigh", DataType_Int32, this.lightsLargeScaleRejectionLayersHigh );
   save( "lightsLargeScaleRejectionGrowthHigh", DataType_Int32, this.lightsLargeScaleRejectionGrowthHigh );
   save( "lightsLargeScaleRejectionLow", DataType_Boolean, this.lightsLargeScaleRejectionLow );
   save( "lightsLargeScaleRejectionLayersLow", DataType_Int32, this.lightsLargeScaleRejectionLayersLow );
   save( "lightsLargeScaleRejectionGrowthLow", DataType_Int32, this.lightsLargeScaleRejectionGrowthLow );
   save( "imageRegistration", DataType_Boolean, this.imageRegistration );

   save( "platesolve", DataType_Boolean, this.platesolve );
   save( "platesolveFallbackManual", DataType_Boolean, this.platesolveFallbackManual );
   save( "imageSolverRa", DataType_Double, this.imageSolverRa );
   save( "imageSolverDec", DataType_Double, this.imageSolverDec );
   save( "imageSolverEpoch", DataType_Double, this.imageSolverEpoch );
   save( "imageSolverFocalLength", DataType_Float, this.imageSolverFocalLength );
   save( "imageSolverPixelSize", DataType_Float, this.imageSolverPixelSize );
   save( "imageSolverForceDefaults", DataType_Boolean, this.imageSolverForceDefaults );

   save( "pixelInterpolation", DataType_Int32, this.pixelInterpolation );
   save( "clampingThreshold", DataType_Float, this.clampingThreshold );
   save( "maxStars", DataType_Int32, this.maxStars );
   save( "distortionCorrection", DataType_Boolean, this.distortionCorrection );
   save( "maxSplinePoints", DataType_Int32, this.maxSplinePoints );
   save( "rigidTransformations", DataType_Boolean, this.rigidTransformations );

   save( "linearPatternSubtraction", DataType_Boolean, this.linearPatternSubtraction );
   save( "linearPatternSubtractionRejectionLimit", DataType_Int32, this.linearPatternSubtractionRejectionLimit );
   save( "linearPatternSubtractionMode", DataType_Int32, this.linearPatternSubtractionMode );

   save( "subframeWeightingEnabled", DataType_Boolean, this.subframeWeightingEnabled );
   save( "subframeWeightingPreset", DataType_Int32, this.subframeWeightingPreset );
   save( "subframesWeightsMethod", DataType_Int32, this.subframesWeightsMethod );
   save( "FWHMWeight", DataType_Int32, this.FWHMWeight );
   save( "eccentricityWeight", DataType_Int32, this.eccentricityWeight );
   save( "SNRWeight", DataType_Int32, this.SNRWeight );
   save( "starsWeight", DataType_Int32, this.starsWeight );
   save( "PSFSignalWeight", DataType_Int32, this.PSFSignalWeight );
   save( "PSFSNRWeight", DataType_Int32, this.PSFSNRWeight );
   save( "pedestal", DataType_Int32, this.pedestal );

   save( "localNormalization", DataType_Boolean, this.localNormalization );
   save( "localNormalizationInteractiveMode", DataType_Boolean, this.localNormalizationInteractiveMode );
   save( "localNormalizationGenerateImages", DataType_Boolean, this.localNormalizationGenerateImages );
   save( "localNormalizationMethod", DataType_Int32, this.localNormalizationMethod );
   save( "localNormalizationMaxIntegratedFrames", DataType_Int32, this.localNormalizationMaxIntegratedFrames );
   save( "localNormalizationBestReferenceSelectionMethod", DataType_Int32, this.localNormalizationBestReferenceSelectionMethod );
   save( "localNormalizationGridSize", DataType_Int32, this.localNormalizationGridSize );
   save( "localNormalizationReferenceFrameGenerationMethod", DataType_Int32, this.localNormalizationReferenceFrameGenerationMethod );
   save( "localNormalizationPsfType", DataType_Int32, this.localNormalizationPsfType );
   save( "localNormalizationPsfGrowth", DataType_Float, this.localNormalizationPsfGrowth );
   save( "localNormalizationPsfMaxStars", DataType_Int32, this.localNormalizationPsfMaxStars );
   save( "localNormalizationPsfMinSNR", DataType_Float, this.localNormalizationPsfMinSNR );
   save( "localNormalizationPsfAllowClusteredSources", DataType_Boolean, this.localNormalizationPsfAllowClusteredSources );
   save( "localNormalizationLowClippingLevel", DataType_Float, this.localNormalizationLowClippingLevel );
   save( "localNormalizationHighClippingLevel", DataType_Float, this.localNormalizationHighClippingLevel );
   save( "reuseLastLNReferenceFrames", DataType_Boolean, this.reuseLastLNReferenceFrames );

   save( "structureLayers", DataType_Int32, this.structureLayers );
   save( "hotPixelFilterRadius", DataType_Int32, this.hotPixelFilterRadius );
   save( "noiseReductionFilterRadius", DataType_Int32, this.noiseReductionFilterRadius );
   save( "minStructureSize", DataType_Int32, this.minStructureSize );
   save( "sensitivity", DataType_Float, this.sensitivity );
   save( "peakResponse", DataType_Float, this.peakResponse );
   save( "brightThreshold", DataType_Float, this.brightThreshold );
   save( "maxStarDistortion", DataType_Float, this.maxStarDistortion );
   save( "allowClusteredSources", DataType_Boolean, this.allowClusteredSources );
   save( "useTriangleSimilarity", DataType_Boolean, this.useTriangleSimilarity );
   save( "reuseLastReferenceFrames", DataType_Boolean, this.reuseLastReferenceFrames );

   save( "integrate", DataType_Boolean, this.integrate );
   save( "autocrop", DataType_Boolean, this.autocrop );
   save( "autoIntegrationMode", DataType_Boolean, this.autoIntegrationMode );

   save( "usePipelineScript", DataType_Boolean, this.usePipelineScript );
   save( "pipelineScriptFile", DataType_String, this.pipelineScriptFile );
   save( "usePipelineBuilderScript", DataType_Boolean, this.usePipelineBuilderScript );
   save( "pipelineBuilderScriptFile", DataType_String, this.pipelineBuilderScriptFile );

   save( "referenceImage", DataType_String, this.referenceImage );
   save( "bestFrameReferenceKeyword", DataType_String, this.bestFrameReferenceKeyword );
   save( "bestFrameReferenceMethod", DataType_Int32, this.bestFrameReferenceMethod );

   save( "debayerOutputMethod", DataType_Int32, this.debayerOutputMethod );
   save( "recombineRGB", DataType_Boolean, this.recombineRGB );
   save( "debayerActiveChannelR", DataType_Boolean, this.debayerActiveChannelR );
   save( "debayerActiveChannelG", DataType_Boolean, this.debayerActiveChannelG );
   save( "debayerActiveChannelB", DataType_Boolean, this.debayerActiveChannelB );

   save( "enableCompactGUI", DataType_Boolean, this.enableCompactGUI );

   save( "VERSION", DataType_String, this.version );

   save( "keywords", DataType_String, JSON.stringify( this.keywords.list ) );

   if ( this.saveFrameGroups )
      save( "groups", DataType_String, this.groupsToStringData() );

   // The execution cache is saved in a dedicated configuration path. We save the current cache only if WBPP is not loaded from a process icon.
   // In fast mode we don't save the cache.
   save( "executionCache", DataType_String, "" );
   if ( !this.cacheHasBeenLoadedFromAnInstance && !this.fastMode )
   {
      let cacheContent = this.executionCache.toString();
      // If the cache is too big, we don't save it
      if ( cacheContent.length > 1024*1024*5 ) {

         let
         {
            readableSize
         } = WBPPUtils.shared();

         let response = new MessageBox( "Large cache detected (" + readableSize( engine.executionCache.size(), 0 ) + "), do you want to purge it?", engine.title, StdIcon_Question, StdButton_Yes, StdButton_No ).execute();
         if ( response == StdButton_Yes )
            engine.executionCache.reset();
      }
      let fname = cacheFName();
      if ( File.exists( fname ) )
         File.remove( fname );
      File.writeTextFile( fname, this.executionCache.toString() );
   }
};

// ----------------------------------------------------------------------------

StackEngine.prototype.defaults =
{
   globals: () =>
   {
   },
   overscan: () =>
   {
      for ( let i = 0; i < 4; ++i )
      {
         engine.overscan.overscan[ i ].enabled = false;
         engine.overscan.overscan[ i ].sourceRect.assign( 0 );
         engine.overscan.overscan[ i ].targetRect.assign( 0 );
      }
      engine.overscan.imageRect.assign( 0 );
   },
   customFormulaWeights: () =>
   {
      engine.subframeWeightingPreset = DEFAULT_SUBFRAMEWEIGHTING_PRESET;
      engine.FWHMWeight = DEFAULT_SUBFRAMEWEIGHTING_FWHM_WEIGHT;
      engine.eccentricityWeight = DEFAULT_SUBFRAMEWEIGHTING_ECCENTRICITY_WEIGHT;
      engine.starsWeight = DEFAULT_SUBFRAMEWEIGHTING_STARS_WEIGHT;
      engine.PSFSignalWeight = DEFAULT_SUBFRAMEWEIGHTING_PSF_SIGNAL_WEIGHT;
      engine.PSFSNRWeight = DEFAULT_SUBFRAMEWEIGHTING_PSF_SNR_WEIGHT;
      engine.SNRWeight = DEFAULT_SUBFRAMEWEIGHTING_SNR_WEIGHT;
      engine.pedestal = DEFAULT_SUBFRAMEWEIGHTING_PEDESTAL;
   },
   imageRegistration: () =>
   {
      engine.pixelInterpolation = DEFAULT_SA_PIXEL_INTERPOLATION;
      engine.clampingThreshold = DEFAULT_SA_CLAMPING_THRESHOLD;
      engine.maxStars = DEFAULT_SA_MAX_STARS;
      engine.distortionCorrection = DEFAULT_SA_DISTORTION_CORRECTION;
      engine.maxSplinePoints = DEFAULT_SA_MAX_SPLINE_POINTS;
      engine.rigidTransformations = DEFAULT_SA_RIGID_TRANSFORMATIONS;
      engine.structureLayers = DEFAULT_SA_STRUCTURE_LAYERS;
      engine.minStructureSize = DEFAULT_SA_MIN_STRUCTURE_SIZE;
      engine.hotPixelFilterRadius = DEFAULT_SA_HOT_PIXEL_FILTER_RADIUS;
      engine.noiseReductionFilterRadius = DEFAULT_SA_NOISE_REDUCTION;
      engine.sensitivity = DEFAULT_SA_SENSITIVITY;
      engine.peakResponse = DEFAULT_SA_PEAK_RESPONSE;
      engine.brightThreshold = DEFAULT_SA_BRIGHT_THRESHOLD;
      engine.maxStarDistortion = DEFAULT_SA_MAX_STAR_DISTORTION;
      engine.allowClusteredSources = DEFAULT_SA_ALLOW_CLUSTERED_SOURCES;
      engine.useTriangleSimilarity = DEFAULT_SA_USE_TRIANGLE_SIMILARITY;
      engine.reuseLastReferenceFrames = DEFAULT_SA_DEFAULT_REUSE_LAST_REFERENCE_FRAMES;
   },
   imageSolver: () =>
   {
      engine.imageSolverRa = DEFAULT_IMAGE_SOLVER_RA;
      engine.imageSolverDec = DEFAULT_IMAGE_SOLVER_DEC;
      engine.imageSolverEpoch = DEFAULT_IMAGE_SOLVER_EPOCH;
      engine.imageSolverFocalLength = DEFAULT_IMAGE_SOLVER_FOCAL_LENGTH;
      engine.imageSolverPixelSize = DEFAULT_IMAGE_SOLVER_PIXEL_SIZE;
      engine.imageSolverForceDefaults = DEFAULT_IMAGE_SOLVER_FORCE_DEFAULTS;
   },
   localNormalization: () =>
   {
      engine.localNormalization = DEFAULT_LOCALNORMALIZATION;
      engine.localNormalizationInteractiveMode = DEFAULT_LOCALNORMALIZATION_INTERACTIVE_MODE;
      engine.localNormalizationGenerateImages = DEFAULT_LOCALNORMALIZATION_GENERATE_IMAGES;
      engine.localNormalizationMethod = DEFAULT_LOCALNORMALIZATION_METHOD;
      engine.localNormalizationMaxIntegratedFrames = DEFAULT_LOCALNORMALIZATION_INTEGRATED_FRAMES;
      engine.localNormalizationBestReferenceSelectionMethod = DEFAULT_LOCALNORMALIZATION_BEST_REFERENCE_METHOD;
      engine.localNormalizationGridSize = DEFAULT_LOCALNORMALIZATION_GRID_SIZE;
      engine.localNormalizationReferenceFrameGenerationMethod = DEFAULT_LOCALNORMALIZATION_REF_FRAME_METHOD;
      engine.localNormalizationPsfType = DEFAULT_LOCALNORMALIZATION_PSF_TYPE;
      engine.localNormalizationPsfGrowth = DEFAULT_LOCALNORMALIZATION_PSF_GROWTH;
      engine.localNormalizationPsfMaxStars = DEFAULT_LOCALNORMALIZATION_PSF_MAX_STARS;
      engine.localNormalizationPsfMinSNR = DEFAULT_LOCALNORMALIZATION_PSF_MIN_SNR;
      engine.localNormalizationPsfAllowClusteredSources = DEFAULT_LOCALNORMALIZATION_PSF_ALLOW_CLUSTERED;
      engine.localNormalizationLowClippingLevel = DEFAULT_LOCALNORMALIZATION_LOW_CLIPPING_LEVEL;
      engine.localNormalizationHighClippingLevel = DEFAULT_LOCALNORMALIZATION_HIGH_CLIPPING_LEVEL;
      engine.reuseLastLNReferenceFrames = DEFAULT_LOCALNORMALIZATION_REUSE_REFERENCE_FRAME;
   },
   imageIntegration: ( imageType ) =>
   {
      engine.combination[ imageType ] = ImageIntegration.prototype.Average;
      engine.rejection[ imageType ] = DEFAULT_REJECTION_METHOD;
      engine.percentileLow[ imageType ] = 0.2;
      engine.percentileHigh[ imageType ] = 0.1;
      engine.sigmaLow[ imageType ] = 4.0;
      engine.sigmaHigh[ imageType ] = 3.0;
      engine.linearFitLow[ imageType ] = 5.0;
      engine.linearFitHigh[ imageType ] = 3.5;
      engine.ESD_Outliers[ imageType ] = 0.3;
      engine.ESD_Significance[ imageType ] = 0.05;
      engine.RCR_Limit[ imageType ] = 0.1;

      if ( imageType == ImageType.FLAT )
      {
         engine.flatsLargeScaleRejection = DEFAULT_LARGE_SCALE_REJECTION;
         engine.flatsLargeScaleRejectionLayers = DEFAULT_LARGE_SCALE_LAYERS;
         engine.flatsLargeScaleRejectionGrowth = DEFAULT_LARGE_SCALE_GROWTH;
      }

      if ( imageType == ImageType.LIGHT )
      {
         engine.minWeight = DEFAULT_MIN_WEIGHT;
         engine.lightsLargeScaleRejectionHigh = DEFAULT_LARGE_SCALE_REJECTION;
         engine.lightsLargeScaleRejectionLayersHigh = DEFAULT_LARGE_SCALE_LAYERS;
         engine.lightsLargeScaleRejectionGrowthHigh = DEFAULT_LARGE_SCALE_GROWTH;
         engine.lightsLargeScaleRejectionLow = DEFAULT_LARGE_SCALE_REJECTION;
         engine.lightsLargeScaleRejectionLayersLow = DEFAULT_LARGE_SCALE_LAYERS;
         engine.lightsLargeScaleRejectionGrowthLow = DEFAULT_LARGE_SCALE_GROWTH;
      }
   }
};

StackEngine.prototype.setDefaultParameters = function()
{
   setDefaultParameters.apply( this );
};

// ----------------------------------------------------------------------------

/**
 * Sets the WBPP's engine default parameters.
 *
 */
function setDefaultParameters()
{
   // General options
   this.detectMasterIncludingFullPath = DEFAULT_MASTER_DETECTION_USES_FULL_PATH;
   this.smartNamingOverride = DEFAULT_SMART_NAMING_OVERRIDE;
   this.saveFrameGroups = DEFAULT_SAVE_FRAME_GROUPS;
   this.outputDirectory = DEFAULT_OUTPUT_DIRECTORY;
   this.fitsCoordinateConvention = DEFAULT_FITS_COORDINATE_CONVENTION;
   this.generateRejectionMaps = engine.fastMode ? false : DEFAULT_GENERATE_REJECTION_MAPS;
   this.preserveWhiteBalance = DEFAULT_PRESERVE_WHITE_BALANCE;
   this.groupingKeywordsEnabled = DEFAULT_GROUPING_KEYWORDS_ACTIVE;
   this.showAstrometricInfo = DEFAULT_SHOW_ASTROMETRIC_INFO;

   // Calibration parameters
   this.darkOptimizationThreshold = 0; // ### deprecated - retained for compatibility
   this.darkOptimizationLow = DEFAULT_DARK_OPTIMIZATION_LOW; // in sigma units from the central value
   this.darkOptimizationWindow = DEFAULT_DARK_OPTIMIZATION_WINDOW;
   this.darkExposureTolerance = DEFAULT_DARK_EXPOSURE_TOLERANCE; // in seconds

   // Image integration parameters
   for ( let imageType = 0; imageType < 4; ++imageType )
      this.defaults.imageIntegration( imageType );

   // Overscan
   this.overscan.enabled = false;
   this.defaults.overscan();

   this.minWeight = DEFAULT_MIN_WEIGHT;

   // Light
   this.lightExposureTolerance = DEFAULT_LIGHT_EXPOSURE_TOLERANCE; // in seconds
   this.lightExposureTolerancePost = DEFAULT_LIGHT_EXPOSURE_TOLERANCE_POST; // in seconds
   this.imageRegistration = DEFAULT_IMAGE_REGISTRATION;

   // Linear Pattern Subtraction
   this.linearPatternSubtraction = DEFAULT_LINEAR_PATTERN_SUBTRACTION;
   this.linearPatternSubtractionRejectionLimit = DEFAULT_LINEAR_PATTERN_SUBTRACTION_SIGMA;
   this.linearPatternSubtractionMode = DEFAULT_LINEAR_PATTERN_SUBTRACTION_MODE;

   // Subframe weights
   this.subframeWeightingEnabled = DEFAULT_SUBFRAMEWEIGHTING_ENABLED;
   this.subframesWeightsMethod = DEFAULT_SUBFRAMEWEIGHTING_METHOD;
   this.defaults.customFormulaWeights();

   // Image Solver
   this.platesolve = DEFAULT_SA_PLATESOLVE;
   this.platesolveFallbackManual = engine.fastMode ? false :DEFAULT_SA_PLATESOLVE_FALLBACK_MANUAL;
   this.defaults.imageSolver();

   // Local normalization
   this.defaults.localNormalization();

   // Image registration
   this.referenceImage = "";
   this.bestFrameReferenceKeyword = "";
   this.bestFrameReferenceMethod = DEFAULT_BEST_REFERENCE_METHOD;
   this.debayerOutputMethod = DEFAULT_DEBAYER_OUTPUT_METHOD;
   this.defaults.imageRegistration();

   // post-calibration
   this.debayerActiveChannelR = true;
   this.debayerActiveChannelG = true;
   this.debayerActiveChannelB = true;
   this.recombineRGB = DEFAULT_RECOMBINE_RGB;

   // pipeline scripting
   this.usePipelineScript = false;
   this.pipelineScriptFile = "";
   this.usePipelineBuilderScript = false;
   this.pipelineBuilderScriptFile = "";

   // FBPP settings
   if ( engine.fastMode )
   {
      // disabled
      this.autocrop = false;
      this.autoIntegrationMode = false;
      this.linearPatternSubtraction = false;
      // enabled (used for "load in WBPP" feautre)
      this.subframeWeightingEnabled = true;
      this.localNormalization = true;
      this.imageRegistration = true;
      this.imageIntegration = true;
   }

   // tracking of groups with auto-enabled fast integration
   this.resetAutomaticFastImagingModeGroups();

   // GUI
   this.enableCompactGUI = false;
   this.GUIBiasPageOverscanControlCollapsed = true;
   this.GUIBiasPageImageIntegrationControlCollapsed = true;
   this.GUIDarkPageImageIntegrationControlCollapsed = true;
   this.GUIFlatPageImageIntegrationControlCollapsed = true;
   this.GUILightPageLPSControlCollapsed = true;
   this.GUILightPageSubframeWeightingControlCollapsed = true;
   this.GUILightPageRegistrationControlCollapsed = true;
   this.GUIPlatesolveControlCollapsed = true;
   this.GUILightPageNormalizationControlCollapsed = true;
   this.GUILightPageIntegrationControlCollapsed = true;

   this.GUIPresetsCollapsed = true;
   this.GUIKeywordsCollapsed = true;
   this.GUIGlobalOptionsCollapsed = true;
   this.GUIReferenceFrameCollapsed = true;
   this.GUIOutputDirCollapsed = true;

   this.GUIGroupOverscanSettingsCollapsed = true;
   this.GUIGroupCalibrationSettingsCollapsed = true;
   this.GUIGroupPedestalSettingsCollapsed = true;
   this.GUIGroupCCSettingsCollapsed = true;
   this.GUIGroupDebayerSettingsCollapsed = true;

   this.globalOptionsHidden = false;

   this.integrate = DEFAULT_INTEGRATE;
   this.autocrop = DEFAULT_AUTOCROP;
   this.autoIntegrationMode = DEFAULT_AUTO_INTEGRATION_MODE;

   this.viewMode = WBPPGroupingMode.PRE;

   this.keywords = new Keywords();

   // automation mode is disabled by default
   this.automationMode = false;

   // simple mode is disabled by default
   this.simple_mode = DEFAULT_SIMPLE_MODE;
}

// ----------------------------------------------------------------------------

/**
 * Import the WBPP settings from an instance.
 *
 */
StackEngine.prototype.importParameters = function( fromJSON )
{
   let
   {
      parameters,
      JSONParameters
   } = WBPPUtils.shared();

   let P = Parameters;
   if ( fromJSON )
   {
      P = JSONParameters;
      parameters = JSONParameters;
      P.clear();
      P.fromJSONtest( fromJSON );
   }
   else if ( !this.automationMode )
   {
      this.setDefaultParameters();
      this.loadSettings();
   }

   if ( P.has( "saveFrameGroups" ) )
      this.saveFrameGroups = P.getBoolean( "saveFrameGroups" );

   if ( P.has( "smartNamingOverride" ) )
      this.smartNamingOverride = P.getBoolean( "smartNamingOverride" );

   if ( P.has( "detectMasterIncludingFullPath" ) )
      this.detectMasterIncludingFullPath = P.getBoolean( "detectMasterIncludingFullPath" );

   if ( P.has( "outputDirectory" ) )
      this.outputDirectory = P.getString( "outputDirectory" );

   if ( P.has( "fitsCoordinateConvention" ) )
      this.fitsCoordinateConvention = P.getInteger( "fitsCoordinateConvention" );

   if ( P.has( "generateRejectionMaps" ) )
      this.generateRejectionMaps = P.getBoolean( "generateRejectionMaps" );

   if ( P.has( "preserveWhiteBalance" ) )
      this.preserveWhiteBalance = P.getBoolean( "preserveWhiteBalance" );

   if ( P.has( "groupingKeywordsEnabled" ) )
      this.groupingKeywordsEnabled = P.getBoolean( "groupingKeywordsEnabled" );

   if ( P.has( "showAstrometricInfo" ) )
      this.showAstrometricInfo = P.getBoolean( "showAstrometricInfo" );

   if ( P.has( "darkOptimizationThreshold" ) )
      this.darkOptimizationThreshold = P.getReal( "darkOptimizationThreshold" );

   if ( P.has( "darkOptimizationLow" ) )
      this.darkOptimizationLow = P.getReal( "darkOptimizationLow" );

   if ( P.has( "darkExposureTolerance" ) )
      this.darkExposureTolerance = P.getReal( "darkExposureTolerance" );

   if ( P.has( "lightExposureTolerance" ) )
      this.lightExposureTolerance = P.getReal( "lightExposureTolerance" );

   if ( P.has( "lightExposureTolerancePost" ) )
      this.lightExposureTolerancePost = P.getReal( "lightExposureTolerancePost" );

   if ( P.has( "overscanEnabled" ) )
      this.overscan.enabled = P.getBoolean( "overscanEnabled" );

   for ( let i = 0; i < 4; ++i )
   {
      if ( P.has( "overscanRegionEnabled" ) )
         this.overscan.overscan[ i ].enabled = parameters.getBooleanIndexed( "overscanRegionEnabled", i );

      if ( P.has( "overscanSourceX0" ) )
         this.overscan.overscan[ i ].sourceRect.x0 = parameters.getIntegerIndexed( "overscanSourceX0", i );

      if ( P.has( "overscanSourceY0" ) )
         this.overscan.overscan[ i ].sourceRect.y0 = parameters.getIntegerIndexed( "overscanSourceY0", i );

      if ( P.has( "overscanSourceX1" ) )
         this.overscan.overscan[ i ].sourceRect.x1 = parameters.getIntegerIndexed( "overscanSourceX1", i );

      if ( P.has( "overscanSourceY1" ) )
         this.overscan.overscan[ i ].sourceRect.y1 = parameters.getIntegerIndexed( "overscanSourceY1", i );

      if ( P.has( "overscanTargetX0" ) )
         this.overscan.overscan[ i ].targetRect.x0 = parameters.getIntegerIndexed( "overscanTargetX0", i );

      if ( P.has( "overscanTargetY0" ) )
         this.overscan.overscan[ i ].targetRect.y0 = parameters.getIntegerIndexed( "overscanTargetY0", i );

      if ( P.has( "overscanTargetX1" ) )
         this.overscan.overscan[ i ].targetRect.x1 = parameters.getIntegerIndexed( "overscanTargetX1", i );

      if ( P.has( "overscanTargetY1" ) )
         this.overscan.overscan[ i ].targetRect.y1 = parameters.getIntegerIndexed( "overscanTargetY1", i );
   }

   if ( P.has( "overscanImageX0" ) )
      this.overscan.imageRect.x0 = P.getInteger( "overscanImageX0" );

   if ( P.has( "overscanImageY0" ) )
      this.overscan.imageRect.y0 = P.getInteger( "overscanImageY0" );

   if ( P.has( "overscanImageX1" ) )
      this.overscan.imageRect.x1 = P.getInteger( "overscanImageX1" );

   if ( P.has( "overscanImageY1" ) )
      this.overscan.imageRect.y1 = P.getInteger( "overscanImageY1" );

   if ( P.has( "minWeight" ) )
      this.minWeight = P.getReal( "minWeight" );

   for ( let i = 0; i < 4; ++i )
   {
      if ( parameters.hasIndexed( "combination", i ) )
         this.combination[ i ] = parameters.getIntegerIndexed( "combination", i );

      if ( parameters.hasIndexed( "rejection", i ) )
         this.rejection[ i ] = parameters.getIntegerIndexed( "rejection", i );

      if ( parameters.hasIndexed( "percentileLow", i ) )
         this.percentileLow[ i ] = parameters.getRealIndexed( "percentileLow", i );

      if ( parameters.hasIndexed( "percentileHigh", i ) )
         this.percentileHigh[ i ] = parameters.getRealIndexed( "percentileHigh", i );

      if ( parameters.hasIndexed( "sigmaLow", i ) )
         this.sigmaLow[ i ] = parameters.getRealIndexed( "sigmaLow", i );

      if ( parameters.hasIndexed( "sigmaHigh", i ) )
         this.sigmaHigh[ i ] = parameters.getRealIndexed( "sigmaHigh", i );

      if ( parameters.hasIndexed( "linearFitLow", i ) )
         this.linearFitLow[ i ] = parameters.getRealIndexed( "linearFitLow", i );

      if ( parameters.hasIndexed( "linearFitHigh", i ) )
         this.linearFitHigh[ i ] = parameters.getRealIndexed( "linearFitHigh", i );

      if ( parameters.hasIndexed( "ESD_Outliers", i ) )
         this.ESD_Outliers[ i ] = parameters.getRealIndexed( "ESD_Outliers", i );

      if ( parameters.hasIndexed( "ESD_Significance", i ) )
         this.ESD_Significance[ i ] = parameters.getRealIndexed( "ESD_Significance", i );

      if ( parameters.hasIndexed( "RCR_Limit", i ) )
         this.RCR_Limit[ i ] = parameters.getRealIndexed( "RCR_Limit", i );
   }

   if ( P.has( "flatsLargeScaleRejection" ) )
      this.flatsLargeScaleRejection = P.getBoolean( "flatsLargeScaleRejection" );

   if ( P.has( "flatsLargeScaleRejectionLayers" ) )
      this.flatsLargeScaleRejectionLayers = P.getInteger( "flatsLargeScaleRejectionLayers" );

   if ( P.has( "flatsLargeScaleRejectionGrowth" ) )
      this.flatsLargeScaleRejectionGrowth = P.getInteger( "flatsLargeScaleRejectionGrowth" );

   if ( P.has( "lightsLargeScaleRejectionHigh" ) )
      this.lightsLargeScaleRejectionHigh = P.getBoolean( "lightsLargeScaleRejectionHigh" );

   if ( P.has( "lightsLargeScaleRejectionLayersHigh" ) )
      this.lightsLargeScaleRejectionLayersHigh = P.getInteger( "lightsLargeScaleRejectionLayersHigh" );

   if ( P.has( "lightsLargeScaleRejectionGrowthHigh" ) )
      this.lightsLargeScaleRejectionGrowthHigh = P.getInteger( "lightsLargeScaleRejectionGrowthHigh" );

   if ( P.has( "lightsLargeScaleRejectionLow" ) )
      this.lightsLargeScaleRejectionLow = P.getBoolean( "lightsLargeScaleRejectionLow" );

   if ( P.has( "lightsLargeScaleRejectionLayersLow" ) )
      this.lightsLargeScaleRejectionLayersLow = P.getInteger( "lightsLargeScaleRejectionLayersLow" );

   if ( P.has( "lightsLargeScaleRejectionGrowthLow" ) )
      this.lightsLargeScaleRejectionGrowthLow = P.getInteger( "lightsLargeScaleRejectionGrowthLow" );

   if ( P.has( "imageRegistration" ) )
      this.imageRegistration = P.getBoolean( "imageRegistration" );

   if ( P.has( "linearPatternSubtraction" ) )
      this.linearPatternSubtraction = P.getBoolean( "linearPatternSubtraction" );

   if ( P.has( "linearPatternSubtractionRejectionLimit" ) )
      this.linearPatternSubtractionRejectionLimit = P.getInteger( "linearPatternSubtractionRejectionLimit" );

   if ( P.has( "linearPatternSubtractionMode" ) )
      this.linearPatternSubtractionMode = P.getInteger( "linearPatternSubtractionMode" );

   if ( P.has( "subframeWeightingEnabled" ) )
      this.subframeWeightingEnabled = P.getBoolean( "subframeWeightingEnabled" );

   if ( P.has( "subframeWeightingPreset" ) )
      this.subframeWeightingPreset = P.getInteger( "subframeWeightingPreset" );

   if ( P.has( "FWHMWeight" ) )
      this.FWHMWeight = P.getInteger( "FWHMWeight" );

   if ( P.has( "eccentricityWeight" ) )
      this.eccentricityWeight = P.getInteger( "eccentricityWeight" );

   if ( P.has( "SNRWeight" ) )
      this.SNRWeight = P.getInteger( "SNRWeight" );

   if ( P.has( "starsWeight" ) )
      this.starsWeight = P.getInteger( "starsWeight" );

   if ( P.has( "PSFSignalWeight" ) )
      this.PSFSignalWeight = P.getInteger( "PSFSignalWeight" );

   if ( P.has( "PSFSNRWeight" ) )
      this.PSFSNRWeight = P.getInteger( "PSFSNRWeight" );

   if ( P.has( "pedestal" ) )
      this.pedestal = P.getInteger( "pedestal" );

   if ( P.has( "localNormalization" ) )
      this.localNormalization = P.getBoolean( "localNormalization" );

   if ( P.has( "localNormalizationInteractiveMode" ) )
      this.localNormalizationInteractiveMode = P.getBoolean( "localNormalizationInteractiveMode" );

   if ( P.has( "localNormalizationGenerateImages" ) )
      this.localNormalizationGenerateImages = P.getBoolean( "localNormalizationGenerateImages" );

   if ( P.has( "localNormalizationMethod" ) )
      this.localNormalizationMethod = P.getInteger( "localNormalizationMethod" );

   if ( P.has( "localNormalizationMaxIntegratedFrames" ) )
      this.localNormalizationMaxIntegratedFrames = P.getInteger( "localNormalizationMaxIntegratedFrames" );

   if ( P.has( "localNormalizationBestReferenceSelectionMethod" ) )
      this.localNormalizationBestReferenceSelectionMethod = P.getInteger( "localNormalizationBestReferenceSelectionMethod" );

   if ( P.has( "localNormalizationGridSize" ) )
      this.localNormalizationGridSize = P.getInteger( "localNormalizationGridSize" );

   if ( P.has( "localNormalizationReferenceFrameGenerationMethod" ) )
      this.localNormalizationReferenceFrameGenerationMethod = P.getInteger( "localNormalizationReferenceFrameGenerationMethod" );

   if ( P.has( "localNormalizationPsfType" ) )
      this.localNormalizationPsfType = P.getInteger( "localNormalizationPsfType" );

   if ( P.has( "localNormalizationPsfGrowth" ) )
      this.localNormalizationPsfGrowth = P.getReal( "localNormalizationPsfGrowth" );

   if ( P.has( "localNormalizationPsfMaxStars" ) )
      this.localNormalizationPsfMaxStars = P.getInteger( "localNormalizationPsfMaxStars" );

   if ( P.has( "localNormalizationPsfMinSNR" ) )
      this.localNormalizationPsfMinSNR = P.getReal( "localNormalizationPsfMinSNR" );

   if ( P.has( "localNormalizationPsfAllowClusteredSources" ) )
      this.localNormalizationPsfAllowClusteredSources = P.getBoolean( "localNormalizationPsfAllowClusteredSources" );

   if ( P.has( "localNormalizationLowClippingLevel" ) )
      this.localNormalizationLowClippingLevel = P.getReal( "localNormalizationLowClippingLevel" );

   if ( P.has( "localNormalizationHighClippingLevel" ) )
      this.localNormalizationHighClippingLevel = P.getReal( "localNormalizationHighClippingLevel" );

   if ( P.has( "reuseLastLNReferenceFrames" ) )
      this.reuseLastLNReferenceFrames = P.getBoolean( "reuseLastLNReferenceFrames" );

   if ( P.has( "subframesWeightsMethod" ) )
      this.subframesWeightsMethod = P.getInteger( "subframesWeightsMethod" );

   if ( P.has( "platesolve" ) )
      this.platesolve = P.getBoolean( "platesolve" );

   if ( P.has( "platesolveFallbackManual" ) )
      this.platesolveFallbackManual = P.getBoolean( "platesolveFallbackManual" );

   if ( P.has( "imageSolverRa" ) )
      this.imageSolverRa = P.getReal( "imageSolverRa" );

   if ( P.has( "imageSolverDec" ) )
      this.imageSolverDec = P.getReal( "imageSolverDec" );

   if ( P.has( "imageSolverEpoch" ) )
      this.imageSolverEpoch = P.getReal( "imageSolverEpoch" );

   if ( P.has( "imageSolverFocalLength" ) )
      this.imageSolverFocalLength = P.getReal( "imageSolverFocalLength" );

   if ( P.has( "imageSolverPixelSize" ) )
      this.imageSolverPixelSize = P.getReal( "imageSolverPixelSize" );

   if ( P.has( "imageSolverForceDefaults" ) )
      this.imageSolverForceDefaults = P.getBoolean( "imageSolverForceDefaults" );

   if ( P.has( "pixelInterpolation" ) )
      this.pixelInterpolation = P.getInteger( "pixelInterpolation" );

   if ( P.has( "clampingThreshold" ) )
      this.clampingThreshold = P.getReal( "clampingThreshold" );

   if ( P.has( "maxStars" ) )
      this.maxStars = P.getInteger( "maxStars" );

   if ( P.has( "distortionCorrection" ) )
      this.distortionCorrection = P.getBoolean( "distortionCorrection" );

   if ( P.has( "maxSplinePoints" ) )
      this.structureLayers = P.getInteger( "maxSplinePoints" );

   if ( P.has( "rigidTransformations" ) )
      this.rigidTransformations = P.getBoolean( "rigidTransformations" );

   if ( P.has( "structureLayers" ) )
      this.structureLayers = P.getInteger( "structureLayers" );

   if ( P.has( "hotPixelFilterRadius" ) )
      this.hotPixelFilterRadius = P.getInteger( "hotPixelFilterRadius" );

   if ( P.has( "noiseReductionFilterRadius" ) )
      this.noiseReductionFilterRadius = P.getInteger( "noiseReductionFilterRadius" );

   if ( P.has( "minStructureSize" ) )
      this.minStructureSize = P.getInteger( "minStructureSize" );

   if ( P.has( "sensitivity" ) )
      this.sensitivity = P.getReal( "sensitivity" );

   if ( P.has( "peakResponse" ) )
      this.peakResponse = P.getReal( "peakResponse" );

   if ( P.has( "brightThreshold" ) )
      this.brightThreshold = P.getReal( "brightThreshold" );

   if ( P.has( "maxStarDistortion" ) )
      this.maxStarDistortion = P.getReal( "maxStarDistortion" );

   if ( P.has( "allowClusteredSources" ) )
      this.allowClusteredSources = P.getBoolean( "allowClusteredSources" );

   if ( P.has( "useTriangleSimilarity" ) )
      this.useTriangleSimilarity = P.getBoolean( "useTriangleSimilarity" );

   if ( P.has( "reuseLastReferenceFrames" ) )
      this.reuseLastReferenceFrames = P.getBoolean( "reuseLastReferenceFrames" );

   if ( P.has( "referenceImage" ) )
      this.referenceImage = P.getString( "referenceImage" );

   if ( P.has( "bestFrameReferenceKeyword" ) )
      this.bestFrameReferenceKeyword = P.getString( "bestFrameReferenceKeyword" );

   // migration from wbpp 2.7.8 and earlier
   if ( P.has( "bestFrameRefernceMethod" ) )
      this.bestFrameReferenceMethod = P.getInteger( "bestFrameRefernceMethod" );

   if ( P.has( "bestFrameReferenceMethod" ) )
      this.bestFrameReferenceMethod = P.getInteger( "bestFrameReferenceMethod" );

   if ( P.has( "debayerOutputMethod" ) )
      this.debayerOutputMethod = P.getInteger( "debayerOutputMethod" );

   if ( P.has( "integrate" ) )
      this.integrate = P.getBoolean( "integrate" );

   if ( P.has( "autocrop" ) )
      this.autocrop = P.getBoolean( "autocrop" );
   if ( P.has( "autoIntegrationMode" ) )
      this.autoIntegrationMode = P.getBoolean( "autoIntegrationMode" );

   if ( P.has( "recombineRGB" ) )
      this.recombineRGB = P.getBoolean( "recombineRGB" );

   if ( P.has( "debayerActiveChannelR" ) )
      this.debayerActiveChannelR = P.getBoolean( "debayerActiveChannelR" );

   if ( P.has( "debayerActiveChannelG" ) )
      this.debayerActiveChannelG = P.getBoolean( "debayerActiveChannelG" );

   if ( P.has( "debayerActiveChannelB" ) )
      this.debayerActiveChannelB = P.getBoolean( "debayerActiveChannelB" );

   if ( P.has( "usePipelineScript" ) )
      this.usePipelineScript = P.getBoolean( "usePipelineScript" );

   if ( P.has( "pipelineScriptFile" ) )
      this.pipelineScriptFile = P.getString( "pipelineScriptFile" );

   if ( P.has( "usePipelineBuilderScript" ) )
      this.usePipelineBuilderScript = P.getBoolean( "usePipelineBuilderScript" );

   if ( P.has( "pipelineBuilderScriptFile" ) )
      this.pipelineBuilderScriptFile = P.getString( "pipelineBuilderScriptFile" );

   if ( P.has( "enableCompactGUI" ) )
      this.enableCompactGUI = P.getBoolean( "enableCompactGUI" );

   let dataVersion = undefined;
   if ( P.has( "VERSION" ) )
      dataVersion = P.getString( "VERSION" );

   if ( P.has( "keywords" ) )
   {
      let dataStr = ByteArray.fromBase64( P.getString( "keywords" ) ).toString();
      let keywords = JSON.parse( dataStr );
      this.keywords.list = this.migrateKeywords( keywords, dataVersion );
   }

   if ( fromJSON && P.has( "testExecutionStatus" ) )
      this.executionStatus = JSON.parse( P.getString( "testExecutionStatus" ) );

   if ( !this.automationMode || fromJSON )
   {
      if ( P.has( "groups" ) )
         this.groupsFromStringData( ByteArray.fromBase64( P.getString( "groups" ) ).toString(), dataVersion );

      if ( P.has( CACHE_VERSION ) )
      // This happens only if the script has been loaded from a process icon or from an external JSON (like tests).
      {
         console.noteln( "has execution cache" );
         try
         {
            this.executionCache.fromString( ByteArray.fromBase64( P.getString( CACHE_VERSION ) ) );
         }
         catch ( e )
         {
            console.warningln( "** Warning: Cache data parsing has failed." );
            this.executionCache.reset();
         }
         this.cacheHasBeenLoadedFromAnInstance = true;
      }

      if ( dataVersion )
         this.migrateFrom( dataVersion );
   }
};

// ----------------------------------------------------------------------------

/**
 * Prepare the export of the WBPP parameters to an instance.
 *
 * @param {Boolean} toJSON if true, the function will return a json object containing the saved parameters
 */
StackEngine.prototype.exportParameters = function( toJSON )
{
   let
   {
      parameters,
      JSONParameters
   } = WBPPUtils.shared();

   let P = Parameters;
   if ( toJSON )
   {
      P = JSONParameters;
      parameters = JSONParameters;
   }

   P.clear();

   P.set( "VERSION", this.version );

   P.set( "saveFrameGroups", this.saveFrameGroups );
   P.set( "smartNamingOverride", this.smartNamingOverride );
   P.set( "detectMasterIncludingFullPath", this.detectMasterIncludingFullPath );
   P.set( "outputDirectory", this.outputDirectory );
   P.set( "fitsCoordinateConvention", this.fitsCoordinateConvention );
   P.set( "generateRejectionMaps", this.generateRejectionMaps );
   P.set( "preserveWhiteBalance", this.preserveWhiteBalance );

   P.set( "groupingKeywordsEnabled", this.groupingKeywordsEnabled );
   P.set( "showAstrometricInfo", this.showAstrometricInfo );

   P.set( "darkOptimizationLow", this.darkOptimizationLow );
   P.set( "darkExposureTolerance", this.darkExposureTolerance );

   P.set( "overscanEnabled", this.overscan.enabled );

   for ( let i = 0; i < 4; ++i )
   {
      parameters.setIndexed( "overscanRegionEnabled", i, this.overscan.overscan[ i ].enabled );
      parameters.setIndexed( "overscanSourceX0", i, this.overscan.overscan[ i ].sourceRect.x0 );
      parameters.setIndexed( "overscanSourceY0", i, this.overscan.overscan[ i ].sourceRect.y0 );
      parameters.setIndexed( "overscanSourceX1", i, this.overscan.overscan[ i ].sourceRect.x1 );
      parameters.setIndexed( "overscanSourceY1", i, this.overscan.overscan[ i ].sourceRect.y1 );
      parameters.setIndexed( "overscanTargetX0", i, this.overscan.overscan[ i ].targetRect.x0 );
      parameters.setIndexed( "overscanTargetY0", i, this.overscan.overscan[ i ].targetRect.y0 );
      parameters.setIndexed( "overscanTargetX1", i, this.overscan.overscan[ i ].targetRect.x1 );
      parameters.setIndexed( "overscanTargetY1", i, this.overscan.overscan[ i ].targetRect.y1 );
   }

   P.set( "overscanImageX0", this.overscan.imageRect.x0 );
   P.set( "overscanImageY0", this.overscan.imageRect.y0 );
   P.set( "overscanImageX1", this.overscan.imageRect.x1 );
   P.set( "overscanImageY1", this.overscan.imageRect.y1 );

   P.set( "minWeight", this.minWeight );

   for ( let i = 0; i < 4; ++i )
   {
      parameters.setIndexed( "combination", i, this.combination[ i ] );
      parameters.setIndexed( "rejection", i, this.rejection[ i ] );
      parameters.setIndexed( "percentileLow", i, this.percentileLow[ i ] );
      parameters.setIndexed( "percentileHigh", i, this.percentileHigh[ i ] );
      parameters.setIndexed( "sigmaLow", i, this.sigmaLow[ i ] );
      parameters.setIndexed( "sigmaHigh", i, this.sigmaHigh[ i ] );
      parameters.setIndexed( "linearFitLow", i, this.linearFitLow[ i ] );
      parameters.setIndexed( "linearFitHigh", i, this.linearFitHigh[ i ] );
      parameters.setIndexed( "ESD_Outliers", i, this.ESD_Outliers[ i ] );
      parameters.setIndexed( "ESD_Significance", i, this.ESD_Significance[ i ] );
      parameters.setIndexed( "RCR_Limit", i, this.RCR_Limit[ i ] );
   }

   P.set( "flatsLargeScaleRejection", this.flatsLargeScaleRejection );
   P.set( "flatsLargeScaleRejectionLayers", this.flatsLargeScaleRejectionLayers );
   P.set( "flatsLargeScaleRejectionGrowth", this.flatsLargeScaleRejectionGrowth );

   P.set( "lightsLargeScaleRejectionHigh", this.lightsLargeScaleRejectionHigh );
   P.set( "lightsLargeScaleRejectionLayersHigh", this.lightsLargeScaleRejectionLayersHigh );
   P.set( "lightsLargeScaleRejectionGrowthHigh", this.lightsLargeScaleRejectionGrowthHigh );

   P.set( "lightsLargeScaleRejectionLow", this.lightsLargeScaleRejectionLow );
   P.set( "lightsLargeScaleRejectionLayersLow", this.lightsLargeScaleRejectionLayersLow );
   P.set( "lightsLargeScaleRejectionGrowthLow", this.lightsLargeScaleRejectionGrowthLow );

   P.set( "imageRegistration", this.imageRegistration );
   P.set( "lightExposureTolerance", this.lightExposureTolerance );
   P.set( "lightExposureTolerancePost", this.lightExposureTolerancePost );

   P.set( "linearPatternSubtraction", this.linearPatternSubtraction );
   P.set( "linearPatternSubtractionRejectionLimit", this.linearPatternSubtractionRejectionLimit );
   P.set( "linearPatternSubtractionMode", this.linearPatternSubtractionMode );
   P.set( "reuseLastLNReferenceFrames", this.reuseLastLNReferenceFrames );

   P.set( "platesolve", this.platesolve );
   P.set( "platesolveFallbackManual", this.platesolveFallbackManual );
   P.set( "imageSolverRa", this.imageSolverRa );
   P.set( "imageSolverDec", this.imageSolverDec );
   P.set( "imageSolverEpoch", this.imageSolverEpoch );
   P.set( "imageSolverFocalLength", this.imageSolverFocalLength );
   P.set( "imageSolverPixelSize", this.imageSolverPixelSize );
   P.set( "imageSolverForceDefaults", this.imageSolverForceDefaults );

   P.set( "pixelInterpolation", this.pixelInterpolation );
   P.set( "clampingThreshold", this.clampingThreshold );
   P.set( "maxStars", this.maxStars );
   P.set( "distortionCorrection", this.distortionCorrection );
   P.set( "maxSplinePoints", this.maxSplinePoints );
   P.set( "rigidTransformations", this.rigidTransformations );
   P.set( "structureLayers", this.structureLayers );
   P.set( "hotPixelFilterRadius", this.hotPixelFilterRadius );
   P.set( "noiseReductionFilterRadius", this.noiseReductionFilterRadius );
   P.set( "minStructureSize", this.minStructureSize );
   P.set( "sensitivity", this.sensitivity );
   P.set( "peakResponse", this.peakResponse );
   P.set( "brightThreshold", this.brightThreshold );
   P.set( "maxStarDistortion", this.maxStarDistortion );
   P.set( "allowClusteredSources", this.allowClusteredSources );
   P.set( "useTriangleSimilarity", this.useTriangleSimilarity );
   P.set( "reuseLastReferenceFrames", this.reuseLastReferenceFrames );

   P.set( "referenceImage", this.referenceImage );
   P.set( "bestFrameReferenceKeyword", this.bestFrameReferenceKeyword );
   P.set( "bestFrameReferenceMethod", this.bestFrameReferenceMethod );
   P.set( "debayerOutputMethod", this.debayerOutputMethod );

   P.set( "subframeWeightingEnabled", this.subframeWeightingEnabled );
   P.set( "subframeWeightingPreset", this.subframeWeightingPreset );
   P.set( "subframesWeightsMethod", this.subframesWeightsMethod );
   P.set( "FWHMWeight", this.FWHMWeight );
   P.set( "eccentricityWeight", this.eccentricityWeight );
   P.set( "SNRWeight", this.SNRWeight );
   P.set( "starsWeight", this.starsWeight );
   P.set( "PSFSignalWeight", this.PSFSignalWeight );
   P.set( "PSFSNRWeight", this.PSFSNRWeight );
   P.set( "pedestal", this.pedestal );

   P.set( "localNormalization", this.localNormalization );
   P.set( "localNormalizationInteractiveMode", this.localNormalizationInteractiveMode );
   P.set( "localNormalizationGenerateImages", this.localNormalizationGenerateImages );
   P.set( "localNormalizationMethod", this.localNormalizationMethod );
   P.set( "localNormalizationMaxIntegratedFrames", this.localNormalizationMaxIntegratedFrames );
   P.set( "localNormalizationBestReferenceSelectionMethod", this.localNormalizationBestReferenceSelectionMethod );
   P.set( "localNormalizationGridSize", this.localNormalizationGridSize );
   P.set( "localNormalizationReferenceFrameGenerationMethod", this.localNormalizationReferenceFrameGenerationMethod );
   P.set( "localNormalizationPsfType", this.localNormalizationPsfType );
   P.set( "localNormalizationPsfGrowth", this.localNormalizationPsfGrowth );
   P.set( "localNormalizationPsfMaxStars", this.localNormalizationPsfMaxStars );
   P.set( "localNormalizationPsfMinSNR", this.localNormalizationPsfMinSNR );
   P.set( "localNormalizationPsfAllowClusteredSources", this.localNormalizationPsfAllowClusteredSources );
   P.set( "localNormalizationLowClippingLevel", this.localNormalizationLowClippingLevel );
   P.set( "localNormalizationHighClippingLevel", this.localNormalizationHighClippingLevel );

   P.set( "integrate", this.integrate );
   P.set( "autocrop", this.autocrop );
   P.set( "autoIntegrationMode", this.autoIntegrationMode );

   P.set( "recombineRGB", this.recombineRGB );
   P.set( "debayerActiveChannelR", this.debayerActiveChannelR );
   P.set( "debayerActiveChannelG", this.debayerActiveChannelG );
   P.set( "debayerActiveChannelB", this.debayerActiveChannelB );

   P.set( "usePipelineScript", this.usePipelineScript );
   P.set( "pipelineScriptFile", this.pipelineScriptFile );
   P.set( "usePipelineBuilderScript", this.usePipelineBuilderScript );
   P.set( "pipelineBuilderScriptFile", this.pipelineBuilderScriptFile );

   P.set( "enableCompactGUI", this.enableCompactGUI );

   P.set( "keywords", new ByteArray( JSON.stringify( this.keywords.list ) ).toBase64() );

   P.set( "groups", new ByteArray( this.groupsToStringData() ).toBase64() );
   P.set( CACHE_VERSION, new ByteArray( this.executionCache.toString() ).toBase64() );

   if ( toJSON )
      P.set( "testExecutionStatus", JSON.stringify( this.executionStatus ||
      {} ) );

   if ( toJSON )
      return JSON.stringify( P, null, 2 );
   return undefined;
};

StackEngine.prototype.exportTest = function( filePath )
{
   for ( let i = 0; i < this.groupsManager.groups.length; ++i )
      if ( this.groupsManager.groups[ i ].hasMaster )
         this.groupsManager.groups[ i ].removeItem( 0 );

   let json = engine.exportParameters( true /* toJson */ );
   File.writeTextFile( filePath, json );
}

var engine = new StackEngine;
// default parameters
engine.setDefaultParameters();

// ----------------------------------------------------------------------------
// EOF BPP-engine.js - Released 2025-01-28T18:19:31Z
