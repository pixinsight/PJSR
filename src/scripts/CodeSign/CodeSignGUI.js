// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// CodeSignGUI.js - Released 2024-03-12T14:32:30Z
// ----------------------------------------------------------------------------
//
// This file is part of PixInsight Code Signing Utility Script 1.1
//
// Copyright (c) 2021-2024 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/*
 * PixInsight Code Signing Utility
 *
 * Copyright (c) 2021-2024 Pleiades Astrophoto. All Rights Reserved.
 * Written by Juan Conejero (PTeam)
 *
 * Graphical user interface.
 */

#include <pjsr/FocusStyle.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/TextAlign.jsh>

/*
 * Signature Generator Dialog
 */
function CodeSigningDialog()
{
   this.__base__ = Dialog;
   this.__base__();

   //

   let emWidth = this.font.width( 'M' );
   let labelWidth1 = this.font.width( "Entitlements:" ) + emWidth;

   //

   this.helpLabel = new Label( this );
   this.helpLabel.styleSheet = this.scaledStyleSheet(
      "QWidget#" + this.helpLabel.uniqueId + " {"
   +     "border: 1px solid gray;"
   +     "padding: 0.25em;"
   +  "}" );
   this.helpLabel.wordWrapping = true;
   this.helpLabel.useRichText = true;
   this.helpLabel.text = "<p><strong>" + TITLE + " version " + VERSION + "</strong><br/>" +
                         "Copyright &copy; 2021-2024 Pleiades Astrophoto. All Rights Reserved.</p>";
   //

   this.files_TreeBox = new TreeBox( this );
   this.files_TreeBox.multipleSelection = true;
   this.files_TreeBox.rootDecoration = false;
   this.files_TreeBox.alternateRowColor = true;
   this.files_TreeBox.setScaledMinSize( 600, 120 );
   this.files_TreeBox.numberOfColumns = 1;
   this.files_TreeBox.headerVisible = false;

   this.filesAdd_Button = new PushButton( this );
   this.filesAdd_Button.text = "Add Files";
   this.filesAdd_Button.icon = this.scaledResource( ":/icons/add.png" );
   this.filesAdd_Button.toolTip = "<p>Add signable files to the list.</p>";
   this.filesAdd_Button.onClick = function()
   {
      let ofd = new OpenFileDialog;
      ofd.multipleSelections = true;
      ofd.caption = "Select Signable Files";
      ofd.filters = [ ["Signable files", "*.js,*.jsh,*.scp,*.xri,*.so,*.dylib,*.dll"],
                      ["JavaScript source files", "*.js"],
                      ["JavaScript header files", "*.jsh"],
                      ["Shell script files", "*.scp"],
                      ["Module binary executable files", "*.so,*.dylib,*.dll"],
                      ["Repository information files", "*.xri"] ];

      if ( ofd.execute() )
      {
         let newFiles = 0;
         for ( let i = 0; i < ofd.fileNames.length; ++i )
            if ( g_workingData.targetFiles.indexOf( ofd.fileNames[i] ) < 0 )
            {
               if ( ++newFiles == 1 )
                  this.dialog.files_TreeBox.canUpdate = false;
               let node = new TreeBoxNode( this.dialog.files_TreeBox );
               node.setText( 0, ofd.fileNames[i] );
               g_workingData.targetFiles.push( ofd.fileNames[i] );
            }
         if ( newFiles > 0 )
         {
            this.dialog.files_TreeBox.canUpdate = true;
            if ( newFiles < ofd.fileNames.length )
               (new MessageBox( format( "<p>Only %d files (out of %d) have been added to the input list; %d files were already selected.</p>",
                                        newFiles, ofd.fileNames.length, ofd.fileNames.length-newFiles ),
                                TITLE, StdIcon_Warning, StdButton_Ok )).execute();
         }
         else
            (new MessageBox( "<p>No files have been added to the input list: all of the specified files were already selected.</p>",
                             TITLE, StdIcon_Warning, StdButton_Ok )).execute();
      }
   };

   this.filesClear_Button = new PushButton( this );
   this.filesClear_Button.text = "Clear";
   this.filesClear_Button.icon = this.scaledResource( ":/icons/clear.png" );
   this.filesClear_Button.toolTip = "<p>Clear the list of input files.</p>";
   this.filesClear_Button.onClick = function()
   {
      this.dialog.files_TreeBox.clear();
      g_workingData.targetFiles = new Array;
   };

   this.filesInvert_Button = new PushButton( this );
   this.filesInvert_Button.text = "Invert Selection";
   this.filesInvert_Button.icon = this.scaledResource( ":/icons/select-invert.png" );
   this.filesInvert_Button.toolTip = "<p>Invert the current selection of input files.</p>";
   this.filesInvert_Button.onClick = function()
   {
      for ( let i = 0; i < this.dialog.files_TreeBox.numberOfChildren; ++i )
         this.dialog.files_TreeBox.child( i ).selected =
               !this.dialog.files_TreeBox.child( i ).selected;
   };

   this.filesRemove_Button = new PushButton( this );
   this.filesRemove_Button.text = "Remove Selected";
   this.filesRemove_Button.icon = this.scaledResource( ":/icons/delete.png" );
   this.filesRemove_Button.toolTip = "<p>Remove all selected files from the list.</p>";
   this.filesRemove_Button.onClick = function()
   {
      g_workingData.targetFiles = new Array;
      for ( let i = 0; i < this.dialog.files_TreeBox.numberOfChildren; ++i )
         if ( !this.dialog.files_TreeBox.child( i ).selected )
            g_workingData.targetFiles.push( this.dialog.files_TreeBox.child( i ).text( 0 ) );
      for ( let i = this.dialog.files_TreeBox.numberOfChildren; --i >= 0; )
         if ( this.dialog.files_TreeBox.child( i ).selected )
            this.dialog.files_TreeBox.remove( i );
   };

   this.filesButtons_Sizer = new HorizontalSizer;
   this.filesButtons_Sizer.spacing = 8;
   this.filesButtons_Sizer.add( this.filesAdd_Button );
   this.filesButtons_Sizer.addSpacing( 8 );
   this.filesButtons_Sizer.addStretch();
   this.filesButtons_Sizer.add( this.filesClear_Button );
   this.filesButtons_Sizer.addSpacing( 8 );
   this.filesButtons_Sizer.addStretch();
   this.filesButtons_Sizer.add( this.filesInvert_Button );
   this.filesButtons_Sizer.add( this.filesRemove_Button );

   this.files_GroupBox = new GroupBox( this );
   this.files_GroupBox.title = "Target Files";
   this.files_GroupBox.sizer = new VerticalSizer;
   this.files_GroupBox.sizer.margin = 8;
   this.files_GroupBox.sizer.spacing = 8;
   this.files_GroupBox.sizer.add( this.files_TreeBox, 100 );
   this.files_GroupBox.sizer.add( this.filesButtons_Sizer );

   //

   let keysFile_ToolTip = "<p>The signing keys file with the private and public keys used to generate code signatures.</p>";

   this.keysFile_Label = new Label( this );
   this.keysFile_Label.text = "Keys file:";
   this.keysFile_Label.minWidth = labelWidth1;
   this.keysFile_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.keysFile_Label.toolTip = keysFile_ToolTip;

   this.keysFile_Edit = new Edit( this );
   this.keysFile_Edit.toolTip = keysFile_ToolTip;
   this.keysFile_Edit.onEditCompleted = function()
   {
      let filePath = File.windowsPathToUnix( this.text.trim() );
      this.text = filePath;
      g_workingData.keysFile = filePath;
   };

   this.keysFileSelect_Button = new ToolButton( this );
   this.keysFileSelect_Button.icon = this.scaledResource( ":/icons/select-file.png" );
   this.keysFileSelect_Button.setScaledFixedSize( 20, 20 );
   this.keysFileSelect_Button.toolTip = "<p>Select the signing keys file.</p>";
   this.keysFileSelect_Button.onClick = function()
   {
      let ofd = new OpenFileDialog;
      ofd.multipleSelections = false;
      ofd.caption = "Select Signing Keys File";
      ofd.filters = [ ["Secure signing keys files", "*.xssk"] ];
      if ( ofd.execute() )
         this.dialog.keysFile_Edit.text = g_workingData.keysFile = ofd.fileNames[0];
   };

   this.keysFile_Sizer = new HorizontalSizer;
   this.keysFile_Sizer.spacing = 4;
   this.keysFile_Sizer.add( this.keysFile_Label );
   this.keysFile_Sizer.add( this.keysFile_Edit, 100 );
   this.keysFile_Sizer.add( this.keysFileSelect_Button );

   //

   let password_ToolTip = "<p>Secure signing keys files (.xssk) are password-protected. " +
      "Enter here the password corresponding to the keys file that you have specified above.</p>";

   this.password_Label = new Label( this );
   this.password_Label.text = "Password:";
   this.password_Label.minWidth = labelWidth1;
   this.password_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.password_Label.toolTip = password_ToolTip;

   this.password_Edit = new Edit( this );
   this.password_Edit.passwordMode = true;
   this.password_Edit.setScaledFixedWidth( 20*emWidth );
   this.password_Edit.toolTip = password_ToolTip;
   this.password_Edit.onEditCompleted = function()
   {
      this.text = this.text.trim();
   };

   this.passwordHide_Button = new ToolButton( this );
   this.passwordHide_Button.checkable = true;
   this.passwordHide_Button.checked = true;
   this.passwordHide_Button.icon = this.scaledResource( ":/icons/control-password.png" );
   this.passwordHide_Button.setScaledFixedSize( 22, 22 );
   this.passwordHide_Button.focusStyle = FocusStyle_NoFocus;
   this.passwordHide_Button.toolTip = "<p>Hide the password.</p>";
   this.passwordHide_Button.onClick = function( checked )
   {
      this.dialog.password_Edit.passwordMode = checked;
   };

   this.passwordClear_Button = new ToolButton( this );
   this.passwordClear_Button.icon = this.scaledResource( ":/icons/clear.png" );
   this.passwordClear_Button.setScaledFixedSize( 22, 22 );
   this.passwordClear_Button.focusStyle = FocusStyle_NoFocus;
   this.passwordClear_Button.toolTip = "<p>Clear the Password field.</p>";
   this.passwordClear_Button.onClick = function()
   {
      this.dialog.password_Edit.text = "";
   };

   this.password_Sizer = new HorizontalSizer;
   this.password_Sizer.spacing = 4;
   this.password_Sizer.add( this.password_Label );
   this.password_Sizer.add( this.password_Edit, 100 );
   this.password_Sizer.add( this.passwordHide_Button );
   this.password_Sizer.add( this.passwordClear_Button );
   this.password_Sizer.addStretch();

   //

   let entitlements_ToolTip = "<p>The security and developer entitlements required for the scripts or modules that will be signed.</p>" +
      "<p>Each entitlement must be written in a single text line.</p>";

   this.entitlements_Label = new Label( this );
   this.entitlements_Label.text = "Entitlements:";
   this.entitlements_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.entitlements_Label.minWidth = labelWidth1 - this.logicalPixelsToPhysical( 1 );
   this.entitlements_Label.setScaledFixedHeight( 60 );
   this.entitlements_Label.toolTip = entitlements_ToolTip;

   this.entitlements_TextBox = new TextBox( this );
   this.entitlements_TextBox.toolTip = entitlements_ToolTip;
   this.entitlements_TextBox.setScaledFixedHeight( 60 );

   this.entitlementsClear_Button = new ToolButton( this );
   this.entitlementsClear_Button.icon = this.scaledResource( ":/icons/clear.png" );
   this.entitlementsClear_Button.setScaledFixedSize( 22, 22 );
   this.entitlementsClear_Button.focusStyle = FocusStyle_NoFocus;
   this.entitlementsClear_Button.toolTip = "<p>Clear the Entitlements field.</p>";
   this.entitlementsClear_Button.onClick = function()
   {
      g_workingData.entitlements = [];
      this.dialog.entitlements_TextBox.text = "";
   };

   this.entitlements_Sizer = new HorizontalSizer;
   this.entitlements_Sizer.spacing = 4;
   this.entitlements_Sizer.add( this.entitlements_Label );
   this.entitlements_Sizer.add( this.entitlements_TextBox, 100 );
   this.entitlements_Sizer.add( this.entitlementsClear_Button );

   //

   this.reset_Button = new PushButton( this );
   this.reset_Button.text = "Reset";
   this.reset_Button.icon = this.scaledResource( ":/icons/reload.png" );
   this.reset_Button.onClick = function()
   {
      g_workingData.reset();
      g_workingData.saveSettings();
      this.dialog.updateControls();
   };

   this.run_Button = new PushButton( this );
   this.run_Button.defaultButton = true;
   this.run_Button.text = "Run";
   this.run_Button.icon = this.scaledResource( ":/icons/power.png" );
   this.run_Button.onClick = function()
   {
      this.dialog.ok();
   };

   this.exit_Button = new PushButton( this );
   this.exit_Button.text = "Exit";
   this.exit_Button.icon = this.scaledResource( ":/icons/close.png" );
   this.exit_Button.onClick = function()
   {
      this.dialog.cancel();
   };

   this.buttons_Sizer = new HorizontalSizer;
   this.buttons_Sizer.spacing = 8;
   this.buttons_Sizer.add( this.reset_Button );
   this.buttons_Sizer.addStretch();
   this.buttons_Sizer.add( this.run_Button );
   this.buttons_Sizer.add( this.exit_Button );

   //

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.spacing = 8;
   this.sizer.add( this.helpLabel );
   this.sizer.addSpacing( 4 );
   this.sizer.add( this.files_GroupBox, 100 );
   this.sizer.add( this.keysFile_Sizer );
   this.sizer.add( this.password_Sizer );
   this.sizer.add( this.entitlements_Sizer );
   this.sizer.add( this.buttons_Sizer );

   this.windowTitle = TITLE;

   this.ensureLayoutUpdated();
   this.adjustToContents();
   this.setMinSize();

   this.onReturn = function( retVal )
   {
      if ( retVal == StdDialogCode_Ok )
      {
         g_workingData.entitlements = [];
         let lines = this.entitlements_TextBox.text.trim().split( '\n' );
         for ( let i = 0; i < lines.length; ++i )
         {
            let entl = lines[i].trim();
            if ( entl.length > 0 )
               g_workingData.entitlements.push( entl );
         }
      }
   };

   this.updateControls = function()
   {
      this.files_TreeBox.clear();
      for ( let i = 0; i < g_workingData.targetFiles.length; ++i )
      {
         let node = new TreeBoxNode( this.files_TreeBox );
         node.setText( 0, g_workingData.targetFiles[i] );
      }

      this.keysFile_Edit.text = g_workingData.keysFile;

      this.entitlements_TextBox.text = g_workingData.entitlements.join( '\n' );
   };

   this.updateControls();
}

CodeSigningDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------
// EOF CodeSignGUI.js - Released 2024-03-12T14:32:30Z
