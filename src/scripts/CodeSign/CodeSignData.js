// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// CodeSignData.js - Released 2024-03-12T14:32:30Z
// ----------------------------------------------------------------------------
//
// This file is part of PixInsight Code Signing Utility Script 1.1
//
// Copyright (c) 2021-2024 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/*
 * PixInsight Code Signing Utility
 *
 * Copyright (c) 2021-2024 Pleiades Astrophoto. All Rights Reserved.
 * Written by Juan Conejero (PTeam)
 *
 * Script working Data.
 */

#include <pjsr/DataType.jsh>

function CodeSigningWorkingData()
{
   this.__base__ = Object;
   this.__base__();

   this.targetFiles = [];
   this.entitlements = [];
   this.keysFile = "";

   this.saveSettings = function()
   {
      function save( key, type, value )
      {
         Settings.write( SETTINGS_KEY_BASE + key, type, value );
      }

      save( "targetFiles", DataType_String, this.targetFiles.join( '\n' ) );
      save( "keysFile", DataType_String, this.keysFile );
      save( "entitlements", DataType_String, this.entitlements.join( '\n' ) );
   };

   this.loadSettings = function()
   {
      function load( key, type )
      {
         return Settings.read( SETTINGS_KEY_BASE + key, type );
      }

      let o = load( "targetFiles", DataType_String );
      if ( o != null )
         this.targetFiles = o.split( '\n' );

      o = load( "keysFile", DataType_String );
      if ( o != null )
         this.keysFile = o;

      o = load( "entitlements", DataType_String );
      if ( o != null )
         this.entitlements = o.split( '\n' );
   };

   this.reset = function()
   {
      this.targetFiles = [];
      this.entitlements = [];
      this.keysFile = "";
   };
}

CodeSigningWorkingData.prototype = new Object;

/*
 * Global working data object
 */
var g_workingData = new CodeSigningWorkingData;

// ----------------------------------------------------------------------------
// EOF CodeSignData.js - Released 2024-03-12T14:32:30Z
