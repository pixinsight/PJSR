// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// BPP-Keywords.js - Released 2025-01-28T18:19:31Z
// ----------------------------------------------------------------------------
//
// This file is part of:
// - WeightedBatchPreprocessing script version 2.8.3
// - FastBatchPreprocessing script version 1.1.6
//
// Copyright (c) 2019-2025 Roberto Sartori
// Copyright (c) 2020-2021 Adam Block
// Copyright (c) 2019 Tommaso Rubechi
// Copyright (c) 2012 Kai Wiechen
// Copyright (c) 2012-2025 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/**
 * Keyword constructor: defines a keyword by name and mode mask.
 *
 * @param {String} name
 * @param {WBPPKeywordMode} mode
 */
function Keyword( name, mode )
{
   this.name = name;
   this.mode = mode;
}

// ----------------------------------------------------------------------------

/**
 * Manages the list of keywords and provides helpers for
 * adding/replacing/removing/sorting and searching keywords.
 * Duplicates and empty keywords are now allowed.
 */
function Keywords()
{
   this.list = [];

   /**
    * Safely add a new keyword and checks for duplicates and empty keyword.
    *
    * @param {String} name
    * @param {String?} mode keyword mode, pre-processing only as default
    * @returns undefined on success, error message in case of error
    */
   this.add = ( name, mode ) =>
   {
      // default mode is pre-processing only
      mode = mode || WBPPKeywordMode.PRE;
      if ( name.length == 0 )
         return "Unable to add an empty keyword.";
      if ( this.contains( name ) )
         return "Keyword \"" + name + "\" is already in the list.";
      this.list.push( new Keyword( name, mode ) );
      return undefined;
   };

   /**
    * Replace an existing keyword name.
    * If the new name to be assigned collides with an existing keywords then
    * the replacement does not occur and the function silently returns.
    *
    * @param {String} oldName
    * @param {String} newName
    */
   this.replace = ( oldName, newName ) =>
   {
      if ( this.contains( newName ) )
         return;
      this.list.forEach( kw =>
      {
         if ( kw.name == oldName )
            kw.name = newName
      } );
   };

   /**
    * Removes a keyword with the given name.
    *
    * @param {String} name
    */
   this.remove = ( name ) =>
   {
      this.list = this.list.filter( k => k.name != name );
   };

   /**
    * Moves a keyword up or down in the list.
    *
    * @param {*} name
    * @param {*} up true to move keyword in the previous position, false to move it in the next
    * @return the new index of the keyword
    */
   this.move = ( name, up ) =>
   {
      let indx = this.list.reduce( ( obj, keyword, index ) =>
      {
         return keyword.name == name ? index : obj;
      }, -1 );
      if ( indx == -1 )
         return undefined;

      // do nothing if keyword is not found or if the keyword would move out of bounds
      if ( indx == -1 || ( indx == 0 && up ) || ( indx == this.list.length - 1 && !up ) )
         return undefined;
      let dstIndx = indx + ( up ? -1 : 1 );
      let tmp = this.list[ dstIndx ];
      this.list[ dstIndx ] = this.list[ indx ];
      this.list[ indx ] = tmp;
      return dstIndx;
   };

   /**
    * Checks if a keyword with the given name exists.
    *
    * @param {string} name
    * @returns Boolean
    */
   this.contains = ( name ) =>
   {
      return this.list.map( k => k.name ).indexOf( name ) > -1;
   };

   /**
    * Filters the given keywords object accordingly to the current keywords configuration.
    * Always returns an empty object if keywords are globally disabled.
    *
    * @param {{String:String}} keywords keywords to be filtered
    * @param {WBPPKeywordMode} mode filtering mode
    * @returns {String:Setting} filtered keywords
    */
   this.filterKeywordsForMode = ( keywords, mode ) =>
   {
      if ( !engine.groupingKeywordsEnabled || !keywords )
      {
         return {};
      }
      let namesForMode = this.list.reduce( ( obj, keyword ) =>
      {
         if ( keyword.mode & mode )
            obj.push( keyword.name );
         return obj;
      }, [] );

      // return the keywords enabled for the given mode
      return Object
         .keys( keywords )
         .filter( name => ( namesForMode.indexOf( name ) != -1 ) )
         .reduce( ( obj, name ) =>
         {
            obj[ name ] = keywords[ name ];
            return obj;
         },
         {} );
   };

   /**
    * Returns the keywords object containing only the keywords enabled for the given mode.
    *
    * @param {WBPPGroupingMode} mode
    */
   this.keywordsForMode = ( mode ) =>
   {
      return this.list.filter( kw => kw.mode & mode );
   };

   /**
    * Switches the mode of the given keyword.
    *
    * @param {String} name
    */
   this.switchMode = ( name ) =>
   {
      let keyword = this.list.filter( kw => kw.name == name );
      if ( keyword.length == 0 )
         return;
      // manually control the loop cycle
      switch ( keyword[ 0 ].mode )
      {
         case WBPPKeywordMode.NONE:
            keyword[ 0 ].mode = WBPPKeywordMode.PRE;
            break;
         case WBPPKeywordMode.PRE:
            keyword[ 0 ].mode = WBPPKeywordMode.PREPOST;
            break;
         case WBPPKeywordMode.PREPOST:
            keyword[ 0 ].mode = WBPPKeywordMode.POST;
            break;
         case WBPPKeywordMode.POST:
            keyword[ 0 ].mode = WBPPKeywordMode.NONE;
            break;
      }
   };

   /**
    * Returns a flat list of all keyword names
    */
   this.names = () =>
   {
      return this.list.map( k => k.name );
   };

   /**
    * Returns the sorted list of keyword names.
    *
    * @param {*} mode the mode of the keywords
    * @return {*}
    */
   this.sortedNames = ( mode ) =>
   {
      let keywords = this.list.filter( k => k.mode == mode ).map( k => k.name );
      keywords.sort();
      return keywords;
   }
}

Keywords.prototype = new Object;

// ----------------------------------------------------------------------------
// EOF BPP-Keywords.js - Released 2025-01-28T18:19:31Z
