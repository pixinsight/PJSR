// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// BPP-main.js - Released 2025-01-28T18:19:31Z
// ----------------------------------------------------------------------------
//
// This file is part of:
// - WeightedBatchPreprocessing script version 2.8.3
// - FastBatchPreprocessing script version 1.1.6
//
// Copyright (c) 2019-2025 Roberto Sartori
// Copyright (c) 2020-2021 Adam Block
// Copyright (c) 2019 Tommaso Rubechi
// Copyright (c) 2012 Kai Wiechen
// Copyright (c) 2012-2025 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

if ( CoreApplication === undefined ||
     CoreApplication.versionRevision === undefined ||
     CoreApplication.versionMajor*1e11
   + CoreApplication.versionMinor*1e8
   + CoreApplication.versionRelease*1e5
   + CoreApplication.versionRevision*1e2 < 100900300000 )
{
   throw new Error( "This script requires PixInsight core version 1.9.3 or higher." );
}

/* beautify ignore:start */
#include <pjsr/Sizer.jsh>
#include <pjsr/DataType.jsh>
#include <pjsr/NumericControl.jsh>
#include "BPP-global.js" // global defines
#include "BPP-helper.js" // helper functions
#include "BPP-engine.js" // stack engine
#include "BPP-GUI.js"    // GUI part
/* beautify ignore:end */

/*
 * Script entry point
 * @param {boolean} fastMode true if the script is running in fast mode.
 */
function BPPmain( fastMode, id, title, settingsKeyBase, version )
{
   // configure the engine with the flag for fast mode
   engine.id = id;
   engine.title = title;
   engine.version = version;
   engine.fastMode = fastMode;
   engine.loadSettingsKeyBase = settingsKeyBase;
   engine.saveSettingsKeyBase = settingsKeyBase;

   // -

   /**
    * @returns true if "automationMode" is given as command line argument and its value is "true".
    */
   function runsInAutomationMode()
   {
      for ( let i = 0; i < jsArguments.length; ++i )
      {
         let items = jsArguments[i].split( "=" );
         if ( items.length == 2 && items[0] == "automationMode" && items[1].toLowerCase() == "true" )
            return true;
      }
      return false;
   }

   // -

   /**
    * @returns the test file name if it is given as command line argument, false otherwise.
    */
   function getTestFile()
   {
      for ( let i = 0; i < jsArguments.length; ++i )
      {
         let items = jsArguments[i].split( "=" );
         if ( items.length == 2 && items[0] == "testFile" )
            return items[1];
      }
      return false;
   }

   // -

   /**
    * Executes the main process.
    * @param {*} testing true if the script is running in test mode.
    */
   function perform( testing )
   {
      try
      {
         console.show();

         let T = new ElapsedTime;

         // run
         engine.cleanProcessLog();
         engine.initializeExecution();

         if ( runsInAutomationMode() )
            cout( "\n:LOGFILE:" + engine.logsFileName + ":LOGFILE:\n" );

         engine.buildExecutionPipeline();
         engine.runPipeline();
         engine.postExecutionActions();

         if ( runsInAutomationMode() )
            if ( engine.operationQueue.interruptionRequested )
               if ( !engine.operationQueue.__interruptedFromTestCheckedScript__ )
                  cout( "\nTEST MANUALLY INTERRUPTED\n" );

         // complete the logging
         console.writeln( "<end><cbr><br>* WeightedBatchPreprocessing: ", T.text );
         console.flush();

         // stop logging
         engine.consoleLogger.stopLogging();

         // show process logs
         if ( !engine.automationMode )
            engine.showProcessLogs();
      }
      catch ( x )
      {
         if ( !engine.automationMode )
            ( new MessageBox( x.message, engine.title + " " + engine.version, StdIcon_Error, StdButton_Ok ) ).execute();
         else if ( testing )
            cout( "\n" + x.message + "\nTEST FAILED" );
         else
            console.criticalln( x.message );
      }
      console.hide();
   };

   // save the current windows on the workspace
   let existingMainWindowIDs = ImageWindow.windows.filter( W => W.isWindow ).map( W => W.mainView.uniqueId );

   // check the execution mode
   if ( Parameters.isViewTarget )
      throw new Error( engine.title + " can only be executed in the global context." );

   // handles the automated mode
   let showDialog = true;
   if ( runsInAutomationMode() )
   {
      showDialog = false;
      console.noteln( ( fastMode ? "FBPP" : "WBPP" ) + " AUTOMATION MODE" );

      let doPerform = true;

      // resets the groups
      engine.groupsManager.clear();

      // enable automationMode and retrieve the test file
      engine.automationMode = true;
      let testFile = getTestFile();

      if ( testFile )
      {
         console.noteln( "testFile: ", testFile );
         cout( "\ntestFile: " + testFile )
         try
         {
            if ( File.exists( testFile ) )
            {
               let json = File.readTextFile( testFile );
               if ( json == undefined )
                  throw new Error( "Unable to read test file: ", testFile );

               // the parameter fileSearchRootPath needs to be parsed before inporting the parameters
               // to allow the searching of the file items inside the search path
               for ( let i = 0; i < jsArguments.length; ++i )
               {
                  let pItems = jsArguments[i].split( "=" );
                  if ( pItems.length == 2 && pItems[0] == "fileSearchRootPath" )
                     {
                        // The fileSearchRootPath parameter is used exclusively in automation mode.
                        // It specifies an alternative directory to locate file items when their original
                        // paths are unavailable. This is especially useful for sharing automated tests
                        // among different machines without having to recreate new tests by manually
                        // loading files from machine-specific paths.
                        //
                        // For example, suppose a file item has the path:
                        //   /home/user/data/set1/lights/lightFrame.fits
                        // If the entire "set1" folder is copied to an external USB drive under:
                        //   /Volume/usbDisk/testdata/set1
                        // then specifying:
                        //   fileSearchRootPath = /Volume/usbDisk/testdata
                        // allows the light frames in the "set1" folder to be found in their new location
                        // on the USB drive.
                        engine.fileSearchRootPath = pItems[1];
                        console.noteln( "fileSearchRootPath: ", engine.fileSearchRootPath );
                        break;
                     }
               }

               // load the parameters stored in the test file
               engine.testFile = testFile;
               engine.importParameters( json );

               // set the pipeline testing script

               // allow a subset of values to be overridden by the test configuration
               for ( let i = 0; i < jsArguments.length; ++i )
               {
                  let pItems = jsArguments[i].split( "=" );

                  // output directory can be assigned arbitrarily
                  if ( pItems.length == 2 && pItems[0] == "outputDir" )
                  {
                     console.noteln( "Test out dir: ", pItems[1] );
                     cout( "\nTest out dir: " + pItems[1] );
                     engine.outputDirectory = pItems[1];
                  }
                  else if ( pItems.length == 2 && pItems[0] == "usePipelineScript" )
                  {
                     // if load only then we don't use the pipeline script
                     if ( engine.testLoadOnly )
                     {
                        console.noteln( "Pipeline script disabled as the test requires 'load only'" );
                        cout( "\nPipeline script disabled as the test requires 'load only'" );
                     }
                     else
                     {
                        console.noteln( "Using pipeline script: ", pItems[1] );
                        cout( "\nUsing pipeline script: " + pItems[1] );
                        engine.usePipelineScript = pItems[1].toLowerCase() == "true";
                     }
                  }
                  else if ( pItems.length == 2 && pItems[0] == "pipelineBuilderScriptFile" )
                  {
                     console.noteln( "Pipeline builder script file: ", pItems[1] );
                     cout( "\nPipeline builder script file: " + pItems[1] );
                     engine.pipelineBuilderScriptFile = pItems[1];
                  }
                  else if ( pItems.length == 2 && pItems[0] == "usePipelineBuilderScript" )
                  {
                     // if load only then we don't use the pipeline script
                     if ( engine.testLoadOnly )
                     {
                        console.noteln( "Use pipeline builder script [disabled by load only]" );
                        cout( "\nUse pipeline builder script [disabled by load only]" );
                     }
                     else
                     {
                        console.noteln( "Use pipeline builder script: ", pItems[1] );
                        cout( "\nUse pipeline builder script: " + pItems[1] );
                        engine.usePipelineBuilderScript = pItems[1].toLowerCase() == "true";
                     }
                  }
                  else if ( pItems.length == 2 && pItems[0] == "pipelineScriptFile" )
                  {
                     console.noteln( "Pipeline script file: ", pItems[1] );
                     cout( "\nPipeline script file: " + pItems[1] );
                     engine.pipelineScriptFile = pItems[1];
                  }
                  else if ( pItems.length == 1 && pItems[0] == "loadOnly" )
                  {
                     console.noteln( "Test load only" );
                     cout( "\nTest load only" );
                     engine.testLoadOnly = true;
                     engine.recordTest = false;
                     doPerform = false;
                     // remove the test results if the test is simply loaded, the pipeline script is also ignored in loadOnly mode
                     engine.executionStatus = undefined;
                     engine.recordTest = false;
                     engine.usePipelineScript = false;
                     engine.usePipelineBuilderScript = false;
                  }
                  else if ( pItems.length == 1 && pItems[0] == "recordTest" && !engine.testLoadOnly )
                  {
                     console.noteln( "Record test" );
                     cout( "\nRecord test" );
                     doPerform = true;
                     engine.recordTest = true;
                     // remove the rest results if the test is recorded
                     engine.executionStatus = undefined;
                  }
               }
            }
            else
               throw new Error( "Test file not found: ", testFile );
         }
         catch ( x )
         {
            cout( "\n" + x.message + "\nTEST FAILED" );
            doPerform = false;
         }
      }
      else
      {
         console.noteln( "no testFile, parsing the command line options" );
         // regular command line without a test file specified
         // parse the settings first
         for ( let i = 0; i < jsArguments.length; ++i )
         {
            let pItems = jsArguments[i].split( "=" );
            if ( pItems.length == 2 && pItems[0] != "file" && pItems[0] != "dir" )
            {
               console.noteln( "automation modem parameter: ", pItems[0], " = ", pItems[1] );

               if ( pItems[0] == "keywords" )
               {
                  // keywords follows a custom syntax:
                  // "keywords=EXPOSURE;PANEL post;FILTER prepost"
                  // EXPOSURE mode is missing so it's assumed 'pre' by default
                  // PANEL mode is 'post'
                  // FILTER mode is both 'pre' and 'post'
                  let keywordItems = pItems[1].split( ";" );

                  for ( let j = 0; j < keywordItems.length; j++ )
                  {
                     let mode = WBPPKeywordMode.PRE;
                     let name;
                     let keywordData = keywordItems[j].trim().split( " " );

                     if ( keywordData.length == 0 )
                        continue;
                     else if ( keywordData.length == 1 )
                        name = keywordData[0];
                     else if ( keywordData.length == 2 )
                     {
                        name = keywordData[0];
                        let modeStr = keywordData[1];
                        if ( modeStr.toLowerCase() == "pre" )
                           mode = WBPPKeywordMode.PRE;
                        else if ( modeStr.toLowerCase() == "post" )
                           mode = WBPPKeywordMode.POST;
                        else if ( modeStr.toLowerCase() == "prepost" )
                           mode = WBPPKeywordMode.PREPOST;
                        else
                        {
                           console.warningln( "unknown mode [" + modeStr + "] for keyword [" + keyword + "]" );
                           continue;
                        }
                     }
                     else if ( keywordData.length > 2 )
                     {
                        console.warningln( "unexpected syntax for keyword: ", keywordData );
                        continue;
                     }

                     engine.keywords.add( name, mode );

                  }
               }
               else
               {
                  // this is used as a parameter value
                  Parameters.set( pItems[0], pItems[1] );
               }
            }
         }
         engine.importParameters();
         engine.rebuild();

         // add file and dir from arguments
         for ( let i = 0; i < jsArguments.length; ++i )
         {
            let pItems = jsArguments[i].split( "=" );
            if ( pItems.length == 2 )
            {
               if ( pItems[0] == "file" )
               {
                  console.noteln( "add file: <raw>" + pItems[1] + "</raw>" );
                  engine.addFile( pItems[1] );
               }
               else if ( pItems[0] == "dir" )
               {
                  console.noteln( "add directory: <raw>" + pItems[1] + "</raw>" );
                  // get the list of compatible file extensions
                  let openFileSupport = new OpenFileDialog;
                  openFileSupport.loadImageFilters();
                  let filters = openFileSupport.filters[0]; // all known format
                  filters.shift();
                  filters = filters.concat( filters.map( f => ( f.toUpperCase() ) ) );
                  let L = new FileList( pItems[1], filters, false /*verbose*/ );
                  L.files.forEach( filePath => engine.addFile( filePath ) );
               }
            }
         }
      }

      // If we have to perform then we run WBPP and do not show the main dialog.
      // IF we do not perform then the main GUI is shown
      if ( doPerform )
         perform();
      else
         showDialog = true;
   }
   else
   {
      // hide the console and present the main GUI
      console.hide();
      engine.importParameters();

      // check if a previous running configuration exists and ask to reload in case
      if ( engine.hasRunningConfiguration() && !engine.fastMode )
      {
         let userChoice = (new MessageBox( "A previous running configuration has been found. Do you want to reload it?",
                                           engine.title + " " + engine.version,
                                           StdIcon_Information,
                                           StdButton_Yes, StdButton_No )).execute();
         if ( userChoice == StdButton_Yes )
            engine.restoreRunningConfiguration();
      }
   }

   if ( showDialog )
   {

      for ( ;; )
      {
         let dialog = new StackDialog();
         dialog.updateControls();

         if ( !dialog.execute() )
            break;

         if ( engine.loadInWBPP )
         {
            // we're are here only if we clicked "open in WBPP" button

            // reconfigure the engine load the WBPP keeping the FBPP settings
            engine.id = WBPP_ID;
            engine.title = WBPP_TITLE;
            engine.version = WBPP_VERSION;
            engine.fastMode = false;
            engine.saveSettings();
            engine.saveSettingsKeyBase = WBPP_SETTINGS_KEY_BASE;
            engine.loadInWBPP = undefined;
            engine.buildExecutionPipeline();
         }
         else
         {
            engine.runDiagnostics();
            if ( !engine.hasDiagnosticMessages() || engine.showDiagnosticMessages( true /*cancelButton*/ ) )
               perform();
            engine.clearDiagnosticMessages();
         }

         processEvents();
         gc();
      }

      engine.removeRunningConfiguration();
      engine.saveSettings();
      engine = null;
   }

   // clean up residual image windows if needed
   ImageWindow.windows.filter( W => W.isWindow ).forEach( W =>
   {
      if ( existingMainWindowIDs.indexOf( W.mainView.uniqueId ) == -1 )
         W.forceClose();
   } );
}

// ----------------------------------------------------------------------------
// EOF BPP-main.js - Released 2025-01-28T18:19:31Z
