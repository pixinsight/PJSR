/*
 * MosaicByCoordinates
 *
 * Astrometry-based construction of astronomical mosaics.
 *
 * Copyright (C) 2013-2024, Andres del Pozo
 * Copyright (C) 2019-2024, Juan Conejero (PTeam)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/* beautify ignore:start */

#feature-id    MosaicByCoordinates : Astrometry > MosaicByCoordinates | Mosaic > MosaicByCoordinates

#feature-icon  @script_icons_dir/MosaicByCoordinates.svg

#feature-info  A script for construction of astrometry-based image mosaics.<br/>\
               <br/>\
               Copyright &copy; 2013-2024 Andr&eacute;s del Pozo<br/>\
               Copyright &copy; 2019-2024 Juan Conejero (PTeam)

if ( CoreApplication === undefined ||
     CoreApplication.versionRevision === undefined ||
     CoreApplication.versionMajor*1e11
   + CoreApplication.versionMinor*1e8
   + CoreApplication.versionRelease*1e5
   + CoreApplication.versionRevision*1e2 < 100900000000 )
{
   throw new Error( "This script requires PixInsight core version 1.9.0 or higher." );
}

#include <pjsr/DataType.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/Interpolation.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/SectionBar.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/UndoFlag.jsh>

#define VERSION "1.3.3"
#define TITLE "MosaicByCoordinates"
#define SETTINGS_MODULE "MosaicByCoordinates"

#include "WCSmetadata.jsh"
#include "SearchCoordinatesDialog.js"
#include "ViewDialog.js"
#include "WarpImage.js"

/* beautify ignore:end */

// ----------------------------------------------------------------------------

/*
 * Enumerations
 */
function ErrorPolicy()
{
}
ErrorPolicy.prototype.Continue = 0;
ErrorPolicy.prototype.Abort = 1;
ErrorPolicy.prototype.Ask = 2;

// ----------------------------------------------------------------------------

function MosaicByCoordinatesDialog( engine )
{
   this.__base__ = Dialog;
   this.__base__();

   this.labelWidth = this.font.width( "Dimensions (pixels):MMM" );
   this.editWidth = this.font.width( "888888888" );

   this.help_Label = new Label( this );
   this.help_Label.frameStyle = FrameStyle_Box;
   this.help_Label.minWidth = 45 * this.font.width( 'M' );
   this.help_Label.margin = 6;
   this.help_Label.wordWrapping = true;
   this.help_Label.useRichText = true;
   this.help_Label.text =
      "<p><b>" + TITLE + " v" + VERSION + "</b> &mdash; Construction of astrometry-based image mosaics.<br/>" +
         "Copyright &copy; 2013-2024 Andr&eacute;s del Pozo | &copy; 2019-2024 Juan Conejero (PTeam)</p>";

   function toggleSectionHandler( section, toggleBegin )
   {
      if ( !toggleBegin )
      {
         section.dialog.setVariableHeight();
         section.dialog.adjustToContents();
         if ( section.dialog.input_Section.isCollapsed() )
            section.dialog.setFixedHeight();
         else
            section.dialog.setMinHeight();
      }
   }

   //

   this.input_Section = new SectionBar( this, "Input Images" );
   this.input_Control = new Control( this );
   this.input_Section.setSection( this.input_Control );
   this.input_Section.onToggleSection = toggleSectionHandler;

   this.fileList_Tree = new TreeBox( this );
   this.fileList_Tree.rootDecoration = false;
   this.fileList_Tree.alternateRowColor = true;
   this.fileList_Tree.multipleSelection = true;
   this.fileList_Tree.headerVisible = false;
   this.fileList_Tree.toolTip = "<p>The list of images which compose the mosaic.</p>";
   this.fileList_Tree.onNodeSelectionUpdated = function()
   {
      this.dialog.EnableFileControls();
   };

   if ( engine.files )
   {
      for ( let i = 0; i < engine.files.length; ++i )
      {
         if ( engine.files[i].startsWith( "window:" ) )
         {
            let windowId = engine.files[i].substr( 7 );
            try
            {
               let window = ImageWindow.windowById( windowId );
               if ( window == null || window.isNull )
               {
                  engine.files.splice( i, 1 );
                  --i;
               }
               else
               {
                  let node = new TreeBoxNode( this.fileList_Tree );
                  node.setText( 0, windowId );
               }
            }
            catch ( ex )
            {
               engine.files.splice( i, 1 );
               --i;
            }
         }
         else
         {
            let node = new TreeBoxNode( this.fileList_Tree );
            node.setText( 0, engine.files[i] );
         }
      }
   }
   else
      engine.files = [];

   this.addFile_Button = new PushButton( this );
   this.addFile_Button.text = "Add Files";
   this.addFile_Button.toolTip = "<p>Add files to the list of input images.</p>";
   this.addFile_Button.onMousePress = function()
   {
      let ofd = new OpenFileDialog;
      ofd.multipleSelections = true;
      ofd.caption = "Select Mosaic Frames";
      ofd.filters = [
         [ "All supported formats", ".xisf", ".fit", ".fits", ".fts" ],
         [ "XISF Files", ".xisf"],
         [ "FITS Files", ".fit", ".fits", ".fts" ]
      ];
      if ( ofd.execute() )
      {
         for ( let i = 0; i < ofd.fileNames.length; ++i )
         {
            engine.files.push( ofd.fileNames[i] );
            let node = new TreeBoxNode( this.dialog.fileList_Tree );
            node.checkable = false;
            node.setText( 0, ofd.fileNames[i] );
         }
         this.dialog.fileList_Tree.adjustColumnWidthToContents( 1 );
      }
      this.dialog.EnableFileControls();
   };

   this.addView_Button = new PushButton( this );
   this.addView_Button.text = "Add Windows";
   this.addView_Button.toolTip = "<p>Add image windows to the list of input images.</p>";
   this.addView_Button.onMousePress = function()
   {
      let viewDialog = new ViewDialog( true/*onlyWindows*/ );
      viewDialog.execute();
      if ( viewDialog.selectedViews )
         for ( let i = 0; i < viewDialog.selectedViews.length; ++i )
         {
            engine.files.push( "window:" + viewDialog.selectedViews[i] );
            let node = new TreeBoxNode( this.dialog.fileList_Tree );
            node.checkable = false;
            node.setText( 0, viewDialog.selectedViews[i] );
         }
      this.dialog.EnableFileControls();
   };

   this.remove_Button = new PushButton( this );
   this.remove_Button.text = "Remove Selected";
   this.remove_Button.toolTip = "<p>Remove the selected items from the list of input images.</p>";
   this.remove_Button.onMousePress = function()
   {
      for ( let i = this.dialog.fileList_Tree.numberOfChildren - 1; i >= 0; --i )
      {
         if ( this.dialog.fileList_Tree.child( i ).selected )
         {
            engine.files.splice( i, 1 );
            this.dialog.fileList_Tree.remove( i );
         }
         this.dialog.EnableFileControls();
      }
   };

   this.clear_Button = new PushButton( this );
   this.clear_Button.text = "Clear";
   this.clear_Button.toolTip = "<p>Clears the list of input images.</p>";
   this.clear_Button.onMousePress = function()
   {
      this.dialog.fileList_Tree.clear();
      engine.files = [];
   };

   this.fileButtons_Sizer = new HorizontalSizer;
   this.fileButtons_Sizer.scaledSpacing = 8;
   this.fileButtons_Sizer.add( this.addFile_Button );
   this.fileButtons_Sizer.add( this.addView_Button );
   this.fileButtons_Sizer.addStretch();
   this.fileButtons_Sizer.add( this.remove_Button );
   this.fileButtons_Sizer.add( this.clear_Button );

   //

   this.input_Control.sizer = new VerticalSizer;
   this.input_Control.sizer.scaledSpacing = 4;
   this.input_Control.sizer.add( this.fileList_Tree, 100 );
   this.input_Control.sizer.add( this.fileButtons_Sizer );

   //

   this.EnableFileControls = function()
   {
      this.remove_Button.enabled = this.fileList_Tree.selectedNodes.length > 0;
      this.clear_Button.enabled = this.fileList_Tree.numberOfChildren > 0;
   };

   this.EnableFileControls();

   //

   this.geometry_Section = new SectionBar( this, "Mosaic Geometry" );
   this.geometry_Control = new Control( this );
   this.geometry_Section.setSection( this.geometry_Control );
   this.geometry_Section.onToggleSection = toggleSectionHandler;

   this.projection_Check = new CheckBox( this );
   this.projection_Check.setMinWidth( this.labelWidth );
   this.projection_Check.checked = (engine.projectionAuto != null) ? !engine.projectionAuto : false;
   this.projection_Check.text = "Projection:";
   this.projection_Check.toolTip = "<p>Projection system of the resulting image.</p>" +
      "<p>If this option is not selected, the script will select the most suitable projection system " +
      "for the area of the sky covered by the mosaic.</p>";
   this.projection_Check.onCheck = function( checked )
   {
      engine.projectionAuto = !checked;
      this.dialog.projection_Combo.enabled = checked;
      this.dialog.projection_Button.enabled = checked;
   };

   this.projection_Combo = new ComboBox( this );
   this.projection_Combo.enabled = this.projection_Check.checked;
   this.projection_Combo.toolTip = "<p>Projection system used for the output mosaic.</p>";
   this.projection_Combo.addItem( "Gnomonic" );
   this.projection_Combo.addItem( "Stereographic" );
   this.projection_Combo.addItem( "Plate-carrée" );
   this.projection_Combo.addItem( "Mercator" );
   this.projection_Combo.addItem( "Hammer-Aitoff" );
   this.projection_Combo.addItem( "Zenithal equal area" );
   this.projection_Combo.addItem( "Orthographic" );
   if ( engine.projection != null )
      this.projection_Combo.currentItem = engine.projection;
   this.projection_Combo.onItemSelected = function()
   {
      engine.projection = this.currentItem;
      engine.projectionOriginMode = 0;
   };

   this.projection_Button = new PushButton( this );
   this.projection_Button.enabled = this.projection_Check.checked
   this.projection_Button.text = "Advanced";
   this.projection_Button.onClick = function()
   {
      (new ConfigProjectionDialog( engine, engine.projection )).execute();
   };

   this.projection_Sizer = new HorizontalSizer;
   this.projection_Sizer.scaledSpacing = 4;
   this.projection_Sizer.add( this.projection_Check );
   this.projection_Sizer.add( this.projection_Combo );
   this.projection_Sizer.add( this.projection_Button );
   this.projection_Sizer.addStretch();

   //

   this.center_Check = new CheckBox( this );
   this.center_Check.setMinWidth( this.labelWidth );
   this.center_Check.checked = (engine.centerCoordsAuto != null) ? !engine.centerCoordsAuto : false;
   this.center_Check.text = "Center coordinates:";
   this.center_Check.toolTip = "<p>These are the spherical equatorial coordinates of the center of the output mosaic.</p>" +
      "<p>If this option is not selected, the center coordinates will be computed automatically using the geometries of all input images.</p>";
   this.center_Check.onCheck = function( checked )
   {
      engine.centerCoordsAuto = !checked;
      this.dialog.center_Editor.enabled = checked;
   };

   this.center_Editor = new CoordinatesEditor( this, new Point( engine.centerRA, engine.centerDec ), this.labelWidth, null, this.center_Check.toolTip );
   this.center_Editor.enabled = this.center_Check.checked;

   //

   this.resolution_Check = new CheckBox( this );
   this.resolution_Check.setMinWidth( this.labelWidth );
   this.resolution_Check.checked = (engine.resolutionAuto != null) ? !engine.resolutionAuto : false;
   this.resolution_Check.text = "Resolution (as/pixel):";
   this.resolution_Check.toolTip = "<p>Resolution of the output mosaic image in arcseconds per pixel.</p>" +
      "<p>If this option is not selected, the resolution will be computed automatically from the input images.</p>";
   this.resolution_Check.onCheck = function( checked )
   {
      engine.resolutionAuto = !checked;
      this.dialog.resolution_Edit.enabled = checked;
   };

   this.resolution_Edit = new Edit( this );
   if ( engine.resolution != null )
      this.resolution_Edit.text = format( "%.3f", engine.resolution * 3600 );
   this.resolution_Edit.toolTip = this.resolution_Check.toolTip;
   this.resolution_Edit.setFixedWidth( this.editWidth );
   this.resolution_Edit.enabled = this.resolution_Check.checked;
   this.resolution_Edit.onTextUpdated = function( value )
   {
      engine.resolution = parseFloat( value ) / 3600;
   };

   this.resolution_Sizer = new HorizontalSizer;
   this.resolution_Sizer.scaledSpacing = 4;
   this.resolution_Sizer.add( this.resolution_Check );
   this.resolution_Sizer.add( this.resolution_Edit );
   this.resolution_Sizer.addStretch();

   //

   this.rotation_Check = new CheckBox( this ) ;
   this.rotation_Check.setMinWidth( this.labelWidth );
   this.rotation_Check.checked = (engine.rotationAuto != null) ? !engine.rotationAuto : false;
   this.rotation_Check.text = "Rotation (deg):";
   this.rotation_Check.toolTip = "<p>Rotation angle of the output mosaic in degrees.</p>" +
      "<p>If this option is not selected, the rotation angle will be computed automatically from the input images.</p>";
   this.rotation_Check.onCheck = function (checked)
   {
      engine.rotationAuto = !checked;
      this.dialog.rotation_Edit.enabled = checked;
   };

   this.rotation_Edit = new Edit( this );
   if ( engine.rotation != null )
      this.rotation_Edit.text = format( "%.3f", engine.rotation );
   this.rotation_Edit.toolTip = this.rotation_Check.toolTip;
   this.rotation_Edit.setFixedWidth( this.editWidth );
   this.rotation_Edit.enabled = this.rotation_Check.checked;
   this.rotation_Edit.onTextUpdated = function( value )
   {
      engine.rotation = parseFloat( value );
   };

   this.rotation_Sizer = new HorizontalSizer;
   this.rotation_Sizer.scaledSpacing = 4;
   this.rotation_Sizer.add( this.rotation_Check );
   this.rotation_Sizer.add( this.rotation_Edit );
   this.rotation_Sizer.addStretch();

   //

   this.dimensions_Check = new CheckBox( this );
   this.dimensions_Check.setMinWidth( this.labelWidth );
   this.dimensions_Check.checked = (engine.dimensionsAuto != null) ? !engine.dimensionsAuto : false;
   this.dimensions_Check.text = "Dimensions (px):";
   this.dimensions_Check.toolTip = "<p>Width and height in pixels of the output mosaic image.</p>" +
      "<p>If this option is not selected, the script will compute optimal output mosaic dimensions automatically.</p>";
   this.dimensions_Check.onCheck = function( checked )
   {
      engine.dimensionsAuto = !checked;
      this.dialog.width_Edit.enabled = checked;
      this.dialog.height_Edit.enabled = checked;
   };

   this.width_Edit = new Edit( this );
   this.width_Edit.setFixedWidth( this.editWidth );
   if ( engine.width != null )
      this.width_Edit.text = format( "%.0f", engine.width );
   this.width_Edit.toolTip = this.dimensions_Check.toolTip;
   this.width_Edit.enabled = this.dimensions_Check.checked;
   this.width_Edit.onTextUpdated = function( value )
   {
      engine.width = parseInt( value );
   };

   this.dimensionsX_Label = new Label( this );
   this.dimensionsX_Label.text = " x ";

   this.height_Edit = new Edit( this );
   this.height_Edit.setFixedWidth( this.editWidth );
   if ( engine.height != null )
      this.height_Edit.text = format( "%.0f", engine.height );
   this.height_Edit.toolTip = this.dimensions_Check.toolTip;
   this.height_Edit.enabled = this.dimensions_Check.checked;
   this.height_Edit.onTextUpdated = function( value )
   {
      engine.height = parseInt( value );
   };

   this.dimensions_Sizer = new HorizontalSizer;
   this.dimensions_Sizer.scaledSpacing = 4;
   this.dimensions_Sizer.add( this.dimensions_Check );
   this.dimensions_Sizer.add( this.width_Edit );
   this.dimensions_Sizer.add( this.dimensionsX_Label );
   this.dimensions_Sizer.add( this.height_Edit );
   this.dimensions_Sizer.addStretch();

   //

   this.geometryParams_Sizer = new VerticalSizer;
   this.geometryParams_Sizer.scaledSpacing = 6;
   this.geometryParams_Sizer.add( this.projection_Sizer );
   this.geometryParams_Sizer.add( this.center_Check );
   this.geometryParams_Sizer.add( this.center_Editor );
   this.geometryParams_Sizer.add( this.resolution_Sizer );
   this.geometryParams_Sizer.add( this.rotation_Sizer );
   this.geometryParams_Sizer.add( this.dimensions_Sizer );

   //

   this.calculate_Button = new PushButton( this );
   this.calculate_Button.text = "Calculate";
   this.calculate_Button.toolTip = "<p>Calculate the values of all parameters set to <i>automatic</i> " +
      "using the geometries of all input images.</p>";
   this.calculate_Button.onClick = function()
   {
      if ( !engine.centerCoordsAuto )
      {
         let coords = this.dialog.center_Editor.GetCoords();
         engine.centerRA = coords.x;
         engine.centerDec = coords.y;
      }
      try
      {
         engine.CalculateAutoParameters();

         if ( engine.centerCoordsAuto )
            this.dialog.center_Editor.SetCoords( new Point( engine.centerRA, engine.centerDec ) );

         if ( engine.projectionAuto )
            this.dialog.projection_Combo.currentItem = engine.projection;

         if ( engine.resolutionAuto )
            this.dialog.resolution_Edit.text = format( "%.3f", engine.resolution * 3600 );

         if ( engine.rotationAuto )
            this.dialog.rotation_Edit.text = format( "%.3f", engine.rotation );

         if ( engine.dimensionsAuto )
         {
            this.dialog.width_Edit.text = format( "%.0f", engine.width );
            this.dialog.height_Edit.text = format( "%.0f", engine.height );
         }
      }
      catch ( ex )
      {
         console.criticalln( ex );
         (new MessageBox( ex.toString(), TITLE, StdIcon_Error, StdButton_Ok )).execute();
      }
   };

   this.calculate_Sizer = new VerticalSizer;
   this.calculate_Sizer.addStretch();
   this.calculate_Sizer.add( this.calculate_Button );
   this.calculate_Sizer.addStretch();

   //

   this.geometry_Control.sizer = new HorizontalSizer;
   this.geometry_Control.sizer.scaledSpacing = 6;
   this.geometry_Control.sizer.add( this.geometryParams_Sizer );
   this.geometry_Control.sizer.add( this.calculate_Sizer );

   //

   this.interpolation_Section = new SectionBar( this, "Interpolation" );
   this.interpolation_Control = new Control( this );
   this.interpolation_Section.setSection( this.interpolation_Control );
   this.interpolation_Section.onToggleSection = toggleSectionHandler;

   let interpolationToolTip = "<p>Pixel interpolation algorithm</p>"
      + "<p>The default automatic mode will select the following interpolation algorithms as a function of the "
      + "rescaling involved in the registration geometrical transformation:</p>"
      + "<ul>"
      + "<li>Cubic B-spline filter interpolation when the scaling factor is under 0.25 approximately.</li>"
      + "<li>Mitchell-Netravali filter interpolation for scaling factors between 0.6 and 0.25 approx.</li>"
      + "<li>Lanczos-3 interpolation in the rest of cases: from moderate size reductions to no rescaling or "
      + "rescaling up.</li>"
      + "</ul>"
      + "<p><b>Lanczos</b> is, in general, the best interpolation algorithm for image registration of linear "
      + "images when no large scale changes are required. Lanczos has excellent detail preservation and subpixel "
      + "registration performance with minimal generation of aliasing effects. Its main drawback is generation of "
      + "undershoot (ringing) artifacts, but this problem can be fixed in most cases with the implemented "
      + "<i>clamping</i> mechanism (see the <i>clamping threshold</i> parameter below).</p>"
      + "<p><b>Bicubic spline</b> is an excellent interpolation algorithm in terms of accuracy and execution "
      + "speed. As Lanczos interpolation, bicubic spline generates ringing artifacts, but the implemented clamping "
      + "mechanism can usually fix most of them. The main problem with bicubic spline is generation of aliasing "
      + "effects, such as moire patterns, which can be problematic with noisy images, especially on sky background "
      + "areas. This is one of the reasons why Lanczos is in general the preferred option.</p>"
      + "<p><b>Bilinear interpolation</b> can be useful to register low SNR linear images, in the rare cases where "
      + "Lanczos and bicubic spline interpolation generates too strong oscillations between noisy pixels that can't "
      + "be avoided completely with the clamping feature. Bilinear interpolation has no ringing artifacts, but it "
      + "generates strong aliasing patterns on noisy areas and provides less accurate results than Lanczos and "
      + "bicubic spline.</p>"
      + "<p><b>Cubic filter interpolations</b>, such as Mitchell-Netravali, Catmull-Rom spline and cubic B-spline, "
      + "provide higher smoothness and subsampling accuracy that can be necessary when the registration transformation "
      + "involves relatively strong size reductions.</p>"
      + "<p><b>Nearest neighbor</b> is the simplest possible pixel interpolation method. It always produces the "
      + "worst results, especially in terms of registration accuracy (no subpixel registration is possible), and "
      + "discontinuities due to the simplistic interpolation scheme. However, in absence of scaling and rotation "
      + "nearest neighbor preserves the original noise distribution in the registered images, a property that can "
      + "be useful in some image analysis applications.</p>";

   this.interpolation_Label = new Label(this);
   this.interpolation_Label.text = "Pixel interpolation:";
   this.interpolation_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.interpolation_Label.toolTip = interpolationToolTip;
   this.interpolation_Label.minWidth = this.labelWidth;

   this.interpolation_Combo = new ComboBox( this );
   this.interpolation_Combo.editEnabled = false;
   this.interpolation_Combo.toolTip = interpolationToolTip;
   this.interpolation_Combo.addItem( "Auto" );
   this.interpolation_Combo.addItem( "Nearest Neighbor" );
   this.interpolation_Combo.addItem( "Bilinear" );
   this.interpolation_Combo.addItem( "Bicubic Spline" );
   this.interpolation_Combo.addItem( "Bicubic B-Spline" );
   this.interpolation_Combo.addItem( "Lanczos-3" );
   this.interpolation_Combo.addItem( "Lanczos-4" );
   this.interpolation_Combo.addItem( "Lanczos-5" );
   this.interpolation_Combo.addItem( "Mitchell-Netravali Filter" );
   this.interpolation_Combo.addItem( "Catmull-Rom Spline Filter" );
   this.interpolation_Combo.addItem( "Cubic B-Spline Filter" );
   this.interpolation_Combo.currentItem = engine.pixelInterpolation + 1; // see pjsr/Interpolation.jsh
   this.interpolation_Combo.onItemSelected = function()
   {
      engine.pixelInterpolation = this.currentItem - 1; // see pjsr/Interpolation.jsh
   };

   this.interpolation_Sizer = new HorizontalSizer;
   this.interpolation_Sizer.scaledSpacing = 4;
   this.interpolation_Sizer.add( this.interpolation_Label );
   this.interpolation_Sizer.add( this.interpolation_Combo );
   this.interpolation_Sizer.addStretch();

   let clampingToolTip = "<p>Clamping threshold for the bicubic spline and Lanczos interpolation algorithms.</p>"
      + "<p>The main drawback of the Lanczos and bicubic spline interpolation algorithms is generation of strong "
      + "undershoot (ringing) artifacts. This is caused by negative lobes of the interpolation functions falling "
      + "over high-contrast edges and isolated bright pixels. This usually happens with linear data, such as raw CCD "
      + "and DSLR images, and rarely with nonlinear or stretched images.</p>"
      + "<p>In the current PixInsight/PCL implementations of these algorithms we have included a clamping mechanism that "
      + "prevents negative interpolated values and ringing artifacts for most images. This parameter represents a "
      + "threshold value to trigger interpolation clamping. A lower threshold leads to a more aggressive deringing effect; "
      + "however, too low of a clamping threshold can degrade interpolation performance, especially in terms of aliasing "
      + "and detail preservation.</p>";

   this.clamping_Control = new NumericControl( this );
   this.clamping_Control.real = true;
   this.clamping_Control.label.text = "Clamping threshold:";
   this.clamping_Control.label.minWidth = this.labelWidth;
   this.clamping_Control.setRange( 0, 1 );
   this.clamping_Control.slider.setRange( 0, 100 );
   this.clamping_Control.slider.setScaledFixedWidth( 250 );
   this.clamping_Control.setPrecision( 2 );
   this.clamping_Control.setValue( engine.clampingThreshold );
   this.clamping_Control.sizer.addStretch();
   this.clamping_Control.toolTip = clampingToolTip;
   this.clamping_Control.onValueUpdated = function( value )
   {
      engine.clampingThreshold = value;
   };

   //

   this.interpolation_Control.sizer = new VerticalSizer;
   this.interpolation_Control.sizer.scaledSpacing = 4;
   this.interpolation_Control.sizer.add( this.interpolation_Sizer );
   this.interpolation_Control.sizer.add( this.clamping_Control );

   //

   this.output_Section = new SectionBar( this, "Output Images" );
   this.output_Control = new Control( this );
   this.output_Section.setSection( this.output_Control );
   this.output_Section.onToggleSection = toggleSectionHandler;

   this.outDir_Label = new Label( this );
   this.outDir_Label.text = "Output directory:";
   this.outDir_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.outDir_Label.setFixedWidth( this.labelWidth );

   this.outDir_Edit = new Edit( this );
   if ( engine.outputDir )
      this.outDir_Edit.text = engine.outputDir;
   this.outDir_Edit.setScaledMinWidth( 300 );
   this.outDir_Edit.toolTip = "<p>The directory where the aligned mosaic images will be written in XISF format.</p>" +
      "<p>If this field is empty, the images will be written on the same directories as their corresponding input images.</p>";
   this.outDir_Edit.onTextUpdated = function( value )
   {
      if ( value.trim().length > 0 )
         engine.outputDir = value.trim();
      else
         engine.outputDir = null;
   };

   this.outDir_Button = new ToolButton( this );
   this.outDir_Button.icon = this.scaledResource( ":/icons/select-file.png" );
   this.outDir_Button.setScaledFixedSize( 20, 20 );
   this.outDir_Button.toolTip = "<p>Select the output directory.</p>";
   this.outDir_Button.onClick = function ()
   {
      let gdd = new GetDirectoryDialog();
      if ( engine.outputDir )
         gdd.initialPath = engine.outputDir;
      gdd.caption = "Output Directory";
      if ( gdd.execute() )
      {
         engine.outputDir = gdd.directory;
         this.dialog.outDir_Edit.text = gdd.directory;
      }
   };

   this.outDir_Sizer = new HorizontalSizer;
   this.outDir_Sizer.scaledSpacing = 4;
   this.outDir_Sizer.add( this.outDir_Label );
   this.outDir_Sizer.add( this.outDir_Edit, 100 );
   this.outDir_Sizer.add( this.outDir_Button );

   this.suffix_Label = new Label( this );
   this.suffix_Label.text = "Output file suffix:";
   this.suffix_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.suffix_Label.setFixedWidth( this.labelWidth );

   this.suffix_Edit = new Edit( this );
   this.suffix_Edit.text = engine.suffix ? engine.suffix : "";
   this.suffix_Edit.minWidth = this.font.width( "_XXXXXXXXXX" );
   this.suffix_Edit.toolTip = "<p>This suffix will be appended to the filename of each output file.</p>";
   this.suffix_Edit.onTextUpdated = function( value )
   {
      engine.suffix = value ? value.trim() : "";
   };

   this.suffix_Sizer = new HorizontalSizer;
   this.suffix_Sizer.scaledSpacing = 4;
   this.suffix_Sizer.add( this.suffix_Label );
   this.suffix_Sizer.add( this.suffix_Edit );
   this.suffix_Sizer.addStretch();

   let overwriteToolTip = "<p>If this option is selected, the script will overwrite existing files with the "
      + "same names as generated output files. This can be dangerous because the original contents of "
      + "overwritten files will be lost.</p>"
      + "<p><b>Enable this option <u>at your own risk.</u></b></p>";

   this.overwrite_Check = new CheckBox( this );
   this.overwrite_Check.text = "Overwrite existing files";
   this.overwrite_Check.checked = engine.overwrite;
   this.overwrite_Check.toolTip = overwriteToolTip;
   this.overwrite_Check.onCheck = function( checked )
   {
      engine.overwrite = checked;
   };

   this.overwrite_Sizer = new HorizontalSizer;
   this.overwrite_Sizer.addUnscaledSpacing( this.labelWidth + this.logicalPixelsToPhysical( 4 ) );
   this.overwrite_Sizer.add( this.overwrite_Check );
   this.overwrite_Sizer.addStretch();

   this.errorPolicy_Label = new Label( this );
   this.errorPolicy_Label.text = "On error:";
   this.errorPolicy_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.errorPolicy_Label.setFixedWidth( this.labelWidth );

   this.errorPolicy_Combo = new ComboBox( this );
   this.errorPolicy_Combo.editEnabled = false;
   this.errorPolicy_Combo.toolTip = "<p>Specify what to do if there are errors during the mosaic generation process.</p>";
   this.errorPolicy_Combo.addItem( "Continue" );
   this.errorPolicy_Combo.addItem( "Abort" );
   this.errorPolicy_Combo.addItem( "Ask User" );
   this.errorPolicy_Combo.currentItem = engine.errorPolicy ? engine.errorPolicy : 0;
   this.errorPolicy_Combo.onItemSelected = function()
   {
      engine.errorPolicy = this.currentItem;
   };

   this.errorPolicy_Sizer = new HorizontalSizer;
   this.errorPolicy_Sizer.scaledSpacing = 4;
   this.errorPolicy_Sizer.add( this.errorPolicy_Label );
   this.errorPolicy_Sizer.add( this.errorPolicy_Combo );
   this.errorPolicy_Sizer.addStretch();

   //

   this.output_Control.sizer = new VerticalSizer;
   this.output_Control.sizer.scaledSpacing = 4;
   this.output_Control.sizer.add( this.outDir_Sizer );
   this.output_Control.sizer.add( this.suffix_Sizer );
   this.output_Control.sizer.add( this.overwrite_Sizer );
   this.output_Control.sizer.add( this.errorPolicy_Sizer );

   //

   this.newInstance_Button = new ToolButton( this );
   this.newInstance_Button.icon = this.scaledResource( ":/process-interface/new-instance.png" );
   this.newInstance_Button.setScaledFixedSize( 20, 20 );
   this.newInstance_Button.toolTip = "New Instance";
   this.newInstance_Button.onMousePress = function()
   {
      this.hasFocus = true;
      engine.SaveParameters();
      this.pushed = false;
      this.dialog.newInstance();
   };

   this.reset_Button = new ToolButton( this );
   this.reset_Button.icon = this.scaledResource( ":/icons/reload.png" );
   this.reset_Button.setScaledFixedSize( 20, 20 );
   this.reset_Button.toolTip = "<p>Resets all parameters to default values.</p>";
   this.reset_Button.onClick = function()
   {
      if ( (new MessageBox( "<p>Do you really want to reset all parameters to their default values?</p>",
                            TITLE, StdIcon_Warning, StdButton_No, StdButton_Yes )).execute() == StdButton_Yes )
      {
         Settings.remove( SETTINGS_MODULE );
         this.dialog.resetRequest = true;
         this.dialog.cancel();
      }
   };

   this.help_Button = new ToolButton( this );
   this.help_Button.icon = this.scaledResource( ":/process-interface/browse-documentation.png" );
   this.help_Button.setScaledFixedSize( 20, 20 );
   this.help_Button.toolTip = "<p>Browse Documentation</p>";
   this.help_Button.onClick = function()
   {
      Dialog.browseScriptDocumentation( "MosaicByCoordinates" );
   };

   this.ok_Button = new PushButton( this );
   this.ok_Button.text = "OK";
   this.ok_Button.icon = this.scaledResource( ":/icons/ok.png" );
   this.ok_Button.onClick = function()
   {
      try
      {
         if ( engine.dimensionsAuto && engine.width * engine.height > 100e6 )
         {
            if ( (new MessageBox( "<p>The resulting mosaic image will have more than 100 Mpx</p>" +
                                  "<p><b>Do you want to continue?</b></p>", TITLE, StdIcon_Warning,
                                  StdButton_Yes, StdButton_No )).execute() != StdButton_Yes )
               return;
         }
         if ( !engine.centerCoordsAuto )
         {
            let coords = this.dialog.center_Editor.GetCoords();
            engine.centerRA = coords.x;
            engine.centerDec = coords.y;
         }
         this.dialog.ok();
      }
      catch ( ex )
      {
         new (MessageBox( ex, TITLE, StdIcon_Error, StdButton_Ok )).execute();
      }
   };

   this.cancel_Button = new PushButton( this );
   this.cancel_Button.text = "Cancel";
   this.cancel_Button.icon = this.scaledResource( ":/icons/cancel.png" );
   this.cancel_Button.onClick = function()
   {
      this.dialog.cancel();
   };

   this.buttons_Sizer = new HorizontalSizer;
   this.buttons_Sizer.scaledSpacing = 6;
   this.buttons_Sizer.add( this.newInstance_Button );
   this.buttons_Sizer.add( this.reset_Button );
   this.buttons_Sizer.add( this.help_Button );
   this.buttons_Sizer.addStretch();
   this.buttons_Sizer.add( this.ok_Button );
   this.buttons_Sizer.add( this.cancel_Button );

   //

   this.sizer = new VerticalSizer;
   this.sizer.scaledMargin = 8;
   this.sizer.scaledSpacing = 8;
   this.sizer.add( this.help_Label );
   this.sizer.addSpacing( 4 );
   this.sizer.add( this.input_Section );
   this.sizer.add( this.input_Control, 100 );
   this.sizer.add( this.geometry_Section );
   this.sizer.add( this.geometry_Control );
   this.sizer.add( this.interpolation_Section );
   this.sizer.add( this.interpolation_Control );
   this.sizer.add( this.output_Section );
   this.sizer.add( this.output_Control );
   this.sizer.addSpacing( 8 );
   this.sizer.add( this.buttons_Sizer );

   this.windowTitle = TITLE;

   this.ensureLayoutUpdated();
   this.adjustToContents();
   this.setMinSize();
}

MosaicByCoordinatesDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------

function MosaicByCoordinatesEngine()
{
   this.__base__ = ObjectWithSettings;
   this.__base__(
      SETTINGS_MODULE,
      "engine",
      new Array(
         [ "files", Ext_DataType_StringArray ],
         [ "centerCoordsAuto", DataType_Boolean ],
         [ "centerRA", DataType_Double ],
         [ "centerDec", DataType_Double ],
         [ "resolutionAuto", DataType_Boolean ],
         [ "resolution", DataType_Double ],
         [ "rotationAuto", DataType_Boolean ],
         [ "rotation", DataType_Double ],
         [ "projectionAuto", DataType_Boolean ],
         [ "projection", DataType_Int32 ],
         [ "projectionOriginMode", DataType_UInt32 ],
         [ "projectionOriginRA", DataType_UInt32 ],
         [ "projectionOriginDec", DataType_UInt32 ],
         [ "dimensionsAuto", DataType_Boolean ],
         [ "width", DataType_Int32 ],
         [ "height", DataType_Int32 ],
         [ "pixelInterpolation", DataType_Int32 ],
         [ "clampingThreshold", DataType_Double ],
         [ "suffix", DataType_String ],
         [ "overwrite", DataType_Boolean ],
         [ "errorPolicy", DataType_Int32 ],
         [ "outputDir", DataType_String ]
      )
   );

   this.files = [];
   this.centerCoordsAuto = true;
   this.centerRA = 0;
   this.centerDec = 0;
   this.resolutionAuto = true;
   this.resolution = 60;
   this.rotationAuto = true;
   this.rotation = 0;
   this.projectionAuto = true;
   this.projection = 3;
   this.projectionOriginMode = 0;
   this.projectionOriginRA = 0;
   this.projectionOriginDec = 0;
   this.dimensionsAuto = true;
   this.width = 1000;
   this.height = 1000;
   this.suffix = "_ra";
   this.pixelInterpolation = Interpolation_Auto;
   this.clampingThreshold = 0.3;
   this.outputDir = "";
   this.overwrite = false;
   this.errorPolicy = ErrorPolicy.prototype.Ask;

   this.Init = function( window )
   {
      this.currentWindow = window;
      this.LoadSettings();
      this.LoadParameters();
   };

   this.LoadMetadataCache = function()
   {
      if ( !this.metadataCache )
         this.metadataCache = {};

      let errorCount = 0;
      for ( let i = 0; i < this.files.length; ++i )
      {
         console.writeln( "<end><cbr><br>" + '-'.repeat( 70 ) );
         console.noteln( format( "* Loading metadata for image %d/%d", i+1, this.files.length ) );
         console.writeln( "<raw>" + this.files[i] + "</raw>" );
         console.flush();

         if ( !this.metadataCache[this.files[i]] )
         {
            let window = null;
            let srcIsFile = !this.files[i].startsWith( "window:" );
            if ( srcIsFile )
               window = ImageWindow.open( this.files[i] )[0];
            else
               window = ImageWindow.windowById( this.files[i].substr( 7 ) );
            if ( !window || window.isNull )
               throw "Error opening image '" + this.files[i] + "'";

            try
            {
               let metadata = new ImageMetadata;
               metadata.ExtractMetadata( window );
               if ( !metadata.projection || !metadata.ref_I_G_linear )
                  throw "The image has no astrometric solution.";
               this.metadataCache[this.files[i]] = metadata;
            }
            catch ( exception )
            {
               console.criticalln( "<end><cbr>" + exception );
               ++errorCount;
            }
            finally
            {
               if ( srcIsFile )
                  window.close();
            }
         }
         else
            console.writeln( "<end><cbr>* Retrieved from metadata cache." );
      }

      return errorCount;
   };

   this.CalculateAutoParameters = function()
   {
      if ( this.files.length == 0 )
         throw "No input images have been specified.";

      let numErrs = this.LoadMetadataCache();
      if ( numErrs > 0 )
         throw "There where errors reading the coordinates of the images.";

      if ( this.resolutionAuto )
         this.CalculateResolution();
      if ( !this.resolution || this.resolution <= 0 )
         throw "The resolution must be greater than zero.";

      if ( this.rotationAuto )
         this.rotation = this.metadataCache[this.files[0]].GetRotation()[0];

      if ( this.centerCoordsAuto || this.projectionAuto )
         this.CalculateMosaicCenterAndProjection();

      if ( this.dimensionsAuto )
      {
         let metadata = this.CreateMetadata( 0, 0 );
         this.CalculateDimensions( metadata );
      }
   };

   this.CalculateMosaicCenterAndProjection = function()
   {
      console.writeln( "<end><cbr><br>" + '-'.repeat( 70 ) );
      console.noteln( "* Optimizing the center and area of the mosaic..." );
      console.flush();

      let x1 = this.centerCoordsAuto ? this.metadataCache[this.files[0]].ra : this.centerRA;
      let y1 = this.centerCoordsAuto ? this.metadataCache[this.files[0]].dec : this.centerDec;
      let x2 = x1;
      let y2 = y1;
      let maxField = 0;
      for ( let f = 0; f < this.files.length; ++f )
      {
         let metadata = this.metadataCache[this.files[f]];
         let x = metadata.ra;
         while ( x < x1 - 180 )
            x += 360;
         while ( x > x1 + 180 )
            x -= 360;
         if ( x < x1 )
            x1 = x;
         if ( x > x2 )
            x2 = x;
         if ( metadata.dec < y1 )
            y1 = metadata.dec;
         if ( metadata.dec > y2 )
            y2 = metadata.dec;

         let field1 = metadata.DistanceI( new Point( 0, 0 ), new Point( metadata.width, metadata.height ) );
         let field2 = metadata.DistanceI( new Point( metadata.width, 0 ), new Point( 0, metadata.height ) );
         if ( isNaN( field1 ) || isNaN( field2 ) )
            maxField = 360;
         else
            maxField = Math.max( maxField, field1, field2 );
      }
      maxField += Math.max( x2 - x1, y2 - y1 );

      if ( this.projectionAuto )
      {
         if ( maxField >= 180 )
            this.projection = 4;
         else if ( maxField > 90 )
            this.projection = 1;
         else if ( maxField > 10 )
            this.projection = 3;
         else
            this.projection = 0;
      }

      if ( this.centerCoordsAuto )
      {
         this.centerRA = (x1 + x2)/2;
         this.centerDec = (y1 + y2)/2;

         let dist = 1e6;
         for ( let i = 0; i < 20 && dist > 0.5; ++i )
         {
            let metadata0 = this.CreateMetadata( 0, 0 );
            let bounds = null;
            for ( let f = 0; f < this.files.length; ++f )
            {
               let metadata1 = this.metadataCache[this.files[f]];
               let fileBounds = WarpImage.reprojectedBounds( metadata0, metadata1 );
               if ( bounds )
                  bounds.unite( fileBounds );
               else
                  bounds = fileBounds;
            }

            let centerI = new Point( (bounds.x0 + bounds.x1)/2, (bounds.y0 + bounds.y1)/2 );
            let centerRD = metadata0.Convert_I_RD( centerI );
            if ( !centerRD )
               break;

            //dist=Math.sqrt((this.centerRA-centerRD.x)*(this.centerRA-centerRD.x)+(this.centerDec-centerRD.y)*(this.centerDec-centerRD.y));
            dist = Math.sqrt( centerI.x*centerI.x + centerI.y*centerI.y );
            this.centerRA = (centerRD.x*2 + this.centerRA)/3;
            this.centerDec = (centerRD.y*2 + this.centerDec)/3;
            let width = Math.ceil( 2 * Math.max( Math.abs( bounds.x0 ), Math.abs( bounds.x1 ) ) );
            let height = Math.ceil( 2 * Math.max( Math.abs( bounds.y0 ), Math.abs( bounds.y1 ) ) );
            console.writeln( format( "alpha = %.6f delta = %.6f dist = %.4f width = %d, height = %d", centerRD.x, centerRD.y, dist, width, height ) );
            console.flush();
         }
      }

      while ( this.centerRA < 0 )
         this.centerRA += 360;
      while ( this.centerRA >= 360 )
         this.centerRA -= 360;
   };

   this.CalculateResolution = function()
   {
      let minRes = this.metadataCache[this.files[0]].resolution;
      for ( let f = 0; f < this.files.length; ++f )
         minRes = Math.min( minRes, this.metadataCache[this.files[f]].resolution );
      this.resolution = minRes;
   };

   this.CalculateDimensions = function( metadata0 )
   {
      let bounds = null;
      for ( let f = 0; f < this.files.length; ++f )
      {
         let metadata1 = this.metadataCache[this.files[f]];
         let fileBounds = WarpImage.reprojectedBounds( metadata0, metadata1 );
         if ( bounds )
            bounds.unite( fileBounds );
         else
            bounds = fileBounds
      }
      this.width = Math.ceil( 2 * Math.max( Math.abs( bounds.x0 ), Math.abs( bounds.x1 ) ) );
      this.height = Math.ceil( 2 * Math.max( Math.abs( bounds.y0 ), Math.abs( bounds.y1 ) ) );
   };

   this.CreateMetadata = function( width, height )
   {
      let metadata = new ImageMetadata();
      metadata.ra = this.centerRa;
      metadata.dec = this.centerDec;
      metadata.resolution = this.resolution;
      metadata.width = width;
      metadata.height = height;
      metadata.rotation = this.rotation;
      metadata.projection = ProjectionFactory( this, this.centerRA, this.centerDec );

      let rot = -this.rotation * Math.PI / 180;
      let cd1_1 = -this.resolution * Math.cos( rot );
      let cd1_2 = -this.resolution * Math.sin( rot );
      let cd2_1 = -this.resolution * Math.sin( rot );
      let cd2_2 = this.resolution * Math.cos( rot );
      let crpix1 = width / 2 + 0.5;
      let crpix2 = height / 2 + 0.5;
      if ( this.projectionOriginMode == 1 )
      {
         let centerG = metadata.projection.Direct( new Point( this.centerRA, this.centerDec ) );
         if ( centerG == null )
            throw "Invalid projection origin.";
         let Kx = cd1_1 * crpix1 + cd1_2 * crpix2 - centerG.x;
         let Ky = cd2_1 * crpix1 + cd2_2 * crpix2 - centerG.y;
         let det = cd1_2 * cd2_1 - cd1_1 * cd2_2;
         crpix1 = (Ky * cd1_2 - Kx * cd2_2) / det;
         crpix2 = (Kx * cd2_1 - Ky * cd1_1) / det;
      }
      let ref_F_G = new Matrix(
         cd1_1, cd1_2, -cd1_1 * crpix1 - cd1_2 * crpix2,
         cd2_1, cd2_2, -cd2_1 * crpix1 - cd2_2 * crpix2,
         0, 0, 1 );
      let ref_F_I = new Matrix(
         1, 0, -0.5,
         0, -1, height + 0.5,
         0, 0, 1 );
      metadata.ref_I_G = ref_F_G.mul( ref_F_I.inverse() );
      metadata.ref_G_I = metadata.ref_I_G.inverse();
      metadata.ref_I_G_linear = metadata.ref_I_G;
      return metadata;
   };

   this.GetOutputPath = function( filePath )
   {
      let outputDir;
      if ( !this.outputDir || this.outputDir.length == 0 )
         outputDir = File.extractDrive( filePath ) + File.extractDirectory( filePath );
      else
         outputDir = this.outputDir;
      if ( !outputDir.endsWith( '/' ) )
         outputDir += '/';

      let newPath = outputDir + File.extractName( filePath ) + this.suffix + ".xisf";

      if ( !this.overwrite )
         if ( File.exists( newPath ) )
            if ( this.errorPolicy == ErrorPolicy.prototype.Ask )
            {
               if ( (new MessageBox( "<p>The file '" + newPath + "' already exists.</p>" + "<p><b>Do you want to overwrite it?</b></p>",
                                    TITLE, StdIcon_Error, StdButton_No, StdButton_Yes )).execute() != StdButton_Yes )
                  throw "The file '" + newPath + "' already exists.";
            }
            else
               throw "The file '" + newPath + "' already exists.";

      return newPath;
   };

   this.Execute = function()
   {
      // Check validity of parameters exposed through Edit controls.
      {
         let badParameterCount = 0;
         if ( this.resolution == null || !isFinite( this.resolution ) || this.resolution <= 0 )
         {
            ++badParameterCount;
            console.criticalln( "<end><cbr><br>*** Error: The resolution must be a real number greater than zero." );
         }
         if ( this.rotation == null || !isFinite( this.rotation ) )
         {
            ++badParameterCount;
            console.criticalln( "<end><cbr><br>*** Error: The rotation angle must be a valid real number expressed in degrees." );
         }
         if ( this.width == null || !isFinite( this.width ) || this.width < 1 )
         {
            ++badParameterCount;
            console.criticalln( "<end><cbr><br>*** Error: The mosaic width must be an integer greater than zero." );
         }
         if ( this.height == null || !isFinite( this.height ) || this.height < 1 )
         {
            ++badParameterCount;
            console.criticalln( "<end><cbr><br>*** Error: The mosaic height must be an integer greater than zero." );
         }
         if ( badParameterCount > 0 )
            return;
      }

      this.CalculateAutoParameters();
      let referenceMetadata = this.CreateMetadata( this.width, this.height );

      let warp = new WarpImage( this.pixelInterpolation, this.clampingThreshold, this.suffix );
      let errors = [];
      for ( let i = 0; i < this.files.length; ++i )
      {
         console.writeln( "<end><cbr><br>" + '-'.repeat( 70 ) );
         console.noteln( format( "* Registering image %d/%d", i+1, this.files.length ) );
         console.writeln( "<raw>" + this.files[i] + "</raw>" );
         console.flush();

         let sourceWindow;
         let sourceIsFile = !this.files[i].startsWith( "window:" );
         if ( sourceIsFile )
            sourceWindow = ImageWindow.open( this.files[i] )[0];
         else
            sourceWindow = ImageWindow.windowById( this.files[i].substr( 7 ) );

         console.flush();

         let outputWindow;

         try
         {
            if ( !sourceWindow || sourceWindow.isNull )
               throw "Error opening image '" + this.files[i] + "'";

            if ( !sourceWindow.hasAstrometricSolution )
               sourceWindow.regenerateAstrometricSolution();

            outputWindow = warp.reprojectedImage( referenceMetadata, sourceWindow );

            console.flush();

            if ( sourceIsFile )
            {
               let newPath = this.GetOutputPath( sourceWindow.filePath );
               outputWindow.saveAs( newPath, false, false, true, false );
            }
            else
            {
               console.noteln( "<end><cbr>* Result window: ", outputWindow.mainView.fullId );
               outputWindow.show();
            }
         }
         catch ( exception )
         {
            console.criticalln( "<end><cbr><br>" + '-'.repeat( 70 ) );
            console.criticalln( "*** Error: " + exception );
            errors.push( { file: this.files[i], err: exception } );
            if ( this.errorPolicy == ErrorPolicy.prototype.Abort )
               return;
            else if ( this.errorPolicy == ErrorPolicy.prototype.Ask )
            {
               if ( (new MessageBox( "<p>" + exception + "</p>" +
                                     "<p><b>Do you want to continue with the process?</b></p>",
                                     TITLE, StdIcon_Error, StdButton_Yes, StdButton_No )).execute() == StdButton_No )
               {
                  console.criticalln( "*** Process aborted" );
                  return;
               }
            }
         }
         finally
         {
            if ( sourceIsFile )
            {
               if ( sourceWindow && !sourceWindow.isNull )
                  sourceWindow.forceClose();
               if ( outputWindow && !outputWindow.isNull )
                  outputWindow.forceClose();
            }
         }

         console.flush();
      }

      if ( errors.length == 0 )
         console.writeln( "<end><cbr><br>Process finished successfully." );
      else
      {
         console.criticalln( format( "<end><cbr><br>*** Process finished with %d errors.", errors.length ) );
         for ( let i = 0; i < errors.length; ++i )
         {
            console.criticalln( "<raw>" + errors[i].file + "</raw>:" );
            console.criticalln( "    " + errors[i].err );
         }
      }
   };
}

MosaicByCoordinatesEngine.prototype = new ObjectWithSettings;

// ----------------------------------------------------------------------------

function main()
{
   console.abortEnabled = true;

   let engine = new MosaicByCoordinatesEngine;
   if ( Parameters.isViewTarget )
   {
      engine.Init( Parameters.targetView.window );
   }
   else
   {
      for ( ;; )
      {
         engine.Init( ImageWindow.activeWindow );
         let dialog = new MosaicByCoordinatesDialog( engine );
         if ( dialog.execute() )
            break;
         if ( dialog.resetRequest )
            engine = new MosaicByCoordinatesEngine;
         else
            return;
      }

      engine.SaveSettings();
   }

   engine.Execute();
}

main();
