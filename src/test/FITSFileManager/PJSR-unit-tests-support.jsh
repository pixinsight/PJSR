// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// PJSR-unit-tests-support.js 
// ----------------------------------------------------------------------------
//
// This file is part of FITSFileManager script version 1.6
//
// Copyright (c) 2012-2021 Jean-Marc Lugrin.
// Copyright (c) 2003-2021 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (http://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#define PT_TEST_OK "OK"

// ---------------------------------------------------------------------------------------------------------
// Micro unit testing support
// ---------------------------------------------------------------------------------------------------------

// Test results (a property for each test named as the test function and with the result as a String, PT_TEST_OK if OK
var pT_testResults={};

var pT_replaceAmps = (function() {
   let escapeMap = { '"': '&quot;', '&': '&amp;', '<': '&lt;', '>': '&gt;' };
   return function(text) {
        return text.replace(/[\"&<>]/g, function (a) { return escapeMap[a]; });
   }
})();



// List all test results
function pT_showTestResults() {
   Console.writeln();
   Console.writeln("------------------------------------------------------------");
   Console.writeln("Test results: ");
   Console.writeln("------------");
   let  successes =0, failures = 0;
   for (let t in pT_testResults) {
      if (pT_testResults.hasOwnProperty(t)) {
         let result = pT_testResults[t];
         if (result === PT_TEST_OK) {
            Console.writeln("    ", t, ": ", pT_replaceAmps(result));
            successes += 1;
         } else {
            Console.writeln(" ** ", t, ": ", pT_replaceAmps(result));
            failures += 1;
         }
      }
   }
   Console. writeln("" + (successes + failures) + " tests, " + successes + " sucesses, " + failures + ", failures");
   return failures===0;
}

// Execute all properties of 'tests', that must be functions, in a protected environment

function pT_executeTests(tests) {
   let startTime, elapsedTime;
   pT_testResults = {};
   for (let f in tests) {
      if (tests.hasOwnProperty(f)) {
         Console.writeln("Starting test: ", f);
         startTime = Date.now().valueOf();
         try {
            tests[f]();
            elapsedTime = Date.now().valueOf() - startTime;
            pT_testPassed(f);
            Console.writeln("Test completed successfully: " + f + " in " + elapsedTime + " ms");
         } catch (e) {
            elapsedTime = Date.now().valueOf() - startTime;
            pT_testFailed(f,"Exception: " + e);
            Console.writeln("** Test failed: " + f + " after " + elapsedTime + " ms because: ", pT_replaceAmps(e.toString()));
         }
      }
   };
   Console.show();
   pT_showTestResults();
}

// Assert support
function pT_assertNull(t) {
   if (t!==null) {
      throw "'" +  t + "' !== null";
   }
}
function pT_assertEquals(e,a) {
   if (e!==a) {
     throw "'" + e + "' !== '" + a +"'";
   }
}
function pT_assertEmptyArray(a) {
   if (!Array.isArray(a)) {
      throw "'" + a +"' is not an Array";
   }
   if (a.length != 0) {
     throw "'" + a + "' is not empty";
   }
}
function pT_assertArrayEquals(e,a) {
   if (e.length!==a.length) {
      let received = "";
      try {
         received = ", received: " + a;
      } catch (e) {
         // ignore
      }
     throw "Lenghts " + e.length + " !== " + a.length + received;
   }
   for (let i=0; i<e.length; i++) {
      if (e[i]!==a[i]) {
        throw "'" + e + "' !== '" + a +"' (index " + i + ")";
      }
   }
}
function pT_assertTrue(e) {
   if (!e) {
      throw "'" + e + "' is not true" ;
   }
}
function pT_assertFalse(e) {
   if (e) {
      throw "'" + e + "' is not false" ;
   }
}
function pT_assertUndefined(u) {
   if (typeof u !== 'undefined') {
     throw "typeof '" + u + "' !== undefined";
   }
}


// ---------------------------------------------------------------------------------------------------------
// Support methods (private)
// ---------------------------------------------------------------------------------------------------------
// Mark a test passed if it does not exist (test must be a string)
function pT_testPassed(test) {
   if (!(pT_testResults.hasOwnProperty(test))) {
      pT_testResults[test] = PT_TEST_OK;
   }
}

// Support methods (private)
// Mark a test failed in all cases (test must be a string), display message about failure
function pT_testFailed(test, msg) {
   pT_testResults[test] = msg;
   Console.writeln("Test ", test, " failed: ", msg);
}

// ---------------------------------------------------------------------------------------------------------
// Methods to support checking the current project for prerequisite
// ---------------------------------------------------------------------------------------------------------
function pT_checkPresenceOfProcesses(requiredProcesses) {
   Console.writeln("  Checking presence of required processes");
   let allProcessInstanceNames = ProcessInstance.icons();
#ifdef DEBUG
   Console.writeln("    vPtest_checkPresenceOfProcesses: All icons on project: ", allProcessInstanceNames);
   Console.writeln("    vPtest_checkPresenceOfProcesses: Required icons: ", requiredProcesses);

#endif
   for (let i=0; i<requiredProcesses.length; i++) {
      let processName = requiredProcesses[i];
      let isProcessPresent =  allProcessInstanceNames.indexOf(processName)>=0;
      if (!isProcessPresent) {
         throw "Process '" + processName + "' not present in current project, reload test project";
      }
   }
}

// Check that all views named in the array of string requiredViews are main views of some window in the project
function pT_checkPresenceOfViews(requiredViews) {

   let thereAreMissingViews = false;

#ifdef DEBUG
   let allValidWindows = ImageWindow.windows.filter(function (w) {return !w.isNull && w.isValidView(w.mainView)});
   Console.writeln("  pT_checkPresenceOfViews: " , allValidWindows.length," Window main views: ", allValidWindows.map(function(w){return w.mainView.id}));
#endif
   Console.writeln("    Checking presence of required views");

   // Check that all required views are present
   for (let i=0; i<requiredViews.length; i++) {
      let viewId = requiredViews[i];
      let view = View.viewById(viewId);
#ifdef DEBUG
      Console.writeln("      pT_checkPresenceOfViews: View ", viewId , " is ", view);
#endif
   if (view.isNull || !view.window.isValidView(view)) {
         thereAreMissingViews = true;
         Console.writeln("View '" + viewId + "' not present in current project");
      }
      Console.writeln("      view " + viewId + " present, uniqueId: " + view.uniqueId);
   }
   if (thereAreMissingViews) {
      throw "Some views are missing in the current project, reload the test project";
   }

}

// Close all windows whose main view is not in the list of required views, assume that all required views are present
function pT_closeNonTestWindows(requiredViews) {

   let requiredViewsUniqueId = [];
   let allValidWindows = ImageWindow.windows.filter(function (w) {return !w.isNull && w.isValidView(w.mainView)});

   // build a list of the uniqueId of all required main views
   for (let i=0; i<requiredViews.length; i++) {
      let viewId = requiredViews[i];
      let view = View.viewById(viewId);
      if (view.isNull || !view.window.isValidView(view)) {
         throw "View '" + viewId + "' not present in current project";
      }
      requiredViewsUniqueId.push(view.uniqueId);
   }


   for (let i=0; i<allValidWindows.length; i++) {
      let currentMainView = allValidWindows[i].mainView;
      let currentViewUniqueId = currentMainView.uniqueId;
      let isRelevantWindow =  requiredViewsUniqueId.indexOf(currentViewUniqueId)>=0;
#ifdef DEBUG
      Console.writeln("    pT_checkPresenceOfViews: Window ", currentMainView.id, " isRelevantWindow: " + isRelevantWindow +",  has index ", requiredViewsUniqueId.indexOf(currentViewUniqueId));
#endif
      if (!isRelevantWindow) {
         // We the user will be requested to confirm
         Console.writeln("    Closing window of view '" + currentMainView.id, "'");
         allValidWindows[i].close();
      }
   }

}
// Close all previews of the window required views, assume that the views are present
function pT_closePreviewsOfTestWindows(requiredViews) {
   Console.writeln("  Closing previews of test windows");
   for (let i=0; i<vPtest_requiredViews.length; i++) {
      let viewId = vPtest_requiredViews[i];
      let view = View.viewById(viewId);
      if (view.isNull || !view.window.isValidView(view)) {
         throw "View '" + viewId + "' not present in current project";
      }
      view.window.deletePreviews();
   }
}









