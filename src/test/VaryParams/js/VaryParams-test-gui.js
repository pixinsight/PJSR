// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// VaryParams-test-gui.js
// ----------------------------------------------------------------------------
//
// This file is part of FITSFileManager script version 1.6
//
// Copyright (c) 2012-2021 Jean-Marc Lugrin.
// Copyright (c) 2003-2021 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (http://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------
"use strict";
// VaryParams-test-gui


#define VERSION "1.5-test"
#define TITLE "VaryParams-test-gui TEST USER INTERFACE (SELECT CANCEL TO EXIT)"

// For debugging inside the gui
#define DEBUG true

#include "../../../scripts/VaryParams/VaryParams-helpers.jsh"
#include "../../../scripts/VaryParams/VaryParams-gui.jsh"


// Execute the VaryParam GUI without doing any action, except logging for debugging.
// Useful to test the GUI part and also ensure that the GUI part does not depend on the engine.


function vP_testGuiMain()
{

   Console.show();
   Console.writeln("VaryParams-test-gui - Actions on the GUI will be logged on the console");

   var environment = vP_makeEnvironment();

   var guiParameters = new Vp_GUIParameters(environment);
   Console.writeln("main: initial " + guiParameters.toString());
   guiParameters.loadSettings();
   Console.writeln("main: after loadSettings " + guiParameters.toString());


   var dialog = new Vp_VaryParamsDialog(environment, guiParameters);
   for ( ;; )
   {

      if ( !dialog.executeAndComplete() ) {

         guiParameters.saveSettings();

         // Only exit on Cancel
         break;
      }

   }

   Console.writeln("VaryParams-test-gui - exiting");

}


vP_testGuiMain();
