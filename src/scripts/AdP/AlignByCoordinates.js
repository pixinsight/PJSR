/*
 * AlignByCoordinates
 *
 * Astrometry-based image registration.
 *
 * Copyright (C) 2013-2024, Andres del Pozo
 * Copyright (C) 2019-2024, Juan Conejero (PTeam)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// Change log:
//
// 2024/11/19 - 1.4.3
//
//    * Now in the Astrometry script menu.
//
// 2024/02/24 - v1.4.2
//
//    * Output files are always written in XISF format (.xisf file extension).
//
// 2023/11/06 - v1.4.1
//
//    * Bugfix: interpolation clamping threshold parameter ignored:
//       https://pixinsight.com/forum/index.php?threads/
//       new-mosaicbycoordinates-ignores-interpolation-clamping-threshold-value.21986/
//
// 2023/10/17 - v1.4
//
//    * Complete source code revision and refactoring.
//
//    * Redesigned registration/undistortion algorithm to make use of the new
//      ImageWindow.astrometricReprojection() PJSR method in core 1.8.9-2.
//
//    * PixInsight core version >= 1.8.9-2 required.
//
// 1.3.1
//
//    * Added support for XISF files.
//
// 1.3
//
//    * Keep the keywords of the images.
//
//    * Better error management.
//
// 1.2
//
//    * Optional faster algorithm for warping the images.
//
// 1.1
//
//    * Removed the capability of creating mosaics.
//
// 1.0
//
//    * Added the capability of aligning images in open windows.
//
// 0.2
//
//    * Fixed error invoking engine.
//
// 0.1
//
//    * Initial test version.

/* beautify ignore:start */

#feature-id    AlignByCoordinates : Astrometry > AlignByCoordinates

#feature-icon  @script_icons_dir/AlignByCoordinates.svg

#feature-info  A script for astrometry-based image registration.<br/>\
               <br/>\
               Copyright &copy; 2013-2024 Andr&eacute;s del Pozo<br/>\
               Copyright &copy; 2019-2024 Juan Conejero (PTeam)

if ( CoreApplication === undefined ||
     CoreApplication.versionRevision === undefined ||
     CoreApplication.versionMajor*1e11
   + CoreApplication.versionMinor*1e8
   + CoreApplication.versionRelease*1e5
   + CoreApplication.versionRevision*1e2 < 100900000000 )
{
   throw new Error( "This script requires PixInsight core version 1.9.0 or higher." );
}

#include <pjsr/DataType.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/Interpolation.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/SectionBar.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/UndoFlag.jsh>

#define VERSION "1.4.3"
#define TITLE "AlignByCoordinates"
#define SETTINGS_MODULE "AlignByCoordinates"

#include "WCSmetadata.jsh"
#include "ViewDialog.js"
#include "WarpImage.js"

/* beautify ignore:end */

// ----------------------------------------------------------------------------

/*
 * Enumerations
 */

function AlignMode()
{
}
AlignMode.prototype.Reference = 0;
AlignMode.prototype.Undistort = 1;

function ErrorPolicy()
{
}
ErrorPolicy.prototype.Continue = 0;
ErrorPolicy.prototype.Abort = 1;
ErrorPolicy.prototype.Ask = 2;

// ----------------------------------------------------------------------------

function AlignByCoordinatesDialog( engine )
{
   this.__base__ = Dialog;
   this.__base__();

   this.labelWidth = this.font.width( "Maximum magnitude:M" );
   this.editWidth = this.font.width( "XXXXXXXXXXXXXXXXX" );

   this.help_Label = new Label( this );
   this.help_Label.frameStyle = FrameStyle_Box;
   this.help_Label.minWidth = 45 * this.font.width( 'M' );
   this.help_Label.margin = 6;
   this.help_Label.wordWrapping = true;
   this.help_Label.useRichText = true;
   this.help_Label.text =
      "<p><b>" + TITLE + " v" + VERSION + "</b> &mdash; Astrometry-based image registration.<br/>" +
         "Copyright &copy; 2013-2024 Andr&eacute;s del Pozo | &copy; 2019-2024 Juan Conejero (PTeam)</p>";

   function toggleSectionHandler( section, toggleBegin )
   {
      if ( !toggleBegin )
      {
         section.dialog.setVariableHeight();
         section.dialog.adjustToContents();
         if ( section.dialog.target_Section.isCollapsed() )
            section.dialog.setFixedHeight();
         else
            section.dialog.setMinHeight();
      }
   }

   //

   let modeToolTip = "<p>This parameter defines the script's execution mode:</p>"
      + "<ul>"
      + "<li><b>Align images against reference</b> performs a regular image registration task to match "
      +   "each target image to the selected reference image.<br/></li>"
      + "<li><b>Undistort images</b> reprojects each target image on a linear astrometric solution "
      +   "with the same center coordinates and image scale as the original solution. This working mode "
      +   "can be applied to perform a geometric normalization by removing arbitrary distortions modeled "
      +   "by our spline-based astrometric solutions.</li>"
      + "</ul>";

   this.mode_Label = new Label( this );
   this.mode_Label.text = "Execution mode:";
   this.mode_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.mode_Label.minWidth = this.labelWidth;
   this.mode_Label.toolTip = modeToolTip;

   this.mode_Combo = new ComboBox( this );
   this.mode_Combo.editEnabled = false;
   this.mode_Combo.addItem( "Align images against reference" );
   this.mode_Combo.addItem( "Undistort images" );
   this.mode_Combo.currentItem = engine.alignMode;
   this.mode_Combo.toolTip = modeToolTip;
   this.mode_Combo.onItemSelected = function()
   {
      engine.alignMode = this.currentItem;
      this.dialog.EnableReferenceControls();
   };

   this.mode_Sizer = new HorizontalSizer;
   this.mode_Sizer.scaledSpacing = 4;
   this.mode_Sizer.add( this.mode_Label );
   this.mode_Sizer.add( this.mode_Combo );
   this.mode_Sizer.addStretch();

   //

   let referenceToolTip = "<p>Registration reference image.</p>"
      + "<p>In normal alignment mode, registered images will be transformed geometrically to align them with respect "
      + "to the reference image. The reference image will not be modified during the registration process.</p>";

   this.reference_Label = new Label( this );
   this.reference_Label.text = "Reference image:";
   this.reference_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.reference_Label.minWidth = this.labelWidth;
   this.reference_Label.toolTip = referenceToolTip;

   this.reference_Edit = new Edit( this );
   if ( engine.referenceViewIdOrPath )
      this.reference_Edit.text = engine.referenceViewIdOrPath;
   this.reference_Edit.setScaledMinWidth( 300 );
   this.reference_Edit.toolTip = referenceToolTip;
   this.reference_Edit.onTextUpdated = function( value )
   {
      engine.referenceViewIdOrPath = value.trim();
      this.text = engine.referenceViewIdOrPath;
   };

   this.reference_Combo = new ComboBox( this );
   this.reference_Combo.toolTip = "<p>Select what the reference image refers to: either a file or a view.</p>";
   this.reference_Combo.addItem( "File" );
   this.reference_Combo.addItem( "View" );
   this.reference_Combo.currentItem = engine.referenceIsPath ? 0 : 1;
   this.reference_Combo.onItemSelected = function( itemIndex )
   {
      engine.referenceIsPath = itemIndex == 0;
      this.dialog.reference_Button.icon = this.dialog.scaledResource(
               engine.referenceIsPath ? ":/icons/select-file.png" : ":/browser/select.png" );
      this.dialog.EnableReferenceControls();
   };

   this.reference_Button = new ToolButton( this );
   this.reference_Button.icon = this.scaledResource( engine.referenceIsPath ? ":/icons/select-file.png" : ":/browser/select.png" );
   this.reference_Button.setScaledFixedSize( 20, 20 );
   this.reference_Button.toolTip = "<p>Select the reference image.</p>";
   this.reference_Button.enabled = engine.alignMode != AlignMode.prototype.Undistort;
   this.reference_Button.onClick = function()
   {
      if ( engine.referenceIsPath )
      {
         let ofd = new OpenFileDialog;
         ofd.initialPath = engine.referenceViewIdOrPath;
         ofd.caption = "Select Reference Image";
         ofd.filters = [
            [ "All supported formats", ".xisf", ".fit", ".fits", ".fts" ],
            [ "XISF Files",  ".xisf"],
            [ "FITS Files", ".fit", ".fits", ".fts" ]
         ];
         if ( ofd.execute() )
         {
            engine.referenceViewIdOrPath = ofd.fileName;
            engine.referenceIsPath = true;
         }
      }
      else
      {
         let viewDialog = new ViewDialog( true/*windowsOnly*/, true/*singleView*/ );
         viewDialog.execute();
         if ( viewDialog.selectedView )
            if ( viewDialog.selectedView.length > 0 )
            {
               engine.referenceViewIdOrPath = viewDialog.selectedView;
               engine.referenceIsPath = false;
            }
      }

      this.dialog.reference_Edit.text = engine.referenceViewIdOrPath;
   };

   this.reference_Sizer = new HorizontalSizer;
   this.reference_Sizer.scaledSpacing = 4;
   this.reference_Sizer.add( this.reference_Label );
   this.reference_Sizer.add( this.reference_Edit, 100 );
   this.reference_Sizer.add( this.reference_Combo );
   this.reference_Sizer.add( this.reference_Button );

   //

   this.EnableReferenceControls = function()
   {
      let haveReference = engine.alignMode != AlignMode.prototype.Undistort;
      this.dialog.reference_Label.enabled = haveReference;
      this.dialog.reference_Edit.enabled = haveReference;
      this.dialog.reference_Combo.enabled = haveReference;
      this.dialog.reference_Button.enabled = haveReference;
   };

   this.EnableReferenceControls();

   //

   this.target_Section = new SectionBar( this, "Target Images" );
   this.target_Control = new Control( this );
   this.target_Section.setSection( this.target_Control );
   this.target_Section.onToggleSection = toggleSectionHandler;

   //

   this.useActive_Radio = new RadioButton( this );
   this.useActive_Radio.text = "Active image window";
   this.useActive_Radio.checked = engine.useActiveImage;
   this.useActive_Radio.minWidth = this.labelWidth;
   this.useActive_Radio.toolTip = "<p>Use the active image as the target for the alignment process.</p>";
   this.useActive_Radio.onCheck = function( value )
   {
      engine.useActiveImage = true;
      this.dialog.EnableFileControls();
   };

   //

   this.files_Radio = new RadioButton( this );
   this.files_Radio.text = "Images:";
   this.files_Radio.checked = !engine.useActiveImage;
   this.files_Radio.minWidth = this.labelWidth;
   this.files_Radio.toolTip = "<p>The script will process a list of files or views.</p>";
   this.files_Radio.onCheck = function (value)
   {
      engine.useActiveImage = false;
      this.dialog.EnableFileControls();
   };

   this.fileList_Tree = new TreeBox( this );
   this.fileList_Tree.rootDecoration = false;
   this.fileList_Tree.alternateRowColor = true;
   this.fileList_Tree.multipleSelection = true;
   this.fileList_Tree.headerVisible = false;
   this.fileList_Tree.toolTip = "<p>The list of images that will be processed.</p>";
   if ( engine.files )
   {
      for ( let i = 0; i < engine.files.length; ++i )
      {
         if ( engine.files[i].startsWith( "window:" ) )
         {
            let windowId = engine.files[i].substr( 7 );
            try
            {
               let window = ImageWindow.windowById( windowId );
               if ( window == null || window.isNull )
               {
                  engine.files.splice( i, 1 );
                  --i;
               }
               else
               {
                  let node = new TreeBoxNode( this.fileList_Tree );
                  node.setText( 0, windowId );
               }
            }
            catch ( ex )
            {
               engine.files.splice( i, 1 );
               --i;
            }
         }
         else
         {
            let node = new TreeBoxNode( this.fileList_Tree );
            node.setText( 0, engine.files[i] );
         }
      }
   }
   else
      engine.files = [];

   this.addFile_Button = new PushButton( this );
   this.addFile_Button.text = "Add Files";
   this.addFile_Button.toolTip = "<p>Add files to the list of target images.</p>";
   this.addFile_Button.onMousePress = function()
   {
      let ofd = new OpenFileDialog;
      ofd.multipleSelections = true;
      ofd.caption = "Select Target Files";
      ofd.filters = [
         [ "All supported formats", ".xisf", ".fit", ".fits", ".fts" ],
         [ "XISF Files",  ".xisf"],
         [ "FITS Files", ".fit", ".fits", ".fts" ]
      ];
      if ( ofd.execute() )
      {
         for ( let i = 0; i < ofd.fileNames.length; ++i )
         {
            engine.files.push( ofd.fileNames[i] ) ;
            let node = new TreeBoxNode( this.dialog.fileList_Tree );
            node.checkable = false;
            node.setText( 0, ofd.fileNames[i] );
         }
         this.dialog.fileList_Tree.adjustColumnWidthToContents( 1 );
      }
   };

   this.addView_Button = new PushButton( this );
   this.addView_Button.text = "Add Windows";
   this.addView_Button.toolTip = "<p>Add windows to the list of target images.</p>";
   this.addView_Button.onMousePress = function()
   {
      let viewDialog = new ViewDialog( true/*onlyWindows*/ );
      viewDialog.execute();
      if ( viewDialog.selectedViews )
         for ( let i = 0; i < viewDialog.selectedViews.length; ++i )
         {
            engine.files.push( "window:" + viewDialog.selectedViews[i] );
            let node = new TreeBoxNode( this.dialog.fileList_Tree );
            node.checkable = false;
            node.setText( 0, viewDialog.selectedViews[i] );
         }
   };

   this.remove_Button = new PushButton( this );
   this.remove_Button.text = "Remove Selected";
   this.remove_Button.toolTip = "<p>Remove the current selection from the list of target images.</p>";
   this.remove_Button.onMousePress = function()
   {
      for ( let i = this.dialog.fileList_Tree.numberOfChildren - 1; i >= 0; --i )
         if ( this.dialog.fileList_Tree.child( i ).selected )
         {
            engine.files.splice( i, 1 );
            this.dialog.fileList_Tree.remove( i );
         }
   };

   this.clear_Button = new PushButton( this );
   this.clear_Button.text = "Clear";
   this.clear_Button.toolTip = "<p>Clear the list of target images.</p>";
   this.clear_Button.onMousePress = function()
   {
      this.dialog.fileList_Tree.clear();
      engine.files = [];
   };

   this.fileButtons_Sizer = new HorizontalSizer;
   this.fileButtons_Sizer.scaledSpacing = 8;
   this.fileButtons_Sizer.add( this.addFile_Button );
   this.fileButtons_Sizer.add( this.addView_Button );
   this.fileButtons_Sizer.addStretch();
   this.fileButtons_Sizer.add( this.remove_Button );
   this.fileButtons_Sizer.add( this.clear_Button );

   this.target_Control.sizer = new VerticalSizer;
   this.target_Control.sizer.scaledSpacing = 4;
   this.target_Control.sizer.add( this.useActive_Radio );
   this.target_Control.sizer.add( this.files_Radio );
   this.target_Control.sizer.add( this.fileList_Tree, 100 );
   this.target_Control.sizer.add( this.fileButtons_Sizer );

   this.EnableFileControls = function()
   {
      this.fileList_Tree.enabled = !engine.useActiveImage;
      this.addFile_Button.enabled = !engine.useActiveImage;
      this.addView_Button.enabled = !engine.useActiveImage;
      this.remove_Button.enabled = !engine.useActiveImage;
      this.clear_Button.enabled = !engine.useActiveImage;
   };

   this.EnableFileControls();

   //

   this.interpolation_Section = new SectionBar( this, "Interpolation" );
   this.interpolation_Control = new Control( this );
   this.interpolation_Section.setSection( this.interpolation_Control );
   this.interpolation_Section.onToggleSection = toggleSectionHandler;

   let interpolationToolTip = "<p>Pixel interpolation algorithm</p>"
      + "<p>The default automatic mode will select the following interpolation algorithms as a function of the "
      + "rescaling involved in the registration geometrical transformation:</p>"
      + "<ul>"
      + "<li>Cubic B-spline filter interpolation when the scaling factor is under 0.25 approximately.</li>"
      + "<li>Mitchell-Netravali filter interpolation for scaling factors between 0.6 and 0.25 approx.</li>"
      + "<li>Lanczos-3 interpolation in the rest of cases: from moderate size reductions to no rescaling or "
      + "rescaling up.</li>"
      + "</ul>"
      + "<p><b>Lanczos</b> is, in general, the best interpolation algorithm for image registration of linear "
      + "images when no large scale changes are required. Lanczos has excellent detail preservation and subpixel "
      + "registration performance with minimal generation of aliasing effects. Its main drawback is generation of "
      + "undershoot (ringing) artifacts, but this problem can be fixed in most cases with the implemented "
      + "<i>clamping</i> mechanism (see the <i>clamping threshold</i> parameter below).</p>"
      + "<p><b>Bicubic spline</b> is an excellent interpolation algorithm in terms of accuracy and execution "
      + "speed. As Lanczos interpolation, bicubic spline generates ringing artifacts, but the implemented clamping "
      + "mechanism can usually fix most of them. The main problem with bicubic spline is generation of aliasing "
      + "effects, such as moire patterns, which can be problematic with noisy images, especially on sky background "
      + "areas. This is one of the reasons why Lanczos is in general the preferred option.</p>"
      + "<p><b>Bilinear interpolation</b> can be useful to register low SNR linear images, in the rare cases where "
      + "Lanczos and bicubic spline interpolation generates too strong oscillations between noisy pixels that can't "
      + "be avoided completely with the clamping feature. Bilinear interpolation has no ringing artifacts, but it "
      + "generates strong aliasing patterns on noisy areas and provides less accurate results than Lanczos and "
      + "bicubic spline.</p>"
      + "<p><b>Cubic filter interpolations</b>, such as Mitchell-Netravali, Catmull-Rom spline and cubic B-spline, "
      + "provide higher smoothness and subsampling accuracy that can be necessary when the registration transformation "
      + "involves relatively strong size reductions.</p>"
      + "<p><b>Nearest neighbor</b> is the simplest possible pixel interpolation method. It always produces the "
      + "worst results, especially in terms of registration accuracy (no subpixel registration is possible), and "
      + "discontinuities due to the simplistic interpolation scheme. However, in absence of scaling and rotation "
      + "nearest neighbor preserves the original noise distribution in the registered images, a property that can "
      + "be useful in some image analysis applications.</p>";

   this.interpolation_Label = new Label(this);
   this.interpolation_Label.text = "Pixel interpolation:";
   this.interpolation_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.interpolation_Label.toolTip = interpolationToolTip;
   this.interpolation_Label.minWidth = this.labelWidth;

   this.interpolation_Combo = new ComboBox( this );
   this.interpolation_Combo.editEnabled = false;
   this.interpolation_Combo.toolTip = interpolationToolTip;
   this.interpolation_Combo.addItem( "Auto" );
   this.interpolation_Combo.addItem( "Nearest Neighbor" );
   this.interpolation_Combo.addItem( "Bilinear" );
   this.interpolation_Combo.addItem( "Bicubic Spline" );
   this.interpolation_Combo.addItem( "Bicubic B-Spline" );
   this.interpolation_Combo.addItem( "Lanczos-3" );
   this.interpolation_Combo.addItem( "Lanczos-4" );
   this.interpolation_Combo.addItem( "Lanczos-5" );
   this.interpolation_Combo.addItem( "Mitchell-Netravali Filter" );
   this.interpolation_Combo.addItem( "Catmull-Rom Spline Filter" );
   this.interpolation_Combo.addItem( "Cubic B-Spline Filter" );
   this.interpolation_Combo.currentItem = engine.pixelInterpolation + 1; // see pjsr/Interpolation.jsh
   this.interpolation_Combo.onItemSelected = function()
   {
      engine.pixelInterpolation = this.currentItem - 1; // see pjsr/Interpolation.jsh
   };

   this.interpolation_Sizer = new HorizontalSizer;
   this.interpolation_Sizer.scaledSpacing = 4;
   this.interpolation_Sizer.add( this.interpolation_Label );
   this.interpolation_Sizer.add( this.interpolation_Combo );
   this.interpolation_Sizer.addStretch();

   let clampingToolTip = "<p>Clamping threshold for the bicubic spline and Lanczos interpolation algorithms.</p>"
      + "<p>The main drawback of the Lanczos and bicubic spline interpolation algorithms is generation of strong "
      + "undershoot (ringing) artifacts. This is caused by negative lobes of the interpolation functions falling "
      + "over high-contrast edges and isolated bright pixels. This usually happens with linear data, such as raw CCD "
      + "and DSLR images, and rarely with nonlinear or stretched images.</p>"
      + "<p>In the current PixInsight/PCL implementations of these algorithms we have included a clamping mechanism that "
      + "prevents negative interpolated values and ringing artifacts for most images. This parameter represents a "
      + "threshold value to trigger interpolation clamping. A lower threshold leads to a more aggressive deringing effect; "
      + "however, too low of a clamping threshold can degrade interpolation performance, especially in terms of aliasing "
      + "and detail preservation.</p>";

   this.clamping_Control = new NumericControl( this );
   this.clamping_Control.real = true;
   this.clamping_Control.label.text = "Clamping threshold:";
   this.clamping_Control.label.minWidth = this.labelWidth;
   this.clamping_Control.setRange( 0, 1 );
   this.clamping_Control.slider.setRange( 0, 100 );
   this.clamping_Control.slider.setScaledFixedWidth( 250 );
   this.clamping_Control.setPrecision( 2 );
   this.clamping_Control.setValue( engine.clampingThreshold );
   this.clamping_Control.sizer.addStretch();
   this.clamping_Control.toolTip = clampingToolTip;
   this.clamping_Control.onValueUpdated = function( value )
   {
      engine.clampingThreshold = value;
   };

   //

   this.interpolation_Control.sizer = new VerticalSizer;
   this.interpolation_Control.sizer.scaledSpacing = 4;
   this.interpolation_Control.sizer.add( this.interpolation_Sizer );
   this.interpolation_Control.sizer.add( this.clamping_Control );

   //

   this.output_Section = new SectionBar( this, "Output Images" );
   this.output_Control = new Control( this );
   this.output_Section.setSection( this.output_Control );
   this.output_Section.onToggleSection = toggleSectionHandler;

   this.outDir_Label = new Label( this );
   this.outDir_Label.text = "Output directory:";
   this.outDir_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.outDir_Label.setFixedWidth( this.labelWidth );

   this.outDir_Edit = new Edit( this );
   if ( engine.outputDir )
      this.outDir_Edit.text = engine.outputDir;
   this.outDir_Edit.setScaledMinWidth( 300 );
   this.outDir_Edit.toolTip = "<p>The directory where the registered/undistorted output images will be written in XISF format.</p>" +
      "<p>If this field is empty, the images will be written on the same directories as their corresponding target images.</p>";
   this.outDir_Edit.onTextUpdated = function( value )
   {
      if ( value.trim().length > 0 )
         engine.outputDir = value.trim();
      else
         engine.outputDir = null;
   };

   this.outDir_Button = new ToolButton( this );
   this.outDir_Button.icon = this.scaledResource( ":/icons/select-file.png" );
   this.outDir_Button.setScaledFixedSize( 20, 20 );
   this.outDir_Button.toolTip = "<p>Select the output directory.</p>";
   this.outDir_Button.onClick = function ()
   {
      let gdd = new GetDirectoryDialog();
      if ( engine.outputDir )
         gdd.initialPath = engine.outputDir;
      gdd.caption = "Output Directory";
      if ( gdd.execute() )
      {
         engine.outputDir = gdd.directory;
         this.dialog.outDir_Edit.text = gdd.directory;
      }
   };

   this.outDir_Sizer = new HorizontalSizer;
   this.outDir_Sizer.scaledSpacing = 4;
   this.outDir_Sizer.add( this.outDir_Label );
   this.outDir_Sizer.add( this.outDir_Edit, 100 );
   this.outDir_Sizer.add( this.outDir_Button );

   this.suffix_Label = new Label( this );
   this.suffix_Label.text = "Output file suffix:";
   this.suffix_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.suffix_Label.setFixedWidth( this.labelWidth );

   this.suffix_Edit = new Edit( this );
   this.suffix_Edit.text = engine.suffix ? engine.suffix : "";
   this.suffix_Edit.minWidth = this.font.width( "_XXXXXXXXXX" );
   this.suffix_Edit.toolTip = "<p>This suffix will be appended to the filename of each output file.</p>";
   this.suffix_Edit.onTextUpdated = function( value )
   {
      engine.suffix = value ? value.trim() : "";
   };

   this.suffix_Sizer = new HorizontalSizer;
   this.suffix_Sizer.scaledSpacing = 4;
   this.suffix_Sizer.add( this.suffix_Label );
   this.suffix_Sizer.add( this.suffix_Edit );
   this.suffix_Sizer.addStretch();

   let overwriteToolTip = "<p>If this option is selected, the script will overwrite existing files with the "
      + "same names as generated output files. This can be dangerous because the original contents of "
      + "overwritten files will be lost.</p>"
      + "<p><b>Enable this option <u>at your own risk.</u></b></p>";

   this.overwrite_Check = new CheckBox( this );
   this.overwrite_Check.text = "Overwrite existing files";
   this.overwrite_Check.checked = engine.overwrite;
   this.overwrite_Check.toolTip = overwriteToolTip;
   this.overwrite_Check.onCheck = function( checked )
   {
      engine.overwrite = checked;
   };

   this.overwrite_Sizer = new HorizontalSizer;
   this.overwrite_Sizer.addUnscaledSpacing( this.labelWidth + this.logicalPixelsToPhysical( 4 ) );
   this.overwrite_Sizer.add( this.overwrite_Check );
   this.overwrite_Sizer.addStretch();

   this.errorPolicy_Label = new Label( this );
   this.errorPolicy_Label.text = "On error:";
   this.errorPolicy_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.errorPolicy_Label.setFixedWidth( this.labelWidth );

   this.errorPolicy_Combo = new ComboBox( this );
   this.errorPolicy_Combo.editEnabled = false;
   this.errorPolicy_Combo.toolTip = "<p>Specify what to do if there are errors during the image registration process.</p>";
   this.errorPolicy_Combo.addItem( "Continue" );
   this.errorPolicy_Combo.addItem( "Abort" );
   this.errorPolicy_Combo.addItem( "Ask User" );
   this.errorPolicy_Combo.currentItem = engine.errorPolicy ? engine.errorPolicy : 0;
   this.errorPolicy_Combo.onItemSelected = function()
   {
      engine.errorPolicy = this.currentItem;
   };

   this.errorPolicy_Sizer = new HorizontalSizer;
   this.errorPolicy_Sizer.scaledSpacing = 4;
   this.errorPolicy_Sizer.add( this.errorPolicy_Label );
   this.errorPolicy_Sizer.add( this.errorPolicy_Combo );
   this.errorPolicy_Sizer.addStretch();

   //

   this.output_Control.sizer = new VerticalSizer;
   this.output_Control.sizer.scaledSpacing = 4;
   this.output_Control.sizer.add( this.outDir_Sizer );
   this.output_Control.sizer.add( this.suffix_Sizer );
   this.output_Control.sizer.add( this.overwrite_Sizer );
   this.output_Control.sizer.add( this.errorPolicy_Sizer );

   //

   this.newInstance_Button = new ToolButton( this );
   this.newInstance_Button.icon = this.scaledResource( ":/process-interface/new-instance.png" );
   this.newInstance_Button.setScaledFixedSize( 20, 20 );
   this.newInstance_Button.toolTip = "New Instance";
   this.newInstance_Button.onMousePress = function()
   {
      this.hasFocus = true;
      engine.SaveParameters();
      this.pushed = false;
      this.dialog.newInstance();
   };

   this.reset_Button = new ToolButton( this );
   this.reset_Button.icon = this.scaledResource( ":/icons/reload.png" );
   this.reset_Button.setScaledFixedSize( 20, 20 );
   this.reset_Button.toolTip = "<p>Resets all parameters to default values.</p>";
   this.reset_Button.onClick = function()
   {
      if ( (new MessageBox( "<p>Do you really want to reset all parameters to their default values?</p>",
                            TITLE, StdIcon_Warning, StdButton_No, StdButton_Yes )).execute() == StdButton_Yes )
      {
         Settings.remove( SETTINGS_MODULE );
         this.dialog.resetRequest = true;
         this.dialog.cancel();
      }
   };

   this.help_Button = new ToolButton( this );
   this.help_Button.icon = this.scaledResource( ":/process-interface/browse-documentation.png" );
   this.help_Button.setScaledFixedSize( 20, 20 );
   this.help_Button.toolTip = "<p>Browse Documentation</p>";
   this.help_Button.onClick = function()
   {
      Dialog.browseScriptDocumentation( "AlignByCoordinates" );
   };

   this.ok_Button = new PushButton( this );
   this.ok_Button.text = "OK";
   this.ok_Button.icon = this.scaledResource( ":/icons/ok.png" );
   this.ok_Button.onClick = function()
   {
      try
      {
         if ( engine.alignMode != AlignMode.prototype.Undistort )
         {
            if ( engine.referenceIsPath && (!engine.referenceViewIdOrPath || engine.referenceViewIdOrPath.trim().length == 0) )
               throw "No reference file path has been specified.";
            if ( !engine.referenceIsPath && !ImageWindow.windowById( engine.referenceViewIdOrPath ).isWindow )
               throw "The specified reference image window is not valid.";
         }

         if ( engine.useActiveImage && (!ImageWindow.activeWindow || !ImageWindow.activeWindow.isWindow ) )
            throw "There is no active image window.";

         if ( !engine.useActiveImage && engine.files.length == 0 )
            throw "No target images have been specified.";

         this.dialog.ok();
      }
      catch ( ex )
      {
         (new MessageBox( ex, TITLE, StdIcon_Error, StdButton_Ok )).execute();
      }
   };

   this.cancel_Button = new PushButton( this );
   this.cancel_Button.text = "Cancel";
   this.cancel_Button.icon = this.scaledResource( ":/icons/cancel.png" );
   this.cancel_Button.onClick = function()
   {
      this.dialog.cancel();
   };

   this.buttons_Sizer = new HorizontalSizer;
   this.buttons_Sizer.scaledSpacing = 8;
   this.buttons_Sizer.add( this.newInstance_Button );
   this.buttons_Sizer.add( this.reset_Button );
   this.buttons_Sizer.add( this.help_Button );
   this.buttons_Sizer.addStretch();
   this.buttons_Sizer.add( this.ok_Button );
   this.buttons_Sizer.add( this.cancel_Button );

   //

   this.sizer = new VerticalSizer;
   this.sizer.scaledMargin = 8;
   this.sizer.scaledSpacing = 8;
   this.sizer.add( this.help_Label );
   this.sizer.addSpacing( 4 );
   this.sizer.add( this.mode_Sizer );
   this.sizer.add( this.reference_Sizer );
   this.sizer.add( this.target_Section );
   this.sizer.add( this.target_Control, 100 );
   this.sizer.add( this.interpolation_Section );
   this.sizer.add( this.interpolation_Control );
   this.sizer.add( this.output_Section );
   this.sizer.add( this.output_Control );
   this.sizer.addScaledSpacing( 8 );
   this.sizer.add( this.buttons_Sizer );

   this.windowTitle = TITLE;

   this.ensureLayoutUpdated();
   this.adjustToContents();
   this.setMinSize();
}

AlignByCoordinatesDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------

function AlignByCoordinatesEngine()
{
   this.__base__ = ObjectWithSettings;
   this.__base__(
      SETTINGS_MODULE,
      "engine",
      new Array(
         [ "referenceViewIdOrPath", DataType_String ],
         [ "referenceIsPath", DataType_Boolean ],
         [ "useActiveImage", DataType_Boolean ],
         [ "files", Ext_DataType_StringArray ],
         [ "alignMode", DataType_UInt32 ],
         [ "pixelInterpolation", DataType_Int32 ],
         [ "clampingThreshold", DataType_Double ],
         [ "suffix", DataType_String ],
         [ "overwrite", DataType_Boolean ],
         [ "errorPolicy", DataType_Int32 ],
         [ "outputDir", DataType_String ]
      )
   );

   this.referenceViewIdOrPath = "";
   this.referenceIsPath = true;
   this.useActiveImage = true;
   this.files = [];
   this.alignMode = AlignMode.prototype.Reference;
   this.suffix = "_ra";
   this.pixelInterpolation = Interpolation_Auto;
   this.clampingThreshold = 0.3;
   this.outputDir = "";
   this.overwrite = false;
   this.errorPolicy = ErrorPolicy.prototype.Ask;

   this.Init = function( w )
   {
      this.currentWindow = w;
      this.LoadSettings();
      this.LoadParameters();
   };

   /*
    * Returns a linear duplicate of the specified astrometric solution.
    */
   this.LinearMetadata = function( metadata )
   {
      let linearMetadata = new ImageMetadata;
      linearMetadata.useFocal = false;
      linearMetadata.xpixsz = metadata.xpixsz;
      linearMetadata.referenceSystem = metadata.referenceSystem;
      linearMetadata.observationTime = metadata.observationTime;
      linearMetadata.ra = metadata.ra;
      linearMetadata.dec = metadata.dec;
      linearMetadata.epoch = metadata.epoch;
      linearMetadata.endTime = metadata.endTime;
      linearMetadata.observationTime = metadata.observationTime;
      linearMetadata.topocentric = metadata.topocentric;
      linearMetadata.obsLongitude = metadata.obsLongitude;
      linearMetadata.obsLatitude = metadata.obsLatitude;
      linearMetadata.obsHeight = metadata.obsHeight;
      linearMetadata.sourceImageWindow = metadata.sourceImageWindow;
      linearMetadata.projection = metadata.projection;
      linearMetadata.width = metadata.width;
      linearMetadata.height = metadata.height;

      let ref_I_G0;
      if ( metadata.ref_I_G_linear )
         ref_I_G0 = metadata.ref_I_G_linear;
      else
         ref_I_G0 = metadata.ref_I_G;

      linearMetadata.ref_I_G_linear = ref_I_G0.mul( Matrix.unitMatrix( 3 ) );
      linearMetadata.ref_I_G = ReferNPolyn.prototype.FromLinearMatrix( ref_I_G0 );
      linearMetadata.ref_G_I = ReferNPolyn.prototype.FromLinearMatrix( ref_I_G0.inverse() );

      let resx = Math.sqrt( linearMetadata.ref_I_G_linear.at( 0, 0 ) * linearMetadata.ref_I_G_linear.at( 0, 0 )
                          + linearMetadata.ref_I_G_linear.at( 0, 1 ) * linearMetadata.ref_I_G_linear.at( 0, 1 ) );
      let resy = Math.sqrt( linearMetadata.ref_I_G_linear.at( 1, 0 ) * linearMetadata.ref_I_G_linear.at( 1, 0 )
                          + linearMetadata.ref_I_G_linear.at( 1, 1 ) * linearMetadata.ref_I_G_linear.at( 1, 1 ) );
      linearMetadata.resolution = (resx + resy) / 2;
      if ( linearMetadata.xpixsz && linearMetadata.xpixsz > 0 )
         linearMetadata.focal = linearMetadata.FocalFromResolution( linearMetadata.resolution );

      return linearMetadata;
   };

   /*
    * Resizes the metadata of an image for making it cover the specified region
    * in pixels.
    */
   this.ResizeLinearMetadata = function( metadata, rect )
   {
      let linearMetadata = new ImageMetadata;
      linearMetadata.focal = metadata.focal;
      linearMetadata.useFocal = false;
      linearMetadata.xpixsz = metadata.xpixsz;
      linearMetadata.referenceSystem = metadata.referenceSystem;
      linearMetadata.observationTime = metadata.observationTime;
      linearMetadata.ra = metadata.ra;
      linearMetadata.dec = metadata.dec;
      linearMetadata.epoch = metadata.epoch;
      linearMetadata.endTime = metadata.endTime;
      linearMetadata.observationTime = metadata.observationTime;
      linearMetadata.topocentric = metadata.topocentric;
      linearMetadata.obsLongitude = metadata.obsLongitude;
      linearMetadata.obsLatitude = metadata.obsLatitude;
      linearMetadata.obsHeight = metadata.obsHeight;
      linearMetadata.sourceImageWindow = metadata.sourceImageWindow;
      linearMetadata.projection = metadata.projection;
      linearMetadata.width = Math.ceil( rect.width );
      linearMetadata.height = Math.ceil( rect.height );

      let ref_I_G0;
      if ( metadata.ref_I_G_linear )
         ref_I_G0 = metadata.ref_I_G_linear;
      else
         ref_I_G0 = metadata.ref_I_G;

      let offset = new Matrix(
         1, 0, rect.left,
         0, 1, rect.top,
         0, 0, 1 );

      linearMetadata.ref_I_G_linear = ref_I_G0.mul( offset );
      linearMetadata.ref_I_G = ReferNPolyn.prototype.FromLinearMatrix( linearMetadata.ref_I_G_linear );
      linearMetadata.ref_G_I = ReferNPolyn.prototype.FromLinearMatrix( linearMetadata.ref_I_G_linear.inverse() );

      return linearMetadata;
   };

   /*
    * Creates a new geometry using the geometry of an image as template and
    * removing the distortions.
    */
   this.UndistortMetadata = function( metadata, rect )
   {
      let metadataLinear = this.ResizeLinearMetadata( metadata, new Rect( 0, 0, metadata.width, metadata.height ) );
      let newBounds = WarpImage.reprojectedBounds( metadataLinear, metadata );
      return this.ResizeLinearMetadata( metadataLinear, newBounds );
   };

   this.GetOutputPath = function( filePath )
   {
      let outputDir;
      if ( !this.outputDir || this.outputDir.length == 0 )
         outputDir = File.extractDrive( filePath ) + File.extractDirectory( filePath );
      else
         outputDir = this.outputDir;
      if ( !outputDir.endsWith( '/' ) )
         outputDir += '/';

      let newPath = outputDir + File.extractName( filePath ) + this.suffix + ".xisf";

      if ( !this.overwrite )
         if ( File.exists( newPath ) )
            if ( this.errorPolicy == ErrorPolicy.prototype.Ask )
            {
               if ( (new MessageBox( "<p>The file '" + newPath + "' already exists.</p>" + "<p><b>Do you want to overwrite it?</b></p>",
                                    TITLE, StdIcon_Error, StdButton_No, StdButton_Yes )).execute() != StdButton_Yes )
                  throw "The file '" + newPath + "' already exists.";
            }
            else
               throw "The file '" + newPath + "' already exists.";

      return newPath;
   };

   this.GetTargetMetadata = function()
   {
      console.writeln( "<end><cbr><br>" + '-'.repeat( 70 ) );
      console.noteln( "* Loading reference image metadata" );

      let metadata = new ImageMetadata;
      if ( this.referenceIsPath )
      {
         console.writeln( "<raw>" + this.referenceViewIdOrPath + "</raw>" );
         let window = ImageWindow.open( this.referenceViewIdOrPath )[0];
         if ( !window || window.isNull )
            throw "Error opening reference image '" + this.referenceViewIdOrPath + "'";
         metadata.ExtractMetadata( window );
         window.close();
      }
      else
      {
         console.writeln( this.referenceViewIdOrPath );
         let window = ImageWindow.windowById( this.referenceViewIdOrPath );
         if ( !window || window.isNull )
            throw "Error accessing reference image window '" + this.referenceViewIdOrPath + "'";
         metadata.ExtractMetadata( window );
      }

      console.flush();

      if ( !metadata.projection || !metadata.ref_I_G_linear )
         throw "The reference image has no astrometric solution.";

      return metadata;
   };

   this.Execute = function()
   {
      let metadataTarget = null;
      if ( this.alignMode != AlignMode.prototype.Undistort )
      {
         try
         {
            metadataTarget = this.GetTargetMetadata();
         }
         catch ( exception )
         {
            console.criticalln( "<end><cbr>*** Error: " + exception );
            return;
         }
      }

      // Initialize WarpObject
      let warp = new WarpImage( this.pixelInterpolation, this.clampingThreshold, this.suffix );

      if ( this.useActiveImage )
      {
         try
         {
            let outputWindow = null;
            if ( this.alignMode == AlignMode.prototype.Undistort )
            {
               let metadataOrg = new ImageMetadata;
               metadataOrg.ExtractMetadata( this.currentWindow );
               let metadataTarget = this.UndistortMetadata( metadataOrg );
               outputWindow = warp.reprojectedImage( metadataTarget, this.currentWindow );
            }
            else
               outputWindow = warp.reprojectedImage( metadataTarget, this.currentWindow );

            outputWindow.show();
         }
         catch ( exception )
         {
            console.criticalln( "<end><cbr><br>" + '-'.repeat( 70 ) );
            console.criticalln( "<end><cbr>*** Error: " + exception.toString() );
         }
      }
      else
      {
         let errors = [];
         for ( let i = 0; i < this.files.length; ++i )
         {
            console.writeln( "<end><cbr><br>" + '-'.repeat( 70 ) );
            console.noteln( format( "* Registering image %d/%d", i+1, this.files.length ) );
            console.writeln( "<raw>" + this.files[i] + "</raw>" );
            console.flush();

            let sourceWindow;
            let sourceIsFile = !this.files[i].startsWith( "window:" );
            if ( sourceIsFile )
               sourceWindow = ImageWindow.open( this.files[i] )[0];
            else
               sourceWindow = ImageWindow.windowById( this.files[i].substr( 7 ) );

            console.flush();

            let outputWindow;

            try
            {
               if ( !sourceWindow || sourceWindow.isNull )
                  throw "Error opening image '" + this.files[i] + "'";

               if ( !sourceWindow.hasAstrometricSolution )
                  sourceWindow.regenerateAstrometricSolution();

               if ( this.alignMode == AlignMode.prototype.Undistort )
               {
                  let metadataOrg = new ImageMetadata;
                  metadataOrg.ExtractMetadata( sourceWindow );
                  let metadataTarget = this.UndistortMetadata( metadataOrg );
                  outputWindow = warp.reprojectedImage( metadataTarget, sourceWindow );
               }
               else
                  outputWindow = warp.reprojectedImage( metadataTarget, sourceWindow );

               if ( sourceIsFile )
               {
                  let newPath = this.GetOutputPath( sourceWindow.filePath );
                  outputWindow.saveAs( newPath, false, false, true, false );
               }
               else
               {
                  console.noteln( "<end><cbr>* Result window: ", outputWindow.mainView.fullId );
                  outputWindow.show();
               }
            }
            catch ( exception )
            {
               console.criticalln( "<end><cbr><br>" + '-'.repeat( 70 ) );
               console.criticalln( "*** Error: " + exception );
               errors.push( { file: this.files[i], err: exception } );
               if ( this.errorPolicy == ErrorPolicy.prototype.Abort )
                  return;
               else if ( this.errorPolicy == ErrorPolicy.prototype.Ask )
               {
                  if ( (new MessageBox( "<p>" + exception + "</p>" +
                                       "<p><b>Do you want to continue with the process?</b></p>",
                                       TITLE, StdIcon_Error, StdButton_Yes, StdButton_No )).execute() == StdButton_No )
                  {
                     console.criticalln( "*** Process aborted" );
                     return;
                  }
               }
            }
            finally
            {
               if ( sourceIsFile )
               {
                  if ( sourceWindow && !sourceWindow.isNull )
                     sourceWindow.forceClose();
                  if ( outputWindow && !outputWindow.isNull )
                     outputWindow.forceClose();
               }
            }
         }

         if ( errors.length == 0 )
            console.writeln( "<end><cbr><br>Process finished successfully." );
         else
         {
            console.criticalln( format( "<end><cbr><br>*** Process finished with %d errors.", errors.length ) );
            for ( let i = 0; i < errors.length; ++i )
            {
               console.criticalln( "<raw>" + errors[i].file + "</raw>:" );
               console.criticalln( "    " + errors[i].err );
            }
         }
      }
   };
}

AlignByCoordinatesEngine.prototype = new ObjectWithSettings;

// ----------------------------------------------------------------------------

function main()
{
   console.abortEnabled = true;

   let engine = new AlignByCoordinatesEngine;

   if ( Parameters.isViewTarget )
   {
      engine.Init( Parameters.targetView.window );
      engine.useActiveImage = true;
   }
   else
   {
      for ( ;; )
      {
         engine.Init( ImageWindow.activeWindow );
         let dialog = new AlignByCoordinatesDialog( engine );
         if ( dialog.execute() )
            break;
         if ( dialog.resetRequest )
            engine = new AlignByCoordinatesEngine;
         else
            return;
      }

      engine.SaveSettings();
   }

   engine.Execute();
}

main();
