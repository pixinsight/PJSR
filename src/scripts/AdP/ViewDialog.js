/*
 * View Selection Dialog
 *
 * Copyright (C) 2013-2024, Andres del Pozo
 * Copyright (C) 2019-2024, Juan Conejero (PTeam)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ADP_VIEWDIALOG_js
#define __ADP_VIEWDIALOG_js

#include <pjsr/StdDialogCode.jsh>

function ViewDialog( onlyWindows, singleView )
{
   this.__base__ = Dialog;
   this.__base__();

   if ( onlyWindows === undefined )
      onlyWindows = false;
   if ( singleView === undefined )
      singleView = false;

   this.onlyWindows = onlyWindows;
   this.singleView = singleView;

   this.selectedView = "";
   this.selectedViews = [];

   this.views_Group = new GroupBox( this );
   this.views_Group.title = "Views";
   this.views_Group.sizer = new VerticalSizer;
   this.views_Group.sizer.margin = 8;
   this.views_Group.sizer.spacing = 6;

   this.views_List = new TreeBox( this );
   this.views_List.rootDecoration = false;
   this.views_List.alternateRowColor = true;
   this.views_List.multipleSelection = false;
   this.views_List.headerVisible = false;
   this.views_List.numberOfColumns = 1;

   let  windows = ImageWindow.openWindows;
   for ( let i = 0; i < windows.length; ++i )
   {
      let window = windows[i];
      let windowNode = new TreeBoxNode( this.views_List );
      windowNode.checkable = !this.singleView;
      if ( !this.singleView )
         windowNode.checked = false;
      windowNode.setText( 0, window.mainView.fullId );
      if ( !this.onlyWindows )
      {
         let previews = window.previews;
         for ( let j = 0; j < previews.length; ++j )
         {
            let previewNode = new TreeBoxNode( this.views_List );
            previewNode.checkable = !this.singleView;
            if ( !this.singleView )
               previewNode.checked = false;
            previewNode.setText( 0, previews[j].fullId );
         }
      }
   }
   this.views_Group.sizer.add( this.views_List );

   if ( !this.singleView )
   {
      this.all_Button = new PushButton( this );
      this.all_Button.text = "Select All";
      this.all_Button.onClick = function()
      {
         for ( let i = 0; i < this.dialog.views_List.numberOfChildren; ++i )
         {
            let node = this.dialog.views_List.child( i );
            node.checked = true;
         }
      };

      this.unselect_Button = new PushButton( this );
      this.unselect_Button.text = "Unselect All";
      this.unselect_Button.onClick = function()
      {
         for ( let i = 0; i < this.dialog.views_List.numberOfChildren; ++i )
         {
            let node = this.dialog.views_List.child( i );
            node.checked = false;
         }
      };

      this.select_Sizer = new HorizontalSizer;
      this.select_Sizer.spacing = 6;
      this.select_Sizer.add( this.all_Button );
      this.select_Sizer.add( this.unselect_Button );
      this.select_Sizer.addStretch();
      this.views_Group.sizer.add( this.select_Sizer );
   }

   this.ok_Button = new PushButton( this );
   this.ok_Button.text = "OK";
   this.ok_Button.icon = this.scaledResource( ":/icons/ok.png" );
   this.ok_Button.onClick = function()
   {
      this.dialog.exitDialog( StdDialogCode_Ok );
   };

   this.cancel_Button = new PushButton( this );
   this.cancel_Button.text = "Cancel";
   this.cancel_Button.icon = this.scaledResource( ":/icons/cancel.png" );
   this.cancel_Button.onClick = function()
   {
      this.dialog.exitDialog( StdDialogCode_Cancel );
   };

   this.buttons_Sizer = new HorizontalSizer;
   this.buttons_Sizer.spacing = 6;
   this.buttons_Sizer.addStretch();
   this.buttons_Sizer.add( this.ok_Button );
   this.buttons_Sizer.add( this.cancel_Button );

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.spacing = 8;
   this.sizer.add( this.views_Group );
   this.sizer.add( this.buttons_Sizer );

   this.exitDialog = function( retVal )
   {
      this.selectedView = "";
      this.selectedViews = [];

      if ( retVal == StdDialogCode_Ok )
      {
         if ( this.singleView )
         {
            let currentNode = this.views_List.currentNode;
            if ( currentNode )
               if ( currentNode.selected )
                  this.selectedView = currentNode.text( 0 );
         }
         else
         {
            for ( let i = 0; i < this.views_List.numberOfChildren; ++i )
            {
               let node = this.views_List.child( i );
               if ( node.checked )
                  this.selectedViews.push( node.text( 0 ) );
            }
         }

         if ( this.selectedView.length == 0 )
            if ( this.selectedViews.length == 0 )
            {
               (new MessageBox( "<p>No view has been selected.</p>", "View Selection", StdIcon_Error, StdButton_Ok )).execute();
               return;
            }
      }

      this.done( retVal );
   };

   this.windowTitle = "View Selection";

   this.ensureLayoutUpdated();
   this.adjustToContents();
}

ViewDialog.prototype = new Dialog;

#endif
