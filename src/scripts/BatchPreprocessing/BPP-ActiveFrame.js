// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// BPP-ActiveFrame.js - Released 2025-01-28T18:19:31Z
// ----------------------------------------------------------------------------
//
// This file is part of:
// - WeightedBatchPreprocessing script version 2.8.3
// - FastBatchPreprocessing script version 1.1.6
//
// Copyright (c) 2019-2025 Roberto Sartori
// Copyright (c) 2020-2021 Adam Block
// Copyright (c) 2019 Tommaso Rubechi
// Copyright (c) 2012 Kai Wiechen
// Copyright (c) 2012-2025 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/**
 * Active Frame object. An Active Frame is a File Item wrapper that keeps a reference
 * to the associatedID version of the File Item and wraps some processing functions along
 * with sum supporting f
 *
 * @param {*} fileItem the file item the active frame refers to
 * @param {*} associatedID the associated id defining the FileItem version
 */
function ActiveFrame( fileItem, associatedID )
{
   // Store the references
   this.fileItem = fileItem;
   this.associatedID = associatedID || "default";

   /**
    * Invoked when a processing step has been performed successfully on this active item.
    * Internally, the corresponding FileItem version will be marked as successfully processed.
    *
    * @param {WBPPFrameProcessingStep} step step executed
    * @param {String} filePath processed output file
    */
   this.processingSucceeded = ( step, filePath, associatedID ) =>
   {
      let id = associatedID && associatedID.length > 0 ? associatedID : this.associatedID;
      this.fileItem.processingSucceeded( step, filePath, id );
      this.sync();
   };

   /**
    * Mark the file as failed to process. Indicates that an error occurred.
    *
    */
   this.processingFailed = ( associatedID ) =>
   {
      let id = associatedID && associatedID.length > 0 ? associatedID : this.associatedID;
      this.fileItem.processingFailed( id );
      this.sync();
   };

   /**
    * Checks if the current active frame has been processed.
    *
    */
   this.isProcessed = () =>
   {
      let processed = this.fileItem.processed[ this.associatedID ];
      return processed != null && Object.keys( processed ).length > 0;
   };

   /**
    * Returns the file name of the current item.
    *
    * @param {*} suffix
    */
   this.currentFileName = () =>
   {
      let name = File.extractName( this.current );
      let ext = File.extractExtension( this.current );

      return name + ext;
   };

   /**
    * Stores the descriptor associated to the current Active Frame.
    *
    * @param {*} descriptor the descriptor to be stored
    */
   this.setDescriptor = ( descriptor ) =>
   {
      this.fileItem.descriptor[ this.associatedID ] = descriptor;
      this.sync();
   };

   /**
    * Stores the local normalization file for the associated ID
    *
    * @param {*} filePath the local normalization file
    */
   this.addLocalNormalizationFile = ( filePath ) =>
   {
      this.fileItem.addLocalNormalizationFile( filePath, this.associatedID );
      this.sync();
   };

   /**
    * Stores the drizzle file for the associated ID
    *
    * @param {*} filePath the drizzle file
    */
   this.addDrizzleFile = ( filePath ) =>
   {
      this.fileItem.addDrizzleFile( filePath, this.associatedID );
      this.sync();
   };

   /**
    * Marks the current associated File Item as a reference frame.
    *
    */
   this.markAsReference = () =>
   {
      this.fileItem.markAsReference( this.associatedID );
      this.sync();
   };

   /**
    * Synchronize the active item with the corresponding fileItem version.
    *
    */
   this.sync = () =>
   {
      // extract and inject the descriptor and the current item in the active frame
      // current is used to refer to the current file path
      this.current = this.fileItem.current[ this.associatedID ];
      // binning and descriptor is used when searching for the best reference frame
      this.binning = this.fileItem.binning;
      this.descriptor = this.fileItem.descriptor[ this.associatedID ];
      // exposure time is used to compute the active frame's total integration time
      this.exposureTime = this.fileItem.exposureTime;
      // get the associated local normalization file
      this.localNormalizationFile = this.fileItem.localNormalizationFile[ this.associatedID ];
      // get the associated drizzle file
      this.drizzleFile = this.fileItem.drizzleFile[ this.associatedID ];
      // track if the current active item was used as reference
      this.isReference = this.fileItem.isReference[ this.associatedID ];
      // sync the solver parameters
      this.solverParams = this.fileItem.solverParams;
      // fast integration is in sync only for light frames and for frames that are not an associated channel,
      // otherwise is disabled
      if ( this.associatedID == "default" && this.fileItem.imageType == ImageType.LIGHT )
         this.__fastIntegration = this.fileItem.__fastIntegration;
      else
         this.__fastIntegration = false;
   };

   // sync
   this.sync();
}

/**
 * Creates a dummy active frames programmatically referencing a file on the disk.
 * This dummy active frame must be used for accessory operations only and must never
 * be used as a concrete instance referencing a real FileItem,
 *
 * @param {*} filePath
 */
ActiveFrame.dummy = function( filePath, cloneFrom )
{
   let imageType = ImageType.LIGHT;
   let filter = "NoFilter";
   let binning = 1;
   let exposure = 0;
   let fileKeywords = {};
   let size = {
      width: 0,
      height: 0
   };
   let isCFA = false;
   let isMaster = false;
   let keywords = {};
   let solverParams = {};
   let overscan;
   if ( cloneFrom == undefined )
   {
      imageType = cloneFrom.imageType;
      filter = cloneFrom.filter;
      binning = cloneFrom.binning;
      exposure = cloneFrom.exposure;
      fileKeywords = cloneFrom.fileKeywords;
      size = cloneFrom.size;
      isCFA = cloneFrom.isCFA;
      isMaster = cloneFrom.isMaster;
      keywords = cloneFrom.keywords;
      solverParams = cloneFrom.solverParams;
      overscan = cloneFrom.overscan;
   }

   let fileItem = new FileItem(
      filePath,
      imageType,
      filter,
      binning,
      exposure,
      fileKeywords,
      size,
      undefined /* matching sizes */ ,
      isCFA,
      isMaster,
      keywords,
      solverParams,
      overscan );

   return new ActiveFrame( fileItem );
}

ActiveFrame.prototype = new Object;

// ----------------------------------------------------------------------------
// EOF BPP-ActiveFrame.js - Released 2025-01-28T18:19:31Z
