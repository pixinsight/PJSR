// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// CorrectMagentaStars.js - Released 2022-06-20T10:49:52Z
// ----------------------------------------------------------------------------
//
// This file is part of CorrectMagentaStars Script version 1.3
//
// Copyright (C) 2019-2022 Roberto Sartori / Edoardo Luca Radice
// All Rights Reserved.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/* beautify ignore:start */
#define TITLE "CorrectMagentaStars"
#define VERSION "1.3"

#feature-id CorrectMagentaStars : Utilities > CorrectMagentaStars

#feature-info A magenta stars corrector utility.< br />\
    <br />\
    This script removes the magenta chromatism commonly present at the end of\
    the Hubble Palette composition.\
    <br />\
    Copyright & copy; 2019-2022 Roberto Sartori, Edoardo Luca Radice. All Rights Reserved.

#include <pjsr/ColorSpace.jsh>
#include <pjsr/DataType.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/SampleType.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/UndoFlag.jsh>
/* beautify ignore:end */

// The script's parameters prototype.
function parametersPrototype()
{
   this.setDefaults = function()
   {
      this.targetView = null;
      this.scnrAmount = 0.8;
      this.scnrPresLight = true;
      this.appliedMask = null;
      this.appliedMaskIsInverted = false;
      //this properties are used to restore original mask settings after Script execution
      this.originalMask = null;
      this.originalMaskIsEnabled = false;
      this.originalMaskIsInverted = false;
   };

   this.setParameters = function()
   {
      Parameters.clear();
      Parameters.set( "scnrAmount", this.scnrAmount );
      Parameters.set( "scnrPresLight", this.scnrPresLight );
   };

   this.getParameters = function()
   {
      if ( Parameters.has( "scnrAmount" ) )
         this.scnrAmount = Parameters.getReal( "scnrAmount" );

      if ( Parameters.has( "scnrPresLight" ) )
         this.scnrPresLight = Parameters.getBoolean( "scnrPresLight" );

   };
}

var parameters = new parametersPrototype();
parameters.setDefaults();
parameters.getParameters();

// Returns a push button with given text and onClick function.
function pushButtonWithTextOnClick( parent, text_, onClick_ )
{
   let button = new PushButton( parent );
   button.text = text_;
   button.onClick = onClick_;
   return button;
}

// The script's parameters dialog prototype.
function parametersDialogPrototype()
{
   this.__base__ = Dialog;
   this.__base__();

   let labelMinWidth = Math.round( this.font.width( "Amount:" ) + 2*this.font.width( 'M' ) );
   let sliderMaxValue = 256;

   this.windowTitle = TITLE;

   this.titlePane = new Label( this );

   this.titlePane.frameStyle = FrameStyle_Box;
   this.titlePane.margin = 4;
   this.titlePane.wordWrapping = true;
   this.titlePane.useRichText = true;
   this.titlePane.text =
      "<p><b>" + TITLE + " Version " + VERSION + "</b> &mdash; " +
      "This script reduces the residual magenta chromatism affecting stars that can be present " +
      "after combining Hydrogen-Alpha, Sulphur-II and Oxygen-III channels using the Hubble palette. " +
      "<p>Copyright &copy; 2019-2022 Roberto Sartori, Edoardo Luca Radice.</p>";

   this.targetView = new VerticalSizer;
   this.targetView.margin = 6;
   this.targetView.spacing = 4;

   this.viewList = new ViewList( this );
   this.viewList.getMainViews();
   if ( parameters.targetView && parameters.targetView.isView )
   {
      this.viewList.currentView = parameters.targetView;

      if ( parameters.targetView.window.mask.mainView &&
         parameters.targetView.window.mask.mainView.isView &&
         parameters.targetView.window.maskEnabled ) //has a mask applied and active
      {
         parameters.originalMaskIsEnabled = parameters.targetView.window.maskEnabled;
         parameters.originalMask = parameters.targetView.window.mask.mainView;
         parameters.originalMaskIsInverted = parameters.targetView.window.maskInverted;
      }
   }
   else
      parameters.targetView = this.viewList.currentView;

   this.viewList.onViewSelected = ( view ) =>
   {
      parameters.targetView = view;

      if ( parameters.targetView && parameters.targetView.isView )
      {
         // Checking if the selected image is masked
         if ( parameters.targetView.window.mask.mainView &&
            parameters.targetView.window.mask.mainView.isView &&
            parameters.targetView.window.maskEnabled ) //has a mask applied and active
         {
            this.MaskList.currentView = parameters.targetView.window.mask.mainView;
            parameters.appliedMask = this.MaskList.currentView;
            parameters.appliedMaskIsInverted = parameters.targetView.window.maskInverted;
            this.cbInverted.checked = parameters.appliedMaskIsInverted;
         }
         else
         {

            parameters.appliedMask = null;
            parameters.appliedMaskIsInverted = false;
            this.cbInverted.checked = false;
            this.MaskList.currentView = new View();
         }
      }
      else
      {
         parameters.appliedMask = null;
         parameters.appliedMaskIsInverted = false;
         this.cbInverted.checked = false;
         this.MaskList.currentView = new View();
      }
   };

   this.targetView.add( this.viewList );
   this.TargetGroup = new GroupBox( this );
   this.TargetGroup.title = "Target view";
   this.TargetGroup.sizer = this.targetView;

   this.maskPane = new VerticalSizer;
   this.maskPane.margin = 6;
   this.maskPane.spacing = 4;

   this.MaskList = new ViewList( this );

   this.MaskList.getMainViews();
   if ( parameters.appliedMask && parameters.appliedMask.isView )
      this.MaskList.currentView = parameters.appliedMask;
   else if ( parameters.targetView && parameters.targetView.isView )
   {
      // I have the image selected: checking if masked
      if ( parameters.targetView.window.mask.mainView &&
         parameters.targetView.window.mask.mainView.isView &&
         parameters.targetView.window.maskEnabled ) //has a mask applied and active
      {
         this.MaskList.currentView = parameters.targetView.window.mask.mainView;
         parameters.appliedMask = this.MaskList.currentView;
         parameters.appliedMaskIsInverted = false;
      }
      else
      {
         parameters.appliedMask = null;
         parameters.appliedMaskIsInverted = false;
         this.MaskList.currentView = new View();
      }
   }

   this.MaskList.onViewSelected = ( view ) =>
   {
      if ( view.id == "" ) // no mask was selected
      {
         parameters.appliedMask = null;
         this.cbInverted.checked = false;
         parameters.originalMaskIsInverted = false;
      }
      else if ( view && parameters.targetView.window.isMaskCompatible( view.window ) ) //check if the selected view in compatible
      {
         parameters.appliedMask = view;
         parameters.appliedMaskIsInverted = false;
         this.cbInverted.checked = false;
      }
      else
      {
         ( new MessageBox(
            "<p>Correct Magenta Stars Error:<br><br>Incompatible mask Image.</p>",
            TITLE,
            StdIcon_Error,
            StdButton_Ok ) ).execute();

         parameters.appliedMask = null;
         parameters.appliedMaskIsInverted = false;
         this.cbInverted.checked = false;
         this.MaskList.currentView = new View();
      }
   };

   this.cbInverted = new CheckBox( this );
   this.cbInverted.checked = parameters.originalMaskIsInverted;
   this.cbInverted.text = "Invert Mask";
   this.cbInverted.onClick = function( checked )
   {
      parameters.appliedMaskIsInverted = checked;
   };

   this.maskPane.add( this.MaskList );
   this.maskPane.add( this.cbInverted );

   this.MaskGroup = new GroupBox( this );
   this.MaskGroup.title = "Mask";
   this.MaskGroup.sizer = this.maskPane;

   this.parameterPane = new VerticalSizer;
   this.parameterPane.margin = 6;
   this.parameterPane.spacing = 10;

   this.scnrAmountControl = new NumericControl( this );
   this.scnrAmountControl.label.text = "Amount:";
   this.scnrAmountControl.label.minWidth = labelMinWidth;
   this.scnrAmountControl.slider.setRange( 0, sliderMaxValue );
   this.scnrAmountControl.slider.setScaledMinWidth( 300 );
   this.scnrAmountControl.setRange( 0.0, 1.0 );
   this.scnrAmountControl.setPrecision( 2 );
   this.scnrAmountControl.setValue( parameters.scnrAmount );
   this.scnrAmountControl.toolTip =
      "<p>SCNR amount applied to the inverted image.</p>";
   this.scnrAmountControl.onValueUpdated = function( value )
   {
      parameters.scnrAmount = value;
   };


   this.cbPresLight = new CheckBox( this );
   this.cbPresLight.checked = parameters.scnrPresLight;
   this.cbPresLight.text = "Preserve lightness";
   this.cbPresLight.toolTip = "<p>Preserves the original CIE L* component in the processed image.</p>";
   this.cbPresLight.onClick = function( checked )
   {
      parameters.scnrPresLight = checked;
   };
   this.PrLiPane = new HorizontalSizer;
   this.PrLiPane.addUnscaledSpacing( labelMinWidth + this.logicalPixelsToPhysical( 4 ) );
   this.PrLiPane.add( this.cbPresLight );
   this.PrLiPane.addStretch();

   this.parameterPane.add( this.scnrAmountControl );
   this.parameterPane.add( this.PrLiPane );

   this.buttonPane = new HorizontalSizer;
   this.buttonPane.spacing = 6;
   this.buttonPane.addStretch();
   this.buttonPane.add( pushButtonWithTextOnClick( this, "Execute",
      function()
      {
         this.dialog.ok();
      } ) );
   this.buttonPane.add( pushButtonWithTextOnClick( this, "Close",
      function()
      {
         this.dialog.cancel();
      } ) );

   this.bottomBarPane = new HorizontalSizer;

   this.newInstanceButton = new ToolButton( this );
   this.newInstanceButton.icon = this.scaledResource( ":/process-interface/new-instance.png" );
   this.newInstanceButton.setScaledFixedSize( 24, 24 );
   this.newInstanceButton.toolTip = "New Instance";
   this.newInstanceButton.onMousePress = function()
   {
      this.hasFocus = true;
      this.pushed = false;
      parameters.setParameters();
      this.dialog.newInstance();
   };

   this.help_Button = new ToolButton( this );
   this.help_Button.icon = this.scaledResource( ":/process-interface/browse-documentation.png" );
   this.help_Button.setScaledFixedSize( 20, 20 );
   this.help_Button.toolTip = "<p>Browse Documentation</p>";
   this.help_Button.onClick = function()
   {
      Dialog.browseScriptDocumentation( "CorrectMagentaStars" );
   };

   this.bottomBarPane.add( this.newInstanceButton );
   this.bottomBarPane.add( this.help_Button );
   this.bottomBarPane.add( this.buttonPane );


   this.sizer = new VerticalSizer;
   this.sizer.margin = 6;
   this.sizer.spacing = 6;
   this.sizer.add( this.titlePane );
   this.sizer.add( this.TargetGroup );
   this.sizer.add( this.MaskGroup );
   this.sizer.add( this.parameterPane );
   this.sizer.add( this.bottomBarPane );

   this.adjustToContents();
   this.setFixedSize();
}

parametersDialogPrototype.prototype = new Dialog;

// The script's process prototype.
function processPrototype()
{
   this.executeMagentaCorrection = function( targetView )
   {
      // Check if a mask has to be used:
      // if needed the mask is applied (disabled)
      var IsMasked = ( parameters.appliedMask && parameters.appliedMask.isView );
      if ( IsMasked )
      {
         parameters.targetView.window.setMask( parameters.appliedMask.window, false );
         parameters.targetView.window.maskEnabled = false;
         parameters.targetView.window.maskInverted = parameters.appliedMaskIsInverted;
      }
      else
         parameters.targetView.window.removeMask()

      let PInvert = new Invert;
      PInvert.executeOn( targetView, true );

      // Enable the mask
      if ( IsMasked )
         parameters.targetView.window.maskEnabled = true;

      let PSCNR = new SCNR;
      PSCNR.amount = Math.max( 0, Math.min( 1, parameters.scnrAmount ) );
      PSCNR.protectionMethod = SCNR.prototype.AverageNeutral;
      PSCNR.colorToRemove = SCNR.prototype.Green;
      PSCNR.preserveLightness = parameters.scnrPresLight;
      // Apply SCNR
      PSCNR.executeOn( targetView, true );

      // Disable the mask
      if ( IsMasked )
         parameters.targetView.window.maskEnabled = false;

      PInvert.executeOn( targetView, true );

      // Restore original mask properties.

      if ( parameters.originalMask && parameters.originalMask.isView )
      {
         parameters.targetView.window.setMask( parameters.originalMask.window, false );
         parameters.targetView.window.maskEnabled = parameters.originalMaskIsEnabled;
         parameters.targetView.window.maskInverted = parameters.originalMaskIsInverted;
      }
      else
      {
         parameters.targetView.window.removeMask();
         parameters.targetView.window.maskEnabled = true;
      }
   };

   this.execute = function()
   {
      this.executeMagentaCorrection( parameters.targetView );
   };
}

var process = new processPrototype();

function colorspaceIsValid( image )
{
   // Grayscale is not a valid colorspace for this script
   return !image.isGrayscale;
}

function performChecksAndExecute()
{
   console.noteln( "Applying CorrectMagentaStars on target: ", ImageWindow.activeWindow.currentView.id );

   if ( !parameters.targetView || !parameters.targetView.image )
   {
      ( new MessageBox(
         "<p>Correct Magenta Stars Error:<br><br>Undefined target view.</p>",
         TITLE,
         StdIcon_Warning,
         StdButton_Ok ) ).execute();
      return;
   }

   if ( !colorspaceIsValid( parameters.targetView.image ) )
   {
      ( new MessageBox(
         "<p>Correct Magenta Stars Error:<br><br>Target view color space cannot be Grayscale.</p>",
         TITLE,
         StdIcon_Warning,
         StdButton_Ok ) ).execute();
      return;
   }

   process.execute();
}

function executeInGlobalContext()
{
   ( new MessageBox(
      "<p>Correct Magenta Stars Error:<br><br>This script cannot be executed on global context.</p>",
      TITLE,
      StdIcon_Error,
      StdButton_Ok ) ).execute();

}

function executeOnTargetView( view )
{
   parameters.targetView = view;
   if ( parameters.targetView.window.mask.mainView &&
      parameters.targetView.window.mask.mainView.isView &&
      parameters.targetView.window.maskEnabled ) //has a mask applied and active
   {
      parameters.originalMask = parameters.targetView.window.mask.mainView;
      parameters.originalMaskIsEnabled = parameters.targetView.window.maskEnabled;
      parameters.originalMaskIsInverted = parameters.targetView.window.maskInverted;
      parameters.appliedMask = parameters.targetView.window.mask.mainView;
      parameters.appliedMaskIsInverted = parameters.targetView.window.maskInverted;
   }

   parameters.getParameters();
   performChecksAndExecute();
}

function main()
{
   console.hide();

   if ( Parameters.isGlobalTarget )
   {
      // Script has been launched in global context, execute and exit
      executeInGlobalContext();
      return;
   }
   if ( Parameters.isViewTarget )
   {
      // Script has been launched on a view target, execute and exit
      executeOnTargetView( Parameters.targetView );
      return;
   }


   parameters.targetView = ImageWindow.activeWindow.currentView;
   // Prepare the dialog
   let parametersDialog = new parametersDialogPrototype();

   // Runloop
   while ( true )
   {
      // Run the dalog
      if ( parametersDialog.execute() == 0 )
      {
         // Dialog closure forced
         return;
      }

      performChecksAndExecute();
   }
}

main();

// ----------------------------------------------------------------------------
// EOF CorrectMagentaStars.js - Released 2022-06-20T10:49:52Z
